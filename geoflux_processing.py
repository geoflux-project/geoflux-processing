# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Geoflux
                                 A QGIS plugin
 Traitement de données d'enquêtes OD
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2021-01-04
        copyright            : (C) 2021 by Aurélie Bousquet - Cerema
        email                : aurelie.bousquet@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Aurélie Bousquet - Cerema'
__date__ = '2021-01-04'
__copyright__ = '(C) 2021 by Aurélie Bousquet - Cerema'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os
import sys
import inspect

from qgis.core import QgsProcessingAlgorithm, QgsApplication
from .geoflux_processing_provider import GeofluxProcessingProvider

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

class GeofluxProcessingPlugin(object):

    def __init__(self):
        self.provider = None
        
    def initProcessing(self):
        """Init Processing provider for QGIS >= 3.8."""
        # Python processing framework
        self.provider = GeofluxProcessingProvider()
        QgsApplication.processingRegistry().addProvider(self.provider)
        # R processing framework
        #self.provider2 = RAlgorithmProvider()
        #QgsApplication.processingRegistry().addProvider(self.provider2)

    def initGui(self):
        self.initProcessing()

    def unload(self):
        QgsApplication.processingRegistry().removeProvider(self.provider)
        #QgsApplication.processingRegistry().removeProvider(self.provider2)
