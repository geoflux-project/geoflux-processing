<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" labelsEnabled="0" simplifyLocal="1" simplifyAlgorithm="0" version="3.10.6-A Coruña" readOnly="0" simplifyDrawingTol="1" simplifyMaxScale="1" minScale="1e+08" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="RuleRenderer">
    <rules key="{2230e620-e8df-4995-8cb0-56dc3cda56c7}">
      <rule filter=" &quot;reagreg&quot; = True" key="{72fe6dec-4add-451e-a89c-6dd8373634bf}" symbol="0"/>
    </rules>
    <symbols>
      <symbol name="0" clip_to_extent="1" alpha="1" force_rhr="0" type="fill">
        <layer class="SimpleFill" enabled="1" locked="0" pass="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="43,131,186,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.66"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="no"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" width="15" labelPlacementMethod="XHeight" scaleBasedVisibility="0" barWidth="5" scaleDependency="Area" opacity="1" diagramOrientation="Up" enabled="0" sizeType="MM" backgroundAlpha="255" penColor="#000000" penAlpha="255" maxScaleDenominator="1e+08" sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" height="15" rotationOffset="270" minimumSize="0" backgroundColor="#ffffff" minScaleDenominator="0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" obstacle="0" dist="0" linePlacementFlags="18" zIndex="0" showAll="1" placement="1">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" value="0" type="double"/>
        <Option name="allowedGapsEnabled" value="false" type="bool"/>
        <Option name="allowedGapsLayer" value="" type="QString"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id_zone" index="0"/>
    <alias name="" field="reagreg" index="1"/>
    <alias name="" field="emissions1" index="2"/>
    <alias name="" field="emissions2" index="3"/>
    <alias name="" field="attractions1" index="4"/>
    <alias name="" field="attractions2" index="5"/>
    <alias name="" field="o_adj_win_cor" index="6"/>
    <alias name="" field="d_adj_win_cor" index="7"/>
    <alias name="" field="o_adj_win_mssim" index="8"/>
    <alias name="" field="d_adj_win_mssim" index="9"/>
    <alias name="" field="o_nlod" index="10"/>
    <alias name="" field="d_nlod" index="11"/>
    <alias name="" field="o_struct_nlod" index="12"/>
    <alias name="" field="d_struct_nlod" index="13"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_zone" applyOnUpdate="0" expression=""/>
    <default field="reagreg" applyOnUpdate="0" expression=""/>
    <default field="emissions1" applyOnUpdate="0" expression=""/>
    <default field="emissions2" applyOnUpdate="0" expression=""/>
    <default field="attractions1" applyOnUpdate="0" expression=""/>
    <default field="attractions2" applyOnUpdate="0" expression=""/>
    <default field="o_adj_win_cor" applyOnUpdate="0" expression=""/>
    <default field="d_adj_win_cor" applyOnUpdate="0" expression=""/>
    <default field="o_adj_win_mssim" applyOnUpdate="0" expression=""/>
    <default field="d_adj_win_mssim" applyOnUpdate="0" expression=""/>
    <default field="o_nlod" applyOnUpdate="0" expression=""/>
    <default field="d_nlod" applyOnUpdate="0" expression=""/>
    <default field="o_struct_nlod" applyOnUpdate="0" expression=""/>
    <default field="d_struct_nlod" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" exp_strength="0" field="id_zone" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="reagreg" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="emissions1" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="emissions2" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="attractions1" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="attractions2" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_adj_win_cor" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_adj_win_cor" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_adj_win_mssim" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_adj_win_mssim" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_nlod" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_nlod" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_struct_nlod" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_struct_nlod" constraints="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id_zone" exp=""/>
    <constraint desc="" field="reagreg" exp=""/>
    <constraint desc="" field="emissions1" exp=""/>
    <constraint desc="" field="emissions2" exp=""/>
    <constraint desc="" field="attractions1" exp=""/>
    <constraint desc="" field="attractions2" exp=""/>
    <constraint desc="" field="o_adj_win_cor" exp=""/>
    <constraint desc="" field="d_adj_win_cor" exp=""/>
    <constraint desc="" field="o_adj_win_mssim" exp=""/>
    <constraint desc="" field="d_adj_win_mssim" exp=""/>
    <constraint desc="" field="o_nlod" exp=""/>
    <constraint desc="" field="d_nlod" exp=""/>
    <constraint desc="" field="o_struct_nlod" exp=""/>
    <constraint desc="" field="d_struct_nlod" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column name="id_zone" hidden="0" type="field" width="-1"/>
      <column name="emissions1" hidden="0" type="field" width="-1"/>
      <column name="emissions2" hidden="0" type="field" width="-1"/>
      <column name="attractions1" hidden="0" type="field" width="-1"/>
      <column name="attractions2" hidden="0" type="field" width="-1"/>
      <column name="o_adj_win_cor" hidden="0" type="field" width="-1"/>
      <column name="d_adj_win_cor" hidden="0" type="field" width="-1"/>
      <column name="o_adj_win_mssim" hidden="0" type="field" width="-1"/>
      <column name="d_adj_win_mssim" hidden="0" type="field" width="-1"/>
      <column name="o_nlod" hidden="0" type="field" width="-1"/>
      <column name="d_nlod" hidden="0" type="field" width="-1"/>
      <column name="o_struct_nlod" hidden="0" type="field" width="-1"/>
      <column name="d_struct_nlod" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column name="reagreg" hidden="0" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attractions1" editable="1"/>
    <field name="attractions2" editable="1"/>
    <field name="d_adj_win_cor" editable="1"/>
    <field name="d_adj_win_mssim" editable="1"/>
    <field name="d_nlod" editable="1"/>
    <field name="d_struct_nlod" editable="1"/>
    <field name="emissions1" editable="1"/>
    <field name="emissions2" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="o_adj_win_cor" editable="1"/>
    <field name="o_adj_win_mssim" editable="1"/>
    <field name="o_nlod" editable="1"/>
    <field name="o_struct_nlod" editable="1"/>
    <field name="reagreg" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="attractions1" labelOnTop="0"/>
    <field name="attractions2" labelOnTop="0"/>
    <field name="d_adj_win_cor" labelOnTop="0"/>
    <field name="d_adj_win_mssim" labelOnTop="0"/>
    <field name="d_nlod" labelOnTop="0"/>
    <field name="d_struct_nlod" labelOnTop="0"/>
    <field name="emissions1" labelOnTop="0"/>
    <field name="emissions2" labelOnTop="0"/>
    <field name="id_zone" labelOnTop="0"/>
    <field name="o_adj_win_cor" labelOnTop="0"/>
    <field name="o_adj_win_mssim" labelOnTop="0"/>
    <field name="o_nlod" labelOnTop="0"/>
    <field name="o_struct_nlod" labelOnTop="0"/>
    <field name="reagreg" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
