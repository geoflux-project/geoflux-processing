<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" labelsEnabled="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" readOnly="0" simplifyDrawingTol="1" maxScale="0" minScale="1e+08" simplifyAlgorithm="0" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 attr="o_struct_nlod" graduatedMethod="GraduatedColor" type="graduatedSymbol" forceraster="0" enableorderby="0" symbollevels="0">
    <ranges>
      <range label="0 - 0,1 " upper="0.100000000000000" render="true" lower="0.000000000000000" symbol="0"/>
      <range label="0,1 - 0,2 " upper="0.200000000000000" render="true" lower="0.100000000000000" symbol="1"/>
      <range label="0,2 - 0,3 " upper="0.300000000000000" render="true" lower="0.200000000000000" symbol="2"/>
      <range label="0,3 - 0,4 " upper="0.400000000000000" render="true" lower="0.300000000000000" symbol="3"/>
      <range label="0,4 - 0,5 " upper="0.500000000000000" render="true" lower="0.400000000000000" symbol="4"/>
      <range label="0,5 - 0,6 " upper="0.600000000000000" render="true" lower="0.500000000000000" symbol="5"/>
      <range label="0,6 - 0,7 " upper="0.700000000000000" render="true" lower="0.600000000000000" symbol="6"/>
      <range label="0,7 - 0,8 " upper="0.800000000000000" render="true" lower="0.700000000000000" symbol="7"/>
      <range label="0,8 - 0,9 " upper="0.900000000000000" render="true" lower="0.800000000000000" symbol="8"/>
      <range label="0,9 - 1 " upper="1.000000000000000" render="true" lower="0.900000000000000" symbol="9"/>
    </ranges>
    <symbols>
      <symbol alpha="1" type="fill" name="0" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="43,131,186,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="1" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="100,171,176,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="2" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="157,211,167,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="3" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="199,233,173,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="4" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="237,248,185,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="5" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="255,237,170,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="6" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="254,201,128,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="7" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="249,158,89,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="8" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="232,91,58,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="9" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="215,25,28,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol alpha="1" type="fill" name="0" clip_to_extent="1" force_rhr="0">
        <layer enabled="1" class="SimpleFill" pass="0" locked="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="243,166,178,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop v="43,131,186,255" k="color1"/>
      <prop v="215,25,28,255" k="color2"/>
      <prop v="0" k="discrete"/>
      <prop v="gradient" k="rampType"/>
      <prop v="0.25;171,221,164,255:0.5;255,255,191,255:0.75;253,174,97,255" k="stops"/>
    </colorramp>
    <classificationMethod id="EqualInterval">
      <symmetricMode symmetrypoint="0" astride="0" enabled="0"/>
      <labelFormat format="%1 - %2 " labelprecision="4" trimtrailingzeroes="1"/>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeType="MM" sizeType="MM" enabled="0" minimumSize="0" barWidth="5" maxScaleDenominator="1e+08" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" rotationOffset="270" penColor="#000000" scaleDependency="Area" backgroundAlpha="255" height="15" lineSizeScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" scaleBasedVisibility="0" width="15" minScaleDenominator="0" penWidth="0" diagramOrientation="Up" penAlpha="255" opacity="1">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" dist="0" zIndex="0" obstacle="0" linePlacementFlags="18" placement="1" priority="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option type="double" name="allowedGapsBuffer" value="0"/>
        <Option type="bool" name="allowedGapsEnabled" value="false"/>
        <Option type="QString" name="allowedGapsLayer" value=""/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_zone" name="" index="0"/>
    <alias field="emissions1" name="" index="1"/>
    <alias field="emissions2" name="" index="2"/>
    <alias field="attractions1" name="" index="3"/>
    <alias field="attractions2" name="" index="4"/>
    <alias field="o_adj_win_cor" name="" index="5"/>
    <alias field="d_adj_win_cor" name="" index="6"/>
    <alias field="o_adj_win_mssim" name="" index="7"/>
    <alias field="d_adj_win_mssim" name="" index="8"/>
    <alias field="o_nlod" name="" index="9"/>
    <alias field="d_nlod" name="" index="10"/>
    <alias field="o_struct_nlod" name="" index="11"/>
    <alias field="d_struct_nlod" name="" index="12"/>
    <alias field="reagreg" name="" index="13"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_zone" expression="" applyOnUpdate="0"/>
    <default field="emissions1" expression="" applyOnUpdate="0"/>
    <default field="emissions2" expression="" applyOnUpdate="0"/>
    <default field="attractions1" expression="" applyOnUpdate="0"/>
    <default field="attractions2" expression="" applyOnUpdate="0"/>
    <default field="o_adj_win_cor" expression="" applyOnUpdate="0"/>
    <default field="d_adj_win_cor" expression="" applyOnUpdate="0"/>
    <default field="o_adj_win_mssim" expression="" applyOnUpdate="0"/>
    <default field="d_adj_win_mssim" expression="" applyOnUpdate="0"/>
    <default field="o_nlod" expression="" applyOnUpdate="0"/>
    <default field="d_nlod" expression="" applyOnUpdate="0"/>
    <default field="o_struct_nlod" expression="" applyOnUpdate="0"/>
    <default field="d_struct_nlod" expression="" applyOnUpdate="0"/>
    <default field="reagreg" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="0" field="id_zone" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="emissions1" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="emissions2" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="attractions1" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="attractions2" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="o_adj_win_cor" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="d_adj_win_cor" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="o_adj_win_mssim" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="d_adj_win_mssim" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="o_nlod" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="d_nlod" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="o_struct_nlod" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="d_struct_nlod" exp_strength="0" notnull_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="reagreg" exp_strength="0" notnull_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_zone" exp="" desc=""/>
    <constraint field="emissions1" exp="" desc=""/>
    <constraint field="emissions2" exp="" desc=""/>
    <constraint field="attractions1" exp="" desc=""/>
    <constraint field="attractions2" exp="" desc=""/>
    <constraint field="o_adj_win_cor" exp="" desc=""/>
    <constraint field="d_adj_win_cor" exp="" desc=""/>
    <constraint field="o_adj_win_mssim" exp="" desc=""/>
    <constraint field="d_adj_win_mssim" exp="" desc=""/>
    <constraint field="o_nlod" exp="" desc=""/>
    <constraint field="d_nlod" exp="" desc=""/>
    <constraint field="o_struct_nlod" exp="" desc=""/>
    <constraint field="d_struct_nlod" exp="" desc=""/>
    <constraint field="reagreg" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column width="-1" type="field" name="id_zone" hidden="0"/>
      <column width="-1" type="field" name="emissions1" hidden="0"/>
      <column width="-1" type="field" name="emissions2" hidden="0"/>
      <column width="-1" type="field" name="attractions1" hidden="0"/>
      <column width="-1" type="field" name="attractions2" hidden="0"/>
      <column width="-1" type="field" name="o_adj_win_cor" hidden="0"/>
      <column width="-1" type="field" name="d_adj_win_cor" hidden="0"/>
      <column width="-1" type="field" name="o_adj_win_mssim" hidden="0"/>
      <column width="-1" type="field" name="d_adj_win_mssim" hidden="0"/>
      <column width="-1" type="field" name="o_nlod" hidden="0"/>
      <column width="-1" type="field" name="d_nlod" hidden="0"/>
      <column width="-1" type="field" name="o_struct_nlod" hidden="0"/>
      <column width="-1" type="field" name="d_struct_nlod" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" name="reagreg" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attractions1" editable="1"/>
    <field name="attractions2" editable="1"/>
    <field name="d_adj_win_cor" editable="1"/>
    <field name="d_adj_win_mssim" editable="1"/>
    <field name="d_nlod" editable="1"/>
    <field name="d_struct_nlod" editable="1"/>
    <field name="emissions1" editable="1"/>
    <field name="emissions2" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="o_adj_win_cor" editable="1"/>
    <field name="o_adj_win_mssim" editable="1"/>
    <field name="o_nlod" editable="1"/>
    <field name="o_struct_nlod" editable="1"/>
    <field name="reagreg" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="attractions1"/>
    <field labelOnTop="0" name="attractions2"/>
    <field labelOnTop="0" name="d_adj_win_cor"/>
    <field labelOnTop="0" name="d_adj_win_mssim"/>
    <field labelOnTop="0" name="d_nlod"/>
    <field labelOnTop="0" name="d_struct_nlod"/>
    <field labelOnTop="0" name="emissions1"/>
    <field labelOnTop="0" name="emissions2"/>
    <field labelOnTop="0" name="id_zone"/>
    <field labelOnTop="0" name="o_adj_win_cor"/>
    <field labelOnTop="0" name="o_adj_win_mssim"/>
    <field labelOnTop="0" name="o_nlod"/>
    <field labelOnTop="0" name="o_struct_nlod"/>
    <field labelOnTop="0" name="reagreg"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
