<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="0" readOnly="0" styleCategories="AllStyleCategories" version="3.10.6-A Coruña" simplifyMaxScale="1" maxScale="0" simplifyDrawingTol="1" simplifyAlgorithm="0" labelsEnabled="0" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" simplifyDrawingHints="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" type="singleSymbol" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol alpha="1" force_rhr="0" clip_to_extent="1" type="marker" name="0">
        <layer enabled="1" locked="0" class="SimpleMarker" pass="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="219,30,42,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="128,17,25,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.2"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="1"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="size">
                  <Option value="true" type="bool" name="active"/>
                  <Option value="10* &quot;size_attra&quot;" type="QString" name="expression"/>
                  <Option value="3" type="int" name="type"/>
                </Option>
              </Option>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id_zone</value>
      <value>"id_zone"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory height="15" diagramOrientation="Up" scaleDependency="Area" enabled="0" lineSizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" lineSizeType="MM" minimumSize="0" scaleBasedVisibility="0" penColor="#000000" rotationOffset="270" backgroundColor="#ffffff" barWidth="5" sizeType="MM" maxScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0" opacity="1" penWidth="0" penAlpha="255" backgroundAlpha="255" width="15" minScaleDenominator="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" zIndex="0" showAll="1" linePlacementFlags="18" dist="0" placement="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="Zones_dbd388b7_1062_42e9_882c_c4708de4b3fc" type="QString" name="Layer"/>
            <Option value="Zones" type="QString" name="LayerName"/>
            <Option value="postgres" type="QString" name="LayerProviderName"/>
            <Option value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(id_zonage = 6) AND id IN (                                     SELECT zone.id                                     FROM w_geoflux_mmr_coherence.corresp_zones JOIN w_geoflux_mmr_coherence.zone macrozone ON (corresp_zones.id_macrozone = macrozone.id)                                                                                JOIN w_geoflux_mmr_coherence.zone zone ON (corresp_zones.id_zone = zone.id)                                                                                JOIN w_geoflux_mmr_coherence.zonage ON (macrozone.id_zonage = zonage.id)                                     WHERE zone.id_zonage = 6 AND macrozone.id_zonage = 1                               )" type="QString" name="LayerSource"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="code_zone" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="emis">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="1.7976931348623157e+308" type="double" name="Max"/>
            <Option value="0" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="ech_emis">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="attir">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="1.7976931348623157e+308" type="double" name="Max"/>
            <Option value="0" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="ech_attir">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="emis_attir">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="1.7976931348623157e+308" type="double" name="Max"/>
            <Option value="0" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="intra">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="1.7976931348623157e+308" type="double" name="Max"/>
            <Option value="0" type="double" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="double" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="ech_intra">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size_emiss">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_attra">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_total">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="size_intra">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_zone" index="0" name="Code zone"/>
    <alias field="emis" index="1" name="Emissions"/>
    <alias field="ech_emis" index="2" name="Echantillon émissions"/>
    <alias field="attir" index="3" name="Attractions"/>
    <alias field="ech_attir" index="4" name="Echantillon attractions"/>
    <alias field="emis_attir" index="5" name="Emissions + attractions"/>
    <alias field="intra" index="6" name="Intrazonaux"/>
    <alias field="ech_intra" index="7" name="Echantillon intrazonaux"/>
    <alias field="size_emiss" index="8" name=""/>
    <alias field="size_attra" index="9" name=""/>
    <alias field="size_total" index="10" name=""/>
    <alias field="size_intra" index="11" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_zone" applyOnUpdate="0" expression=""/>
    <default field="emis" applyOnUpdate="0" expression=""/>
    <default field="ech_emis" applyOnUpdate="0" expression=""/>
    <default field="attir" applyOnUpdate="0" expression=""/>
    <default field="ech_attir" applyOnUpdate="0" expression=""/>
    <default field="emis_attir" applyOnUpdate="0" expression=""/>
    <default field="intra" applyOnUpdate="0" expression=""/>
    <default field="ech_intra" applyOnUpdate="0" expression=""/>
    <default field="size_emiss" applyOnUpdate="0" expression=""/>
    <default field="size_attra" applyOnUpdate="0" expression=""/>
    <default field="size_total" applyOnUpdate="0" expression=""/>
    <default field="size_intra" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint field="id_zone" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="emis" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="ech_emis" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="attir" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="ech_attir" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="emis_attir" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="intra" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="ech_intra" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="size_emiss" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="size_attra" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="size_total" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
    <constraint field="size_intra" exp_strength="0" constraints="3" notnull_strength="1" unique_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_zone" desc="" exp=""/>
    <constraint field="emis" desc="" exp=""/>
    <constraint field="ech_emis" desc="" exp=""/>
    <constraint field="attir" desc="" exp=""/>
    <constraint field="ech_attir" desc="" exp=""/>
    <constraint field="emis_attir" desc="" exp=""/>
    <constraint field="intra" desc="" exp=""/>
    <constraint field="ech_intra" desc="" exp=""/>
    <constraint field="size_emiss" desc="" exp=""/>
    <constraint field="size_attra" desc="" exp=""/>
    <constraint field="size_total" desc="" exp=""/>
    <constraint field="size_intra" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column type="field" hidden="0" name="id_zone" width="98"/>
      <column type="field" hidden="0" name="emis" width="72"/>
      <column type="field" hidden="0" name="attir" width="71"/>
      <column type="field" hidden="0" name="emis_attir" width="134"/>
      <column type="field" hidden="0" name="intra" width="79"/>
      <column type="field" hidden="1" name="size_emiss" width="-1"/>
      <column type="field" hidden="1" name="size_attra" width="-1"/>
      <column type="field" hidden="1" name="size_total" width="-1"/>
      <column type="field" hidden="1" name="size_intra" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="ech_emis" width="133"/>
      <column type="field" hidden="0" name="ech_attir" width="138"/>
      <column type="field" hidden="0" name="ech_intra" width="153"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attir" editable="1"/>
    <field name="ech_attir" editable="1"/>
    <field name="ech_emis" editable="1"/>
    <field name="ech_intra" editable="1"/>
    <field name="emis" editable="1"/>
    <field name="emis_attir" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="intra" editable="1"/>
    <field name="size_attra" editable="1"/>
    <field name="size_emiss" editable="1"/>
    <field name="size_intra" editable="1"/>
    <field name="size_total" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="attir" labelOnTop="0"/>
    <field name="ech_attir" labelOnTop="0"/>
    <field name="ech_emis" labelOnTop="0"/>
    <field name="ech_intra" labelOnTop="0"/>
    <field name="emis" labelOnTop="0"/>
    <field name="emis_attir" labelOnTop="0"/>
    <field name="id_zone" labelOnTop="0"/>
    <field name="intra" labelOnTop="0"/>
    <field name="size_attra" labelOnTop="0"/>
    <field name="size_emiss" labelOnTop="0"/>
    <field name="size_intra" labelOnTop="0"/>
    <field name="size_total" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
