<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" styleCategories="AllStyleCategories" version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" simplifyMaxScale="1" maxScale="0" simplifyDrawingHints="1" labelsEnabled="0" simplifyLocal="1" readOnly="0" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" forceraster="0" symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" name="0" clip_to_extent="1" type="fill" alpha="1">
        <layer locked="0" pass="0" class="SimpleFill" enabled="1">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="162,162,162,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="42,34,201,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.66" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="no" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory enabled="0" diagramOrientation="Up" barWidth="5" rotationOffset="270" lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" penWidth="0" height="15" opacity="1" width="15" penAlpha="255" penColor="#000000" maxScaleDenominator="1e+08" scaleBasedVisibility="0" backgroundAlpha="255" backgroundColor="#ffffff" minimumSize="0" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" scaleDependency="Area" labelPlacementMethod="XHeight">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" priority="0" placement="1" dist="0" showAll="1" linePlacementFlags="18" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option value="0" name="allowedGapsBuffer" type="double"/>
        <Option value="false" name="allowedGapsEnabled" type="bool"/>
        <Option value="" name="allowedGapsLayer" type="QString"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_macrozone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_macro_mcor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_macro_mcor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_macro_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_macro_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id_macrozone" index="0"/>
    <alias name="" field="emissions1" index="1"/>
    <alias name="" field="emissions2" index="2"/>
    <alias name="" field="attractions1" index="3"/>
    <alias name="" field="attractions2" index="4"/>
    <alias name="" field="o_macro_mcor" index="5"/>
    <alias name="" field="d_macro_mcor" index="6"/>
    <alias name="" field="o_macro_mssim" index="7"/>
    <alias name="" field="d_macro_mssim" index="8"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id_macrozone" applyOnUpdate="0"/>
    <default expression="" field="emissions1" applyOnUpdate="0"/>
    <default expression="" field="emissions2" applyOnUpdate="0"/>
    <default expression="" field="attractions1" applyOnUpdate="0"/>
    <default expression="" field="attractions2" applyOnUpdate="0"/>
    <default expression="" field="o_macro_mcor" applyOnUpdate="0"/>
    <default expression="" field="d_macro_mcor" applyOnUpdate="0"/>
    <default expression="" field="o_macro_mssim" applyOnUpdate="0"/>
    <default expression="" field="d_macro_mssim" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" exp_strength="0" field="id_macrozone" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="emissions1" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="emissions2" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="attractions1" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="attractions2" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_macro_mcor" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_macro_mcor" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="o_macro_mssim" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="d_macro_mssim" constraints="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_macrozone"/>
    <constraint desc="" exp="" field="emissions1"/>
    <constraint desc="" exp="" field="emissions2"/>
    <constraint desc="" exp="" field="attractions1"/>
    <constraint desc="" exp="" field="attractions2"/>
    <constraint desc="" exp="" field="o_macro_mcor"/>
    <constraint desc="" exp="" field="d_macro_mcor"/>
    <constraint desc="" exp="" field="o_macro_mssim"/>
    <constraint desc="" exp="" field="d_macro_mssim"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column name="emissions1" width="-1" hidden="0" type="field"/>
      <column name="emissions2" width="-1" hidden="0" type="field"/>
      <column name="attractions1" width="-1" hidden="0" type="field"/>
      <column name="attractions2" width="-1" hidden="0" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
      <column name="id_macrozone" width="-1" hidden="0" type="field"/>
      <column name="o_macro_mcor" width="-1" hidden="0" type="field"/>
      <column name="d_macro_mcor" width="-1" hidden="0" type="field"/>
      <column name="o_macro_mssim" width="-1" hidden="0" type="field"/>
      <column name="d_macro_mssim" width="-1" hidden="0" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="attractions1" editable="1"/>
    <field name="attractions2" editable="1"/>
    <field name="d_adj_win_cor" editable="1"/>
    <field name="d_adj_win_mssim" editable="1"/>
    <field name="d_macro_mcor" editable="1"/>
    <field name="d_macro_mssim" editable="1"/>
    <field name="d_nlod" editable="1"/>
    <field name="d_struct_nlod" editable="1"/>
    <field name="emissions1" editable="1"/>
    <field name="emissions2" editable="1"/>
    <field name="id_macrozone" editable="1"/>
    <field name="id_zone" editable="1"/>
    <field name="o_adj_win_cor" editable="1"/>
    <field name="o_adj_win_mssim" editable="1"/>
    <field name="o_macro_mcor" editable="1"/>
    <field name="o_macro_mssim" editable="1"/>
    <field name="o_nlod" editable="1"/>
    <field name="o_struct_nlod" editable="1"/>
    <field name="reagreg" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="attractions1" labelOnTop="0"/>
    <field name="attractions2" labelOnTop="0"/>
    <field name="d_adj_win_cor" labelOnTop="0"/>
    <field name="d_adj_win_mssim" labelOnTop="0"/>
    <field name="d_macro_mcor" labelOnTop="0"/>
    <field name="d_macro_mssim" labelOnTop="0"/>
    <field name="d_nlod" labelOnTop="0"/>
    <field name="d_struct_nlod" labelOnTop="0"/>
    <field name="emissions1" labelOnTop="0"/>
    <field name="emissions2" labelOnTop="0"/>
    <field name="id_macrozone" labelOnTop="0"/>
    <field name="id_zone" labelOnTop="0"/>
    <field name="o_adj_win_cor" labelOnTop="0"/>
    <field name="o_adj_win_mssim" labelOnTop="0"/>
    <field name="o_macro_mcor" labelOnTop="0"/>
    <field name="o_macro_mssim" labelOnTop="0"/>
    <field name="o_nlod" labelOnTop="0"/>
    <field name="o_struct_nlod" labelOnTop="0"/>
    <field name="reagreg" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
