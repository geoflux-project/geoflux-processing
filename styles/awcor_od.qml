<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" labelsEnabled="0" styleCategories="AllStyleCategories" simplifyLocal="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" minScale="1e+08" maxScale="0" simplifyDrawingHints="1" simplifyDrawingTol="1" simplifyAlgorithm="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 attr="adj_win_cor" symbollevels="0" forceraster="0" type="graduatedSymbol" enableorderby="0" graduatedMethod="GraduatedColor">
    <ranges>
      <range upper="-0.800000000000000" label="-1 - -0,8 " lower="-1.000000000000000" symbol="0" render="true"/>
      <range upper="-0.600000000000000" label="-0,8 - -0,6 " lower="-0.800000000000000" symbol="1" render="true"/>
      <range upper="-0.400000000000000" label="-0,6 - -0,4 " lower="-0.600000000000000" symbol="2" render="true"/>
      <range upper="-0.200000000000000" label="-0,4 - -0,2 " lower="-0.400000000000000" symbol="3" render="true"/>
      <range upper="0.000000000000000" label="-0,2 - 0 " lower="-0.200000000000000" symbol="4" render="true"/>
      <range upper="0.200000000000000" label="0 - 0,2 " lower="0.000000000000000" symbol="5" render="true"/>
      <range upper="0.400000000000000" label="0,2 - 0,4 " lower="0.200000000000000" symbol="6" render="true"/>
      <range upper="0.600000000000000" label="0,4 - 0,6 " lower="0.400000000000000" symbol="7" render="true"/>
      <range upper="0.800000000000000" label="0,6 - 0,8 " lower="0.600000000000000" symbol="8" render="true"/>
      <range upper="1.000000000000000" label="0,8 - 1 " lower="0.800000000000000" symbol="9" render="true"/>
    </ranges>
    <symbols>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="0">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="215,25,28,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="232,91,58,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="2">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="249,158,89,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="3">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="254,201,128,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="4">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="255,237,170,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="5">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="237,248,185,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="6">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="199,233,173,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="7">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="157,211,167,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="8">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="100,171,176,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="9">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="43,131,186,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol alpha="1" clip_to_extent="1" force_rhr="0" type="line" name="0">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="243,166,178,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.66" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop v="215,25,28,255" k="color1"/>
      <prop v="43,131,186,255" k="color2"/>
      <prop v="0" k="discrete"/>
      <prop v="gradient" k="rampType"/>
      <prop v="0.25;253,174,97,255:0.5;255,255,191,255:0.75;171,221,164,255" k="stops"/>
    </colorramp>
    <classificationMethod id="Pretty">
      <symmetricMode symmetrypoint="0" enabled="0" astride="0"/>
      <labelFormat trimtrailingzeroes="1" format="%1 - %2 " labelprecision="4"/>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions" value="&quot;id_zone_orig&quot;"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory scaleBasedVisibility="0" lineSizeScale="3x:0,0,0,0,0,0" penAlpha="255" sizeType="MM" scaleDependency="Area" sizeScale="3x:0,0,0,0,0,0" height="15" width="15" minScaleDenominator="0" penColor="#000000" labelPlacementMethod="XHeight" backgroundAlpha="255" backgroundColor="#ffffff" rotationOffset="270" minimumSize="0" opacity="1" maxScaleDenominator="1e+08" barWidth="5" diagramOrientation="Up" penWidth="0" enabled="0" lineSizeType="MM">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" field="" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" showAll="1" placement="2" priority="0" linePlacementFlags="18" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone_orig">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_dest">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg_orig">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg_dest">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="valeur1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="valeur2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="norm_valeur1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="norm_valeur2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_macrozone_orig">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_macrozone_dest">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="distance">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="classe_dist">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adj_win_ssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id_zone_orig" index="0" name=""/>
    <alias field="id_zone_dest" index="1" name=""/>
    <alias field="reagreg_orig" index="2" name=""/>
    <alias field="reagreg_dest" index="3" name=""/>
    <alias field="valeur1" index="4" name=""/>
    <alias field="valeur2" index="5" name=""/>
    <alias field="norm_valeur1" index="6" name=""/>
    <alias field="norm_valeur2" index="7" name=""/>
    <alias field="id_macrozone_orig" index="8" name=""/>
    <alias field="id_macrozone_dest" index="9" name=""/>
    <alias field="distance" index="10" name=""/>
    <alias field="classe_dist" index="11" name=""/>
    <alias field="adj_win_cor" index="12" name=""/>
    <alias field="adj_win_ssim" index="13" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_zone_orig" expression="" applyOnUpdate="0"/>
    <default field="id_zone_dest" expression="" applyOnUpdate="0"/>
    <default field="reagreg_orig" expression="" applyOnUpdate="0"/>
    <default field="reagreg_dest" expression="" applyOnUpdate="0"/>
    <default field="valeur1" expression="" applyOnUpdate="0"/>
    <default field="valeur2" expression="" applyOnUpdate="0"/>
    <default field="norm_valeur1" expression="" applyOnUpdate="0"/>
    <default field="norm_valeur2" expression="" applyOnUpdate="0"/>
    <default field="id_macrozone_orig" expression="" applyOnUpdate="0"/>
    <default field="id_macrozone_dest" expression="" applyOnUpdate="0"/>
    <default field="distance" expression="" applyOnUpdate="0"/>
    <default field="classe_dist" expression="" applyOnUpdate="0"/>
    <default field="adj_win_cor" expression="" applyOnUpdate="0"/>
    <default field="adj_win_ssim" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="id_zone_orig" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="id_zone_dest" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="reagreg_orig" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="reagreg_dest" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="valeur1" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="valeur2" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="norm_valeur1" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="norm_valeur2" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="id_macrozone_orig" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="id_macrozone_dest" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="distance" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="classe_dist" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="adj_win_cor" constraints="0" notnull_strength="0" unique_strength="0"/>
    <constraint exp_strength="0" field="adj_win_ssim" constraints="0" notnull_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id_zone_orig" desc="" exp=""/>
    <constraint field="id_zone_dest" desc="" exp=""/>
    <constraint field="reagreg_orig" desc="" exp=""/>
    <constraint field="reagreg_dest" desc="" exp=""/>
    <constraint field="valeur1" desc="" exp=""/>
    <constraint field="valeur2" desc="" exp=""/>
    <constraint field="norm_valeur1" desc="" exp=""/>
    <constraint field="norm_valeur2" desc="" exp=""/>
    <constraint field="id_macrozone_orig" desc="" exp=""/>
    <constraint field="id_macrozone_dest" desc="" exp=""/>
    <constraint field="distance" desc="" exp=""/>
    <constraint field="classe_dist" desc="" exp=""/>
    <constraint field="adj_win_cor" desc="" exp=""/>
    <constraint field="adj_win_ssim" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column width="-1" hidden="0" type="field" name="id_zone_orig"/>
      <column width="-1" hidden="0" type="field" name="id_zone_dest"/>
      <column width="-1" hidden="0" type="field" name="valeur1"/>
      <column width="92" hidden="0" type="field" name="valeur2"/>
      <column width="167" hidden="0" type="field" name="norm_valeur1"/>
      <column width="154" hidden="0" type="field" name="norm_valeur2"/>
      <column width="155" hidden="0" type="field" name="id_macrozone_orig"/>
      <column width="143" hidden="0" type="field" name="id_macrozone_dest"/>
      <column width="-1" hidden="0" type="field" name="distance"/>
      <column width="-1" hidden="0" type="field" name="classe_dist"/>
      <column width="-1" hidden="0" type="field" name="adj_win_cor"/>
      <column width="-1" hidden="0" type="field" name="adj_win_ssim"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="-1" hidden="0" type="field" name="reagreg_orig"/>
      <column width="-1" hidden="0" type="field" name="reagreg_dest"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="adj_win_cor"/>
    <field editable="1" name="adj_win_ssim"/>
    <field editable="1" name="classe_dist"/>
    <field editable="1" name="distance"/>
    <field editable="1" name="id_macrozone_dest"/>
    <field editable="1" name="id_macrozone_orig"/>
    <field editable="1" name="id_zone_dest"/>
    <field editable="1" name="id_zone_orig"/>
    <field editable="1" name="norm_valeur1"/>
    <field editable="1" name="norm_valeur2"/>
    <field editable="1" name="reagreg_dest"/>
    <field editable="1" name="reagreg_orig"/>
    <field editable="1" name="valeur1"/>
    <field editable="1" name="valeur2"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="adj_win_cor"/>
    <field labelOnTop="0" name="adj_win_ssim"/>
    <field labelOnTop="0" name="classe_dist"/>
    <field labelOnTop="0" name="distance"/>
    <field labelOnTop="0" name="id_macrozone_dest"/>
    <field labelOnTop="0" name="id_macrozone_orig"/>
    <field labelOnTop="0" name="id_zone_dest"/>
    <field labelOnTop="0" name="id_zone_orig"/>
    <field labelOnTop="0" name="norm_valeur1"/>
    <field labelOnTop="0" name="norm_valeur2"/>
    <field labelOnTop="0" name="reagreg_dest"/>
    <field labelOnTop="0" name="reagreg_orig"/>
    <field labelOnTop="0" name="valeur1"/>
    <field labelOnTop="0" name="valeur2"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone_orig</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
