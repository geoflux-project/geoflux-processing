<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" simplifyDrawingTol="1" simplifyAlgorithm="0" simplifyLocal="1" maxScale="0" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyDrawingHints="1" readOnly="0" simplifyMaxScale="1" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" type="graduatedSymbol" forceraster="0" symbollevels="0" attr="o_adj_win_mssim" graduatedMethod="GraduatedColor">
    <ranges>
      <range symbol="0" render="true" upper="0.000000000000000" label="-1 - 0 " lower="-1.000000000000000"/>
      <range symbol="1" render="true" upper="0.100000000000000" label="0 - 0,1 " lower="0.000000000000000"/>
      <range symbol="2" render="true" upper="0.200000000000000" label="0,1 - 0,2 " lower="0.100000000000000"/>
      <range symbol="3" render="true" upper="0.300000000000000" label="0,2 - 0,3 " lower="0.200000000000000"/>
      <range symbol="4" render="true" upper="0.400000000000000" label="0,3 - 0,4 " lower="0.300000000000000"/>
      <range symbol="5" render="true" upper="0.500000000000000" label="0,4 - 0,5 " lower="0.400000000000000"/>
      <range symbol="6" render="true" upper="0.600000000000000" label="0,5 - 0,6 " lower="0.500000000000000"/>
      <range symbol="7" render="true" upper="0.700000000000000" label="0,6 - 0,7 " lower="0.600000000000000"/>
      <range symbol="8" render="true" upper="0.800000000000000" label="0,7 - 0,8 " lower="0.700000000000000"/>
      <range symbol="9" render="true" upper="0.900000000000000" label="0,8 - 0,9 " lower="0.800000000000000"/>
      <range symbol="10" render="true" upper="1.000000000000000" label="0,9 - 1 " lower="0.900000000000000"/>
    </ranges>
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="0" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="215,25,28,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="1" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="231,84,55,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="10" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="43,131,186,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="2" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="246,144,83,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="3" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="254,190,116,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="4" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,223,154,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="5" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,255,191,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="6" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="222,242,180,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="7" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="188,228,170,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="8" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="145,203,169,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="9" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="94,167,177,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="fill" name="0" alpha="0.686275">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="243,166,178,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop k="color1" v="215,25,28,255"/>
      <prop k="color2" v="43,131,186,255"/>
      <prop k="discrete" v="0"/>
      <prop k="rampType" v="gradient"/>
      <prop k="stops" v="0.25;253,174,97,255:0.5;255,255,191,255:0.75;171,221,164,255"/>
    </colorramp>
    <classificationMethod id="Pretty">
      <symmetricMode astride="0" enabled="0" symmetrypoint="0"/>
      <labelFormat format="%1 - %2 " trimtrailingzeroes="1" labelprecision="4"/>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" labelPlacementMethod="XHeight" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" barWidth="5" penColor="#000000" penWidth="0" maxScaleDenominator="1e+08" backgroundAlpha="255" scaleDependency="Area" scaleBasedVisibility="0" sizeType="MM" enabled="0" sizeScale="3x:0,0,0,0,0,0" height="15" lineSizeType="MM" minimumSize="0" penAlpha="255" minScaleDenominator="0" backgroundColor="#ffffff" rotationOffset="270" width="15">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" priority="0" showAll="1" placement="1" linePlacementFlags="18" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emissions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="attractions2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_adj_win_mssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="o_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="d_struct_nlod">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id_zone" name=""/>
    <alias index="1" field="emissions1" name=""/>
    <alias index="2" field="emissions2" name=""/>
    <alias index="3" field="attractions1" name=""/>
    <alias index="4" field="attractions2" name=""/>
    <alias index="5" field="o_adj_win_cor" name=""/>
    <alias index="6" field="d_adj_win_cor" name=""/>
    <alias index="7" field="o_adj_win_mssim" name=""/>
    <alias index="8" field="d_adj_win_mssim" name=""/>
    <alias index="9" field="o_nlod" name=""/>
    <alias index="10" field="d_nlod" name=""/>
    <alias index="11" field="o_struct_nlod" name=""/>
    <alias index="12" field="d_struct_nlod" name=""/>
    <alias index="13" field="reagreg" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id_zone" expression=""/>
    <default applyOnUpdate="0" field="emissions1" expression=""/>
    <default applyOnUpdate="0" field="emissions2" expression=""/>
    <default applyOnUpdate="0" field="attractions1" expression=""/>
    <default applyOnUpdate="0" field="attractions2" expression=""/>
    <default applyOnUpdate="0" field="o_adj_win_cor" expression=""/>
    <default applyOnUpdate="0" field="d_adj_win_cor" expression=""/>
    <default applyOnUpdate="0" field="o_adj_win_mssim" expression=""/>
    <default applyOnUpdate="0" field="d_adj_win_mssim" expression=""/>
    <default applyOnUpdate="0" field="o_nlod" expression=""/>
    <default applyOnUpdate="0" field="d_nlod" expression=""/>
    <default applyOnUpdate="0" field="o_struct_nlod" expression=""/>
    <default applyOnUpdate="0" field="d_struct_nlod" expression=""/>
    <default applyOnUpdate="0" field="reagreg" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="id_zone" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="emissions1" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="emissions2" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="attractions1" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="attractions2" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="o_adj_win_cor" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="d_adj_win_cor" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="o_adj_win_mssim" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="d_adj_win_mssim" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="o_nlod" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="d_nlod" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="o_struct_nlod" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="d_struct_nlod" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="reagreg" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id_zone" exp=""/>
    <constraint desc="" field="emissions1" exp=""/>
    <constraint desc="" field="emissions2" exp=""/>
    <constraint desc="" field="attractions1" exp=""/>
    <constraint desc="" field="attractions2" exp=""/>
    <constraint desc="" field="o_adj_win_cor" exp=""/>
    <constraint desc="" field="d_adj_win_cor" exp=""/>
    <constraint desc="" field="o_adj_win_mssim" exp=""/>
    <constraint desc="" field="d_adj_win_mssim" exp=""/>
    <constraint desc="" field="o_nlod" exp=""/>
    <constraint desc="" field="d_nlod" exp=""/>
    <constraint desc="" field="o_struct_nlod" exp=""/>
    <constraint desc="" field="d_struct_nlod" exp=""/>
    <constraint desc="" field="reagreg" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" name="id_zone" hidden="0"/>
      <column width="-1" type="field" name="emissions1" hidden="0"/>
      <column width="-1" type="field" name="emissions2" hidden="0"/>
      <column width="-1" type="field" name="attractions1" hidden="0"/>
      <column width="-1" type="field" name="attractions2" hidden="0"/>
      <column width="-1" type="field" name="o_adj_win_cor" hidden="0"/>
      <column width="-1" type="field" name="d_adj_win_cor" hidden="0"/>
      <column width="-1" type="field" name="o_adj_win_mssim" hidden="0"/>
      <column width="-1" type="field" name="d_adj_win_mssim" hidden="0"/>
      <column width="-1" type="field" name="o_nlod" hidden="0"/>
      <column width="-1" type="field" name="d_nlod" hidden="0"/>
      <column width="-1" type="field" name="o_struct_nlod" hidden="0"/>
      <column width="-1" type="field" name="d_struct_nlod" hidden="0"/>
      <column width="-1" type="field" name="reagreg" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="attractions1"/>
    <field editable="1" name="attractions2"/>
    <field editable="1" name="d_adj_win_cor"/>
    <field editable="1" name="d_adj_win_mssim"/>
    <field editable="1" name="d_nlod"/>
    <field editable="1" name="d_struct_nlod"/>
    <field editable="1" name="emissions1"/>
    <field editable="1" name="emissions2"/>
    <field editable="1" name="id_zone"/>
    <field editable="1" name="o_adj_win_cor"/>
    <field editable="1" name="o_adj_win_mssim"/>
    <field editable="1" name="o_nlod"/>
    <field editable="1" name="o_struct_nlod"/>
    <field editable="1" name="reagreg"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="attractions1"/>
    <field labelOnTop="0" name="attractions2"/>
    <field labelOnTop="0" name="d_adj_win_cor"/>
    <field labelOnTop="0" name="d_adj_win_mssim"/>
    <field labelOnTop="0" name="d_nlod"/>
    <field labelOnTop="0" name="d_struct_nlod"/>
    <field labelOnTop="0" name="emissions1"/>
    <field labelOnTop="0" name="emissions2"/>
    <field labelOnTop="0" name="id_zone"/>
    <field labelOnTop="0" name="o_adj_win_cor"/>
    <field labelOnTop="0" name="o_adj_win_mssim"/>
    <field labelOnTop="0" name="o_nlod"/>
    <field labelOnTop="0" name="o_struct_nlod"/>
    <field labelOnTop="0" name="reagreg"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
