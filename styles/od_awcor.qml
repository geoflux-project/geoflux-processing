<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" simplifyDrawingTol="1" simplifyAlgorithm="0" simplifyLocal="1" maxScale="0" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyDrawingHints="1" readOnly="0" simplifyMaxScale="1" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" type="graduatedSymbol" forceraster="0" symbollevels="0" attr="adj_win_cor" graduatedMethod="GraduatedColor">
    <ranges>
      <range symbol="0" render="true" upper="0.000000000000000" label="-1 - 0 " lower="-1.000000000000000"/>
      <range symbol="1" render="true" upper="0.100000000000000" label="0 - 0,1 " lower="0.000000000000000"/>
      <range symbol="2" render="true" upper="0.200000000000000" label="0,1 - 0,2 " lower="0.100000000000000"/>
      <range symbol="3" render="true" upper="0.300000000000000" label="0,2 - 0,3 " lower="0.200000000000000"/>
      <range symbol="4" render="true" upper="0.400000000000000" label="0,3 - 0,4 " lower="0.300000000000000"/>
      <range symbol="5" render="true" upper="0.500000000000000" label="0,4 - 0,5 " lower="0.400000000000000"/>
      <range symbol="6" render="true" upper="0.600000000000000" label="0,5 - 0,6 " lower="0.500000000000000"/>
      <range symbol="7" render="true" upper="0.700000000000000" label="0,6 - 0,7 " lower="0.600000000000000"/>
      <range symbol="8" render="true" upper="0.800000000000000" label="0,7 - 0,8 " lower="0.700000000000000"/>
      <range symbol="9" render="true" upper="0.900000000000000" label="0,8 - 0,9 " lower="0.800000000000000"/>
      <range symbol="10" render="true" upper="1.000000000000000" label="0,9 - 1 " lower="0.900000000000000"/>
    </ranges>
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="0" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="215,25,28,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="1" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,84,55,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="10" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="43,131,186,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="2" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="246,144,83,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="3" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="254,190,116,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="4" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,223,154,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="5" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,255,191,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="6" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="222,242,180,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="7" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="188,228,170,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="8" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="145,203,169,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="9" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="94,167,177,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="line" name="0" alpha="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleLine">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="243,166,178,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <prop k="color1" v="215,25,28,255"/>
      <prop k="color2" v="43,131,186,255"/>
      <prop k="discrete" v="0"/>
      <prop k="rampType" v="gradient"/>
      <prop k="stops" v="0.25;253,174,97,255:0.5;255,255,191,255:0.75;171,221,164,255"/>
    </colorramp>
    <classificationMethod id="Jenks">
      <symmetricMode astride="0" enabled="0" symmetrypoint="0"/>
      <labelFormat format="%1 - %2 " trimtrailingzeroes="1" labelprecision="3"/>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="&quot;id_zone_orig&quot;" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" labelPlacementMethod="XHeight" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" barWidth="5" penColor="#000000" penWidth="0" maxScaleDenominator="1e+08" backgroundAlpha="255" scaleDependency="Area" scaleBasedVisibility="0" sizeType="MM" enabled="0" sizeScale="3x:0,0,0,0,0,0" height="15" lineSizeType="MM" minimumSize="0" penAlpha="255" minScaleDenominator="0" backgroundColor="#ffffff" rotationOffset="270" width="15">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" priority="0" showAll="1" placement="2" linePlacementFlags="18" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_zone_orig">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_dest">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg_orig">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reagreg_dest">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="valeur1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="valeur2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="norm_valeur1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="norm_valeur2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_macrozone_orig">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_macrozone_dest">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="distance">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="classe_dist">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adj_win_cor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="adj_win_ssim">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id_zone_orig" name=""/>
    <alias index="1" field="id_zone_dest" name=""/>
    <alias index="2" field="reagreg_orig" name=""/>
    <alias index="3" field="reagreg_dest" name=""/>
    <alias index="4" field="valeur1" name=""/>
    <alias index="5" field="valeur2" name=""/>
    <alias index="6" field="norm_valeur1" name=""/>
    <alias index="7" field="norm_valeur2" name=""/>
    <alias index="8" field="id_macrozone_orig" name=""/>
    <alias index="9" field="id_macrozone_dest" name=""/>
    <alias index="10" field="distance" name=""/>
    <alias index="11" field="classe_dist" name=""/>
    <alias index="12" field="adj_win_cor" name=""/>
    <alias index="13" field="adj_win_ssim" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id_zone_orig" expression=""/>
    <default applyOnUpdate="0" field="id_zone_dest" expression=""/>
    <default applyOnUpdate="0" field="reagreg_orig" expression=""/>
    <default applyOnUpdate="0" field="reagreg_dest" expression=""/>
    <default applyOnUpdate="0" field="valeur1" expression=""/>
    <default applyOnUpdate="0" field="valeur2" expression=""/>
    <default applyOnUpdate="0" field="norm_valeur1" expression=""/>
    <default applyOnUpdate="0" field="norm_valeur2" expression=""/>
    <default applyOnUpdate="0" field="id_macrozone_orig" expression=""/>
    <default applyOnUpdate="0" field="id_macrozone_dest" expression=""/>
    <default applyOnUpdate="0" field="distance" expression=""/>
    <default applyOnUpdate="0" field="classe_dist" expression=""/>
    <default applyOnUpdate="0" field="adj_win_cor" expression=""/>
    <default applyOnUpdate="0" field="adj_win_ssim" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="id_zone_orig" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="id_zone_dest" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="reagreg_orig" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="reagreg_dest" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="valeur1" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="valeur2" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="norm_valeur1" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="norm_valeur2" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="id_macrozone_orig" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="id_macrozone_dest" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="distance" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="classe_dist" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="adj_win_cor" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="adj_win_ssim" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id_zone_orig" exp=""/>
    <constraint desc="" field="id_zone_dest" exp=""/>
    <constraint desc="" field="reagreg_orig" exp=""/>
    <constraint desc="" field="reagreg_dest" exp=""/>
    <constraint desc="" field="valeur1" exp=""/>
    <constraint desc="" field="valeur2" exp=""/>
    <constraint desc="" field="norm_valeur1" exp=""/>
    <constraint desc="" field="norm_valeur2" exp=""/>
    <constraint desc="" field="id_macrozone_orig" exp=""/>
    <constraint desc="" field="id_macrozone_dest" exp=""/>
    <constraint desc="" field="distance" exp=""/>
    <constraint desc="" field="classe_dist" exp=""/>
    <constraint desc="" field="adj_win_cor" exp=""/>
    <constraint desc="" field="adj_win_ssim" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" name="id_zone_orig" hidden="0"/>
      <column width="-1" type="field" name="id_zone_dest" hidden="0"/>
      <column width="-1" type="field" name="valeur1" hidden="0"/>
      <column width="92" type="field" name="valeur2" hidden="0"/>
      <column width="167" type="field" name="norm_valeur1" hidden="0"/>
      <column width="154" type="field" name="norm_valeur2" hidden="0"/>
      <column width="155" type="field" name="id_macrozone_orig" hidden="0"/>
      <column width="143" type="field" name="id_macrozone_dest" hidden="0"/>
      <column width="-1" type="field" name="distance" hidden="0"/>
      <column width="-1" type="field" name="classe_dist" hidden="0"/>
      <column width="-1" type="field" name="adj_win_cor" hidden="0"/>
      <column width="-1" type="field" name="adj_win_ssim" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" name="reagreg_orig" hidden="0"/>
      <column width="-1" type="field" name="reagreg_dest" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="adj_win_cor"/>
    <field editable="1" name="adj_win_ssim"/>
    <field editable="1" name="classe_dist"/>
    <field editable="1" name="distance"/>
    <field editable="1" name="id_macrozone_dest"/>
    <field editable="1" name="id_macrozone_orig"/>
    <field editable="1" name="id_zone_dest"/>
    <field editable="1" name="id_zone_orig"/>
    <field editable="1" name="norm_valeur1"/>
    <field editable="1" name="norm_valeur2"/>
    <field editable="1" name="reagreg_dest"/>
    <field editable="1" name="reagreg_orig"/>
    <field editable="1" name="valeur1"/>
    <field editable="1" name="valeur2"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="adj_win_cor"/>
    <field labelOnTop="0" name="adj_win_ssim"/>
    <field labelOnTop="0" name="classe_dist"/>
    <field labelOnTop="0" name="distance"/>
    <field labelOnTop="0" name="id_macrozone_dest"/>
    <field labelOnTop="0" name="id_macrozone_orig"/>
    <field labelOnTop="0" name="id_zone_dest"/>
    <field labelOnTop="0" name="id_zone_orig"/>
    <field labelOnTop="0" name="norm_valeur1"/>
    <field labelOnTop="0" name="norm_valeur2"/>
    <field labelOnTop="0" name="reagreg_dest"/>
    <field labelOnTop="0" name="reagreg_orig"/>
    <field labelOnTop="0" name="valeur1"/>
    <field labelOnTop="0" name="valeur2"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id_zone_orig</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
