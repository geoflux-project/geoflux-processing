<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyMaxScale="1" simplifyDrawingHints="1" simplifyLocal="1" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyAlgorithm="0" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0" readOnly="0" version="3.10.6-A Coruña" maxScale="0" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" enableorderby="0" symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol alpha="1" clip_to_extent="1" name="0" force_rhr="0" type="line">
        <layer class="GeometryGenerator" enabled="1" pass="0" locked="0">
          <prop v="Line" k="SymbolType"/>
          <prop v="$geometry" k="geometryModifier"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" clip_to_extent="1" name="@0@0" force_rhr="0" type="line">
            <layer class="ArrowLine" enabled="1" pass="0" locked="0">
              <prop v="1" k="arrow_start_width"/>
              <prop v="MM" k="arrow_start_width_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="arrow_start_width_unit_scale"/>
              <prop v="1" k="arrow_type"/>
              <prop v="1" k="arrow_width"/>
              <prop v="MM" k="arrow_width_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="arrow_width_unit_scale"/>
              <prop v="3" k="head_length"/>
              <prop v="MM" k="head_length_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="head_length_unit_scale"/>
              <prop v="2.9" k="head_thickness"/>
              <prop v="MM" k="head_thickness_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="head_thickness_unit_scale"/>
              <prop v="0" k="head_type"/>
              <prop v="1" k="is_curved"/>
              <prop v="1" k="is_repeated"/>
              <prop v="2" k="offset"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_unit_scale"/>
              <prop v="0" k="ring_filter"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties" type="Map">
                    <Option name="arrowStartWidth" type="Map">
                      <Option name="active" type="bool" value="true"/>
                      <Option name="expression" type="QString" value="&quot;size&quot;*10"/>
                      <Option name="type" type="int" value="3"/>
                    </Option>
                    <Option name="arrowWidth" type="Map">
                      <Option name="active" type="bool" value="true"/>
                      <Option name="expression" type="QString" value="&quot;size&quot;*10"/>
                      <Option name="type" type="int" value="3"/>
                    </Option>
                  </Option>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
              <symbol alpha="1" clip_to_extent="1" name="@@0@0@0" force_rhr="0" type="fill">
                <layer class="SimpleFill" enabled="1" pass="0" locked="0">
                  <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
                  <prop v="0,0,255,255" k="color"/>
                  <prop v="bevel" k="joinstyle"/>
                  <prop v="0,0" k="offset"/>
                  <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
                  <prop v="MM" k="offset_unit"/>
                  <prop v="35,35,35,255" k="outline_color"/>
                  <prop v="solid" k="outline_style"/>
                  <prop v="0.26" k="outline_width"/>
                  <prop v="MM" k="outline_width_unit"/>
                  <prop v="solid" k="style"/>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option name="name" type="QString" value=""/>
                      <Option name="properties"/>
                      <Option name="type" type="QString" value="collection"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeType="MM" scaleBasedVisibility="0" height="15" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" penColor="#000000" lineSizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" width="15" opacity="1" penAlpha="255" minimumSize="0" backgroundAlpha="255" rotationOffset="270" maxScaleDenominator="1e+08" scaleDependency="Area" minScaleDenominator="0" penWidth="0" enabled="0" lineSizeType="MM" barWidth="5" backgroundColor="#ffffff">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="2" priority="0" obstacle="0" linePlacementFlags="18" dist="0" zIndex="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_orig">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" type="bool" value="false"/>
            <Option name="AllowNull" type="bool" value="false"/>
            <Option name="FilterExpression" type="QString" value=""/>
            <Option name="Key" type="QString" value="id"/>
            <Option name="Layer" type="QString" value="Zones_75862904_af83_4a71_848e_439852544fdf"/>
            <Option name="LayerName" type="QString" value="Zones"/>
            <Option name="LayerProviderName" type="QString" value="postgres"/>
            <Option name="LayerSource" type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(1=2)"/>
            <Option name="NofColumns" type="int" value="1"/>
            <Option name="OrderByValue" type="bool" value="false"/>
            <Option name="UseCompleter" type="bool" value="false"/>
            <Option name="Value" type="QString" value="code_zone"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id_zone_dest">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" type="bool" value="false"/>
            <Option name="AllowNull" type="bool" value="false"/>
            <Option name="FilterExpression" type="QString" value=""/>
            <Option name="Key" type="QString" value="id"/>
            <Option name="Layer" type="QString" value="Zones_75862904_af83_4a71_848e_439852544fdf"/>
            <Option name="LayerName" type="QString" value="Zones"/>
            <Option name="LayerProviderName" type="QString" value="postgres"/>
            <Option name="LayerSource" type="QString" value="dbname='geomob' host=localhost port=5432 user='aurelie_bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;w_geoflux_mmr_coherence&quot;.&quot;zone&quot; (geom) sql=(1=2)"/>
            <Option name="NofColumns" type="int" value="1"/>
            <Option name="OrderByValue" type="bool" value="false"/>
            <Option name="UseCompleter" type="bool" value="false"/>
            <Option name="Value" type="QString" value="code_zone"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="valeur">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="echant">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="size">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="id"/>
    <alias index="1" name="Code zone orig" field="id_zone_orig"/>
    <alias index="2" name="Code zone dest" field="id_zone_dest"/>
    <alias index="3" name="Valeur" field="valeur"/>
    <alias index="4" name="Echant" field="echant"/>
    <alias index="5" name="" field="size"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="id_zone_orig" expression="" applyOnUpdate="0"/>
    <default field="id_zone_dest" expression="" applyOnUpdate="0"/>
    <default field="valeur" expression="" applyOnUpdate="0"/>
    <default field="echant" expression="" applyOnUpdate="0"/>
    <default field="size" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" unique_strength="1" notnull_strength="1" field="id" exp_strength="0"/>
    <constraint constraints="1" unique_strength="0" notnull_strength="1" field="id_zone_orig" exp_strength="0"/>
    <constraint constraints="1" unique_strength="0" notnull_strength="1" field="id_zone_dest" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="valeur" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="echant" exp_strength="0"/>
    <constraint constraints="3" unique_strength="1" notnull_strength="1" field="size" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="id_zone_orig"/>
    <constraint exp="" desc="" field="id_zone_dest"/>
    <constraint exp="" desc="" field="valeur"/>
    <constraint exp="" desc="" field="echant"/>
    <constraint exp="" desc="" field="size"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="1" sortExpression="&quot;valeur&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="1" name="id" type="field"/>
      <column width="-1" hidden="0" name="id_zone_orig" type="field"/>
      <column width="-1" hidden="0" name="id_zone_dest" type="field"/>
      <column width="74" hidden="0" name="valeur" type="field"/>
      <column width="74" hidden="0" name="echant" type="field"/>
      <column width="-1" hidden="1" name="size" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="code_zone_dest" editable="1"/>
    <field name="code_zone_orig" editable="1"/>
    <field name="echant" editable="1"/>
    <field name="flux" editable="1"/>
    <field name="id" editable="1"/>
    <field name="id_zone_d" editable="1"/>
    <field name="id_zone_dest" editable="1"/>
    <field name="id_zone_o" editable="1"/>
    <field name="id_zone_orig" editable="1"/>
    <field name="size" editable="1"/>
    <field name="size_flux" editable="1"/>
    <field name="size_valeur" editable="1"/>
    <field name="valeur" editable="1"/>
    <field name="weight" editable="1"/>
    <field name="zone_d" editable="1"/>
    <field name="zone_o" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="code_zone_dest" labelOnTop="0"/>
    <field name="code_zone_orig" labelOnTop="0"/>
    <field name="echant" labelOnTop="0"/>
    <field name="flux" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="id_zone_d" labelOnTop="0"/>
    <field name="id_zone_dest" labelOnTop="0"/>
    <field name="id_zone_o" labelOnTop="0"/>
    <field name="id_zone_orig" labelOnTop="0"/>
    <field name="size" labelOnTop="0"/>
    <field name="size_flux" labelOnTop="0"/>
    <field name="size_valeur" labelOnTop="0"/>
    <field name="valeur" labelOnTop="0"/>
    <field name="weight" labelOnTop="0"/>
    <field name="zone_d" labelOnTop="0"/>
    <field name="zone_o" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
