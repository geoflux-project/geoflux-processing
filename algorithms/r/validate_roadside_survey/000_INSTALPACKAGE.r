#chooseCRANmirror()
#liste site miroir
#https://cran.r-project.org/mirrors.html
#France: quelques sites miroirs CRAN
#    "https://pbil.univ-lyon1.fr/CRAN/" ==> Dept. of Biometry & Evol. Biology, University of Lyon
#    "https://mirror.ibcp.fr/pub/CRAN/" ==> CNRS IBCP, Lyon
#    "https://cran.biotools.fr/"        ==> IBDM, Marseille
#    "https://ftp.igh.cnrs.fr/pub/CRAN/ ==> Institut de Genetique Humaine, Montpellier
#    "http://cran.irsn.fr/"             ==> French Nuclear Safety Institute, Paris 

ins.pack<-library()$results[,1]
nom.pack<-c("sp","rgdal","rgeos","tidyr","dplyr","stringr","Hmisc","lubridate","RPostgreSQL","osrm","sf")
for (nom in nom.pack){
  if (!(nom %in% ins.pack)){install.packages(nom,repos="https://mirror.ibcp.fr/pub/CRAN/")}
}