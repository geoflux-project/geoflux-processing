#############################################
        if ( (v.pzc[1] %in% ent.base) & (v.pzc[2] %in% ent.base) & (v.pzc[3] %in% ent.base) ) {    
          nom.col<-NULL
          nom.col<-tolower(paste(num.test,v.pzc[1],v.pzc[2],v.pzc[3],sep="_") )           
          test9xx<-NULL
          test9xx<-enq.fich[,c("w_rid",v.pzc[1],v.pzc[2],v.pzc[3])]
          test9xx$cont<-apply(test9xx[,c(v.pzc[1],v.pzc[2],v.pzc[3]) ], MARGIN=1,FUN=FCohPZCPG)
          #test9xx.id<-test9xx$w_rid[test9xx$cont==1]
          #enq.fich[,nom.col]<-""
          #enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont!=""] ,nom.col]<-"x"
          enq.fich[,nom.col]<-""
          enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont!=""] ,nom.col]<-"1"
          enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]
          #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          if (num.test %in% ind.fix){                                                                                       #20210429
            enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]   #20210429
          }                                                                                                                 #20210429
          #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="1",])
          #enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]
          #enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="x",])
          temptxc.synth<-c(num.test,nom.test,nom.crois,as.character(ifelse(is.null(enq.resu),"-",enq.resu)),nom.col)
          txf.synth<-rbind(txf.synth,temptxc.synth)		  
        } 