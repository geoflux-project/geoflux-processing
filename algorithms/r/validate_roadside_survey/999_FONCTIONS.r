##################################################################
#91--------------------------------------------------------------
FRechComPoste<-function(xy){
# xy<-c(903516.049,6845290.976)  
# xy<-c(620540    ,7121875)  
  resu<-NULL
  #MyQuery<-paste("SELECT code_com, libelle, st_x(geom_point), st_y(geom_point) FROM ",ref_schema,".",ref_table," WHERE ( st_within(st_setsrid(st_makepoint(",xy[[1]],",", xy[[2]],"), 2154), n_geographie_2021.geom_polygon) AND n_geographie_2021.type_lieu=3 )  LIMIT 1",sep="")
  MyQuery<-paste("SELECT code_com, libelle FROM ",ref_schema,".",ref_table," WHERE ( st_within(st_setsrid(st_makepoint(",xy[[1]],",", xy[[2]],"), 2154), n_geographie_2021.geom_polygon) AND n_geographie_2021.type_lieu=3 )  LIMIT 1",sep="")
  resu<-dbGetQuery(con, MyQuery)
  if (nrow(resu)!=1){resu<-data.frame("code_com"=NA,"libelle"=NA)}
  return(resu)
}

############################################################################################################################################################################
#93--------------------------------------------------------------
#utiliser toupper ?
FRechCentlU93v3<-function(xx,cx,cy){
  # xx<-enq.fich[4325,c("pays_orig","zone_orig","commune_orig")];xx; cx<-"xol93";cy<-"yol93"
 # appeler PG zonage + filtre sur pzc 1 2 3 
  resu<-NA;xx1<-NULL;xx2<-NULL;xx3<-NULL
  #pays.x<-"code_pays"
  #dep.x<-"code_zone"
  #insee.x<-"code_com" 
  list.conv.n<-c(NA,"X","I","","NA")
  if (xx[[1]] %in% list.conv.n) {xx1<-"N"} else {xx1<-xx[[1]]}
  if (xx[[2]] %in% list.conv.n) {xx2<-"N"} else {xx2<-xx[[2]]}
  if (xx[[3]] %in% list.conv.n) {xx3<-"N"} else {xx3<-xx[[3]]}
  
  pzc.serie<-0
  pzc.serie<-paste(typ.lieu.pzc,collapse = ",")

  
  MyQuery<-paste("SELECT st_x(st_transform(geom_point, 2154)) as ",cx,", st_y(st_transform(geom_point, 2154)) as ",cy," FROM ",ref_schema,".",ref_table," WHERE ( type_lieu IN (",pzc.serie,") AND code_pays='",xx1,"' AND code_zone = '",xx2,"' AND code_com = '",xx3,"') LIMIT 1",sep="") 
  resu<-dbGetQuery(con, MyQuery)
  if (nrow(resu)!=1){resu<-data.frame("x"=NA,"y"=NA);colnames(resu)<-c(cx,cy)}
  return(resu)
}
############################################################################################################################################################################
#90--------------------------------------------------------------
FCohPZCPG<-function(xx){
  resu0<-"?"
  MyQuery<-NULL
  MyQuery<- paste("SELECT code_pays, code_zone, code_com FROM ",ref_schema,".",ref_table," WHERE (code_pays='",xx[[1]],"' AND code_zone='",xx[[2]],"' AND code_com='",xx[[3]],"' ) LIMIT 1",sep="")
  zon.fich.cont<-dbGetQuery(con, MyQuery)
  if (nrow(zon.fich.cont)>0){resu0<-""} else{resu0<-"x"}
  if (xx[[1]] %in% c("N") & xx[[2]] %in% c("N") & xx[[3]] %in% c("N") ){resu0<-""} #possible pour travail et domicile
  #if (xx[[1]] %in% c("X") & xx[[2]] %in% c("X") & xx[[3]] %in% c("X") ){resu0<-""} #possible si lieu ind�termin�
  return(resu0)
}

##################################################################
#01--------------------------------------------------------------
FQuestPosee<-function(x0){
     resu0<- 0
     resu1<-vv.qxpr$posee[vv.qxpr$clef==x0[[1]]]
     if (!identical(resu1,character(0))){resu0<-resu1}
     return(resu0)
}
##################################################################
#02--------------------------------------------------------------
FHHMMSS<-function(hhh){
     library(lubridate)
     #12345678
     #HH:MM:SS
     # hh<-"06:16:00"
     # hh<-" 6:16: 0"
      # hh<-" 6:16: 0 AM"
     # hhh<-"06:55:37"
     # hhh<-c(5,"07:07:51")
     hh<-as.character(hhh[1]) #as.character(hhh[1])
     mess.erreur<-"ErreurFormat" #"ErreurFormat"
     mess.ok<-"OkFormat" #"OkFormat"
     
     resu0<-"?"
     hh.long<-NULL; hh.long<-nchar(hh)  #doit etre 8
     hh.long2<-nchar(gsub(" ","",hh,fixed=T))  #doit toujours etre 8
     #grep(":",hh,fixed=T)
     hh.2p1<-NULL;hh.2p1<-substr(hh,3,3) #doit etre ":"
     hh.2p2<-NULL;hh.2p2<-substr(hh,6,6) #doit etre ":"
     hh.am<-NULL;hh.am<-str_detect(toupper(hh),"AM") #doit etre FALSE
     hh.pm<-NULL;hh.pm<-str_detect(toupper(hh),"PM") #doit etre FALSE
     
     hh.hh<-NULL;hh.hh<-as.numeric(substr(hh,1,2))
     hh.mm<-NULL;hh.mm<-as.numeric(substr(hh,4,5))
     hh.ss<-NULL;hh.ss<-as.numeric(substr(hh,7,8))
     
     #hh.long;hh.long2;hh.2p1;hh.2p2
     #hh.am;hh.pm
     #hh.hh;hh.mm;hh.ss
     if (hh.long==8 & hh.long2==hh.long){ 
          if (hh.am==T | hh.pm==T) { resu0<-mess.erreur}
          if (hh.am==F & hh.pm==F & !(hh.2p1==":" & hh.2p2==":") ) {resu0<-mess.erreur }
          if (hh.am==F & hh.pm==F &  (hh.2p1==":" & hh.2p2==":") ) {
               if (hh.hh %in% c(0:23) & hh.mm %in% c(0:59) & hh.ss %in% c(0:59)) {resu0<-mess.ok} else {resu0<-mess.erreur }
          }
     } else {resu0<-mess.erreur}
     return(resu0)
}
##################################################################
#03--------------------------------------------------------------
FtestDGITM3<-function(xtest) {
  #transforme la condition en entr�e en condition au format R
  #ajout de str_pad pour pb nb car champs<--suppression
  #et liste les champs utilis�s
  #ATTENTION SEULEMENT "ET" possible
  filtrefinal<-NULL
  nontrouve<-c("nontrouve")
  varenqlist<-tolower(c(cod.fich$champ,"W_VLPL","VOPDKM","VODKM","VOPKM","VPDKM","RATIODIST.VODXVOPD","CODE_CONTROLE_OD")) #colnames(enq.fich2)
  ##separation des hyp et de la conclusion
  
  #hypoth�se: liste des variables et valeurs possibles
  hyp_etou<-NULL
  if ( is.integer0(grep("et",xtest)) & is.integer0(grep("ou",xtest)) ) {hyp_etou<-unlist(strsplit(xtest,split="et"));tmpop<-" & " }
  if ( !is.integer0(grep("et",xtest)) & is.integer0(grep("ou",xtest)) ) {hyp_etou<-unlist(strsplit(xtest,split="et"));tmpop<-" & " }
  if ( is.integer0(grep("et",xtest)) & !is.integer0(grep("ou",xtest)) ) {	hyp_etou<-unlist(strsplit(xtest,split="ou"));tmpop<-" | " }
  
  #pour chaque hypoth�se
  for (i in 1:length(hyp_etou)){
    erreuroperateur<-FALSE
    #recherche de l'op�ration parmi "==","!=",">","<",">=","<="
    op<-FRechOp(hyp_etou[i],listoperateur)
    if (is.null(op)){erreuroperateur<-TRUE}
    if (erreuroperateur==FALSE) {
      lhyp_etou<-unlist(strsplit(hyp_etou[i],split=op))
      
      #1.recherche des variables
      var<-unlist(strsplit(lhyp_etou[1],split=","))
      #a. verif que la variable figure dans la liste des champs du dessin d'enregistrement
      if ( identical(intersect(varenqlist,var),character(0)) ==TRUE ) {nontrouve<-c(nontrouve,var) }
      #b.recherche des fichiers utilis�s
      # pour DGITM, un seul fichier d'enqu�te: enq.fich2
      #listfichavar<-unique(lapply(var,FUN= function(x) ( xTOUTENT$fich[which(xTOUTENT$yven==x)]) ))
      #listfichavar<-unique(lapply(var,FUN= function(x) ( xTOUTENT$fich[which(varenqlist==x)]) ))
      
      #2. recherche des valeurs en fonction de l'op�rateur
      val<-unlist(strsplit(lhyp_etou[2],split=","))
      if ( max(val %in% varenqlist)==FALSE ){#les valeurs ne sont pas des champs
        nop<-NULL
        #if (op %in% c("==","!=")){nop<-" %in% ";fichvar<-paste("temp$",var,nop,sep="")}
        
        #if (op %in% c("==","!=")){nop<-" %in% ";fichvar<-paste("str_pad(","temp$",var,",pas_str_pad,pad=\"0\")",nop,sep="")}
        #if (op %in% c(">","<",">=","<=")){nop<-op;fichvar<-paste("as.numeric(temp$",var,")",nop,sep="")}
        if (op %in% c("==","!=")){nop<-" %in% ";fichvar<-paste("temp$",var,nop,sep="")}
        if (op %in% c(">","<",">=","<=")){nop<-op;fichvar<-paste("as.numeric(temp$",var,")",nop,sep="")}
        #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        #TRAITEMENT DE VALEURS X,I,N pour champs num�riques?!!!!!!!!!!!!!!!!!!!!!!!!
        
      } else {#les valeurs sont des champs
        if (op=="!=") {nop<-"=="} else {nop<-op}
        #fichvar<-paste("as.numeric(temp$",var,")",nop,sep="")
        #fichvar<-paste("str_pad(","temp$",var,",pas_str_pad,pad=\"0\")",nop,sep="") #voir pb nbcar champ
        fichvar<-paste("temp$",var,nop,sep="") #voir pb nbcar champ
        
      }
      
      if ( max(val %in% varenqlist)==FALSE ){#les valeurs ne sont pas des champs
        val2<-paste('"',val,'"',sep="")
        #val2<-paste("str_pad(",'"',val,'"',",pas_str_pad,pad=\"0\")",sep="")
        if (max(val) %in% c("vide")) {val2<-paste('"',val,'"',sep="");val2<-gsub("\"vide\"","blanc",val2)}
        if (op %in% c("==","!=")) {
          val2<-paste(val2,collapse=",")
          val2<-paste("c(",val2,")",sep="")
          #fichvar2<-lapply(fichvar,FUN=function(x) (paste(ifelse(op=="!=","!",""),"(",x,val2,")",sep="")       ) )
        } else {#val ne doit avoir qu'une valeur autre que vide
          val2<-paste("as.numeric(",val2,")",sep="")
          
        }
        
      } else {
        #val2<-paste( "as.numeric(temp$",val,")",sep="")
        #val2<-paste("str_pad(",'"',val,'"',",pas_str_pad,pad=\"0\")",sep="")
        val2<-paste( "(temp$",val,")",sep="")
        
      } #val ne doit avoir qu'une seule valeur
      
      fichvar2<-lapply(fichvar,FUN=function(x) (paste(ifelse(op=="!=","!",""),"(",x,val2,")",sep="")       ) )
      
      if (length(fichvar2)==1){filtre<-fichvar2} else  {filtre<-paste(fichvar2,collapse=" & ")}
      
      if (is.null(filtrefinal)) {filtrefinal<-filtre} else {filtrefinal<-paste(filtrefinal,filtre, sep=tmpop)}
    }
  } #ssss
  if (erreuroperateur==FALSE) {
    #chaine<-paste("temp01<-temp[which(",filtrefinal,"),]",sep="")
    chaine<-paste("(",filtrefinal,")",sep=" ")
    
    if ( length(nontrouve)==1 ) {return(chaine)} else {return(nontrouve) }
  } else {return("erreur d op�rateur: � v�rifier") }
  
}
##################################################################
#04--------------------------------------------------------------
#
Fhypcond<-function(rela) {
  #separation des hyp et de la conclusion
  xch01<-gsub(" ","",rela)
  xch02<-unlist(strsplit(xch01,split="alors"))
}
##################################################################
#05--------------------------------------------------------------
is.integer0 <- function(x)
{    #fonction pour identifier la r�ponse integer(0)
  is.integer(x) && length(x) == 0L
}

############################################################################################################################################################################
#06--------------------------------------------------------------
SansAccent<-function(x)
{
  #return(chartr("������������", "eeeeEEEEaAcC", x))
  return(chartr("�����������������������������������������������������", "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc", x))
}

############################################################################################################################################################################
#07--------------------------------------------------------------
SansPv<-function(x)
{
  return(gsub(";","-",x))
}

############################################################################################################################################################################
#08--------------------------------------------------------------
Fchar<-function(x)
{
  y<-as.character(x)
  return(y)
}
############################################################################################################################################################################
#09--------------------------------------------------------------
FRechOp<-function(ch00,lop){#traduction des op�rateurs
  sop<-NULL;trouve=FALSE
  for (j in 1:length(lop)){
    if ( !is.integer0(grep(lop[j],ch00)) & trouve==FALSE ){sop<-lop[j];j<-length(lop)+10;trouve=TRUE}
  }
  return(sop)
}
############################################################################################################################################################################
#10--------------------------------------------------------------
FCohPZC<-function(xx){
  resu0<-"?"
  zon.fich.cont<-zon.fich@data[ (xx[[1]] %in% zon.fich@data$code_pays) &
                                  (xx[[2]] %in% zon.fich@data$code_zone) &
                                  (xx[[3]] %in% zon.fich@data$code_com)
                                ,]
  if (nrow(zon.fich.cont)>0){resu0<-""} else{resu0<-"x"}
  if (xx[[1]] %in% c("N") & xx[[2]] %in% c("N") & xx[[3]] %in% c("N") ){resu0<-""} #possible pour travail et domicile
  #if (xx[[1]] %in% c("X") & xx[[2]] %in% c("X") & xx[[3]] %in% c("X") ){resu0<-""} #possible si lieu ind�termin�
  return(resu0)
}
############################################################################################################################################################################
#10--------------------------------------------------------------
FCohPZC_v0<-function(xx){
  resu0<-"?"
  zon.fich.cont<-zon.fich@data[ (xx[[1]] %in% zon.fich@data$code_pays) &
                                  (xx[[2]] %in% zon.fich@data$code_zone) &
                                  (xx[[3]] %in% zon.fich@data$code_com)
                                ,]
  if (nrow(zon.fich.cont)>0){resu0<-""} else{resu0<-"x"}
  if (xx[[1]] %in% c("N") & xx[[2]] %in% c("N") & xx[[3]] %in% c("N") ){resu0<-""} #possible pour travail et domicile
  return(resu0)
}
############################################################################################################################################################################
##11--------------------------------------------------------------
##utiliser toupper ?
#FRechPostel93U<-function(xx,ao){
#  resu<-NA
#  poste.p<-"CODE_POSTE"
#  num.pts<-"NUM_POINT"
#  resu<-as.numeric(pts.fich@data[
#                                  pts.fich@data[,poste.p]==xx[[1]] & pts.fich@data[,num.pts]==xx[[2]]
#                                ,ao])
#  if (length(resu)==0){resu<-NA} else {resu<-resu[1]}
#  return(resu) 
#}
############################################################################################################################################################################
#11--------------------------------------------------------------
#utiliser toupper ?
FRechPostel93Uv2<-function(xx,ao){
  resu<-NA
  poste.p<-"code_poste"
  num.pts<-"num_point"
  resu<-as.numeric(pts.fich@data[
    pts.fich@data[,poste.p]==xx[[1]] & pts.fich@data[,num.pts]==xx[[2]]
    ,ao])
  if (length(resu)==0){resu<-NA} else {resu<-resu[1]}
  return(resu) 
}
############################################################################################################################################################################
#12--------------------------------------------------------------
#utiliser toupper ?
FRechPostel93UCharv2<-function(xx,ao){
  resu<-NA
  poste.p<-"code_poste"
  num.pts<-"num_point"
  resu<-(pts.fich@data[
    pts.fich@data[,poste.p]==xx[[1]] & pts.fich@data[,num.pts]==xx[[2]]
    ,ao])
  if (length(resu)==0){resu<-NA} else {resu<-resu[1]}
  return(resu) 
}
############################################################################################################################################################################
##13--------------------------------------------------------------
##utiliser toupper ?
#FRechCentlU93<-function(xx,ao){
#  resu<-NA
#  pays.x<-"CODE_PAYS"
#  dep.x<-"CODE_ZONE"
#  insee.x<-"CODE_COM" 
#  resu<-as.numeric( zon.fich@data[
#                                  zon.fich@data[,pays.x]==xx[[1]] &
#                                  zon.fich@data[,dep.x]==xx[[2]] &
#                                  zon.fich@data[,insee.x]==xx[[3]]
#                                 ,ao])
#  if (length(resu)==0){resu<-NA} else {resu<-resu[1]}
#  return(resu) 
#}
############################################################################################################################################################################
#13--------------------------------------------------------------
#utiliser toupper ?
FRechCentlU93v2<-function(xx,ao){
  # xx<-enq.fich[4325,c("pays_orig","zone_orig","commune_orig")];xx; ao<-"xcl93"
  resu<-NA;xx1<-NULL;xx2<-NULL;xx3<-NULL
  pays.x<-"code_pays"
  dep.x<-"code_zone"
  insee.x<-"code_com" 
  list.conv.n<-c(NA,"X","I","","NA")
  if (xx[[1]] %in% list.conv.n) {xx1<-"N"} else {xx1<-xx[[1]]}
  if (xx[[2]] %in% list.conv.n) {xx2<-"N"} else {xx2<-xx[[2]]}
  if (xx[[3]] %in% list.conv.n) {xx3<-"N"} else {xx3<-xx[[3]]}

  resu<-as.numeric( zon.fich@data[
    zon.fich@data[,pays.x]==xx1 &
      zon.fich@data[,dep.x]==xx2 &
      zon.fich@data[,insee.x]==xx3
    ,ao])
  if (length(resu)==0){resu<-NA} else {resu<-resu[1]}
  return(resu) 
}
############################################################################################################################################################################
#14--------------------------------------------------------------
FAzimutToTrigo<-function(xx){
  resu<-NA
  if(!is.na(xx[[1]])){
    if (xx[[1]] >=0 & xx[[1]] <=90){resu<-90-xx[[1]]}
    if (xx[[1]] >90 & xx[[1]] <=360){resu<-450-xx[[1]]}
  }
  return(resu)
}
############################################################################################################################################################################
#15--------------------------------------------------------------
#Angle sens trigo par rapport � l'horizontale en degr�
fAngleSensTrigo<-function(xy){ 
  #xy(         1,          2,          3,          4)
  #   "xodirl93", "yodirl93", "xddirl93", "yddirl93"
  resua<-NA
  if (max(is.na(xy))==TRUE) {
    resua<-NA
  }else{
    diff.y<-(xy[4]-xy[2])
    diff.x<-(xy[3]-xy[1]) 
    aod<-atan( diff.y / diff.x)*180/pi;
    decal<-0
    if(diff.y>=0 & diff.x>=0) {decal<-0} #12-12h15
    if(diff.y<0 & diff.x>=0) {decal<-360} #12h15-12h30
    if(diff.y<0 & diff.x<0) {decal<-180} #12h30-12h45
    if(diff.y>0 & diff.x<0) {decal<-180} #12h30-12h45
    resua<-aod+decal
  }
  
  return(resua)
}
############################################################################################################################################################################
#16--------------------------------------------------------------
FAngle2<-function(aa){
  #c("OPH","HPD"), 2 angles en degr� entre 0 et 360� dans le sens trigo
  resua<-NA
  if(max(is.na(aa))==FALSE){
    resua<-min(abs(aa[1]-aa[2]),360-abs(aa[1]-aa[2]))
  }
  return(resua)
}
############################################################################################################################################################################
#17--------------------------------------------------------------
fDistVOm<-function(vv,dunite) {
  #c("xol93","yol93","xdl93","ydl93")
  #dunite: c("km","m")
  du1<-c("km","hm","dam","m","dm", "cm",  "mm")
  du2<-c(1000, 100,   10,  1, 0.1, 0.01, 0.001)
  du3<-du2[du1==dunite]
  
  if (max(is.na(vv))==TRUE) {dvo<-NA} else {
    dvo<-( sqrt( (vv[3]-vv[1])^2  + (vv[4]-vv[2])^2 ) ) / du3
  }
  return(dvo)
}
############################################################################################################################################################################
#18a--------------------------------------------------------------
FCodeControleInt<-function(cc){
     num.code<-c(-1                                 ,0        ,1         ,2         ,8                           ,9                )
     int.code<-c("Controle non fait(manque donn�es)","Rejet�e","Conserv�e","Invers�e","Contr�le manuel mais O=D=P","Contr�le manuel")
     inti<-"Inconnu"  
     if (as.numeric(cc) %in% num.code){inti<-int.code[num.code==cc]}
     return(inti)
}
############################################################################################################################################################################
#18--------------------------------------------------------------
FCodeControle<-function(cc){
  #c("vOPkm","vPDkm","vODkm","RatioDistvODxvOPD","ANGLE_OPD","ANGLE_DPPD") 
  # qq<-fich.opd2[,c("vOPkm","vPDkm","vODkm","RatioDistvODxvOPD","ANGLE_OPD","ANGLE_DPPD","code_o","code_d","GISCO_ID") ]
  #c(      1,      2,      3,                   4,          5,           6,             7,             8,          9)  
  #c("VOPKM","VPDKM","VODKM","RATIODIST.VODXVOPD","ANGLE_OPD","ANGLE_DPPD","COMMUNE_ORIG","COMMUNE_DEST","POSTE.COM")
  vop<-as.numeric(cc[1])
  vpd<-as.numeric(cc[2])
  vod<-as.numeric(cc[3])
  divdist<-as.numeric(cc[4])
  ang.opd<-as.numeric(cc[5])
  ang.dppd<-as.numeric(cc[6])
  como <- cc[7] #SQ
  comd <- cc[8] #SQ
  comp <- cc[9] #SQ
  
  rejet<-0 #OD rejetee
  conserve<-1 #OD conservee
  inverse<-2 #OD inversee a controler
  manuel<-9 #OD a controler manuellement
  lieu_ident<-8 #SQ OD initialement a controler manuellement (code 9) mais dont l'origine = destination = commune du poste (COMMUNE_ORIG =COMMUNE_DEST=POSTE.COM)
                # avec info commune different de vide/blanc, X,I,N et NA, "NA"
  erreur<- -1 #controle non fait par manque d'info (ex origine, destination ou poste inconnu)
  
  blanc0<-c(makeNstr(" ",c(0:500)))
  
  resuc<-NA
  if ( length( intersect( c(NA,Inf,-Inf),cc ) )>0 ){resuc<-erreur} else {
    if (vop<=5 | vpd<=5 | vod>=150)  {resuc<-manuel}
    if (vop>5 & vpd>5 & vod<150)  {
      resuc<-manuel
      if (vod<=vop | vod<=vpd | divdist<=0.7 | ang.opd<=80){resuc<-rejet} else {
        if (divdist>=0.95 & ang.opd>=130){
          if (ang.dppd<=80){resuc<-conserve}
          if (ang.dppd>=130){resuc<-inverse}
          if (80<ang.dppd & ang.dppd<130) {resuc<-manuel}
        } #if (divdist>=0.95 & ang.opd>=130)
      } # if (vod<=vop | vod<=vpd | divdist<=0.7 | ang.opd<=80)
    } #if (vop>5 & vpd>5 & vod<150)
    if (resuc==9 & como==comd & como==comp & !(como %in% c(NA,"NA","X","I","N",blanc0)) ){resuc<-lieu_ident} #SQ
  } # if ( length( intersect( c(NA,Inf,-Inf),cc ) )>0 )
  return(resuc)
}
############################################################################################################################################################################
#19--------------------------------------------------------------

############################################################################################################################################################################
##########################################################################
################## Distances OD et ODP ###################################
##########################################################################
FinfoDistv2<-function(xx){
  # xx<-fich.opd2[1,]
  #ex: fich.opd2$tempdist<-apply(fich.opd2[,dist.fich[,"var_E"]],MARGIN=1,FUN=FinfoDist)
  #> dist.fich[,"var_E"]
  #"INSEEO" "INSEED" "Poste" 
  # xx comporte 3 elts inseeo, inseed et poste
  #1-chercher l'objet de la zone d'origine
  
  pto<-NULL
  #pays.o<-dist.fich$var_T[1]
  #dep.o<-dist.fich$var_T[2]
  #insee.o<-dist.fich$var_T[3]
  
  #pays.d<-dist.fich$var_T[4]
  #dep.d<-dist.fich$var_T[5]
  #insee.d<-dist.fich$var_T[6]
  
  #poste.e<-dist.fich$var_T[7]
  
  insee.o<-"code_zon"
  insee.d<-"code_zon"
  poste.e<-"poste_point"
  
  insee.o;insee.d;poste.e
  if ( max(is.na(xx[[1]]),is.na(xx[[2]]),is.na(xx[[3]]))==0  ){
    pto<-fzon84[
      fzon84@data[,insee.o]==xx[[1]]
      ,]
    ptd<-fzon84[
      fzon84@data[,insee.d]==xx[[2]]
      ,]
    
    ptp<-f1poste84[f1poste84@data[,poste.e]==toupper(xx[[3]]),]
    
    if (nrow(pto@data)==1 & nrow(ptd@data)==1 & nrow(ptp@data)==1){
      iti_OP<-NULL;iti_PD<-NULL;distOPD<-NULL;tempsOPD<-NULL
      iti_OP<-osrmRoute(src = gCentroid(pto),dst = gCentroid(ptp),
                        overview = "full", returnclass = "sf")
      iti_PD<-osrmRoute(src = gCentroid(ptp),dst = gCentroid(ptd),
                        overview = "full", returnclass = "sf") 
      
      distOPDkm<-iti_OP$distance+iti_PD$distance
      tempsOPDmin<-iti_OP$duration+iti_PD$duration
      
    } else {distOPDkm<-NA;tempsOPDmin<-NA}
    #calcul distance OD
    if (nrow(pto@data)==1 & nrow(ptd@data)==1){
      iti_OD<-NULL;distOD<-NULL;tempsOD<-NULL
      iti_OD<-osrmRoute(src = gCentroid(pto),dst = gCentroid(ptd),
                        overview = "full", returnclass = "sf")  
      distODkm<-iti_OD$distance
      tempsODmin<-iti_OD$duration    
    }else {distODkm<-NA;tempsODmin<-NA}
  } else {
    distOPDkm<-NA
    tempsOPDmin<-NA
    distODkm<-NA
    tempsODmin<-NA
  }
  
  return(data.frame(distOPDkm,tempsOPDmin,distODkm,tempsODmin))
  
} #function
