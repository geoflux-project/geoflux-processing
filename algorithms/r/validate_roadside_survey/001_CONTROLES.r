################################################################################################################################################################
################################################################################################################################################################
#                                                          B. DEFINITION DES CHEMINS
################################################################################################################################################################
################################################################################################################################################################
options(digits=10)
options(scipen=2)

options(max.print=5.5E5)
rm(list=ls())
gc()

hh01<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")

initial.options <- commandArgs(trailingOnly = FALSE)
initial.options

file.arg.name <- "--file="
script.name <- gsub(file.arg.name, "", initial.options[grep(file.arg.name, initial.options)],fixed=T)
script.basename <- dirname(script.name)
other.name <- file.path(script.basename, "other.R")
#print("#------------------------------#")
#script.name 
#script.basename
#other.name
script.basename2<-gsub("0_prog","",script.basename)
#print("#------------------------------#")
#script.basename2

#print(paste("Sourcing",other.name,"from",script.name))
#if ( identical(script.basename,character(0)) ){x<-paste(getwd(),"/",sep="")} else {
#  if ( str_detect(script.basename,"0_prog")==T ){x<-script.basename2}else{x<-paste(getwd(),"/",sep="")}
#}
if ( identical(script.basename,character(0)) ){x<-paste(getwd(),"/","0_prog","/",sep="")} else {x<-script.basename}


##x<-paste(getwd(),"/",sep="")
#chemin<-gsub('/+','\\\\',x)
chemin<-x
cheminP<-paste(chemin,"/",sep="")
#cheminP<-paste(chemin,"0_prog/",sep="")
#cheminD0<-paste(chemin,"1_donnees",sep="") #20171106
#cheminD<-paste(chemin,"1_donnees/",sep="")

source( paste(cheminP,"000_PARAM.r",sep="") )

################################################################################################################################################################
################################################################################################################################################################
#                                                          A. CHARGEMENT DES PACKAGES
################################################################################################################################################################
################################################################################################################################################################
#C:/Users/gery.lelievre/Documents/.Renviron
#chooseCRANmirror()
#liste site miroir
#https://cran.r-project.org/mirrors.html
#France: quelques sites miroirs CRAN
#    "https://pbil.univ-lyon1.fr/CRAN/" ==> Dept. of Biometry & Evol. Biology, University of Lyon
#    "https://mirror.ibcp.fr/pub/CRAN/" ==> CNRS IBCP, Lyon
#    "https://cran.biotools.fr/"        ==> IBDM, Marseille
#    "https://ftp.igh.cnrs.fr/pub/CRAN/ ==> Institut de Genetique Humaine, Montpellier
#    "http://cran.irsn.fr/"             ==> French Nuclear Safety Institute, Paris 

# create local user library path (not present by default)
dir.create(path = Sys.getenv("R_LIBS_USER"), showWarnings = FALSE, recursive = TRUE)

ins.pack<-library()$results[,1]
nom.pack<-c("sp","rgdal","rgeos","tidyr","dplyr","stringr","Hmisc","lubridate","RPostgreSQL","osrm","sf")
for (nom in nom.pack){
  if (!(nom %in% ins.pack)){install.packages(nom,repos=param_mirror, lib = Sys.getenv("R_LIBS_USER"))}
}

library(sp)
library(rgdal) #pour readOGR
library(rgeos) #pour gCentroid
library(tidyr) #pour pivot_longer
library(dplyr) #pour mutate/join
library(stringr) #pour str_replace  et str_detect
library(Hmisc) #pour makeNstr
library(lubridate) #pour parse_date_time
library(RPostgreSQL)
library(sf)
library(osrm)


################################################################################################################################################################
################################################################################################################################################################
#                                                          C. CHARGEMENT FICHIER PARAMETRES ET PARAMETRES COMPLEMENTAIRES
################################################################################################################################################################
################################################################################################################################################################

#source( paste(cheminP,"000_PARAM.r",sep="") )

blanc<-c(makeNstr(" ",c(0:500))) #chaine de caractere = serie de " " pour controle champ "vide"
pas_str_pad<-20
encodage01<-"UTF-8" #SOUS NOTEPAD++ utiliser conversion en UTF-8 SANS BOM
nom.dos03<-""
nom.fich.synth<-"09_SYNTHESE_01"
entete.a.supprimer<-c("controle","xol93","yol93","xdl93","ydl93","xpl93","ypl93","poste.angle.azimut","poste.angle.trigo","oph","hpd","angle_opd","angle_dppd")
#liste des champs min pour lesquels la codif doit �tre ok, sert avec les test pointes par ind_min=x � d�finir l'indicateur min de qualite

indic.min<-c("id_itw","code_poste","num_point","per_enq","type_veh","pays_dest","zone_dest","commune_dest","motif_dest","pays_orig","zone_orig","commune_orig","motif_orig","plaques_orange")

#creation du dossier de sortie si il n'existe pas
rep_sortie.list<-unlist(strsplit(rep_sortie,split="/",fixed=T))
lf_sortie<-NULL;lf_sortie<-list.files(paste(paste(rep_sortie.list[1:(length(rep_sortie.list)-1)],collapse = "/"),"/",sep=""))
if (!(rep_sortie.list[length(rep_sortie.list)] %in% lf_sortie)){dir.create(paste(rep_sortie,"/",sep=""))} 


################################################################################################################################################################
################################################################################################################################################################
#                                                         D. CHARGEMENT FICHIER DES FONCTIONS
################################################################################################################################################################
################################################################################################################################################################

source( paste(cheminP,"999_FONCTIONS.r",sep="") )


################################################################################################################################################################
################################################################################################################################################################
#                                                          E. LECTURES DES FICHIERS
#                                           REMARQUE: POUR LES FICHIERS CSV? ACCEPTE T ON UN AUTRE FORMAT (txt/TXT?; xls/xlsx...?)
################################################################################################################################################################
################################################################################################################################################################
#_______________________________________________________________________________________________________________________________________________________________
#F.1-FICHIER DE CODIF (CSV SEP ";", DECIMAL ".")
#_______________________________________________________________________________________________________________________________________________________________
#old: chemin.cod<-paste(cheminD,cod.dos,sep="/")
#old: cod.fich<-read.delim(paste(chemin.cod,"/",cod.nom,sep=""),sep=cod.sep,header=T,colClasses="character")

#cod.fich<-read.delim(gsub('/+','\\\\',fichier_codif),sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
cod.fich<-read.delim(fichier_codif,sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
#entete du fichier + valeur de "champ" en minuscule ("compatibilite postgresql")
colnames(cod.fich)<-tolower(colnames(cod.fich))
#cod.fich$champ<-tolower(cod.fich$champ)

#_______________________________________________________________________________________________________________________________________________________________
#F.2-FICHIER DES QUESTIONS PAR POSTE (CSV SEP ";")
#_______________________________________________________________________________________________________________________________________________________________
#old: qxp.fich<-read.delim(paste(chemin.cod,"/",qxp.nom,sep=""),sep=qxp.sep,header=T,colClasses="character")

#qxp.fich<-read.delim(gsub('/+','\\\\',fichier_quest),sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
qxp.fich<-read.delim(fichier_quest,sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
#entete du fichier + valeur de "champ" + "code_poste" et "num_point" en minuscule ("compatibilite postgresql")
colnames(qxp.fich)<-tolower(colnames(qxp.fich))
#qxp.fich$champ<-tolower(qxp.fich$champ)
#qxp.fich$code_poste<-tolower(qxp.fich$code_poste)
#qxp.fich$num_point<-tolower(qxp.fich$num_point)


#_______________________________________________________________________________________________________________________________________________________________
#F.3-FICHIER DES TESTS CROISES (CSV SEP ";", DECIMAL ".")
#_______________________________________________________________________________________________________________________________________________________________
#old: txc.fich<-read.delim(paste(chemin.cod,"/",txc.nom,sep=""),sep=txc.sep,header=T,colClasses="character")

#txc.fich<-read.delim(gsub('/+','\\\\',fichier_tests),sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
txc.fich<-read.delim(fichier_tests,sep=separateur_csv,header=T,colClasses="character",encoding=encodage01)
#entete du fichier + valeur de "test" ("compatibilite postgresql")
colnames(txc.fich)<-tolower(colnames(txc.fich))
txc.fich$test<-tolower(txc.fich$test)
txc.fich$domaine<-tolower(txc.fich$domaine)

#_______________________________________________________________________________________________________________________________________________________________
# F.4-CONNEXION AU SERVEUR DE BASES DE DONNEES CONTENANT LE REFERENTIEL GEOGRAPHIQUE
#_______________________________________________________________________________________________________________________________________________________________
# Rem: on utilise la table de zonage n_geographie_2021 (ou millesime en cours) du schema o_geoflux_ref
#      on peut avoir les coordonnees du centroides de la zone (ou point dans zone si centroide externe) avec geom_point

con = dbConnect(PostgreSQL(), host = param_host, port = param_port, user = param_user, password = param_pwd, dbname = param_dbname)

#_______________________________________________________________________________________________________________________________________________________________
#F.5-FICHIER DES  POINTS D ENQUETES (SHAPE LAMBERT93). FOURNI AU MOMENT DE L ENQUETE
#_______________________________________________________________________________________________________________________________________________________________
#--F.5.1: lecture du fichier des points d'enquete (dans creation de "pts.fich")
#������������������������������������������������������������������������������

fichier_points_enq.list<-unlist(strsplit(fichier_points_enq,split="/",fixed=T))
pts.nom<-fichier_points_enq.list[length(fichier_points_enq.list)]
chemin.cod<-paste(fichier_points_enq.list[1:(length(fichier_points_enq.list)-1)],collapse = "/")

if (tolower((substr(pts.nom,nchar(pts.nom)-4+1,nchar(pts.nom)))) %in% c(".shp")) {pts.nom.court<-substr(pts.nom,1,nchar(pts.nom)-4)} else {pts.nom.court<-pts.nom}
pts.fich<-readOGR(dsn = chemin.cod, stringsAsFactors = F , layer = pts.nom.court,encoding=encodage01,use_iconv = TRUE)

#--F.5.2: passage en Lambert93 (rem: la table doit normalement deja etre en L93)
#�������������������������������������������������������������������������������
#pts.fich<-spTransform(pts.fich,CRS("+init=epsg:2154"))

#--F.5.2: ajout des coordonnees de centroides
#��������������������������������������������
pts.fich@data$xpl93<-gCentroid(pts.fich,byid=T)$x
pts.fich@data$ypl93<-gCentroid(pts.fich,byid=T)$y

#--F.5.3: passage en minuscule de l'entete du fichier ("compatibilite postgresql")
#���������������������������������������������������������������������������������
colnames(pts.fich@data)<-tolower(colnames(pts.fich@data))

#--F.5.4: ajout des "code_com", "libelle" et coordonnees X,Y du centroide issus du fichier zonage (PG) et correspondant a la commune du points d'enquete
#�������������������������������������������������������������������������������������������������������������������������������������������������������
list.com.pts<-apply(pts.fich@data[,c("xpl93","ypl93")],MARGIN=1,FUN=function(xy) { FRechComPoste(xy) } )
com.pts<-NULL
com.pts<-do.call("rbind",list.com.pts)
pts.fich@data<-cbind(pts.fich@data,com.pts)
colnames(pts.fich@data)<-tolower(colnames(pts.fich@data)) #par securite on raplique la conversion de l'entete en minuscule

typ.lieu.pzc<-NULL;typ.lieu.pzc<-c(1,2,3)
typ.lieu.pole<-NULL;typ.lieu.pole<-c(4)
typ.lieu.voie<-NULL;typ.lieu.voie<-c(5)
typ.lieu.adr<-NULL;typ.lieu.adr<-c(6)
typ.lieu.port<-NULL;typ.lieu.port<-c(7)
typ.lieu.front<-NULL;typ.lieu.front<-c(8)

##### qq<-paste(typ.lieu.pzc,collapse = ",")

##### #F.x4-FICHIER DU ZONAGE (SHAPE LAMBERT93)
##### #et ajout des coordonnees des centroides de zone
##### 
##### #zon.nom.tab<-unlist(strsplit(zon.nom,split=".",fixed=T))
##### #zon.nom.length<-length(zon.nom.tab)
##### #if ( zon.nom.length>=2 & tolower(zon.nom.tab[zon.nom.length]) %in% c("shp") ) {zon.nom.court<-paste(zon.nom.tab[1:(zon.nom.length-1)],collapse = ".")
##### #} else {zon.nom.court<-zon.nom.tab}
##### 
##### #i: lecture
##### #fichier_ref_geo.list<-gsub('/+','\\\\',fichier_ref_geo)
##### fichier_ref_geo.list<-unlist(strsplit(fichier_ref_geo,split="/",fixed=T))
##### zon.nom<-fichier_ref_geo.list[length(fichier_ref_geo.list)]
##### #chemin.cod<-paste(fichier_ref_geo.list[1:(length(fichier_ref_geo.list)-1)],collapse = "\\")
##### chemin.cod<-paste(fichier_ref_geo.list[1:(length(fichier_ref_geo.list)-1)],collapse = "/")
##### 
##### if (tolower((substr(zon.nom,nchar(zon.nom)-4+1,nchar(zon.nom)))) %in% c(".shp")) {zon.nom.court<-substr(zon.nom,1,nchar(zon.nom)-4)} else {zon.nom.court<-zon.nom}
##### zon.fich.ini<-readOGR(dsn = chemin.cod, stringsAsFactors = F , layer = zon.nom.court,encoding=encodage01,use_iconv = TRUE)
##### 
##### #ii: passage en Lambert93 (rem: la table doit deja etre en L93)
##### zon.fich.ini<-spTransform(zon.fich.ini,CRS("+init=epsg:2154"))
##### 
##### #iii: ajout des coordonnees de centroides
##### zon.fich.ini@data$xcl93<-gCentroid(zon.fich.ini,byid=T)$x
##### zon.fich.ini@data$ycl93<-gCentroid(zon.fich.ini,byid=T)$y
##### 
##### #entete du fichier en minuscule ("compatibilite postgresql")
##### colnames(zon.fich.ini@data)<-tolower(colnames(zon.fich.ini@data))
##### 
##### #transformation des NA en "N" dans les champs "code_zone","code_com","code_pole","code_rue","code_adres". "code_pays non concernee car ne doit pas etre NA
##### ent.code1<-c("code_zone","code_com","code_pole","code_rue","code_adres")
##### for (vv.code in intersect(ent.code1,colnames(zon.fich.ini@data))){
#####   zon.fich.ini@data[is.na(zon.fich.ini@data[,vv.code]),vv.code]<-"N"
##### }
##### 
##### #pointage des zones qui sont niveau pays/zon/commune
##### typ.lieu.pzc<-NULL
##### typ.lieu.pzc<-c(1,2,3)
##### 
##### #F.x5-FICHIER DES  POINTS D ENQUETES (SHAPE LAMBERT93)
##### #et ajout des coordonnees du point si elles ne figurent pas dans le fichier (colonnes "X" et "Y")
##### #et ajout du code commune issue du fichier zonage et contenant le poste d'enquete (CODE_COM)
##### #i: lecture
##### fichier_points_enq.list<-unlist(strsplit(fichier_points_enq,split="/",fixed=T))
##### pts.nom<-fichier_points_enq.list[length(fichier_points_enq.list)]
##### chemin.cod<-paste(fichier_points_enq.list[1:(length(fichier_points_enq.list)-1)],collapse = "/")
##### 
##### if (tolower((substr(pts.nom,nchar(pts.nom)-4+1,nchar(pts.nom)))) %in% c(".shp")) {pts.nom.court<-substr(pts.nom,1,nchar(pts.nom)-4)} else {pts.nom.court<-pts.nom}
##### pts.fich<-readOGR(dsn = chemin.cod, stringsAsFactors = F , layer = pts.nom.court,encoding=encodage01,use_iconv = TRUE)
##### 
##### #ii: passage en Lambert93 (rem: la table doit deja etre en L93)
##### pts.fich<-spTransform(pts.fich,CRS("+init=epsg:2154"))
##### 
##### #iii: ajout des coordonnees de centroides
##### pts.fich@data$xpl93<-gCentroid(pts.fich,byid=T)$x
##### pts.fich@data$ypl93<-gCentroid(pts.fich,byid=T)$y
##### 
##### #iv: ajout du "code_com" issue du fichier zonage et correspondant a la commune du points d'enquete
##### pts.fich.ent0<-colnames(pts.fich@data)
##### pts.fich<-cbind(pts.fich,over(pts.fich,zon.fich.ini[zon.fich.ini@data$code_pays=="FR" & zon.fich.ini$type_lieu %in% c(3),]))
##### pts.fich<-pts.fich[,c(pts.fich.ent0,"code_com","libelle")]
##### 
##### 
##### #entete du fichier en minuscule ("compatibilite postgresql")
##### colnames(pts.fich@data)<-tolower(colnames(pts.fich@data))
##### 
##### ##F.x6-FICHIER DES CODIF PORTS (CSV SEP ";", DECIMAL ".")
##### ##old: por.fich<-read.delim(paste(chemin.cod,"/",por.nom,sep=""),sep=por.sep,header=T,colClasses="character")
##### ##por.fich<-read.delim(gsub('/+','\\\\',fichier_ref_ports),sep=separateur_csv,header=T,colClasses="character")
##### #
##### ##entete du fichier + valeur de "test" ("compatibilite postgresql")
##### ##colnames(por.fich)<-tolower(colnames(por.fich))
##### #
##### ###i: lecture
##### #fichier_ref_ports.list<-unlist(strsplit(fichier_ref_ports,split="/",fixed=T))
##### #por.nom<-fichier_ref_ports.list[length(fichier_ref_ports.list)]
##### #chemin.cod<-paste(fichier_ref_ports.list[1:(length(fichier_ref_ports.list)-1)],collapse = "/")
##### #
##### #
##### #if (tolower((substr(por.nom,nchar(por.nom)-4+1,nchar(por.nom)))) %in% c(".shp")) {por.nom.court<-substr(por.nom,1,nchar(por.nom)-4)} else {por.nom.court<-por.nom}
##### #por.fich.shp<-readOGR(dsn = chemin.cod, stringsAsFactors = F , layer = por.nom.court)
##### #
##### ##ii: passage en Lambert93 (rem: la table doit deja etre en L93)
##### #por.fich.shp<-spTransform(por.fich.shp,CRS("+init=epsg:2154"))
##### #
##### ##entete du fichier en minuscule ("compatibilite postgresql")
##### #colnames(por.fich.shp@data)<-tolower(colnames(por.fich.shp@data))
##### 
##### #recuperation des data seules
##### #por.fich<-por.fich.shp@data
##### por.fich<-zon.fich.ini@data[as.numeric(zon.fich.ini$type_lieu) %in% c(7),]
##### 
##### #F.x7 et plus-FICHIERS VOIE, ADRESSE, POLE
##### #!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##### # A FAIRE!!!!!!!!!!!!!!!!!!!!
##### #!!!!!!!!!!!!!!!!!!!!!!!!!!!!


################################################################################################################################################################
################################################################################################################################################################
#                                                          G. TRAITEMENT:  POUR UN FICHIER D'ENQUETE
################################################################################################################################################################
################################################################################################################################################################
hh02<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
#_______________________________________________________________________________________________________________________________________________________________
#G.1-LECTURE DU FICHIER D'ENQUETE
#_______________________________________________________________________________________________________________________________________________________________
#ATTENTION: ne pas avoir de guillemet " ni le symbole de s�parateur de champ (ex: ;) dans le fichier sinon erreur de lecture
enq.fich<-NULL
enq.sep<-separateur_csv
enq.fich <- read.delim(rep_enq,sep=enq.sep,header=T,colClasses="character",encoding=encodage01)

# conversion en minuscule de l'entete ("compatibilite postgresql")
colnames(enq.fich)<-tolower(colnames(enq.fich))

#_______________________________________________________________________________________________________________________________________________________________
#G.2-CREATION, SI IL N'EXISTE PAS, DU SOUS DOSSIER PROPRE A L'ENQUETE ET QUI CONTIENDRA LES RESULTATS 
#_______________________________________________________________________________________________________________________________________________________________
rep_enq.list<-unlist(strsplit(rep_enq,split="/",fixed=T))
rep_enq.nom<-rep_enq.list[length(rep_enq.list)]
rep_enq.nom2<-unlist(strsplit(rep_enq.nom,split=".",fixed=T))
rep_enq.nom3<-paste(rep_enq.nom2[1:(length(rep_enq.nom2)-1)],collapse = ".")
lf_resu_enq<-NULL
lf_resu_enq<-list.files(rep_sortie)
rep_enq.chem<-paste(rep_sortie,"/",rep_enq.nom3,"/",sep="")
if (!(rep_enq.nom3 %in% lf_resu_enq)){dir.create(rep_enq.chem)}

#_______________________________________________________________________________________________________________________________________________________________
#G.3-AJOUT EN DEBUT DE FICHIER D'UN RID (w_rid) ET DE COLONNES TEMPORAIRES EN PREVISION DES CROISEMENTS DE DONNEES (w_temp1, w_temp2)
#_______________________________________________________________________________________________________________________________________________________________
enq.fich$w_rid<-as.numeric(row(as.data.frame(enq.fich[[1]])))
enq.fich$w_temp1<-""
enq.fich$w_temp2<-""
enq.fich$w_vlpl<-""
ncol(enq.fich)
champs.sup<-c("w_rid","w_temp1","w_temp2","w_vlpl")
ent80<-c(champs.sup,setdiff(colnames(enq.fich),champs.sup))
enq.fich<-enq.fich[,ent80]
ncol(enq.fich)

#_______________________________________________________________________________________________________________________________________________________________
#G.4-CONTROLES COHERENCE DES CHAMPS ENTRE LES 3 FICHIERS: ENQUETE, CODIFICATIONS ET QUESTIONS POSEES PAR POSTE
#_______________________________________________________________________________________________________________________________________________________________
enq.fich.champ<-setdiff(colnames(enq.fich),champs.sup)
cod.fich.champ<-unique(cod.fich[,"champ"])
qxp.fich.champ<-unique(qxp.fich[,"champ"])

#--G.4.1-Verification de l'unicite des noms champs dans le fichier d'enquete en traitement
#�����������������������������������������������������������������������������������������
f00test<-NULL;f00resu<-NULL;f00err<-0
f00test<-unique(enq.fich.champ[duplicated(enq.fich.champ)])
if (!identical(f00test, character(0))){f00resu<-data.frame("CHAMPS_NON_UNIQUE"=f00test);f00err<-1} else {f00resu<-data.frame("CHAMPS_NON_UNIQUE"=c("OK:Pas de doublons de champs dans la base d'enquete"));f00err<-0}

#--G.4.2-Verification de l'existence de tous les champs dans chacun des 3 fichiers (enquete, codif, questions posees)
#��������������������������������������������������������������������������������������������������������������������
list.tot.champ<-unique(c(enq.fich.champ,cod.fich.champ,qxp.fich.champ))
f01test<-data.frame("CHAMPS"=list.tot.champ)
f01test$PRESENT_BASE<-0
f01test$PRESENT_BASE[f01test$CHAMPS %in% enq.fich.champ]<-1
f01test$PRESENT_QUESTION<-0
f01test$PRESENT_QUESTION[f01test$CHAMPS %in% qxp.fich.champ]<-1
f01test$PRESENT_CODIF<-0
f01test$PRESENT_CODIF[f01test$CHAMPS %in% cod.fich.champ]<-1
f01err<-0
f01test<-f01test[f01test$PRESENT_BASE==0 | f01test$PRESENT_QUESTION==0 | f01test$PRESENT_CODIF==0 ,]
if (nrow(f01test)==0){ent.f01test<-colnames(f01test);f01test<-rbind(f01test,c("OK: Tous les champs figurent dans les 3 fichiers","","",""));colnames(f01test)<-ent.f01test;f01err<-0 } else {f01err<-1}

#--G.4.2-Controle que l'ensemble des code_poste+num_point presents dans l'enquete figure
#        dans le fichier des points d'enquete et dans le fichier des questions posees par poste
#������������������������������������������������������������������������������������������������
if ("code_poste" %in% intersect(colnames(enq.fich),intersect(colnames(pts.fich@data),colnames(qxp.fich))) &
    "num_point" %in% intersect(colnames(enq.fich),intersect(colnames(pts.fich@data),colnames(qxp.fich)))) {
#           #                  liste clef code_poste+num_point pour chaque fichier
    list.post.enq<-unique(paste(enq.fich$code_poste,enq.fich$num_point,sep="#"))
    list.post.pts<-unique(paste(pts.fich@data$code_poste,pts.fich@data$num_point,sep="#"))
    list.post.qxp<-unique(paste(qxp.fich$code_poste,qxp.fich$num_point,sep="#"))
  
    f03test<-data.frame("LISTE_POSTE"=list.post.enq)
    f03test$PRESENT_BASE<-0
    f03test$PRESENT_BASE[f03test$LISTE_POSTE %in% list.post.enq]<-1
    f03test$PRESENT_QUESTION<-0
    f03test$PRESENT_QUESTION[f03test$LISTE_POSTE %in% list.post.pts]<-1
    f03test$PRESENT_CODIF<-0
    f03test$PRESENT_CODIF[f03test$LISTE_POSTE %in% list.post.qxp]<-1
  
    f03test<-f03test[f03test$PRESENT_BASE==0 | f03test$PRESENT_QUESTION==0 | f03test$PRESENT_CODIF==0 ,]
    f03err<-0
    if (nrow(f03test)==0){
         ent.f03test<-colnames(f03test);
         f03test<-rbind(f03test,c("OK: Tous les CODE_POSTE+NUM_POINT de l'enquete sont dans les fichiers points et questionnaires","","",""));
         f03test<-rbind(f03test,c("##########")); #,c("##########"),c("##########"));
         colnames(f03test)<-ent.f03test ;f03err<-0
    } else {f03err<-1}
} else { f03test<-NULL; f03test<-data.frame("LISTE_POSTE"="ERREUR: Verifier existance des de CODE_POSTE+NUM_POINT dans les 3 fichiers d'enquete, des points et questionnaire");f03err<-2}

#--G.4.3-Presence des champs "obligatoires" pour le controle OD manuel
#���������������������������������������������������������������������
#enq.fich.champ 
od.champ<-tolower( c("pole_dest_ini", "pole_dest_prec", "pole_dest", "voie_dest_ini", "voie_dest_prec", "voie_dest", "num_voie_dest_prec", "adresse_dest", "port_embarq_dest_ini", "port_embarq_dest", "pole_orig_ini", "pole_orig_prec", "pole_orig", "voie_orig_ini", "voie_orig_prec", "voie_orig", "num_voie_orig_prec", "adresse_orig", "port_debarq_orig_ini", "port_debarq_orig", "motif_orig_ini", "motif_orig_prec", "motif_orig", "motif_dest_ini", "motif_dest_prec", "motif_dest"))  
f04test<-NULL;f04test<-setdiff(od.champ,enq.fich.champ)
f04err<-NULL
if (!identical(f04test, character(0))){f04resu<-data.frame("CHAMPS_OBLIGATOIRES_POUR_CONTROLE_MANUEL_OD_ABSENTS"=f04test);f04err<-1

} else {f04resu<-data.frame("CHAMPS_OBLIGATOIRES_POUR_CONTROLE_MANUEL_OD_ABSENTS"="OK: Tous les champs necessaires sont presents");f04err<-0}
f04resu<-rbind(f04resu,c("##########"))

#--G.4.4-Presence des champs "obligatoires" pour le controle marchandises
#rem: pour info car leur absence n'est pas bloquante car ce ne sont pas des questions du tronc commun
#���������������������������������������������������������������������
mr.champ<-tolower(c("marchandises_prec", "marchandises"))
f05test<-NULL;f05test<-setdiff(mr.champ,enq.fich.champ)
f05err<-NULL  
if (!identical(f05test, character(0))){f05resu<-data.frame("POUR_INFO_CHAMPS_MINIMUM_NECESSAIRES_POUR_CONTROLE_MANUEL_MARCHANDISES_ABSENTS"=f05test);f05err<-1

} else {f05resu<-data.frame("POUR_INFO_CHAMPS_MINIMUM_NECESSAIRES_POUR_CONTROLE_MANUEL_MARCHANDISES_ABSENTS"="OK: Tous les champs necessaires pour un controle sont presents");f05err<-0}
f05resu<-rbind(f05resu,c("##########"),c("##########"),c("##########"))

#--G.4.4-Concatenation des resultats des differents tests
#��������������������������������������������������������
#Rem: avant la concatenation, il faut uniformiser le nb de colonne ainsi que l'entete de chaque table de resultat
ent00<-colnames(f00resu)
ent01<-colnames(f01test)
ent02<-colnames(f03test)
ent03<-colnames(f04resu)
ent04<-colnames(f05resu)  

f00resu<-rbind(ent00,f00resu)
f01test<-rbind(ent01,f01test)
f03test<-rbind(ent02,f03test)
f04resu<-rbind(ent03,f04resu)
f05resu<-rbind(ent04,f05resu)

max.long<-max(length(ent01),length(ent00),length(ent02),length(ent03),length(ent04))
if( length(ent00)<max.long){ftemp00<-matrix(data="",nrow=nrow(f00resu), ncol=(max.long-length(ent00)));f00resu<-cbind(f00resu,ftemp00)}
if( length(ent01)<max.long){ftemp00<-matrix(data="",nrow=nrow(f01test), ncol=(max.long-length(ent01)));f01test<-cbind(f01test,ftemp00)}
if( length(ent02)<max.long){ftemp00<-matrix(data="",nrow=nrow(f03test), ncol=(max.long-length(ent02)));f03test<-cbind(f03test,ftemp00)}
if( length(ent03)<max.long){ftemp00<-matrix(data="",nrow=nrow(f04resu), ncol=(max.long-length(ent03)));f04resu<-cbind(f04resu,ftemp00)}
if( length(ent04)<max.long){ftemp00<-matrix(data="",nrow=nrow(f05resu), ncol=(max.long-length(ent04)));f05resu<-cbind(f05resu,ftemp00)}

ent03<-paste("V",c(1:max.long),sep="")
colnames(f00resu)<-ent03
colnames(f01test)<-ent03
colnames(f03test)<-ent03
colnames(f04resu)<-ent03
colnames(f05resu)<-ent03

f00resu<-rbind(c("##########"),c("##########"),c("##########"),c("##########"),f00resu,c("##########"),f01test,c("##########"),f03test,c("##########"),f04resu,c("##########"),f05resu)

nom.dos01<-nom.dos03 #nom.dos01<-"01_NOMS_CHAMPS_CONTROLE"
lf_resu_dos<-list.files(paste(rep_enq.chem,sep=""))
if (!(nom.dos01 %in% lf_resu_dos) & nom.dos01!=""){dir.create(paste(rep_enq.chem,nom.dos01,"/",sep=""))} 
f00resu[2,1]<-"fichier : ";f00resu[2,2]<-rep_enq.nom #enq.nom
enq.nom<-rep_enq.nom
f00resu[3,1]<-hh02
#write.table(f00resu,paste(cheminR,dos.resu,"\\",nom.dos01,"\\",nom.dos01,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
#write.table(f00resu,paste(cheminR,dos.resu,"\\",nom.dos01,"\\",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")

#_______________________________________________________________________________________________________________________________________________________________
#G.5-RESTE DU TRAITEMENT
#_______________________________________________________________________________________________________________________________________________________________
#Rem: si on a au moins une "erreur" dans les tests precedents (or presence ou non champ marchandises), on arrete les controles
#     et on renvoie un fichier de synthses indiquant les elements a corriger avnt de relancer les controles
#<<<<<<<<<<<<<<<<<<<<<<
#<<<<<<<<<<<<<<<<<<<<<<
if (f00err>0 | f01err>0 | f03err>0 | f04err>0 ) { #| f05err>0
#   #cas au moins une "erreur" dans les tests precedents (or presence ou non champ marchandises), on arrete les controles + export synthese
    info.arret<-matrix(data="",nrow=5,ncol=ncol(f00resu))
    colnames(info.arret)<-colnames(f00resu)
    info.arret[1,1]<-makeNstr("#",pas_str_pad*4)
    info.arret[2,2]<-"ARRET DU CONTROLE"
    info.arret[3,2]<-"RESOUDRE LES PROBLEMES RELEVES (HORS INFO MARCHANDISES)"
    info.arret[4,2]<-"AVANT NOUVEL EXCUTION DE LA PROCEDURE"
    info.arret[5,1]<-makeNstr("#",pas_str_pad*4)
    #f00resu<-rbind(f00resu,info.arret)
    #write.table(rbind(f00resu,info.arret),paste(cheminR,dos.resu,"\\",nom.dos01,"\\",nom.dos01,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
    f00resu<-rbind(f00resu,info.arret)
    hh60<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
    f00resu[3,2]<-hh60
    #
    write.table(f00resu,paste(rep_enq.chem,nom.dos01,"/",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="",quote=F,fileEncoding=encodage01)
#  
} else {
#<<<<<<<<<<<<<<<<<<<<<<
#<<<<<<<<<<<<<<<<<<<<<<
#   #cas au moins une "erreur" dans les tests precedents (or presence ou non champ marchandises), on arrete les controles + export synthese
#
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#--G.5.1- controle des codifications par champ present dans la base d'enquete
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#
#   #_____preparation du fichier de sortie
   fich.vv.err<-data.frame("V0"=c("fichier",enq.nom),"V1"=c("",""),"V2"=c("",""),"V3"=c("",""),"V4"=c("",""),"V5"=c("",""),"V6"=c("",""),"V7"=c("",""),"V8"=c("",""),"V9"=c("",""),stringsAsFactors = F)
   fich.vv.err<-rbind(fich.vv.err,c("","question","nb_valeurs_suspectes","type_controle","liste_valeurs_suspectes","nb_question_non_posee<>N","nb_question_posee=N","NR_(Poste_ou_VLPL_inconnu)","nb_valeur_X","nb_valeur_I"))
  
#  #_____information de w_vlpl sur la base des codifs contenues dans "type_veh"
#  #     sur le principe [11,14]==> VL, [15,18]==>PL et XX dans les autres cas ("X")
#  #     cela servira a certain test qui ont besoin de connaitre la distinction VL/PL     
    enq.fich$w_vlpl<-"XX"
    enq.fich$w_vlpl[enq.fich$type_veh %in% c(11:14) | enq.fich$type_veh %in% c("11","12","13","14")]<-"VL"
    enq.fich$w_vlpl[enq.fich$type_veh %in% c(15:18) | enq.fich$type_veh %in% c("15","16","17","18")]<-"PL"
  
#   #_____ajout des colonnes "controle","nb_cont","ch_cont","nb_cont_min"
    enq.fich$controle<-""
    enq.fich$nb_cont<-0 #nb de suspicion d'erreur de codif dans cette pahse de controle
    enq.fich$ch_cont<-""
    enq.fich$pt_err<-0 #vaudra au final 1 si on moins une suspicion d'erreur dans la ligne d'enquete
    enq.fich$nb_cont_min<-0 #certains champs doivent a minima etre correctement remplis pour valdier la base. On pointe ici le nb d'erreur de codif sur ces champs
  
    champs.cont1<-tolower(c("controle","nb_cont","ch_cont","pt_err","nb_cont_min"))
  
    enq.fich.ent<-colnames(enq.fich)
     #vv<-"TYPE_CARROSSERIE";vv<-tolower(vv)
     #vv<-"NB_ESSIEUX";vv<-tolower(vv)
     #vv<-"ID_ITW";vv<-tolower(vv)
     #vv<-"NUM_ENQ";vv<-tolower(vv)
     #vv<-"TYPE_CARROSSERIE_PREC";vv<-tolower(vv)
     #vv<-"TYPE_VEH";vv<-tolower(vv)
     #vv<-"ID_ITW";vv<-tolower(vv)
     #vv<-"PORT_EMBARQ_DEST";vv<-tolower(vv)
     #vv<-"PAYS_DEST_INI";vv<-tolower(vv)
     #vv<-"PAYS_DEST";vv<-tolower(vv)
     #vv<-"COMMUNE_DEST";vv<-tolower(vv)
     #vv<-"NB_ESSIEUX";vv<-tolower(vv)
     # vv<-"MOTIF_DEST_VL";vv<-tolower(vv)
     # vv<-"heure";vv<-tolower(vv)
     # vv<-tolower(vv)
     # vv<-"pole_dest"
     #vv<-"pole_orig_ini"
     #vv<-"voie_orig_ini"
     #+++++++++++++++   

#   #_____parcours de chaque champ pour controle codif  
    WW<-setdiff(enq.fich.ent,c(champs.sup,champs.cont1)) 
    
    for ( vv in WW ){
#        #_____recherche de la variable dans le fichier de codification (cod.fich)
         if ( ! (vv %in% cod.fich$champ) | ! (vv %in% qxp.fich$champ) ){
#             #si le champ de la abse d'enquete ne figure pas dans le ficier de codif ou le fichier des questions, on renvoie une alerte
#             #4444444444444444444444444444444444444444444444444444444444444444444444                   
              fich.vv.err<-rbind(fich.vv.err,c("",vv,"erreur","Champ non trouve","x","x","x","x","x","x"))
#             #4444444444444444444444444444444444444444444444444444444444444444444444                   
      
         } else {
              vv.cod<-NULL;vv.cont<-NULL
              vv.cod<-cod.fich[cod.fich$champ==vv,]
              vv.cont<-vv.cod[1,"controle"]
#        #_____recherche question du champ vv posee pour l enquete (on regarde le poste et le type de vehicule)  
#        #     #on filtre le fichier des questions sur le champ et le numero de poste, pour verifier ensuite si la question est posee ou non sur ce type de vehicule
              vv.qxp<-NULL
              vv.qxp<-qxp.fich[qxp.fich$champ==vv,]
              vv.qxpr<-NULL
              vv.qxpr<-pivot_longer(data=vv.qxp,cols=c("quest_vl","quest_pl"),names_to="type_veh",values_to = "posee")
              vv.qxpr$type_veh<-gsub("quest_","",vv.qxpr$type_veh,fixed = T)
              vv.qxpr$clef<-toupper(paste(vv.qxpr$code_poste,vv.qxpr$num_point,vv.qxpr$type_veh,sep="#"))
              
#        #_____definition dans le fichier d'enquete d ela clef de recherche code_poste+num_point+type de vehicule (VL/PL)          
              enq.fich$w_temp1<-toupper(paste(enq.fich$code_poste,enq.fich$num_point,enq.fich$w_vlpl,sep="#"))
              enq.fich$w_temp2<- 0
              
#        #_____traitement prealable particulier, on formate le champ per_enq sur 2car (ex: 1==> 01)
              if("per_enq" %in% colnames(enq.fich)){
                                                    enq.fich$per_enq[!(is.na(as.numeric(enq.fich$per_enq))) ]<-
                       sprintf("%02.0f",as.numeric( enq.fich$per_enq[!(is.na(as.numeric(enq.fich$per_enq))) ] )) 
              }
#        #_____ajout de l'information question posee ou non par enquete de la base dans w_temp2      
              enq.fich$w_temp2<-apply(enq.fich[,c("w_temp1","w_temp2")],MARGIN=1,FUN=FQuestPosee)
              #enq.fich$w_temp1<-apply(enq.fich[,c("code_poste","num_point","w_vlpl",vv)], MARGIN=1,FUN=FQuestPosee)
      
#        #_____fichier temporaire pour informer/iterer les champs "controle", "nb_cont","ch_cont"
              temp.data<-data.frame(rid=enq.fich$w_rid)
              temp.data$tempv<-"-"
              temp.data$nb<-0  
              temp.data$nb.min<-0 
      
      #         #              c("UNICITE","X","INTERVALLE","ZONAGE","CODIF","PORT")
      #         #             #�����������������������������������������������������������������������������������������������������������������������������������������   
#        #_____controle codif en focntion du type
#        # UNICITE ==> on verifie que la valeur du champ est unique par poste et sur l'ensemble de la base
#        # INTERVALLE ==> on verifie ques les valeurs du champ sont dans l'intervalle indique dans le fichier de codification ex [1,10] ==> valeur de vv 1<= vv <= 10
#        # ZONAGE   ==> on verifie que la codif est dans les codifs de la table de zonage, concerne les champs pays, zone, commune avec type_lieu=1,2,3          
#        # PORT   ==> on verifie que la codif est dans les codifs de la table de zonage, concerne les champs port embarquement et debarquement avec type_lieu=7          
#        # POLE   ==> on verifie que la codif est dans les codifs de la table de zonage, concerne les champs port embarquement et debarquement avec type_lieu=4          
#        # CODIF   ==> on verifie que la codif est dans les codifs de la table de codif          
#        # X   ==> champ libre sans codif          
#        # en cours : VOIE, ADRESSE, ES_France (points entr�e/sortie France)   
              
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="UNICITE" & vv=="id_itw"){
              vv.err<-NULL
              vv.err<-enq.fich[,c("w_rid","id_itw","code_poste","num_point","w_vlpl")]
              vv.err$cpt<-1
              vv.err$clef_poste<-paste(vv.err$code_poste,vv.err$num_point,sep="#")
              sep.90<-"*"
              vv.err$clef_posteid<-paste(vv.err$clef_poste,vv.err$id_itw,sep=sep.90)
              vv.err.intitule<-tolower(vv.cont)        

#       #_____unicite id_itw dans la base
              id_itw.uni.base<-NULL;id_itw.uni.base.info<-NULL
              id_itw.uni.base<-unique(vv.err$id_itw)
              if (length(id_itw.uni.base)!=length(enq.fich$id_itw)) {id_itw.uni.base.info<-"ID_ITW non uniques sur l'ensemble de la base"} else {id_itw.uni.base.info<-"ID_ITW uniques sur la base"}
        
              id_itw.uni.poste<-NULL
              id_itw.uni.poste<-unique(vv.err$clef_posteid);length(id_itw.uni.poste) ;length(enq.fich$id_itw) 
              if (length(id_itw.uni.poste)!=length(enq.fich$id_itw)) {
              id_itw.uni.poste.info<-"ID_ITW non uniques par poste"
              id_itw.doublon<-NULL
              fich.agg<-NULL
              fich.agg<-aggregate(vv.err$cpt,list("val"=vv.err[,"clef_posteid"]),FUN=sum) ;nrow(fich.agg)                                                                         
              fich.agg<-fich.agg[order(fich.agg$val),]
              fich.agg<-fich.agg[fich.agg$x!=1,]
              fich.agg$code_poste.num_point<-apply(X=data.frame(fich.agg$val),MARGIN=1,FUN=function(xx){unlist(strsplit(xx,split=sep.90,fixed=T))[[1]]})
              fich.agg$id_itw<-apply(X=data.frame(fich.agg$val),MARGIN=1,FUN=function(xx){unlist(strsplit(xx,split=sep.90,fixed=T))[[2]]})
              fich.agg<-fich.agg[order(fich.agg$code_poste.num_point,fich.agg$id_itw),]
              fich.agg$cpt<-1
              fich.agg$cum<-ave(fich.agg$cpt,by=fich.agg$code_poste.num_point,FUN=cumsum)
              fich.agg$max<-ave(fich.agg$cpt,by=fich.agg$code_poste.num_point,FUN=sum)
          
              fich.agg$code_poste.num_point2<-fich.agg$code_poste.num_point
              fich.agg$code_poste.num_point2[fich.agg$cum>1]<-""
              fich.agg$denomb<-paste("\'",fich.agg$id_itw,"\'","(",fich.agg$x,")",sep="")   
              fich.agg$pref<-"";fich.agg$pref[fich.agg$cum==1]<-"{"
              fich.agg$inter<-"";fich.agg$inter[ fich.agg$cum!=fich.agg$max]<-" - "
              fich.agg$suff<-"";fich.agg$suff[fich.agg$max==fich.agg$cum]<-"} - "
              fich.agg[nrow(fich.agg),"suff"]<-"}"
              fich.agg$denomb2<-paste(fich.agg$code_poste.num_point2,fich.agg$pref,fich.agg$denomb,fich.agg$inter,fich.agg$suff,sep="")
              vdenomb<-paste(fich.agg$denomb2,collapse="") 
              nb.val<-NULL;nb.val<-nrow(fich.agg)
          
              vv.err.rid<-NULL; vv.err.rid<-vv.err$w_rid[vv.err$clef_posteid %in% fich.agg$val]

         } else {vdenomb<-""; id_itw.uni.poste.info<-"ID_ITW uniques par poste";nb.val<-0;vv.err.rid<-c("pasderid")}   
        
              fich.vv.err.itw<-c("",vv,nb.val,vv.cont,vdenomb,id_itw.uni.poste.info,paste("rem: ",id_itw.uni.base.info,sep=""),"NC","NC","NC")
              fich.vv.err<-rbind(fich.vv.err,fich.vv.err.itw)
        
#       #_____information des controles                                                                                                                     
              temp.data$tempv[temp.data$rid %in% vv.err.rid]<-paste(vv,"_(",vv.err.intitule,")",sep="")                                                    
              temp.data$nb[!(temp.data$tempv %in% c("-"))]<-1 
              temp.data$nb.min[!(temp.data$tempv %in% c("-")) & vv %in% indic.min]<-1 #20210429                                                                                               
        
              enq.fich[,"controle"]<-paste(enq.fich[,"controle"],temp.data$tempv,sep="#")                                                                    
              enq.fich[,"nb_cont"]<-enq.fich[,"nb_cont"]+temp.data$nb    
              enq.fich[,"nb_cont_min"]<-enq.fich[,"nb_cont_min"]+temp.data$nb.min   #20210429       
        
#       #               #1111111111111111111111111111111111111111111111111111111111111111111111                                                                             
#       #_______________fichier listant les valeurs de la variable vv crois� avec w_vlpl et code_poste+num_point                                         
              vv.list00<-NULL                                                                                                                                
              vv.list00<-cbind(data.frame("code_poste.num_point"=paste(enq.fich$code_poste,enq.fich$num_point,sep="#")),enq.fich[,c("w_vlpl",vv)],"cpt"=1)   
              colnames(vv.list00)[colnames(vv.list00)==vv]<-"VV" 
              vv.list00.tabs<-NULL
              vv.list00.tabs<-as.data.frame(xtabs(~VV+w_vlpl+code_poste.num_point,data=vv.list00))                                                           
        
              vv.list00.tot<-NULL
              vv.list00.tot<-as.data.frame(xtabs(~VV+w_vlpl,data=vv.list00))                                                                                 
              vv.list00.tot$code_poste.num_point<-"Ensemble_poste"                                                                                           
              vv.list00.tot<-vv.list00.tot[,colnames(vv.list00.tabs)]                                                                                        
        
              vv.list00.tabs<-rbind(vv.list00.tot,vv.list00.tabs)                                                                                            
        
              vv.list00.piv<-NULL                                                                                                                                               
              vv.list00.piv<-pivot_wider(data=vv.list00.tabs,names_from=w_vlpl,values_from = Freq,values_fill = 0)                                           
        
              vv.list00.piv2<-NULL
              vv.list00.piv2<-data.frame("VV"=unique(vv.list00.piv[,"VV"]))                                                                                  
              vv.list00.piv2<-rbind(data.frame("VV"=c("",vv)),vv.list00.piv2)                                                                                
        
              vv.list00.ent<-intersect(c("VL","PL","XX"),colnames(vv.list00.piv))                                                                            
              #poste0<-"Poste 10#1"                                                                                                                           
              #poste0<-"Poste 12#1"                                                                                                                           
        
              for (poste0 in unique(vv.list00.piv$code_poste.num_point)) {                                                                                   
                   temp.piv<-NULL                                                                                                                               
                   temp.piv<-vv.list00.piv[vv.list00.piv$code_poste.num_point==poste0,vv.list00.ent]                                                            
                   temp.piv<-rbind(colnames(temp.piv),temp.piv)                                                                                                 
                   temp.piv<-rbind(c(poste0),temp.piv)                                                                                                          
                   vv.list00.piv2<-cbind(vv.list00.piv2,temp.piv)                                                                                               
              }                                                                                                                                              
#       #_______________#ajout info question posee/non posee                                                                                                           
              qpn.vect<-c("QUESTION POSEE/NON POSEE==>")                                                                                                     
              qpn.fichq<-NULL                                                                                                                                
              qpn.fich<-data.frame(data.frame("code_poste.num_point"=paste(enq.fich$code_poste,enq.fich$num_point,sep="#")),enq.fich[,c("w_vlpl","w_temp2")])
              qpn.fich$clef<-paste(qpn.fich$code_poste.num_point,qpn.fich$w_vlpl,qpn.fich$w_temp2,sep="-")                                                   
              qpn.fich<-qpn.fich[!duplicated(qpn.fich$clef),]                                                                                                
              qpn.fich$qpn<-"NR"                                                                                                                             
              qpn.fich$qpn[qpn.fich$w_temp2 %in% c(1)]<-"posee"                                                                                              
              qpn.fich$qpn[qpn.fich$w_temp2 %in% c(2)]<-"non posee"                                                                                          
              for (j in 2:ncol(vv.list00.piv2)) {                                                                                                            
                   if (nrow(qpn.fich[qpn.fich$code_poste.num_point %in% (vv.list00.piv2[1,j]) & qpn.fich$w_vlpl %in% (vv.list00.piv2[2,j]),])==1) {          
                        resu.temp<-qpn.fich$qpn[qpn.fich$code_poste.num_point %in% (vv.list00.piv2[1,j]) & qpn.fich$w_vlpl %in% (vv.list00.piv2[2,j])]       
                   } else {                                                                                                                                  
                        resu.temp<-"NR"                                                                                                                      
                   }                                                                                                                                         
              qpn.vect<-c(qpn.vect,resu.temp)                                                                                                           
              }                                                                                                                                              
              vv.list00.piv2<-rbind(vv.list00.piv2,qpn.vect)                                                                                                 
              vv.list00.piv2<-vv.list00.piv2[c(1,2,nrow(vv.list00.piv2),3:(nrow(vv.list00.piv2)-1)),]                                                        
              nom.dos02<-"02_LISTE_VALEURS_PAR_CHAMP"   
 
              lf_resu_dos<-list.files(rep_enq.chem)
              if (!(nom.dos02 %in% lf_resu_dos)){dir.create(paste(rep_enq.chem,nom.dos02,"/",sep=""))}
              write.table(vv.list00.piv2,paste(rep_enq.chem,nom.dos02,"/",vv,".csv",sep=""),row.names=FALSE,col.names=F,sep=";",na="",quote=F,fileEncoding=encodage01)             
        #lf_resu_dos<-list.files(paste(cheminR,dos.resu,"\\",sep=""))
        #if (!(nom.dos02 %in% lf_resu_dos)){dir.create(paste(cheminR,dos.resu,"\\",nom.dos02,"\\",sep=""))}                                             
        #write.table(vv.list00.piv2,paste(cheminR,dos.resu,"\\",nom.dos02,"\\",vv,".csv",sep=""),row.names=FALSE,col.names=F,sep=";",na="")             
#       #               #3333333333333333333333333333333333333333333333333333333333333333333333                                                                             
              
        
         } #if(vv.cont=="UNICITE" & vv=="ID_ITW") #concerne ID_ITW faire unicite globale sur la base et par poste d'enquete
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="X"){
              #pas de codif
              vv.err<-NULL
              vv.err<-enq.fich[(enq.fich[,vv] %in% blanc),]
              #vv.err.intitule<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.cont<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
        
        
        } #on ne fait rien, champ libre, juste verifier N si question non pos�e et pas de vide        
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="INTERVALLE" & vv=="heure"){
              minmax<-NULL
              minmax<-parse_date_time(unlist(strsplit(str_replace_all(vv.cod[1,"modalite"],pattern="[\\[\\]]",replacement=""),split="-",fixed=T)), '%H:%M:%S')
        
              heure.fich<-NULL
              heure.fich<-data.frame(enq.fich[,c("w_rid","w_temp1","w_temp2","w_vlpl",vv)])
        
#         #_____ controle du format HH:MM:SS ==> 8 car et pas d'espace " "+ ":" en car 3 et 6  et pas de "MA" "PM"
              heure.fich$w_format<-""
              heure.fich$w_format<-apply(data.frame(heure.fich[,c(vv)]),MARGIN=1,FUN=FHHMMSS)
        #         #             #ii: controle intervalle
              heure.fich$w_intervalle<-0
              heure.fich$w_intervalle[!( parse_date_time(heure.fich[,vv],'%H:%M:%S') >= minmax[1] &
                                         parse_date_time(heure.fich[,vv],'%H:%M:%S') <= minmax[2] )]<-1
#         #_____ pointage de l'ensemble des erreurs
              heure.fich$w_erreur<-0
              heure.fich$w_erreur[heure.fich$w_intervalle %in% c(1)]<-1
              heure.fich$w_erreur[heure.fich$w_format %in% c("ErreurFormat")]<-2
        
              vv.err<-NULL
              vv.err<-enq.fich[enq.fich$w_rid %in% heure.fich$w_rid[heure.fich$w_erreur>0],]
              heure.fich<-NULL
              vv.cont<-paste(vv.cont,"",vv.cod[1,"modalite"],sep=" ")
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
        
         } #if(vv.cont=="INTERVALLE" & vv=="heure")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="INTERVALLE" & tolower(vv)!="heure"){ # NB_ESSIEUX; NB_OCC; NB_CONd : entier (avec trunc())
#         #             #Extraire les min et max de l'intervalle
#         #             #nn<-c(-5:5 +0.1);trunc(nn);floor(nn)
              minmax<-NULL
              minmax<-as.numeric(unlist(strsplit(str_replace_all(vv.cod[1,"modalite"],pattern="[\\[\\]]",replacement=""),split="-",fixed=T)))
              vv.err<-NULL
              vv.err<-enq.fich
              vv.err$w_temp3<-0 #pb: valeur non numerique possible et si num�rique, valeur decimale possible
              vv.err$w_temp3[is.na(as.numeric(enq.fich[,vv]))]<-1
              vv.err$w_temp3[!(
              as.numeric(enq.fich[,vv]) >= as.numeric(minmax[1]) &
              as.numeric(enq.fich[,vv]) <= as.numeric(minmax[2])) ]<-2
              vv.err<-vv.err[vv.err$w_temp3>0,]
              vv.cont<-paste(vv.cont,"",vv.cod[1,"modalite"],sep=" ")
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
#         #             #en erreur
#         #             # si question posee: pas dans l'intervalle ou non num�rique si question pos�e
#         #             # si question non posee: valeurs numerique ou non numerique !=N (pas de X,I)                   
        
           } #if(vv.cont=="INTERVALLE" & vv!="heure")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="ZONAGE"){
              vv.pzc<-NULL;vv.champ<-NULL;typ.lieu.cont<-"NULL"
              vv.pzc<-unlist(strsplit(vv,split="_",fixed=T))
              mod.comp<-NULL   
              #champ commence par pays ==> X,N; si orig/dest==> X seulement
              if (vv.pzc[1] %in% c("pays") &  (vv.pzc[2] %in% c("dest","orig")) ){mod.comp<-c("X")}
              if (vv.pzc[1] %in% c("pays") & !(vv.pzc[2] %in% c("dest","orig")) ){mod.comp<-c("X","N")}
              if (vv.pzc[1] %in% c("zone")                                      ){mod.comp<-c("X","N")}
              if (vv.pzc[1] %in% c("commune") & !(substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini"))){mod.comp<-c("X","N")}
              if (vv.pzc[1] %in% c("commune") &  (substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini"))){mod.comp<-c("X",",I","N")}

              if (vv.pzc[1] == "pays")   {vv.champ<-"code_pays"; typ.lieu.cont<-c(typ.lieu.pzc[1:1])}
              if (vv.pzc[1] == "zone")   {vv.champ<-"code_zone"; typ.lieu.cont<-c(typ.lieu.pzc[2:2])}
              if (vv.pzc[1] == "commune"){vv.champ<-"code_com";  typ.lieu.cont<-c(typ.lieu.pzc[3:3])}
              
              if (vv == "immat_pays"){vv.champ<-"code_pays"; mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.pzc[1:3]) }
              if (vv == "immat_dep" ){vv.champ<-"code_zone"; mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.pzc[2:3]) }
              
              if (vv.pzc[1] %in% c("pole")) {vv.champ<-"code_pole";mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.pole)}

              if (vv.pzc[1] %in% c("voie")  & !(substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini"))) {vv.champ<-"code_voie"; mod.comp<-c("X","N")    ; typ.lieu.cont<-c(typ.lieu.voie)}
              if (vv.pzc[1] %in% c("voie")  &  (substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini"))) {vv.champ<-"code_voie"; mod.comp<-c("X","I","N"); typ.lieu.cont<-c(typ.lieu.voie)}

              if (vv.pzc[1] %in% c("adresse")) {vv.champ<-"code_adr";mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.adr)}
              
              if (vv.pzc[1] %in% c("port"))    {vv.champ<-"code_port";mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.port)}
              
              if (vv %in% c("entree_france","sortie_france")) {vv.champ<-"code_front";mod.comp<-c("X","N"); typ.lieu.cont<-c(typ.lieu.front)}


              
              vv.err<-NULL

              pzc.serie<-0
              pzc.serie<-paste(typ.lieu.cont,collapse = ",")
              vv.serie<-NULL
              vv.serie<-paste(paste("'",unique(enq.fich[,vv]),"'",sep=""),collapse = ",")
              #vv.serie<-paste("'",unique(enq.fich[,vv]),"'",sep="")
              
              MyQuery<-NULL
              MyQuery<-paste("SELECT DISTINCT ",vv.champ," FROM ",ref_schema,".",ref_table," WHERE (type_lieu IN (",pzc.serie,") AND ",vv.champ," IN (",vv.serie,"))",sep="")

              vv;vv.champ;mod.comp;typ.lieu.cont;MyQuery
                            
              list.zon<-NULL
              list.zon<-dbGetQuery(con,MyQuery)
              list.finale<-NULL
              if (nrow(list.zon)<=0){list.finale<-c(mod.comp)}else{list.finale<-c(list.zon[[1]],mod.comp)}
              vv.err<-enq.fich[!(enq.fich[,vv] %in% list.finale),]
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333                 
####         #         #!!!!!!!!!!!!!!!! Koungou (97605) ==> FR97605000? (Mayotte) !!!!!!!!!!!!!!
        
         } # if(vv.cont=="ZONAGE") 
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="CODIF"){
#         #             #table de codif presente dans ce fichier
              vv.err<-NULL
              vv.err<-enq.fich[!(enq.fich[,vv] %in% vv.cod$modalite),]
              vv.err.intitule<-tolower(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333              
         } #if(vv.cont=="CODIF")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="PORT"){
              mod.comp<-NULL            
              if (substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini") ) { mod.comp<-c("X","N")} else { mod.comp<-c("X","N")}
              vv.err<-NULL
              port.serie<-NULL
              port.serie<-paste(typ.lieu.port,collapse = ",")
              vv.serie<-NULL
              vv.serie<-paste(paste("'",unique(enq.fich[,vv]),"'",sep=""),collapse = ",")
              vv.champ<-"code_port"
              MyQuery<-NULL
              MyQuery<-paste("SELECT DISTINCT ",vv.champ," FROM ",ref_schema,".",ref_table," WHERE (type_lieu IN (",port.serie,") AND ",vv.champ," IN (",vv.serie,"))",sep="")
              
              por.fich<-NULL
              por.fich<-dbGetQuery(con,MyQuery)
              list.finale<-NULL
              if (nrow(por.fich)<=0){list.finale<-c(mod.comp)} else {list.finale<-unique(c(por.fich[[1]],mod.comp))}              
              vv.err<-enq.fich[!(enq.fich[,vv] %in% list.finale),]
              vv.err.intitule<-(vv.cont)           
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333              
              } #if(vv.cont=="PORT")         
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="POLE"){
              vv.pzc<-NULL;vv.champ<-NULL
              vv.pzc<-unlist(strsplit(vv,split="_",fixed=T))
              mod.comp<-NULL   
              if (vv.pzc[1] %in% c("pole") & !(substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini")) ){mod.comp<-c("X","N")}
              if (vv.pzc[1] %in% c("pole") &  (substr(vv,nchar(vv)-2,nchar(vv)) %in% c("ini")) ){mod.comp<-c("X","N","I")}
              if (vv.pzc[1] %in% c("pole")){vv.champ<-"code_pole"}
              vv.err<-NULL
              
              pole.serie<-0
              pole.serie<-paste(typ.lieu.pole,collapse = ",")
              vv.serie<-NULL
              vv.serie<-paste(paste("'",unique(enq.fich[,vv]),"'",sep=""),collapse = ",")
              #vv.serie<-paste("'",unique(enq.fich[,vv]),"'",sep="")
              
              MyQuery<-NULL
              MyQuery<-paste("SELECT DISTINCT ",vv.champ," FROM ",ref_schema,".",ref_table," WHERE (type_lieu IN (",pole.serie,") AND ",vv.champ," IN (",vv.serie,"))",sep="")
              
              list.zon<-NULL
              list.zon<-dbGetQuery(con,MyQuery)
              list.finale<-NULL
              if (nrow(list.zon)<=0){list.finale<-c(mod.comp)}else{list.finale<-c(list.zon[[1]],mod.comp)}
              vv.err<-enq.fich[!(enq.fich[,vv] %in% list.finale),]
              vv.err.intitule<-(vv.cont)              

#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333                 
        
      }  #if(toupper(vv.cont)=="POLE")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="ADRESSE"){
              #A FAIRE
              #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
              #ACTION TEMPORAIRE LE TEMPS DE FINALISER LA BASE ADRESSE
              #         #             #table de codif presente dans ce fichier
              vv.err<-NULL
              vv.err<-enq.fich[(enq.fich[,vv] %in% blanc),]
              #vv.err.intitule<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.cont<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
              #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
        
        
         } #if(toupper(vv.cont)=="ADRESSE")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="VOIE"){
             #A FAIRE
             #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
             #ACTION TEMPORAIRE LE TEMPS DE FINALISER LA BASE ADRESSE
             #         #             #table de codif presente dans ce fichier
             vv.err<-NULL
             vv.err<-enq.fich[(enq.fich[,vv] %in% blanc),]
             #vv.err.intitule<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
             vv.cont<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
             vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
             source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
             #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
         } #if(toupper(vv.cont)=="VOIE")
#        ...............................................................................................................................................................
         if(toupper(vv.cont)=="FRONTIERE" | toupper(vv.cont)=="ES_FRANCE" ){ #if(toupper(vv.cont)=="ES_FRANCE")
              #A FAIRE
              #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
              #ACTION TEMPORAIRE LE TEMPS DE FINALISER LA BASE ADRESSE
              #         #             #table de codif presente dans ce fichier
              vv.err<-NULL
              vv.err<-enq.fich[(enq.fich[,vv] %in% blanc),]
              #vv.err.intitule<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.cont<-paste(toupper(vv.cont),"==> on regarde les valeurs vides",sep="")
              vv.err.intitule<-(vv.cont)
#         #             #1111111111111111111111111111111111111111111111111111111111111111111111
              source( paste(cheminP,"999_SUB_01.r",sep="") )
#         #             #3333333333333333333333333333333333333333333333333333333333333333333333               
             #ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
         } #if(toupper(vv.cont)=="ES_FRANCE")
#        ...............................................................................................................................................................
#         #              fich.vv.err<-rbind(fich.vv.err,c("",vv,ifelse(nrow(vv.err)==0,nrow(vv.err),nrow(fich.agg)),vv.cont,vdenomb,info.n,info.p,info.r,info.x,info.i))
         } #if ( ! (vv %in% cod.fich$champ) | ! (vv %in% qxp.fich$champ) )
    
     } #for ( vv in setdiff(enq.fich.ent,c(champs.sup,champs.cont1)) )
#         #             # sauvegarde du ficher de synthese

    enq.fich$nb_cont_tot<-enq.fich$nb_cont
  
    max.long<-NULL;ent00<-NULL;ent01<-NULL                                                                                                             
    ent00<-colnames(fich.vv.err)                                                                                                                       
    ent01<-colnames(f00resu)                                                                                                                         
    max.long<-max(length(ent00),length(ent01))                                                                                                         
    if( length(ent00)<max.long){ftemp00<-matrix(data="",nrow=nrow(fich.vv.err), ncol=(max.long-length(ent00)));fich.vv.err<-cbind(fich.vv.err,ftemp00)}
    if( length(ent01)<max.long){ftemp00<-matrix(data="",nrow=nrow(f00resu)  , ncol=(max.long-length(ent01)));f00resu  <-cbind(f00resu  ,ftemp00)}
  
    ent03<-paste("V",c(1:max.long),sep="")                                                                                                             
    colnames(fich.vv.err)<-ent03                                                                                                                       
    colnames(f00resu)<-ent03                                                                                                                         
  
    fich.vv.err<-rbind(f00resu,fich.vv.err)                        
    #write.table(fich.vv.err,paste(cheminR,dos.resu,"\\",nom.dos03,"\\",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="")
    write.table(fich.vv.err,paste(rep_enq.chem,"/",nom.dos03,"/",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="",quote=F,fileEncoding = encodage01)
    #write.table(fich.vv.err,paste(cheminR,dos.resu,"\\",nom.dos03,"\\",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
    
#  #_____simplification chaine des erreurs            
  enq.fich$ch_cont<-gsub("#-","",enq.fich$controle,fixed=T)

#
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#--G.5.2- CALCUL DES DISTANCES VOL D'OISEAU ET ANGLE POUR CONTROLE SELON NOTE CEREMA FH & BZ ETAPE 1
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
#         #ATTENTION possible doublons de points dans le fichier des points d'enquete si plusieurs jours d'enquete.
#         #==> lors de la recherche de coordonnees, veiller a ne pas avoir de doublons
#==> pour O et D, filtre d ela table sur type_lieu=1,2,3 (niveau pays/zone/commune)
#         #_____filtre de la table des codes zones sur les niveuax pays/zone/commune
  #ICI
  #zon.fich<-zon.fich.ini[as.numeric(zon.fich.ini@data$type_lieu) %in% typ.lieu.pzc,]
  
#       #_____Ajout des coord centroide origine xol93,yol93 depuis le fichier de zonage sous PG
        list.xy<-NULL
        list.xy<-apply(enq.fich[,c("pays_orig","zone_orig","commune_orig")],MARGIN=1,FUN=function(x) {FRechCentlU93v3(x,"xol93","yol93")})
        df.xy<-NULL
        df.xy<-do.call("rbind",list.xy)
        enq.fich<-cbind(enq.fich,df.xy)

#       #_____Ajout des coord centroide destination xdl93,ydl93 depuis le fichier de zonage sous PG
        list.xy<-NULL
        list.xy<-apply(enq.fich[,c("pays_dest","zone_dest","commune_dest")],MARGIN=1,FUN=function(x) {FRechCentlU93v3(x,"xdl93","ydl93")})
        df.xy<-NULL
        df.xy<-do.call("rbind",list.xy)
        enq.fich<-cbind(enq.fich,df.xy)
 
#       #_____Ajout des coord poste depuis le fichier de poste pts.fich (xpl93,ypl93) +angle + code_com lieu du poste
        enq.fich$xpl93<-""    
        enq.fich$ypl93<-""
        enq.fich$poste.angle.azimut<-""  
        enq.fich$poste.com<-""  

                enq.fich$xpl93<-apply((enq.fich[,c("code_poste","num_point")]),MARGIN=1,FUN=function(x) {FRechPostel93Uv2(x,"xpl93")})
        enq.fich$ypl93<-apply((enq.fich[,c("code_poste","num_point")]),MARGIN=1,FUN=function(x) {FRechPostel93Uv2(x,"ypl93")})
        enq.fich$poste.angle.azimut<-apply((enq.fich[,c("code_poste","num_point")]),MARGIN=1,FUN=function(x) {FRechPostel93Uv2(x,   "angle")})     
        enq.fich$poste.com<- apply((enq.fich[,c("code_poste","num_point")]),MARGIN=1,FUN=function(x) {FRechPostel93UCharv2(x,"code_com")}) 
  
  
#       #_____conversion angle en sens trigo (0� = 15h)
        enq.fich$poste.angle.trigo<-apply(data.frame(enq.fich[,"poste.angle.azimut"]),MARGIN=1,FUN=FAzimutToTrigo)
#       #_____calcul angle op-axe horizontal oph (sens P vers O)
        enq.fich$oph<-apply(data.frame(enq.fich[,c("xpl93","ypl93","xol93","yol93")]),MARGIN=1,FUN=fAngleSensTrigo)
#       #_____calcul angle op-axe horizontal hpd (sens P vers D)
        enq.fich$hpd<-apply(data.frame(enq.fich[,c("xpl93","ypl93","xdl93","ydl93")]),MARGIN=1,FUN=fAngleSensTrigo)
#       #_____angle opd
        enq.fich$angle_opd<-apply(enq.fich[,c("oph","hpd")],MARGIN=1,FUN=FAngle2)
  
#       #_____angle PD/direction poste
        enq.fich$angle_dppd<-apply(enq.fich[,c("poste.angle.trigo","hpd")],MARGIN=1,FUN=FAngle2)
  
  
#       #_____calcul distance OD,OP,PD vol d'oiseau et ratio od/opd
        enq.fich$vopkm<-apply(enq.fich[,c("xol93","yol93","xpl93","ypl93")],MARGIN=1,FUN=function(vv){fDistVOm(vv,"km")})
        enq.fich$vpdkm<-apply(enq.fich[,c("xdl93","ydl93","xpl93","ypl93")],MARGIN=1,FUN=function(vv){fDistVOm(vv,"km")})
        enq.fich$vopdkm<-enq.fich$vopkm+enq.fich$vpdkm
        enq.fich$vodkm<-apply(enq.fich[,c("xol93","yol93","xdl93","ydl93")],MARGIN=1,FUN=function(vv){fDistVOm(vv,"km")})
        enq.fich$ratiodist.vodxvopd<-enq.fich$vodkm/enq.fich$vopdkm
  
#       #_____code controle
        #  fich.opd2$code_controle_od<-apply(fich.opd2[,c("vopkm","vpdkm","vodkm","ratiodistvodxvopd","angle_opd","angle_dppd")],MARGIN=1,FUN=FCodeControle)
        #fich.cont <- fich.opd2[,c("vopkm","vpdkm","vodkm","ratiodistvodxvopd","angle_opd","angle_dppd","code_o","code_d","gisco_id")] #SQ
        #fich.opd2$code_controle_od<-apply(fich.cont,MARGIN=1,FUN=FCodeControle) #SQ 
        ent.cont<-c("vopkm","vpdkm","vodkm","ratiodist.vodxvopd","angle_opd","angle_dppd","commune_orig","commune_dest","poste.com")
        enq.fich$code_controle_od<-apply(enq.fich[,ent.cont],MARGIN=1,FUN=FCodeControle)
  
        table(enq.fich$code_controle_od)                       
  
#       #_____synthese repartition par code par poste?
        #opd.synth<-NULL
        #opd.synth<-data.frame("code_poste.num_point"=paste(enq.fich$code_poste,enq.fich$num_point,sep="#"),"code_controle_od"=enq.fich$code_controle_od)
        opd.synth2<-NULL
        opd.synth2<-data.frame(xtabs(~code_poste+num_point+code_controle_od,data=enq.fich))
  
        opd.piv<-NULL                                                                                                                                               
        opd.piv<-pivot_wider(data=opd.synth2,names_from=code_controle_od,values_from = Freq,values_fill = 0)   
  
        opd.tot<-NULL
        opd.tot<-data.frame("code_poste"="Ensemble","num_point"="",t(data.frame((apply(opd.piv[,setdiff(colnames(opd.piv),c("code_poste","num_point"))],MARGIN=2,FUN=sum)))))
        colnames(opd.tot)<-colnames(opd.piv)
  
        opd.ent<-t(data.frame(colnames(opd.piv)));colnames(opd.ent)<-colnames(opd.piv)
  
        opd.mat<-NULL
        opd.mat<-matrix(data="",nrow=3,ncol=ncol(opd.tot))
        opd.mat[1,1]<-  makeNstr("#",pas_str_pad)
        opd.mat[2,3]<-  "CODE CONTROLE OD"
        colnames(opd.mat)<-colnames(opd.piv)
        opd.tot<-rbind(opd.mat,opd.ent,opd.piv,opd.tot)
        for (ccol in 3:ncol(opd.tot)){opd.tot[3,ccol]<-FCodeControleInt(opd.tot[3+1,ccol])}
  
        rm(opd.synth2,opd.piv,opd.ent,opd.mat)
  
        max.long<-NULL;ent00<-NULL;ent01<-NULL
        fich.synth02<-fich.vv.err
        ent00<-colnames(fich.synth02)
        ent01<-colnames(opd.tot)
        max.long<-max(length(ent00),length(ent01))
        if( length(ent00)<max.long){ftemp00<-matrix(data="",nrow=nrow(fich.synth02), ncol=(max.long-length(ent00)));fich.synth02<-cbind(fich.synth02,ftemp00)}
        if( length(ent01)<max.long){ftemp00<-matrix(data="",nrow=nrow(opd.tot)  , ncol=(max.long-length(ent01)));opd.tot  <-cbind(opd.tot  ,ftemp00)}
  
        ent03<-paste("V",c(1:max.long),sep="")
        colnames(fich.synth02)<-ent03
        colnames(opd.tot)<-ent03    
  
        fich.synth02<-rbind(fich.synth02,opd.tot)
        hh40<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
        #fich.synth02[3,2]<-hh40
        #write.table(fich.synth02,paste(cheminR,dos.resu,"\\",nom.dos03,"\\",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="")
        write.table(fich.synth02,paste(rep_enq.chem,"/",nom.dos03,"/",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="",quote=F,fileEncoding = encodage01)
        #write.table(fich.synth02,paste(rep_enq.chem,"/",nom.dos03,"/",nom.fich.synth,"xx.csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="",quote=F,fileEncoding = encodage01)
  
        hh01;hh40          

#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#--G.5.3- TESTS CROISES NON FIXES
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
#         #l'enquete ne satisfaisant pas le test en question est pointee dans la base par "x" dans une colonne cree et correspondant au test.
  
         txc.synth<-NULL
         txc.synth<-rbind(data.frame("V0"="Test","V1"="controle","V2"="croisement","V3"="nb_incoherence","V4"="champ_de_controle_dans _base",stringsAsFactors=FALSE),c("","","","",""))
         txc.synth<-rbind(txc.synth[2,],txc.synth)
         txc.synth<-rbind(txc.synth[1,],txc.synth)
         txc.synth[1,1]<-c(makeNstr("#",pas_str_pad))              
  
         listoperateur<-c("==","!=",">=","<=",">","<") #attention a l'ordre entre ">=" et ">"
         listoperateur2<-c(listoperateur,",","alors","et","ou")  
  
#        #_____filtre sur les tests croises non fixes

         txc.extr<-NULL
         #txc.extr<-txc.fich[!(tolower(txc.fich[[2]]) %in% c("test fixe")),]
        txc.extr<-txc.fich[!(tolower(txc.fich[,"domaine"]) %in% c("test fixe")),]
        # i<-4
        for (i in 1:nrow(txc.extr)) {
        # i<-1
        #test01<-tolower(txc.extr[i,"code"]);test01
              test01<-(txc.extr[i,"code"]);test01
              nFStest<-tolower(paste(txc.extr$test[i],txc.extr$domaine[i],sep="_"))
              intitule<-txc.extr[i,"intitule"] #$intitule[i]
              ind.pointeur<-tolower(txc.extr[i,"ind_min"]) #20210429
              hyp<-Fhypcond(test01)
              resu1<-FtestDGITM3(hyp[1]);hyp[1];resu1
              if(length(hyp)==1) {resu2<-"fin test"} else {resu2<-FtestDGITM3(hyp[2])};hyp[2];resu2
              test01;"#################"
              resu1;"#################"
              resu2
              #tt<-enq.fich[eval(parse(text=resu1)),]
    
              if (resu1[1]=="erreur d operateur: a verifier" | resu2[1]=="erreur d operateur: a verifier") {
                   temptxc.synth<-c(nFStest,intitule,test01,"erreur d operateur: a verifier","-") # A MAJ longueur vecteur!!!!!
                   txc.synth<-rbind(txc.synth,temptxc.synth)
              } else {
                   resu1Num<-FtestDGITM3(hyp[1]);hyp[1];resu1Num;resu1==resu1Num
                   #resu2<-ifelse(length(hyp)==1,"fin test",Ftest(hyp[2]));hyp[2];resu2
                   #resu2Num<-ifelse(length(hyp)==1,"fin test",FtestNum(hyp[2]));hyp[2];resu2Num
                   #xtest<-hyp[2];xtest
                   if(length(hyp)==1) {resu2Num<-"fin test"} else {resu2Num<-FtestDGITM3(hyp[2])};hyp[2];resu2Num;resu2==resu2Num
                   resu1<-resu1Num;resu2<-resu2Num
                   if (resu1[1]=="nontrouve" | resu2[1]=="nontrouve"){#des champs sont non trouves dans le dessin
                        tmptest01<-c("champs non trouves")
                        #if  (resu1[1]=="nontrouve") {tmptest01<-paste(tmptest01,resu1[2:length(resu1)],Collapse=", ")}
                        #if  (resu2[1]=="nontrouve") {tmptest01<-paste(tmptest01,resu2[2:length(resu2)],Collapse=", ")}
                        if  (resu1[1]=="nontrouve") {for (jj in 2:length(resu1)) {tmptest01<-paste(tmptest01,resu1[jj],sep=" : ")} }
                        if  (resu2[1]=="nontrouve") {for (jj in 2:length(resu2)) {tmptest01<-paste(tmptest01,resu2[jj],sep=" : ")} }
                        temptxc.synth<-c(nFStest,intitule,test01,tmptest01,"-")
                        txc.synth<-rbind(txc.synth,temptxc.synth)
                   } else {
                        temp<-NULL
                        #temp<-(enq.fich2[,!(colnames(enq.fich2) %in% c("clef","tempdist","e_temp1"))])
                        temp<-enq.fich
                        #for (vv in colnames(temp)){ # vv<-"sexe"
                        #temp[(temp[,vv] %in% blanc),vv]<-str_pad(temp[temp[,vv] %in% blanc,vv],pas_str_pad,pad=" ")
                        #temp[,vv]<-str_pad(temp[,vv],pas_str_pad,pad="0")
                        #}
                        #repassage de l'entete de temp en champ enquete
                        #stenttemp<-colnames(temp)
                        #venttemp<-stenttemp
                        #venttemp[which(venttemp %in% xTOUTENT$yven )]<-unlist(lapply(venttemp[which(venttemp %in% xTOUTENT$yven )],FUN=function(x) (xTOUTENT$ven[which(xTOUTENT$yven==x)] )))
                        #colnames(temp)<-venttemp	
        
                        TESTEXP<-NULL
                        if (resu2=="fin test"){
                             #testexpression<-paste("TESTEXP<-temp[which( ",resu1,"),]",sep="")
                             testexpression<-paste("TESTEXP<-temp[which( !",resu1,"),]",sep="") #<--------------c'est le bon test
                        } else {
                             #testexpression<-paste("TESTEXP<-temp[which( ",resu1," & ",resu2,"),]",sep="")
                             testexpression<-paste("TESTEXP<-temp[which( ",resu1," & !",resu2,"),]",sep="") #<--------------c'est le bon test
                        }
                        options(warn=-1) #.............................
                        eval(parse(text=testexpression))
                        options(warn=0)  #.............................
                        #retourner enq.fich2 avec les rid correspondant!!!!!!!!!!!!!!
                        temptxc.synth<-c(nFStest,intitule,test01,as.character(ifelse(is.null(nrow(TESTEXP)),"-",nrow(TESTEXP))),nFStest)
                        txc.synth<-rbind(txc.synth,temptxc.synth)
                        enq.fich[,nFStest]<-""
                        #enq.fich[as.numeric(enq.fich$w_rid) %in% as.numeric(TESTEXP$w_rid),nFStest]<-"x"
                        #enq.fich$nb_cont_tot[enq.fich[,nFStest] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nFStest] %in% c("x")]
                        enq.fich[as.numeric(enq.fich$w_rid) %in% as.numeric(TESTEXP$w_rid),nFStest]<-"1"
                        enq.fich$nb_cont_tot[enq.fich[,nFStest] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nFStest] %in% c("1")]
                        if (ind.pointeur %in% c("x")){
                             enq.fich$nb_cont_min[enq.fich[,nFStest] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nFStest] %in% c("1")]  
                         }
                         #write.table(TESTEXP,paste(cheminR,cheminTC,"\\",nFStest,".txt",sep=""),row.names=FALSE,col.names=TRUE,sep=";")
                   } #
              } #if (resu1[1]=="erreur d operateur: a verifier" | resu2[1]=="erreur d operateur: a verifier")
        } #rt for (i in 1:nrow(txc.extr))
  
         ncol1<-ncol(enq.fich)
         ncol2<-ncol(txc.synth)
         ncol1;ncol2
  
         max.long<-NULL;ent00<-NULL;ent01<-NULL
         ent00<-colnames(fich.synth02)
         ent01<-colnames(txc.synth)
         max.long<-max(length(ent00),length(ent01))
         if( length(ent00)<max.long){ftemp00<-matrix(data="",nrow=nrow(fich.synth02), ncol=(max.long-length(ent00)));fich.synth02<-cbind(fich.synth02,ftemp00)}
         if( length(ent01)<max.long){ftemp00<-matrix(data="",nrow=nrow(txc.synth)  , ncol=(max.long-length(ent01)));txc.synth  <-cbind(txc.synth  ,ftemp00)}
  
         ent03<-paste("V",c(1:max.long),sep="")
         colnames(fich.synth02)<-ent03
         colnames(txc.synth)<-ent03
  
         fich.synth02<-rbind(fich.synth02,txc.synth) 
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#--G.5.4- TESTS CROISES FIXES
#���������������������������������������������������������������������������������������������������������������������������������������������������������������
#���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
         txc.fix<-NULL
         txc.fix<-txc.fich[(tolower(txc.fich[,"domaine"]) %in% c("test fixe")),]
         ind.fix<-txc.fix[txc.fix[,"ind_min"] %in% c("x"),"test"]
  
         txf.synth<-NULL
         txf.synth<-rbind(data.frame("V0"="Test fixe","V1"="controle","V2"="croisement","V3"="nb_incoherence","V4"="champ_de_controle_dans _base",stringsAsFactors=FALSE),c("","","","",""))
         txf.synth<-rbind(txf.synth[2,],txf.synth)
         txf.synth[1,1]<-c(makeNstr("#",pas_str_pad))
  
#        #_____G.5.4.1: coherence immat_pays et immat_dep
#        #              immat_dep rempli par code zone ssi immat_pays="FR", immat_dep vaut "N" dans les autres cas
         if ("immat_pays" %in% colnames(enq.fich) & "immat_dep" %in% colnames(enq.fich)) {
              nom.test<-"coherence immat_pays et immat_dep"
              nom.crois<-"immat_paysximmat_dep"
              nom.col<-NULL
              num.test<-"test901"
              nom.col<-tolower(paste(num.test,"_immat_pays_dep",sep=""))

              MyQuery<- paste("SELECT DISTINCT code_zone FROM ",ref_schema,".",ref_table," WHERE (type_lieu=",2,") AND code_pays='FR'",sep="")
              zon.fich.fr<-NULL
              zon.fich.fr<-dbGetQuery(con, MyQuery)
              
              test901<-NULL
              test901<-enq.fich[ !(
                                     (    enq.fich$immat_pays %in% c("FR")  & enq.fich$immat_dep %in% zon.fich.fr$code_zone  ) |
                                     (  !(enq.fich$immat_pays %in% c("FR")) & enq.fich$immat_dep %in% c("N")                 )
              ),]
    
              enq.fich[,nom.col]<-""
              #enq.fich[enq.fich$w_rid %in% test901$w_rid,nom.col]<-"x"
              #enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]
              #enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="x",])
    
              enq.fich[enq.fich$w_rid %in% test901$w_rid,nom.col]<-"1"
              enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]
              #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
              if (num.test %in% ind.fix){                                                                                       #20210429
                   enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]   #20210429
              }                                                                                                                 #20210429
              #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
              enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="1",])
    
              temptxc.synth<-c(num.test,nom.test,nom.crois,as.character(ifelse(is.null(enq.resu),"-",enq.resu)),nom.col)
             txf.synth<-rbind(txf.synth,temptxc.synth)
         } #if ("immat_pays" %in% colnames(enq.fich) & "immat_dep" %in% colnames(enq.fich))
  
#        #_____G.5.4.2: coherence pays_dest,  zone_dest et commune_dest
         v.pzc<-paste(c("pays","zone","commune"),"dest",sep="_") 
         num.test<-"test902"
         nom.test<-"coherence pays_dest,  zone_dest et commune_dest (hors inconnu X-X-X et non pertinent N-N-N)"
         nom.crois<-"pays_destxzone_destxcommune_dest"
         ent.base<-colnames(enq.fich)
  
         source( paste(cheminP,"999_SUB_02.r",sep="") )   
  
#        #_____G.5.4.3: coherence pays_orig,  zone_orig et commune_orig
         v.pzc<-paste(c("pays","zone","commune"),"orig",sep="_") 
         num.test<-"test903"
         nom.test<-"coherence pays_orig,  zone_orig et commune_orig (hors inconnu X-X-X et non pertinent N-N-N)"
         nom.crois<-"pays_origxzone_origxcommune_orig"
         ent.base<-colnames(enq.fich)
  
         source( paste(cheminP,"999_SUB_02.r",sep="") )   
  
  
#        #_____G.5.4.4: coherence pole_dest et commune_dest (verifier que les 10 premiers car de pole_dest = commune_dest, si pole_dest != X et N)
         extr01<-"dest"
         num.test<-"test904"
         v.cp<-paste(c("commune","pole"),extr01,sep="_")
         nom.test<-paste("coherence pole_",extr01," avec commune_",extr01,"(si pole_",extr01," different de X et N)",sep="")
         nom.crois<-"commune_destxpole_dest"
         nom.col<-NULL
         nom.col<-tolower(paste(num.test,v.cp[1],v.cp[2],sep="_") )           
         test9xx<-NULL
         test9xx<-enq.fich[,c("w_rid",v.cp[1],v.cp[2])]
         test9xx$pref10<-substr(test9xx[,v.cp[2]],1,10)
         test9xx$cont<-0
         test9xx$cont[ !(test9xx$pref10 %in% c("N","X")) & test9xx$pref10!=test9xx[,v.cp[1]] ] <-1
         enq.fich[,nom.col]<-""
         enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont==1] ,nom.col]<-"1"
         enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]
         #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
         if (num.test %in% ind.fix){                                                                                       #20210429
           enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]   #20210429
         }                                                                                                                 #20210429
         #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
         enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="1",])
         #enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]
         #enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="x",])
         temptxc.synth<-c(num.test,nom.test,nom.crois,as.character(ifelse(is.null(enq.resu),"-",enq.resu)),nom.col)
         txf.synth<-rbind(txf.synth,temptxc.synth)	                        
  
  
#        #_____G.5.4.5: coherence pole_orig et commune_orig (verifier que les 10 premiers car de pole_orig = commune_orig, si pole_orig != X et N) 
         extr01<-"orig"
         num.test<-"test905"
         v.cp<-paste(c("commune","pole"),extr01,sep="_")
         nom.test<-paste("coherence pole_",extr01," avec commune_",extr01,"(si pole_",extr01," different de X et N)",sep="")
         nom.crois<-"commune_destxpole_orig"
         nom.col<-NULL
         nom.col<-tolower(paste(num.test,v.cp[1],v.cp[2],sep="_") )           
         test9xx<-NULL
         test9xx<-enq.fich[,c("w_rid",v.cp[1],v.cp[2])]
         test9xx$pref10<-substr(test9xx[,v.cp[2]],1,10)
         test9xx$cont<-0
         test9xx$cont[ !(test9xx$pref10 %in% c("N","X")) & test9xx$pref10!=test9xx[,v.cp[1]] ] <-1
         enq.fich[,nom.col]<-""
         enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont==1] ,nom.col]<-"1"
         enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]
         #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
         if (num.test %in% ind.fix){                                                                                       #20210429
           enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]   #20210429
         }                                                                                                                 #20210429
         #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
         enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="1",])
         #enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]
         #enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="x",])
         temptxc.synth<-c(num.test,nom.test,nom.crois,as.character(ifelse(is.null(enq.resu),"-",enq.resu)),nom.col)
         txf.synth<-rbind(txf.synth,temptxc.synth)
  
#        #_____G.5.4.6: coherence pays_trav,  zone_trav et commune_trav
         v.pzc<-paste(c("pays","zone","commune"),"trav",sep="_") 
         num.test<-"test906"
         nom.test<-"coherence pays_trav,  zone_trav et commune_trav (hors inconnu X-X-X et non pertinent N-N-N)"
         nom.crois<-"pays_travxzone_travxcommune_trav"
         ent.base<-colnames(enq.fich)
  
         source( paste(cheminP,"999_SUB_02.r",sep="") )   
  
  
#        #_____G.5.4.7: coherence pays_dom,  zone_dom et commune_dom
         v.pzc<-paste(c("pays","zone","commune"),"dom",sep="_") 
         num.test<-"test907"
         nom.test<-"coherence pays_dom,  zone_dom et commune_dom (hors inconnu X-X-X et non pertinent N-N-N)"
         nom.crois<-"pays_domxzone_travxcommune_dom"
         ent.base<-colnames(enq.fich)
  
         source( paste(cheminP,"999_SUB_02.r",sep="") )   
  
#        #_____G.5.4.8: orig(pays+zone+commune)!=dest(pays+zone+commune) (hors pays=X + zone=X + commune=X)
         v.pzc.o<-paste(c("pays","zone","commune"),"orig",sep="_") 
         v.pzc.d<-paste(c("pays","zone","commune"),"dest",sep="_") 
         nom.test<-"origine different de destination"
         nom.crois<-"pays_zone_com_destxpays_zone_comxorig"
         num.test<-"test908"
         ent.base<-colnames(enq.fich) 
         if (v.pzc.o[1] %in% ent.base & v.pzc.o[2] %in% ent.base &v.pzc.o[3] %in% ent.base &
             v.pzc.d[1] %in% ent.base & v.pzc.d[2] %in% ent.base &v.pzc.d[3] %in% ent.base ){
           nom.col<-NULL
           nom.col<-tolower(paste(num.test,"O_different_de_D",sep="_"))            
           test9xx<-NULL          
           #test9xx<-enq.fich[ !( enq.fich[,v.pzc.o[1]] %in% c("N") & enq.fich[,v.pzc.o[2]] %in% c("N") & enq.fich[,v.pzc.o[3]] %in% c("N")) & 
           test9xx<-enq.fich[ !( enq.fich[,v.pzc.o[1]] %in% c("X") & enq.fich[,v.pzc.o[2]] %in% c("X") & enq.fich[,v.pzc.o[3]] %in% c("X")) & 
                                enq.fich[,v.pzc.o[1]]==enq.fich[,v.pzc.d[1]] &
                                enq.fich[,v.pzc.o[2]]==enq.fich[,v.pzc.d[2]] &
                                enq.fich[,v.pzc.o[3]]==enq.fich[,v.pzc.d[3]] 
                              ,]
           enq.fich[,nom.col]<-""
           #enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont!=""] ,nom.col]<-"x"   
           #enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("x")]
           #enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="x",])
           
           enq.fich[enq.fich$w_rid %in% test9xx$w_rid[test9xx$cont!=""] ,nom.col]<-"1"   
           enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_tot[enq.fich[,nom.col] %in% c("1")]
           #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
           if (num.test %in% ind.fix){                                                                                       #20210429
             enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]<-1+ enq.fich$nb_cont_min[enq.fich[,nom.col] %in% c("1")]   #20210429
           }                                                                                                                 #20210429
           #xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
           enq.resu<-nrow(enq.fich[enq.fich[,nom.col]=="1",])
           
           temptxc.synth<-c(num.test,nom.test,nom.crois,as.character(ifelse(is.null(enq.resu),"-",enq.resu)),nom.col)
           txf.synth<-rbind(txf.synth,temptxc.synth)
         }    #(v.pzc.o[1] %in% ent.base & v.pzc.o[2] %in% ent.base &v.pzc.o[3] %in% ent.base &
         
         
         max.long<-NULL;ent00<-NULL;ent01<-NULL
         ent00<-colnames(fich.synth02)
         ent01<-colnames(txf.synth)
         max.long<-max(length(ent00),length(ent01))
         if( length(ent00)<max.long){ftemp00<-matrix(data="",nrow=nrow(fich.synth02), ncol=(max.long-length(ent00)));fich.synth02<-cbind(fich.synth02,ftemp00)}
         if( length(ent01)<max.long){ftemp00<-matrix(data="",nrow=nrow(txf.synth)  , ncol=(max.long-length(ent01)));txf.synth  <-cbind(txf.synth  ,ftemp00)}
         
         ent03<-paste("V",c(1:max.long),sep="")
         colnames(fich.synth02)<-ent03
         colnames(txf.synth)<-ent03
         
         fich.synth02<-rbind(fich.synth02,txf.synth)
         #ajout du  nombre moyen d'erreur sur l'ensemble de la base = nb_cont_tot[err codif + tests crois�s]/nrow(enq.fich)
         info.ligne<-nrow(enq.fich)
         info.err<-sum(as.numeric(enq.fich$nb_cont_tot),na.rm=T)
         info.err.min<-sum(as.numeric(enq.fich$nb_cont_min),na.rm=T)
         info.matrix<-matrix(data="",nrow=5,ncol=ncol(fich.synth02))
         #info.matrix<-matrix(data="",nrow=4,ncol=20)
         info.matrix[1,1]<-makeNstr("#",pas_str_pad*4)
         info.matrix[2,1]<-paste("Nombre enqu�tes : ",info.ligne,sep="")
         info.matrix[3,1]<-paste("Nombre moyen de points � contr�ler point�s par enqu�te (codif+tests) : ",round(info.err/info.ligne,digits = 5))
         info.matrix[4,1]<-paste("Indicateur minimal moyen (codif+tests) : ",round(info.err.min/info.ligne,digits = 5))
         info.matrix[5,1]<-makeNstr("#",pas_str_pad*4)
         colnames(info.matrix)<-colnames(fich.synth02)
         fich.synth02<-rbind(fich.synth02,info.matrix)
         
         hh60<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
         fich.synth02[3,2]<-hh60
         #write.table(fich.synth02,paste(cheminR,dos.resu,"\\",nom.dos03,"\\",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="")
         write.table(fich.synth02,paste(rep_enq.chem,"/",nom.dos03,"/",nom.fich.synth,".csv",sep=""),row.names=FALSE,col.names=FALSE,sep=";",na="",quote=F,fileEncoding = encodage01)
  
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #--G.5.5- MATRICE OD des motifs (VL,PL uniquement pas XX indetermine)
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
         e_vlpl<-"w_vlpl"
         
         for (typ.veh in c("VL","PL")){
           #vmotifo<-paste("motif_orig",typ.veh,sep="_")
           #vmotifd<-paste("motif_dest",typ.veh,sep="_")
           
           vmotifo<-"motif_orig"
           vmotifd<-"motif_dest"
           
           if (vmotifo %in% colnames(enq.fich) & vmotifd %in% colnames(enq.fich)){
             
             #             #recuperation des intitules des motifs
             mot.int<-cod.fich[cod.fich$champ %in% c(vmotifo),c("modalite","libelle")]
             mot.fich<-enq.fich[,c("w_vlpl","code_poste","num_point",vmotifo,vmotifd)]
             colnames(mot.fich)<-c("w_vlpl","code_poste","num_point","motif_orig","motif_dest")
             mot.fich$motif_orig[mot.fich$motif_orig %in% c(NA,"NA",blanc)]<-"NA"
             mot.fich$motif_dest[mot.fich$motif_dest %in% c(NA,"NA",blanc)]<-"NA"
             #             #mot.fich<-left_join(mot.fich,mot.int,by=c("motif_orig"="modalite"))
             
             mot.synth2<-NULL
             mot.synth2<-data.frame(xtabs(~code_poste+num_point+w_vlpl+motif_orig+motif_dest,data=mot.fich))
             
             mot.piv<-NULL                                                                                                                                               
             mot.piv<-pivot_wider(data=mot.synth2,names_from="motif_dest",values_from = Freq,values_fill = 0)   
             mot.piv<-mot.piv[order(mot.piv$w_vlpl,mot.piv$code_poste,mot.piv$num_point),]
             mot.piv<-left_join(mot.piv,mot.int,by=c("motif_orig"="modalite"))
             mot.piv$libelle[is.na(mot.piv$libelle)]<-"x"
             ncol(mot.piv)
             mot.piv<-mot.piv[,c(1:3,ncol(mot.piv),4:(ncol(mot.piv)-1))]
             ncol(mot.piv)
             mot.piv<-mot.piv[mot.piv$w_vlpl %in% typ.veh,]
             
             mot.tot<-mot.piv
             mot.tot$code_poste<-"Ensemble"
             mot.tot$num_point<-""
             mot.tot<-aggregate(mot.tot[,setdiff(colnames(mot.tot),c("code_poste","num_point","w_vlpl","libelle","motif_orig"))],
                                by=list( "code_poste"=mot.tot$code_poste,"num_point"=mot.tot$num_point,
                                         "w_vlpl"=mot.tot$w_vlpl,"libelle"=mot.tot$libelle,"motif_orig"=mot.tot$motif_orig
                                ),FUN=sum)
             
             mot.tot<-rbind(mot.tot,mot.piv)
             nom.dos04<-"03_MATRICE_MOTIF"   
             lf_resu_dos2<-list.files(paste(rep_enq.chem,sep=""))
             if (!(nom.dos04 %in% lf_resu_dos2)){dir.create(paste(rep_enq.chem,nom.dos04,"/",sep=""))}                                             
             write.table(mot.tot,paste(rep_enq.chem,"/",nom.dos04,"/",nom.dos04,"_",typ.veh,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="",quote=F,fileEncoding = encodage01)
             
             #lf_resu_dos2<-list.files(paste(cheminR,dos.resu,"\\",sep=""))
             #if (!(nom.dos04 %in% lf_resu_dos2)){dir.create(paste(cheminR,dos.resu,"\\",nom.dos04,"\\",sep=""))}                                             
             #write.table(mot.tot,paste(cheminR,dos.resu,"\\",nom.dos04,"\\","04_MATRICE_MOTIF","_",typ.veh,".csv",sep=""),row.names=FALSE,col.names=T,sep=";",na="")      
             
           } #if (vmotifo %in% colnames(enq.fich) & vmotifd %in% colnames(enq.fich))
         } #for (typ.veh in c("VL","PL"))"        
         
         
         hh70<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
         hh01;hh70
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #--G.5.6- COMPARAISON VALEURS CHAMP INI ET FINAL
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
         chp.ini<-NULL
         chp.ini<-colnames(enq.fich)[substr( colnames(enq.fich),nchar(colnames(enq.fich))-3,nchar(colnames(enq.fich)) )=="_ini"]
         for (vv.ini in chp.ini) {
           #         #                   #filtre sur le champ ini et le champ final
           # vv.ini<-"TYPE_CARROSSERIE_INI"
           vv.fin<-substr(vv.ini,1,nchar(vv.ini)-4);vv.ini;vv.fin
           if (vv.fin %in% colnames(enq.fich) & vv.ini %in% colnames(enq.fich) ){
             #         #                    #par poste
             vv.fich01<-NULL
             vv.fich<-enq.fich[,c("code_poste","num_point",vv.ini,vv.fin)]  
             vv.fich$cpt<-1
             vv.fich[is.na(vv.fich[,vv.ini]),vv.ini]<-"NA"
             vv.fich[is.na(vv.fich[,vv.fin]),vv.fin]<-"NA"
             
             vv.agg<-NULL
             vv.agg<-aggregate(x=vv.fich$cpt,by=list(
               "code_poste"=vv.fich$code_poste,
               "num_point"=vv.fich$num_point,
               "chini"=vv.fich[,vv.ini],
               "chfin"=vv.fich[,vv.fin]),FUN=sum)
             vv.agg<-vv.agg[order(vv.agg$code_poste,vv.agg$num_point,vv.agg$chini,vv.agg$chfin),]
             vv.agg$egalite<-FALSE
             vv.agg$egalite[vv.agg$chini==vv.agg$chfin]<-TRUE
             #         #                    #Ensemble
             vv.fich$code_poste<-"Ensemble"
             vv.fich$num_point<-""
             vv.aggtot<-NULL
             vv.aggtot<-aggregate(x=vv.fich$cpt,by=list(
               "code_poste"=vv.fich$code_poste,
               "num_point"=vv.fich$num_point,
               "chini"=vv.fich[,vv.ini],
               "chfin"=vv.fich[,vv.fin]),FUN=sum)
             vv.aggtot<-vv.aggtot[order(vv.aggtot$code_poste,vv.aggtot$num_point,vv.aggtot$chini,vv.aggtot$chfin),]
             vv.aggtot$egalite<-FALSE
             vv.aggtot$egalite[vv.aggtot$chini==vv.aggtot$chfin]<-TRUE
             
             vv.aggtot<-rbind(vv.aggtot,vv.agg)
             colnames(vv.aggtot)<-tolower(c("code_poste","num_point",vv.ini,vv.fin,"frequence","egalite"))
             #         #                    #Sauvegarde
             nom.dos05<-"04_LISTE_CHAMP_INI_FINAL"   
             lf_resu_dos2<-list.files(paste(rep_enq.chem,sep=""))
             if (!(nom.dos05 %in% lf_resu_dos2)){dir.create(paste(rep_enq.chem,nom.dos05,"/",sep=""))}                                             
             write.table(vv.aggtot,paste(rep_enq.chem,"/",nom.dos05,"/",vv.ini,"-",vv.fin,".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="",quote=F ,fileEncoding = encodage01)
             
             
             #lf_resu_dos2<-list.files(paste(cheminR,dos.resu,"\\",sep=""))
             #if (!(nom.dos05 %in% lf_resu_dos2)){dir.create(paste(cheminR,dos.resu,"\\",nom.dos05,"\\",sep=""))}                                             
             #write.table(vv.aggtot,paste(cheminR,dos.resu,"\\",nom.dos05,"\\",vv.ini,"-",vv.fin,".csv",sep=""),row.names=FALSE,col.names=T,sep=";",na="")             
             
           } #if (vv.fin %in% colnames(enq.fich) & vv.ini %in% colnames(enq.fich) )
         } # for (vv.ini in chp.ini) 
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #--G.5.7- TABLEAU nb_cont_tot (nb "erreurs"/"interrogations" par enqueteur par poste)
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������
         #���������������������������������������������������������������������������������������������������������������������������������������������������������������  #  
         enq.fich$pt_err[enq.fich$nb_cont_tot>0]<-1
         err.enq0<-NULL
         ent.plus<-NULL;ent.plus<-colnames(enq.fich)[toupper(substr(colnames(enq.fich),1,4)) %in% c("TEST")]
         err.enq0<-enq.fich[,unique(c("code_poste","num_point","num_enq","pt_err","nb_cont_tot","nb_cont",ent.plus))]
         err.enq0$nb_enq<-1
         #agg.enq0<-NULL
         #agg.enq0<-aggregate(err.enq0$nb_cont_tot,by=list("code_poste"=enq.fich$code_poste,"num_point"=enq.fich$num_point,"num_enq"=enq.fich$num_enq),FUN=function(x){sum(x,na.rm=T)})
         agg.enq1<-NULL
         agg.enq1<-aggregate(err.enq0[,unique(c("nb_enq","pt_err","nb_cont_tot","nb_cont",ent.plus))],by=list("code_poste"=enq.fich$code_poste,"num_point"=enq.fich$num_point,"num_enq"=enq.fich$num_enq),FUN=function(x){sum(as.numeric(x),na.rm=T)})
         agg.enq1<-agg.enq1[order(agg.enq1$code_poste,agg.enq1$num_point,agg.enq1$num_enq),]
         #         #              #sauvegarde
         write.table(agg.enq1,paste(rep_enq.chem,"/",nom.dos03,"/","09_NB_ERRxENQ_02",".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
         #write.table(agg.enq1,paste(cheminR,dos.resu,"\\",nom.dos03,"\\","09_NB_ERRxENQ_02",".csv",sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
         
         hh90<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
         hh01;hh90
         
         #_______________________________________________________________________________________________________________________________________________________________
         #G.5-ENREGISTREMENT FICHIER ENQUETE ENRICHI
         #_______________________________________________________________________________________________________________________________________________________________
         ent.sort0<-setdiff(colnames(enq.fich),c("w_temp1","w_temp2",entete.a.supprimer))
         ent.sort0<-c(setdiff(ent.sort0,c("nb_cont_tot","nb_cont_min")),c("nb_cont_tot","nb_cont_min"))
         write.table(enq.fich[,ent.sort0],paste(rep_enq.chem,nom.dos01,"/",enq.nom,sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="",quote=F,fileEncoding=encodage01)
         
  
  #nom.dos06<-enq.dos #"1_FichEnq"
  #lf_resu_dos2<-list.files(paste(cheminR,"\\",sep=""))
  #if (!(nom.dos06 %in% lf_resu_dos2)){dir.create(paste(cheminR,"\\",nom.dos06,"\\",sep=""))}                                             
  
  #write.table(enq.fich[,ent.sort0],paste(cheminR,"\\",nom.dos06,"\\",enq.nom,sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
  #write.table(enq.fich[,ent.sort0],paste(cheminR,"\\",dos.resu,"\\",enq.nom,sep=""),row.names=FALSE,col.names=TRUE,sep=";",na="")
  
  #!!!!!!!!!!!!!!!!!ICI FIN
  # comparaison valeurs INI et FINAL
  #        
  
  #} #if (f00err>0 | f01err>0 | f03err>0 ) 
  
  
  
  ################################################################################################################################################################              
  # coh�rence IMMAT pays/immat_dep
  #paysO zone O ET COMO 
  #idem D
  #////////////////////////////////////////////////////////////////
  # extraction du champ dans le fic
  #dans le fichier de synth�se, on ne regarde que la codif pr�sente dans la codif donn�e
  #dans lefichier d'enquete resu , on croise de plus ave cle fait que la question soit pos� ou non
  #si G.2!=erreur on poursuit le traitement du fichier d'enquete, sinon on retourne les erreurs identifiees et on passe au fichier d'enquete suivant
  #G.3  ==> PAR POSTE PRESENT DANS LE FICHIER, controle des codifications presentes dans la base d'enquete en utilisant le fichier de codif
  #ATTENTION: plusieurs controles: codifs (y compris zonage), intervalle, unicite de l' ID_ITW (combine avec le poste d'enquete et le numero de point ou veut-on un id_itw unique sur l'ensemble de la base?)
  #A croiser avec le fichier question par poste pour identifier les questions posees
  #i-liste des erreurs (fichier de synthese)
  #ii-sortir un fichier par champ listant la repartition des codifs (distinction VL/PL+par poste?)
  #iii-sortir par enqu�teur, par poste et par champ du volume d'occurence des erreurs
  #G.3 ==> les test croises
  #l'enquete ne satisfaisant pas le test est pointee dans le fichier d'enquete
  #G.4 ==> controle selon les distances vol d'oiseau et angle des directions des postes
  #i-integrer les coordonnees des centroides de zones O et D et poste d'enquete en Lambert 93 + angle poste
  #ii-calcul OD, OP,PD, OPD et angle "DHPD" + ratio (note Cerema FH/BZ)
  #iii-ajout du code de validation
  #fichiers de sorties
  #G.1: 1 fichier qui reprend les resultats des tests d'unicite de champs et presence des champs
  #si G.1!=erreur
  #==> G.2: -par champ par poste, un fichier listant les codif presentes
  #         -fichier motif O x motif D par poste par VL/PL?
  #         -synthese 1 des erreurs de codifs (xposte?) + pointage dans le fichier d'enquete
  #         -synthese 2 par enqueteurxpostexVLPL
  #         -coh�rence pay-dep-commune en ORIGINE et DESTINATION
  #         -tcd champ ini et finaux
  #==> G.3: -ajout dans la synthese 1 des resultats des test croises
  #==> G.4: -ajout dans le fichier d'enquete
  ################################################################################################################################################################              
  
  #} #for (enq.nom in lf)
  
  hh1000<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
  hh01;hh1000                        
  #faire le filtre dle atable zonage sur les type_lieu 1 � 3 zon.fich<-zon.fich.ini....    
  #remplacer les eventuel "X" par des"N" avant recherche calcul Od (ie meme si on n'a pas la pr�cision voulu, on calcul l'OD)   
  
} #if (f00err>0 | f01err>0 | f03err>0 | f04err>0 )
warnings()
hh1001<-format(Sys.time(), "%d/%m/%Y %H:%M:%S")
hh01;hh1001
dbDisconnect(con)
