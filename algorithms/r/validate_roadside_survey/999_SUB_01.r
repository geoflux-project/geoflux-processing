#             #1111111111111111111111111111111111111111111111111111111111111111111111                                                                             
                   if(nrow(vv.err)>=1){                                                                                                                           
                     vv.err$w_temp1<-1  
                     fich.agg<-NULL
                     fich.agg<-aggregate(vv.err$w_temp1,list("val"=vv.err[,vv]),FUN=sum)                                                                          
                     fich.agg<-fich.agg[order(fich.agg$val),]                                                                                                     
                     fich.agg$denomb<-paste("\'",fich.agg$val,"\'"," (",fich.agg$x,")",sep="")                                                                    
                     vdenomb<-paste(fich.agg$denomb,collapse=" - ")                                                                                                
                   } else {vdenomb<-""}                                                                                                                           
#             #1111111111111111111111111111111111111111111111111111111111111111111111                                                                             
#             #2222222222222222222222222222222222222222222222222222222222222222222222                                                                             
                   #controle que lorsque la question n'est pas posee, la valeur est "N" (or TYPE_VEH inconnu)                                                     
                   vv.err.n<-enq.fich[ enq.fich$w_vlpl %in% c("VL","PL") & enq.fich$w_temp2==2 & enq.fich[,vv]!="N",]                                             
                   info.n<-""                                                                                                                                     
                   if (nrow(vv.err.n)>0){info.n<-nrow(vv.err.n)} else {info.n<-"OK (0)"}                                                                          
                                                                                                                                                                  
                   #controle que lorsque la question est posee, la valeur n'est pas "N" (or TYPE_VEH inconnu)                                                     
                   vv.err.p<-enq.fich[ enq.fich$w_vlpl %in% c("VL","PL") & enq.fich$w_temp2==1 & enq.fich[,vv]=="N",]                                             
                   info.p<-""                                                                                                                                     
                   if (nrow(vv.err.p)>0){info.p<-nrow(vv.err.p)} else {info.p<-"OK (0)"}                                                                          
                                                                                                                                                                  
                   #pour m�moire ligne NR (poste inconnu ou type VLPL=XX)                                                                                         
                   vv.err.r<-enq.fich[ !(enq.fich$w_vlpl %in% c("VL","PL")),]                                                                                     
                   info.r<-""                                                                                                                                     
                   if (nrow(vv.err.r)>0){info.r<-nrow(vv.err.r)} else {info.r<-"OK (0)"}                                                                          
                                                                                                                                                                  
                   # nombre d'enquete valant "X" pour le champ                                                                                                    
                   vv.err.x<-enq.fich[ enq.fich[,vv]=="X",]                                                                                                       
                   info.x<-""                                                                                                                                     
                   if (nrow(vv.err.x)>0){info.x<-nrow(vv.err.x)} else {info.x<-"0"}                                                                               
                                                                                                                                                                  
                   # nombre d'enquete valant "I" pour le champ                                                                                                    
                   vv.err.i<-enq.fich[ enq.fich[,vv]=="I",]                                                                                                       
                   info.i<-""                                                                                                                                     
                   if (nrow(vv.err.i)>0){info.i<-nrow(vv.err.i)} else {info.i<-"0"}                                                                               
                                                                                                                                                                  
                   #information des controles                                                                                                                     
                   temp.data$tempv[temp.data$rid %in% vv.err$w_rid]<-paste(vv,"_(",vv.err.intitule,")",sep="")                                                    
                   temp.data$tempv[temp.data$rid %in% vv.err.n$w_rid]<-paste(vv,"_(NonPosee)",sep="")                                                             
                   temp.data$tempv[temp.data$rid %in% vv.err.p$w_rid]<-paste(vv,"_(Posee)",sep="")                                                                
                   temp.data$tempv[temp.data$rid %in% vv.err.r$w_rid]<-paste(vv,"_(XX)",sep="")                                                                   
                   temp.data$nb[!(temp.data$tempv %in% c("-"))]<-1  
                   temp.data$nb.min[!(temp.data$tempv %in% c("-")) & vv %in% indic.min]<-1 #20210429                                                                                               
                   enq.fich[,"controle"]<-paste(enq.fich[,"controle"],temp.data$tempv,sep="#")                                                                    
                   enq.fich[,"nb_cont"]<-enq.fich[,"nb_cont"]+temp.data$nb
                   enq.fich[,"nb_cont_min"]<-enq.fich[,"nb_cont_min"]+temp.data$nb.min   #20210429       
#             #2222222222222222222222222222222222222222222222222222222222222222222222                #                                                            
#             #3333333333333333333333333333333333333333333333333333333333333333333333                                                                             
                   #             fichier listant les valeurs de la variable vv crois� avec w_vlpl et code_poste+num_point                                         
                   vv.list00<-NULL                                                                                                                                
                   vv.list00<-cbind(data.frame("code_poste.num_point"=paste(enq.fich$code_poste,enq.fich$num_point,sep="#")),enq.fich[,c("w_vlpl",vv)],"cpt"=1)   
                   colnames(vv.list00)[colnames(vv.list00)==vv]<-"VV" 
                   vv.list00.tabs<-NULL
                   vv.list00.tabs<-as.data.frame(xtabs(~VV+w_vlpl+code_poste.num_point,data=vv.list00))                                                           
                                                                                                                                                                  
                   vv.list00.tot<-NULL
                   vv.list00.tot<-as.data.frame(xtabs(~VV+w_vlpl,data=vv.list00))                                                                                 
                   vv.list00.tot$code_poste.num_point<-"Ensemble_poste"                                                                                           
                   vv.list00.tot<-vv.list00.tot[,colnames(vv.list00.tabs)]                                                                                        
                                                                                                                                                                  
                   vv.list00.tabs<-rbind(vv.list00.tot,vv.list00.tabs)                                                                                            
                   
                   vv.list00.piv<-NULL                                                                                                                                               
                   vv.list00.piv<-pivot_wider(data=vv.list00.tabs,names_from=w_vlpl,values_from = Freq,values_fill = 0)                                           
                                                                                                                                                                  
                   vv.list00.piv2<-NULL
                   vv.list00.piv2<-data.frame("VV"=unique(vv.list00.piv[,"VV"]))                                                                                  
                   vv.list00.piv2<-rbind(data.frame("VV"=c("",vv)),vv.list00.piv2)                                                                                
                                                                                                                                                                  
                   vv.list00.ent<-intersect(c("VL","PL","XX"),colnames(vv.list00.piv))                                                                            
                   #poste0<-"Poste 10#1"                                                                                                                           
                   #poste0<-"Poste 12#1"                                                                                                                           
                                                                                                                                                                  
                   for (poste0 in unique(vv.list00.piv$code_poste.num_point)) {                                                                                   
                     temp.piv<-NULL                                                                                                                               
                     temp.piv<-vv.list00.piv[vv.list00.piv$code_poste.num_point==poste0,vv.list00.ent]                                                            
                     temp.piv<-rbind(colnames(temp.piv),temp.piv)                                                                                                 
                     temp.piv<-rbind(c(poste0),temp.piv)                                                                                                          
                     vv.list00.piv2<-cbind(vv.list00.piv2,temp.piv)                                                                                               
                   }                                                                                                                                              
#                  #ajout info question posee/non posee                                                                                                           
                   qpn.vect<-c("QUESTION POSEE/NON POSEE==>")                                                                                                     
                   qpn.fichq<-NULL                                                                                                                                
                   qpn.fich<-data.frame(data.frame("code_poste.num_point"=paste(enq.fich$code_poste,enq.fich$num_point,sep="#")),enq.fich[,c("w_vlpl","w_temp2")])
                   qpn.fich$CLEF<-paste(qpn.fich$code_poste.num_point,qpn.fich$w_vlpl,qpn.fich$w_temp2,sep="-")                                                   
                   qpn.fich<-qpn.fich[!duplicated(qpn.fich$CLEF),]                                                                                                
                   qpn.fich$QPN<-"NR"                                                                                                                             
                   qpn.fich$QPN[qpn.fich$w_temp2 %in% c(1)]<-"posee"                                                                                              
                   qpn.fich$QPN[qpn.fich$w_temp2 %in% c(2)]<-"non posee"                                                                                          
                   for (j in 2:ncol(vv.list00.piv2)) {                                                                                                            
                        if (nrow(qpn.fich[qpn.fich$code_poste.num_point %in% (vv.list00.piv2[1,j]) & qpn.fich$w_vlpl %in% (vv.list00.piv2[2,j]),])==1) {          
                             resu.temp<-qpn.fich$QPN[qpn.fich$code_poste.num_point %in% (vv.list00.piv2[1,j]) & qpn.fich$w_vlpl %in% (vv.list00.piv2[2,j])]       
                        } else {                                                                                                                                  
                             resu.temp<-"NR"                                                                                                                      
                        }                                                                                                                                         
                        qpn.vect<-c(qpn.vect,resu.temp)                                                                                                           
                   }                                                                                                                                              
                   vv.list00.piv2<-rbind(vv.list00.piv2,qpn.vect)                                                                                                 
                   vv.list00.piv2<-vv.list00.piv2[c(1,2,nrow(vv.list00.piv2),3:(nrow(vv.list00.piv2)-1)),]                                                        
                   nom.dos02<-"02_LISTE_VALEURS_PAR_CHAMP" 
                   #lf_resu_dos<-list.files(paste(cheminR,dos.resu,"\\",sep=""))
                   #if (!(nom.dos02 %in% lf_resu_dos)){dir.create(paste(cheminR,dos.resu,"\\",nom.dos02,"\\",sep=""))}                                             
                   #write.table(vv.list00.piv2,paste(cheminR,dos.resu,"\\",nom.dos02,"\\",vv,".csv",sep=""),row.names=FALSE,col.names=F,sep=";",na="")  
                   lf_resu_dos<-list.files(rep_enq.chem)
                   if (!(nom.dos02 %in% lf_resu_dos)){dir.create(paste(rep_enq.chem,nom.dos02,"/",sep=""))}
                   write.table(vv.list00.piv2,paste(rep_enq.chem,nom.dos02,"/",vv,".csv",sep=""),row.names=FALSE,col.names=F,sep=";",na="",quote=F,fileEncoding="UTF-8")             
                   
#             #3333333333333333333333333333333333333333333333333333333333333333333333 
#             #4444444444444444444444444444444444444444444444444444444444444444444444                   
                   fich.vv.err<-rbind(fich.vv.err,c("",vv,ifelse(nrow(vv.err)==0,nrow(vv.err),nrow(fich.agg)),vv.cont,vdenomb,info.n,info.p,info.r,info.x,info.i))
#             #4444444444444444444444444444444444444444444444444444444444444444444444                   
                   