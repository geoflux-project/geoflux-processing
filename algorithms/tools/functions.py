#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os
import string
import subprocess
import tempfile
import pandas

from PyQt5 import QtSql
import qgis

from qgis.core import * 

def add_layer(uri, layer_name, schema_name, table_name, geom_column, subset_string, id_column, parent_node, position, expanded, visible, style_file):
    uri.setDataSource(schema_name, table_name, geom_column, subset_string, id_column)
    layer = QgsVectorLayer(uri.uri(), layer_name, "postgres")
    if layer.isValid():
        QgsProject.instance().addMapLayer(layer, False)
        node_layer = QgsLayerTreeLayer(layer)
        node_layer.setExpanded(expanded)
        parent_node.insertChildNode(position, node_layer)
        node_layer.setItemVisibilityChecked(visible)
        layer.loadNamedStyle(style_file)
    else:
        raise QgsProcessingException("La couche "+layer_name+" n'a pas pu être chargée.")
    return layer

def pg_array2py_list( pg_array ):
    if (str(pg_array) == "NULL"):
        return []
    else:
        return list(map(int, pg_array[1:len(pg_array)-1].split(",")))

def pg_array2py_str_list( pg_array ):
    if (str(pg_array) == "NULL"):
        return []
    else:
        return list(map(str, pg_array[1:len(pg_array)-1].split(",")))

def clean_sql_string( string ):
    return unicode((str(string)).replace("'", "''")) 

def set_string_from_template( template_filename, values):
    s = ""
    with open(template_filename, "r", encoding="utf-8") as fi:
        # replace every occurence of "%(key)" with its corresponding value
        t = fi.read()
        for k, v in values.items():
            t = t.replace( '%(' + k + ')', v )
        s = s + t 
    s = s.replace('\n', ' ') 
    return s
    
def set_file_from_template( template_filename, values, extension ):
    """Set the SQL file by filling a template with values."""
    filename = ""
    with open(template_filename, "r", encoding="utf-8") as fi:
        with tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", delete=False, suffix="."+extension) as tmpfile:
            filename = tmpfile.name
            # replace every occurence of "%(key)" with its corresponding value
            t = fi.read()
            for k, v in values.items():
                t = t.replace( '%(' + k + ')', v )
            tmpfile.write(t)
    return filename

def transform_external_cmd( cmd ):
    line_cmd = (cmd[0] + ' "' +'" "'.join(cmd[1:]) + '"').replace( '\\', '/' )
    return line_cmd

def execute_external_cmd( cmd ):
    line_cmd = transform_external_cmd( cmd ) 
    r = subprocess.run( line_cmd , capture_output=False )
    return r.returncode

def execute_cmd( cmd, logfile ):
    if logfile:
        try:
            out = open(logfile, "a")
            err = out
        except IOError as err:
            sys.stderr.write("%s : I/O error : %s\n" % (logfile, err))
    else:
        out = sys.stdout
        err = sys.stderr
    retcode = 0
    try:
        out.flush()
        p = subprocess.Popen(cmd, stdin = subprocess.PIPE, stdout = out, stderr = err)
        p.communicate()
        out.write("\n")
        retcode = p.returncode
    except OSError as err:
        sys.stderr.write("Error calling %s : %s \n" % (" ".join(cmd), err))
    if logfile:
        out.close()

def field_to_value_relation(source_layer, source_field_name, target_layer, target_id_field, target_field_name):
    for source_field in source_layer.fields():
        for target_field in target_layer.fields():
            if ((source_field.name() == source_field_name) and (target_field.name() == target_field_name)):
                try:
                    config = {'AllowMulti': False,
                              'AllowNull': False,
                              'FilterExpression': '',
                              'Key': target_id_field,
                              'Layer': target_layer.id(),
                              'LayerName': target_layer.name(), 
                              'NofColumns': 1,
                              'OrderByValue': False,
                              'UseCompleter': False,
                              'Value': target_field.name()}
                    widget_setup = QgsEditorWidgetSetup('ValueRelation', config)
                    source_layer.setEditorWidgetSetup(source_layer.fields().indexFromName( source_field_name ), widget_setup)
                                        
                except:
                    pass
    else:
        return False
    return True   

def field_to_value_map(layer, field_name, map):
    for field in layer.fields():
        if (field.name() == field_name):
            try:
                widget_setup = QgsEditorWidgetSetup('ValueMap', map)
                layer.setEditorWidgetSetup(layer.fields.indexFromName( field_name ), widget_setup)
                
            except:
                pass

def table_columns(db, schema_name, table_name):
    s = "SELECT array_agg(a.attname) as col_names \
         FROM pg_catalog.pg_attribute a LEFT JOIN pg_catalog.pg_attrdef adef ON a.attrelid=adef.adrelid AND a.attnum=adef.adnum \
         WHERE a.attrelid = (SELECT oid FROM pg_catalog.pg_class WHERE relname='"+table_name+"' \
           AND relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = '"+schema_name+"')) \
           AND a.attnum > 0 AND NOT a.attisdropped"
    q = QtSql.QSqlQuery(db)
    q.exec_(unicode(s))
    q.next()
    return pg_array2py_str_list(q.value(0))

def missing_mandatory_columns(db, schema_name, table_name, mandatory_columns):
    existing_columns = table_columns(db, schema_name, table_name)
    missing_columns = list(set(mandatory_columns) - set(existing_columns))
    return missing_columns
    
def missing_mandatory_tables(db, schema_name, mandatory_tables):
    s="SELECT array_agg(table_name::character varying) as table_names FROM information_schema.tables WHERE table_schema ='"+schema_name+"'"
    q = QtSql.QSqlQuery(db)
    q.exec_(unicode(s))
    q.next()
    existing_tables = pg_array2py_str_list(q.value(0))
    missing_tables = list(set(mandatory_tables) - set(existing_tables))
    return missing_tables
