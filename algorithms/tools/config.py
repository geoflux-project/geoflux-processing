#!/usr/bin/env python
# -*- coding: utf-8 -*-
PGRESTORE_EXE="C:/Program Files/PostgreSQL/10/bin/pg_restore.exe"
PGDUMP_EXE="C:/Program Files/PostgreSQL/10/bin/pg_dump.exe"
PSQL_EXE="C:/Program Files/PostgreSQL/10/bin/psql.exe"
SHP2PGSQL_EXE="C:/Program Files/PostgreSQL/10/bin/shp2pgsql.exe"
PGSQL2SHP_EXE="C:/Program Files/PostgreSQL/10/bin/pgsql2shp.exe"
RSCRIPT_EXE="C:/Program Files/R/R-4.1.0/bin/Rscript.exe"
SRID=2154