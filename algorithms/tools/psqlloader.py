#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

#
# Tempus data loader

import os
import sys
import subprocess
from .config import *
from .functions import *

class PsqlLoader:
    def __init__(self, host = "", user = "", port = "", dbname = "", sqlfile = "", filename = "", replacements = {}, logfile = None):
        """Psql Loader constructor

        sqlfile is a text file containing SQL instructions
        
        replacements is a dictionnary containing values for key found in sqlfile to
            be replaced. Every occurence of "%key%" in the file will be replaced
            by the corresponding value in the dictionnary.
        """
        self.sqlfilecontents = sqlfile
        self.filename = ''
        self.replacements = replacements
        self.logfile = logfile
        self.host = host
        self.port = port
        self.dbname = dbname
        self.user = user
    
    def set_sqlfile(self, sqlfile):
        self.filename = sqlfile

    def set_from_template(self, template_filename, values):
        self.filename = set_file_from_template(template_filename, values, "sql")

    def load(self):
        """Execute SQL file in the DB."""
        # call psql with sqlfile
        command = [os.path.abspath(PSQL_EXE)]
        command.append("--host=%s" % self.host)
        command.append("--username=%s" % self.user)
        command.append("--port=%s" % self.port)
        command.append("--dbname=%s" % self.dbname)
        if self.logfile:
            try:
                out = open(self.logfile, "a")
                err = out
            except IOError as err:
                sys.stderr.write("%s : I/O error : %s\n" % (self.logfile, err))
        else:
            out = sys.stdout
            err = sys.stderr
        retcode = 0
        try:
            out.write("======= Executing SQL %s\n" % os.path.basename(self.filename) )
            out.flush()
            p = subprocess.Popen(command + ["-f", self.filename, "-v", "ON_ERROR_STOP=1"], stdin = subprocess.PIPE, stdout = out, stderr = err)
            p.communicate()
            out.write("\n")
            retcode = p.returncode
        except OSError as err:
            sys.stderr.write("Error calling %s : %s \n" % (" ".join(command), err))
        if self.logfile:
            out.close()
