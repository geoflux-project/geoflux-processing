# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Geoflux
                                 A QGIS plugin
 Traitement de données d'enquêtes OD
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2021-01-04
        copyright            : (C) 2021 by Aurélie Bousquet - Cerema
        email                : aurelie.bousquet@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Aurélie Bousquet - Cerema'
__date__ = '2021-01-04'
__copyright__ = '(C) 2021 by Aurélie Bousquet - Cerema'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtGui import QIcon
from PyQt5.QtCore import QCoreApplication,QVariant
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import *
from qgis.core import (
                           QgsProcessing,
                           QgsFeatureSink,
                           QgsProcessingAlgorithm,
                           QgsProcessingParameterFeatureSource,
                           QgsProcessingParameterFeatureSink,
                           QgsProcessingParameterNumber,
                           QgsProcessingParameterBoolean,
                           QgsProcessingParameterString,
                           QgsProcessingParameterExtent,
                           QgsProcessingParameterField,
                           QgsProcessingParameterExpression,
                           QgsProcessingParameterFileDestination,
                           QgsSpatialIndex,
                           QgsGeometry,
                           QgsFeature,
                           QgsCoordinateTransform,
                           QgsCoordinateReferenceSystem
                      )
import codecs
import numpy
import math
import json
import csv
import processing

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink)
from processing.tools import postgis

from .tools.functions import *
from .tools.psqlloader import *
from .tools.config import *
from .tools.standard import *

pluginPath = os.path.dirname(os.path.dirname(__file__))

class TempImportRoadsideSurvey(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    DATABASE = 'DATABASE' 
    SCHEMA = 'SCHEMA' 
    FICHIER_POINT_ENQ = 'FICHIER_POINT_ENQ' 
    FICHIER_INTERVIEW = 'FICHIER_INTERVIEW' 
    FICHIER_POINT_CPT_AUTO = 'FICHIER_POINT_CPT_AUTO' 
    FICHIER_CPT_AUTO = 'FICHIER_CPT_AUTO'
    FICHIER_CPT_MANUEL = 'FICHIER_CPT_MANUEL'
    NON_STANDARD = 'NON_STANDARD'
    FICHIER_QUESTIONNAIRE = 'FICHIER_QUESTIONNAIRE'
    FICHIER_CODIF = 'FICHIER_CODIF'
    OUTPUT1 = 'OUTPUT1'
    OUTPUT2 = 'OUTPUT2'
    OUTPUT3 = 'OUTPUT3'
    OUTPUT4 = 'OUTPUT4'
    OUTPUT5 = 'OUTPUT5'
    OUTPUT6 = 'OUTPUT6'
    OUTPUT7 = 'OUTPUT7'
    LOGFILE = 'LOGFILE'
    
    
    
    def initAlgorithm(self, config):
        db_param = QgsProcessingParameterString(
            self.DATABASE,
            self.tr('Connexion base de données PostgreSQL'))
        db_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.ConnectionWidgetWrapper'}})
        self.addParameter(db_param)
        
        schema_param = QgsProcessingParameterString(
            self.SCHEMA,
            self.tr('Schéma de destination'), '', False, False)
        schema_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.SchemaWidgetWrapper',
                'connection_param': self.DATABASE}})
        self.addParameter(schema_param)
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_QUESTIONNAIRE,
                self.tr("Fichier des questionnaires (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_CODIF,
                self.tr("Fichier des codifications (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.FICHIER_POINT_ENQ,
                self.tr("Couche des points d'enquête (.shp)"),
                types=[QgsProcessing.TypeVectorPoint], 
                defaultValue=None
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_INTERVIEW,
                self.tr("Fichier des interviews (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
                
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_CPT_MANUEL,
                self.tr("Fichier des comptages manuels (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.NON_STANDARD,
                self.tr("Accepter des comptages manuels non standard ?"),
                False, 
                False
            )
        )
        
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.FICHIER_POINT_CPT_AUTO,
                self.tr("Couche des points de comptage automatique (.shp)"),
                types=[QgsProcessing.TypeVectorPoint], 
                defaultValue=None
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_CPT_AUTO,
                self.tr("Fichier des comptages automatiques (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.LOGFILE,
                self.tr('Fichier de log de psql'),
                optional = False, 
                fileFilter = 'Log files (*.log)'
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="temp_connection")        
        connection = self.parameterAsString(parameters, self.DATABASE, context)
        uri = postgis.uri_from_name(connection)
        db.setHostName(uri.host())
        db.setPort(int(uri.port()))
        db.setDatabaseName(uri.database())
        db.setUserName(uri.username())
        db.setPassword(uri.password())
        ok = db.open()
        schema = self.parameterAsString(parameters, self.SCHEMA, context) 
        point_enq = self.parameterAsVectorLayer(parameters, self.FICHIER_POINT_ENQ, context)
        interview = self.parameterAsFile(parameters, self.FICHIER_INTERVIEW, context)
        comptage_manuel = self.parameterAsFile(parameters, self.FICHIER_CPT_MANUEL, context)
        point_comptage_auto = self.parameterAsVectorLayer(parameters, self.FICHIER_POINT_CPT_AUTO, context)
        comptage_auto = self.parameterAsFile(parameters, self.FICHIER_CPT_AUTO, context)
        questionnaire = self.parameterAsFile(parameters, self.FICHIER_QUESTIONNAIRE, context)
        codif = self.parameterAsFile(parameters, self.FICHIER_CODIF, context)
        non_standard = self.parameterAsBoolean(parameters, self.NON_STANDARD, context) 
        log_file = os.path.splitext(self.parameterAsFileOutput(parameters, self.LOGFILE, context))[0]
        
        # Create table structures
        ploader = PsqlLoader(host = uri.host(), user = uri.username(), port = uri.port(), dbname = uri.database(), sqlfile = "", logfile=log_file)
        subs = {}
        subs['schema'] = schema
        subs['table_questionnaire'] = TEMP_TABLE_QUEST_TERRAIN+SUFFIXE_IMPORT
        subs['table_codif'] = TEMP_TABLE_CODIF+SUFFIXE_IMPORT
        subs['table_cpt_auto'] = TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT
        subs['table_cpt_manuel'] = TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT
        sqlfile = pluginPath+"/algorithms/sql/temp_import_roadside_survey.sql"
        if (os.path.isfile(sqlfile)):
            ploader.set_from_template(sqlfile, subs)
            ploader.load()
        
        f = [i.name() for i in point_enq.fields()]
        lst = [value for value in POINT_ENQ_MANDATORY_COL if (value not in f and value != "geom")]
        if (lst != []):
            raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des points d'enquête, colonne(s) """+str(lst)+""" manquante(s).\n""") )
        else: 
            processing.run("qgis:importintopostgis", { 'CREATEINDEX' : True, \
                                                       'DATABASE' : connection, \
                                                       'DROP_STRING_LENGTH' : True, \
                                                       'ENCODING' : 'UTF-8', \
                                                       'FORCE_SINGLEPART' : False, \
                                                       'GEOMETRY_COLUMN' : 'geom', \
                                                       'INPUT' : point_enq, \
                                                       'LOWERCASE_NAMES' : True, \
                                                       'OVERWRITE' : True, \
                                                       'PRIMARY_KEY' : "id", \
                                                       'SCHEMA' : schema, \
                                                       'TABLENAME' : TEMP_TABLE_POINT_ENQ_TERRAIN+SUFFIXE_IMPORT \
                                                     } \
                          )
        
        f = [i.name() for i in point_comptage_auto.fields()]
        lst = [value for value in POINT_CPT_AUTO_MANDATORY_COL if (value not in f and value != "geom")]
        if (lst != []):
            raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des points de comptages automatiques, colonne(s) """+str(lst)+""" manquante(s).\n""") )
        else: 
            processing.run("qgis:importintopostgis", { 'CREATEINDEX' : True, \
                                                       'DATABASE' : connection, \
                                                       'DROP_STRING_LENGTH' : True, \
                                                       'ENCODING' : 'UTF-8', \
                                                       'FORCE_SINGLEPART' : False, \
                                                       'GEOMETRY_COLUMN' : 'geom', \
                                                       'INPUT' : point_comptage_auto, \
                                                       'LOWERCASE_NAMES' : True, \
                                                       'OVERWRITE' : True, \
                                                       'PRIMARY_KEY' : "id", \
                                                       'SCHEMA' : schema, \
                                                       'TABLENAME' : TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT \
                                                     } \
                          )
        
        if (codif != ""): 
            with open(codif, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                lst = [value for value in CODIF_MANDATORY_COL if value not in reader.fieldnames]
                if (lst != []):
                    raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des codifications, colonne(s) """+str(lst)+""" manquante(s).\n""") )
                else:
                    for row in reader:
                        s="INSERT INTO "+schema+"."+TEMP_TABLE_CODIF+SUFFIXE_IMPORT+"( champ, type, controle, modalite, libelle ) \
                           VALUES ('"+unicode(str(row['champ']))+"', '"+unicode(str(row['type']))+"', '"+unicode(str(row['controle']))+"', '"+str(row['modalite'])+"', '"+unicode(str(row['libelle']))+"')"
                        q = QtSql.QSqlQuery(db)
                        q.exec_(unicode(s))
        
        if (questionnaire != ""): 
            with open(questionnaire, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                lst = [value for value in QUEST_MANDATORY_COL if value not in reader.fieldnames]
                if (lst != []):
                    raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des questionnaires, colonne(s) """+str(lst)+""" manquante(s).\n""") )
                else:
                    for row in reader:
                        s="INSERT INTO "+schema+"."+TEMP_TABLE_QUEST_TERRAIN+SUFFIXE_IMPORT+"( code_poste, num_point, ordre, libelle, champ, quest_vl, quest_pl ) \
                           VALUES ('"+str(row['code_poste'])+"', "+str(row['num_point'])+", "+str(row['ordre'])+", '"+clean_sql_string(row['libelle'])+"', '"+str(row['champ'])+"', '"+str(row['quest_vl'])+"', '"+str(row['quest_pl'])+"')"
                        q = QtSql.QSqlQuery(db)
                        q.exec_(unicode(s))
        
        if (comptage_auto != ""): 
            with open(comptage_auto, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                lst = [value for value in CPT_AUTO_MANDATORY_COL if value not in reader.fieldnames]
                if (lst != []):
                    raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des comptages automatiques, colonne(s) """+str(lst)+""" manquante(s).\n""") )
                else:
                    for row in reader:
                        if len(str(row['per'])) == 1:
                            row['per'] = '0'+str(row['per'])
                        elif len(str(row['per'])) == 2:
                            row['per'] = str(row['per'])
                        else:
                            raise QgsProcessingException( self.tr("""Erreur, type de période non défini : """+str(row['per'])))
                        s="INSERT INTO "+schema+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+"( id_point, jour, per, vl, pl ) \
                           VALUES ("+str(row['id_point'])+", '"+str(row['jour'])+"'::date, '"+row['per']+"', "+str(row['vl'])+", "+str(row['pl'])+")"
                        q = QtSql.QSqlQuery(db)
                        q.exec_(unicode(s))
        
        if (comptage_manuel != ""): 
            with open(comptage_manuel, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                lst = [value for value in CPT_MANUEL_MANDATORY_COL if value not in reader.fieldnames]
                lst_opt1 = [value for value in CPT_MANUEL_MANDATORY_COL_OPT1 if value not in reader.fieldnames]
                lst_opt2 = [value for value in CPT_MANUEL_MANDATORY_COL_OPT2 if value not in reader.fieldnames]
                lst_opt = [value for value in CPT_MANUEL_OPT_COL if value in reader.fieldnames]
                if ( non_standard == False ) and ( lst != [] or (lst_opt1 != [] and lst_opt2 != []) ):
                    raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des comptages manuels, colonne(s) """+str(lst + lst_opt1)+""" ou """+str(lst + lst_opt2)+""" manquante(s).\n""") )
                else:
                    for row in reader:
                        if len(str(row['per_enq'])) == 1:
                            row['per'] = '0'+str(row['per_enq'])
                        elif len(str(row['per_enq'])) == 2:
                            row['per'] = str(row['per_enq'])
                        else:
                            raise QgsProcessingException( self.tr("""Erreur, type de période non défini : """+str(row['per'])))
                        s="INSERT INTO "+schema+"."+TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT+"( "+",".join(["\""+col+"\"" for col in CPT_MANUEL_MANDATORY_CHAR_COL])+","+",".join(["\""+col+"\"" for col in CPT_MANUEL_MANDATORY_NUM_COL])+ ") \
                           VALUES ("+",".join(["'"+row[col]+"'" for col in CPT_MANUEL_MANDATORY_CHAR_COL])+","+",".join([row[col] for col in CPT_MANUEL_MANDATORY_NUM_COL])+")"
                        q = QtSql.QSqlQuery(db)
                        q.exec_(unicode(s))
                        
                        # Bloc de code à mettre à jour quand le standard sera définitivement fixé pour les comptages manuels, en fonction de la méthode de redressement qui sera choisie.
                        # Pour le moment, on vérifie qu'on a la distinction des PL soit en TMD FR / SANS TMD FR / ET (version DREAL HdF), soit en 2-3 essieux / 4 essieux et + croisée avec FR / ET (version DTer Est)
                        if (lst_opt1 == []): # Colonnes du standard pour les PL, version DREAL HdF
                            s1="UPDATE "+schema+"."+TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT+" \
                                SET "+",".join(["\""+col+"\"" + " = " + str(row[col]) for col in CPT_MANUEL_MANDATORY_COL_OPT1])+" \
                                WHERE code_poste = '"+row["code_poste"]+"' AND num_point = "+row["num_point"]+" AND per_enq = '"+ row["per_enq"]+"'"
                            q1 = QtSql.QSqlQuery(db)
                            q1.exec_(unicode(s1))
                        
                        if (lst_opt2 == []): # Colonnes du standard pour les PL, version DTer Est
                            s2="UPDATE "+schema+"."+TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT+" \
                                SET "+",".join(["\""+col+"\"" + " = " + str(row[col]) for col in CPT_MANUEL_MANDATORY_COL_OPT2])+" \
                                WHERE code_poste = '"+row["code_poste"]+"' AND num_point = "+row["num_point"]+" AND per_enq = '"+ row["per_enq"]+"'"
                            q2 = QtSql.QSqlQuery(db)
                            q2.exec_(unicode(s2))
                        
                        if ((lst_opt != []) and non_standard): # Colonnes hors standard (pour l'intégration des anciennes enquêtes)
                            s3 = "UPDATE "+schema+"."+TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT+" \
                                  SET "+",".join(["\""+col+"\"" + " = " + str(row[col]) for col in lst_opt])+" \
                                  WHERE code_poste = '"+row["code_poste"]+"' AND num_point = "+row["num_point"]+" AND per_enq = '"+ row["per_enq"]+"'"
                            q2 = QtSql.QSqlQuery(db)
                            q2.exec_(unicode(s2))  
                        elif (lst_opt != []):
                            feedback.pushInfo(self.tr("Les colonnes "+str(lst_opt)+" du fichier des comptages manuels ne font pas partie du standard. Elles n'ont pas été importées. Cochez l'option \"Accepter des comptages manuels non standard ?\" et refaites l'import si vous souhaitez les conserver."))
        
        # Création de la table des interviews
        s = "WITH q AS ( \
                    SELECT avg(ordre) as ordre, champ \
                    FROM "+schema+"."+TEMP_TABLE_QUEST_TERRAIN+SUFFIXE_IMPORT+" \
                    GROUP BY champ\
                ) \
                SELECT champ \
                FROM q \
                ORDER BY ordre"
        q = QtSql.QSqlQuery(db)
        q.exec_(unicode(s))
        
        s2="DROP TABLE IF EXISTS "+schema+"."+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+"; CREATE TABLE "+schema+"."+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+"("
        fields = []
        while q.next():
            fields = fields + [str(q.value(0))]
            if (q.value(0) != "id_itw" and q.value(0) != "num_point" and q.value(0) != "code_controle_od"):
                s2 = s2 + str(q.value(0))+" character varying, "
            else:
                s2 = s2 + str(q.value(0))+" integer, "
        s2 = s2[:len(s2)-2]+");\
        ALTER TABLE "+schema+"."+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+" ADD CONSTRAINT "+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+"_pk PRIMARY KEY(code_poste, num_point, id_itw);"
        q = QtSql.QSqlQuery(db)
        q.exec_(unicode(s2))
        
        # Import des interviews
        if (interview != ""): 
            with open(interview, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                lst = [value for value in fields if value not in reader.fieldnames]
                if (lst != []):
                    raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier interviews, colonne(s) """+str(lst)+""" déclarées dans le questionnaire, manquante(s).\n""") )
                else:
                    for row in reader:
                        s1="INSERT INTO "+schema+"."+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+" ("
                        s2=" VALUES ("
                        for f in fields:
                            s1 = s1 + f +", "
                            if (f != "id_itw" and f != "num_point"):
                                s2 = s2 +"'"+ clean_sql_string(row[f])+"', "
                            else:
                                s2 = s2 + str(row[f])+", "
                        s1 = s1[:len(s1)-2]+")"
                        s2 = s2[:len(s2)-2]+")"
                        q = QtSql.QSqlQuery(db)
                        ok = q.exec_(unicode(s1 + s2))
                        if not ok:
                            raise QgsProcessingException( self.tr("""La ligne """+clean_sql_string(row["id_itw"])+""" du fichier des interviews n'a pas pu être importée. """+unicode(s1 + s2)) )
                        
        
        uri.setDataSource(schema, TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT, None, "", "")            
        vlayer1 = QgsVectorLayer(uri.uri(), "Interviews", "postgres")
        if not vlayer1.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des interviews"""))
        context.temporaryLayerStore().addMapLayer(vlayer1)
        context.addLayerToLoadOnCompletion(vlayer1.id(), QgsProcessingContext.LayerDetails("Interviews", context.project(), self.OUTPUT1))        
            
        uri.setDataSource(schema, TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT, "geom", "", "")            
        vlayer2 = QgsVectorLayer(uri.uri(), "Points comptage auto", "postgres")
        if not vlayer2.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des points de comptage automatique"""))
        context.temporaryLayerStore().addMapLayer(vlayer2)
        context.addLayerToLoadOnCompletion(vlayer2.id(), QgsProcessingContext.LayerDetails("Points comptage auto", context.project(), self.OUTPUT2)) 
        
        uri.setDataSource(schema, TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT, None, "", "")            
        vlayer3 = QgsVectorLayer(uri.uri(), "Comptages automatiques", "postgres")
        if not vlayer3.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des comptages automatiques"""))
        context.temporaryLayerStore().addMapLayer(vlayer3)
        context.addLayerToLoadOnCompletion(vlayer3.id(), QgsProcessingContext.LayerDetails("Comptages automatiques", context.project(), self.OUTPUT3)) 
        
        uri.setDataSource(schema, TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT, None, "", "")            
        vlayer4 = QgsVectorLayer(uri.uri(), "Comptages manuels", "postgres")
        if not vlayer4.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des comptages manuels"""))
        context.temporaryLayerStore().addMapLayer(vlayer4)
        context.addLayerToLoadOnCompletion(vlayer4.id(), QgsProcessingContext.LayerDetails("Comptages manuels", context.project(), self.OUTPUT4)) 
        
        uri.setDataSource(schema, TEMP_TABLE_POINT_ENQ_TERRAIN+SUFFIXE_IMPORT, "geom", "", "")            
        vlayer5 = QgsVectorLayer(uri.uri(), "Points d'enquête", "postgres")
        if not vlayer5.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des points d'enquête"""))
        context.temporaryLayerStore().addMapLayer(vlayer5)
        context.addLayerToLoadOnCompletion(vlayer5.id(), QgsProcessingContext.LayerDetails("Points d'enquête", context.project(), self.OUTPUT5)) 
        
        uri.setDataSource(schema, TEMP_TABLE_QUEST_TERRAIN+SUFFIXE_IMPORT, None, "", "")            
        vlayer6 = QgsVectorLayer(uri.uri(), "Questionnaires", "postgres")
        if not vlayer6.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des questionnaires"""))
        context.temporaryLayerStore().addMapLayer(vlayer6)
        context.addLayerToLoadOnCompletion(vlayer6.id(), QgsProcessingContext.LayerDetails("Questionnaires", context.project(), self.OUTPUT6)) 
        
        uri.setDataSource(schema, TEMP_TABLE_CODIF+SUFFIXE_IMPORT, None, "", "")            
        vlayer7 = QgsVectorLayer(uri.uri(), "Codifications", "postgres")
        if not vlayer7.isValid():
            raise QgsProcessingException(self.tr("""Impossible de charger la table des codifications"""))
        context.temporaryLayerStore().addMapLayer(vlayer7)
        context.addLayerToLoadOnCompletion(vlayer7.id(), QgsProcessingContext.LayerDetails("Codifications", context.project(), self.OUTPUT7)) 
        
        return {}
    
    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "temp_import_roadside_survey"
    
    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(unicode("Importer enquêtes OD routières dans un schéma temporaire"))
    
    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Importer des données")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "import_data"
    
    def icon(self):
        return QIcon(os.path.join(pluginPath, "icons", "icon_import.png"))
    
    def tr(self, string):
        return QCoreApplication.translate('Processing', string)
    
    def shortHelpString(self):
        return self.tr("""
        Import de données d'enquêtes OD terrain dans un schéma temporaire de la base PostgreSQL pour l'apurement et le redressement des échantillons.
        
        Attention : si les tables :
        - """+TEMP_TABLE_ITW_TERRAIN+SUFFIXE_IMPORT+""", 
        - """+TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT+""", 
        - """+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+""", 
        - """+TEMP_TABLE_CPT_MANUEL+SUFFIXE_IMPORT+""", 
        - """+TEMP_TABLE_POINT_ENQ_TERRAIN+SUFFIXE_IMPORT+""", 
        - """+TEMP_TABLE_CODIF+SUFFIXE_IMPORT+""" et 
        - """+TEMP_TABLE_QUEST_TERRAIN+SUFFIXE_IMPORT+"""
        existent déjà dans le schéma cible, elles seront écrasées. 
        """)
    
    def createInstance(self):
        return TempImportRoadsideSurvey()
