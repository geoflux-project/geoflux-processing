# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Geoflux
                                 A QGIS plugin
 Traitement de données d'enquêtes OD
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2021-01-04
        copyright            : (C) 2021 by Aurélie Bousquet - Cerema
        email                : aurelie.bousquet@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Aurélie Bousquet - Cerema'
__date__ = '2021-01-04'
__copyright__ = '(C) 2021 by Aurélie Bousquet - Cerema'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtGui import QIcon
from PyQt5.QtCore import QCoreApplication,QVariant
from qgis.core import *
from qgis.utils import *
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterString,
                       QgsProcessingParameterExtent,
                       QgsProcessingParameterField,
                       QgsProcessingParameterExpression,
                       QgsProcessingParameterFileDestination,
                       QgsSpatialIndex,
                       QgsGeometry,
                       QgsFeature,
                       QgsCoordinateTransform,
                       QgsCoordinateReferenceSystem
                       )
import codecs
import numpy
import math
import json

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink)
from processing.tools import postgis

from .tools.psqlloader import *
from .tools.config import *

pluginPath = os.path.dirname(os.path.dirname(__file__))

class RestoreSQL(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    DATABASE = 'DATABASE'
    FICHIER = 'FICHIER'
    LOGFILE = 'LOGFILE'
    
    def initAlgorithm(self, config):
        db_param = QgsProcessingParameterString(
            self.DATABASE,
            self.tr('Connexion base de données PostgreSQL')
        )
        db_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.ConnectionWidgetWrapper'}})
        self.addParameter(db_param)
                
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER,
                self.tr("Fichier de sauvegarde (.sql)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'SQL files (*.sql)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.LOGFILE,
                self.tr('Fichier de log'),
                optional = False, 
                fileFilter = 'Log files (*.log)'
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        connection = self.parameterAsString(parameters, self.DATABASE, context)
        uri = postgis.uri_from_name(connection)
        fichier = self.parameterAsFile(parameters, self.FICHIER, context)
        log_file = os.path.splitext(self.parameterAsFileOutput(parameters, self.LOGFILE, context))[0]
        
        ploader = PsqlLoader(host = uri.host(), user = uri.username(), port = uri.port(), dbname = uri.database(), sqlfile = "", logfile=log_file)
        subs = {}
        if (os.path.isfile(fichier)):
            ploader.set_sqlfile(fichier)
            ploader.load()
        
        return {}

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "restore_sql"
    
    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(unicode("Restaurer sauvegarde SQL"))

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Administrer la base")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "admin"

    def icon(self):
        return QIcon(os.path.join(pluginPath, "icons", "icon_import.png"))

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)
    
    def shortHelpString(self):
        return self.tr("""
        Restauration d'un fichier de sauvegarde PostgreSQL au format "fichier plat" avec extension .sql.
        Peut contenir une base complète, un schéma ou une table.
        
        - Connexion base de données PostgreSQL : choisir une connexion existante dans QGIS.
        - Fichier de sauvegarde : fichier de sauvegarde. Le nom du schéma à créer est défini dans le fichier de sauvegarde, s'il existe déjà dans la base, une erreur sera générée par l'utilitaire psql. 
        - Fichier de log : choisir le fichier où seront stockées les informations de log de la commande PSQL (ce fichier est utile en cas d'échec de la commande, pour en comprendre les raisons).
        
        NB : vérifiez la compatibilité entre l'utilitaire psql utilisé (voir paramètres) et le serveur PostgreSQL ciblé.
        """)
    
    def createInstance(self):
        return RestoreSQL()
