chcp 65001 > null
%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
	SELECT zone.code_zone, macrozone.code_zone as code_macrozone, corresp_zones.part_zone ^
	FROM %(matrix_schema).corresp_zones JOIN %(matrix_schema).zone ON (corresp_zones.id_zone = zone.id) ^
										JOIN %(matrix_schema).zone macrozone ON (corresp_zones.id_macrozone = macrozone.id) ^
	WHERE zone.id_zonage = %(id_zonage) AND macrozone.id_zonage = %(id_macrozonage) ^
	ORDER BY 1, 2 ^
) TO ^'%(dir_path)/corresp_zonage_%(id_zonage)_%(id_macrozonage).csv^' CSV HEADER DELIMITER ';' ENCODING 'UTF-8'; ^" %(user) 

