chcp 65001 > nul
%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT id_point_cpt as point_cpt, jour, per, duree, vl, pl ^
                    FROM %(survey_schema).cpt_auto_par_jour
                    WHERE id_point_cpt = ANY(ARRAY%(points_cpt)) ^
                    ORDER BY 1, 2, 3, 4 ^
                    ) TO %(dir_path)/%(table_cpt_auto_par_jour).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8'; ^" %(user) 

%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT id_point_cpt as point_cpt, type_jour, case when vacances_sco then 1 when not vacances_sco then 2 else 0 end as vacances_sco, per, duree, date_debut, date_fin, vl, pl ^
                    FROM %(survey_schema).cpt_auto_moy ^
                    WHERE id_point_cpt = ANY(ARRAY%(points_cpt)) ^
                    ORDER BY 1, 2, 3, 4, 5, 6, 7 ) TO %(dir_path)/%(table_cpt_auto_moy).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8' ^" %(user)
                    
%(pgsql2shp_exe) -f %(dir_path)/%(table_point_cpt).shp -h %(host) -p %(port) -u %(user) %(db_name) ^"SELECT id, angle, materiel, route, pr, abs, def_pl, comment, geom ^
                                                                                                         FROM %(survey_schema).point_cpt ^
                                                                                                         WHERE p.id = ANY(ARRAY%(points_cpt)) ^
                                                                                                         ORDER BY 1 ^"
                                                                                                         