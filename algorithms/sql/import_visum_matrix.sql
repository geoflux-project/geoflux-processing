SELECT setval('%(schema_travail).matrice_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_travail).matrice), False);

INSERT INTO %(schema_travail).matrice(nom, id_zonage, scenario, periode, modes, motifs)
VALUES('%(nom_matrice)', (SELECT id FROM %(schema_travail).zonage WHERE nom = '%(nom_zonage)'), '%(nom_scenario)', '%(periode)', ARRAY%(modes), ARRAY%(motifs));
  
DO
$$
DECLARE
ech boolean;
BEGIN
    INSERT INTO %(schema_travail).compo_matrice(id_matrice, id_zone_orig, id_zone_dest, valeur)
        SELECT (SELECT id FROM %(schema_travail).matrice WHERE nom = '%(nom_matrice)'), 
               zone_o.id, 
               zone_d.id, 
               temp.%(col_val)::real
        FROM %(schema_travail).temp JOIN %(schema_travail).zone zone_o ON (temp.%(col_o) = zone_o.code_zone)
                                    JOIN %(schema_travail).zone zone_d ON (temp.%(col_d) = zone_d.code_zone)
                                    JOIN %(schema_travail).zonage ON (zonage.id = zone_o.id_zonage AND zonage.id = zone_d.id_zonage)
        WHERE temp.%(col_val)>0 AND zonage.nom = '%(nom_zonage)';
        
    IF (%(ech) = True) THEN
        UPDATE %(schema_travail).compo_matrice
        SET echantillon = temp.%(col_ech)
        FROM %(schema_travail).temp, %(schema_travail).zone zone_o, %(schema_travail).zone zone_d
        WHERE temp.%(col_o) = zone_o.code_zone AND temp.%(col_d) = zone_d.code_zone AND compo_matrice.id_matrice = (SELECT id FROM %(schema_travail).matrice WHERE nom = '%(nom_matrice)');
    END IF;    
END
$$;

DROP TABLE IF EXISTS %(schema_travail).temp;