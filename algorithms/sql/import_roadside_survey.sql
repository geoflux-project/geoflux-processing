-- Insertion des campagnes d'enquêtes
SELECT setval('%(schema_dest).campagne_enq_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).campagne_enq), false);
INSERT INTO %(schema_dest).campagne_enq(nom, gestionnaire, prive)
SELECT '%(nom_campagne)', %(gestionnaire), false
EXCEPT 
SELECT nom, %(gestionnaire), false
FROM %(schema_dest).campagne_enq
WHERE nom = '%(nom_campagne)';

-- Insertion des intervenants
INSERT INTO %(schema_dest).entreprise_etablissement(siren_siret)
SELECT gest::bigint FROM %(schema_source).%(table_point_enq)
UNION
SELECT command::bigint FROM %(schema_source).%(table_point_enq)
UNION
SELECT amo::bigint FROM %(schema_source).%(table_point_enq)
UNION
SELECT prest::bigint FROM %(schema_source).%(table_point_enq)
UNION
SELECT gest::bigint FROM %(schema_source).%(table_point_cpt_auto)
UNION
SELECT prest::bigint FROM %(schema_source).%(table_point_cpt_auto)
EXCEPT
SELECT siren_siret FROM %(schema_dest).entreprise_etablissement;

UPDATE %(schema_dest).entreprise_etablissement
SET commanditaire = TRUE
FROM %(schema_source).%(table_point_enq)
WHERE %(table_point_enq).command::bigint = entreprise_etablissement.siren_siret;

UPDATE %(schema_dest).entreprise_etablissement
SET prestataire = TRUE
FROM %(schema_source).%(table_point_enq)
WHERE %(table_point_enq).prest::bigint = entreprise_etablissement.siren_siret OR %(table_point_enq).amo::bigint = entreprise_etablissement.siren_siret;

UPDATE %(schema_dest).entreprise_etablissement
SET gestionnaire_route = TRUE
FROM %(schema_source).%(table_point_enq)
WHERE %(table_point_enq).gest::bigint = entreprise_etablissement.siren_siret;

UPDATE %(schema_dest).entreprise_etablissement
SET prestataire = TRUE
FROM %(schema_source).%(table_point_cpt_auto)
WHERE %(table_point_cpt_auto).prest::bigint = entreprise_etablissement.siren_siret;

UPDATE %(schema_dest).entreprise_etablissement
SET gestionnaire_route = TRUE
FROM %(schema_source).%(table_point_cpt_auto)
WHERE %(table_point_cpt_auto).gest::bigint = entreprise_etablissement.siren_siret;

-- Insertion des points de comptage automatiques
DROP TABLE IF EXISTS %(schema_source).%(table_point_cpt_auto)_idmap;
CREATE TABLE %(schema_source).%(table_point_cpt_auto)_idmap 
(
	id serial, 
	id_ini integer
);
SELECT setval('%(schema_source).%(table_point_cpt_auto)_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_cpt), false);

INSERT INTO %(schema_source).%(table_point_cpt_auto)_idmap(id_ini)
SELECT id
FROM %(schema_source).%(table_point_cpt_auto);

INSERT INTO %(schema_dest).point_cpt (id, id_gest, angle, lib_sens, sens_confondus, route, pr, abs, materiel, def_pl, comment, geom, gestionnaire, prive)
SELECT %(table_point_cpt_auto)_idmap.id, gest::bigint, angle, lib_sens, false, route, pr, abs, materiel::smallint, def_pl, comment, geom, %(gestionnaire), false
FROM %(schema_source).%(table_point_cpt_auto) JOIN %(schema_source).%(table_point_cpt_auto)_idmap ON (%(table_point_cpt_auto).id = %(table_point_cpt_auto)_idmap.id_ini);

SELECT setval('%(schema_dest).point_cpt_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_cpt), false);

-- Insertion des comptages automatiques
INSERT INTO %(schema_dest).cpt_auto_par_jour (id_point_cpt, jour, per, duree, vl, pl, gestionnaire)
SELECT id_point, jour::date, per::smallint, %(per_ag_cpt_auto), vl, pl, %(gestionnaire)
FROM %(schema_source).%(table_cpt_auto) JOIN %(schema_source).%(table_point_cpt_auto)_idmap ON (%(table_cpt_auto).id_point = %(table_point_cpt_auto)_idmap.id_ini);

-- Insertion des points d'enquête 
DROP TABLE IF EXISTS %(schema_source).%(table_point_enq)_idmap;
CREATE TABLE %(schema_source).%(table_point_enq)_idmap 
(
	id serial, 
	code_poste character varying, 
	num_point integer
);
SELECT setval('%(schema_source).%(table_point_enq)_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_enq_terrain), false);

WITH q AS (
    SELECT code_poste, num_point, (SELECT id FROM %(schema_dest).campagne_enq WHERE nom = '%(nom_campangne)')
    FROM %(schema_source).%(table_point_enq)
    EXCEPT
    SELECT code_poste_ini, num_point_ini, id_campagne_enq
    FROM %(schema_dest).point_enq_terrain
)
INSERT INTO %(schema_source).%(table_point_enq)_idmap(code_poste, num_point)
SELECT code_poste, num_point
FROM q;

INSERT INTO %(schema_dest).point_enq_terrain (id, code_poste_ini, num_point_ini, angle, lib_sens, date_enq, route, id_gest, pr, abs, 
												 prec_loc, protocole, support, enq_vl, enq_pl, id_command, id_amo, id_prest, id_campagne_enq,
												 comment, nb_enq, nb_pers, id_point_cpt, geom, millesime, gestionnaire, prive)
SELECT %(table_point_enq)_idmap.id, %(table_point_enq).code_poste, %(table_point_enq).num_point, angle, lib_sens, date_enq::date, route, gest::bigint, pr, abs, prec_loc, 
       protocole::smallint, support::smallint, case when enq_vl = '1' then true when enq_vl = '2' then false end, case when enq_pl = '1' then true when enq_pl = '2' then false end, command::bigint, amo::bigint, prest::bigint, (SELECT id FROM %(schema_dest).campagne_enq WHERE nom = 'Matrice nationale'), 
	   comment, nb_enq, nb_pers, (SELECT id FROM %(schema_source).%(table_point_cpt_auto)_idmap WHERE id_ini = point_cpt), geom, millesime, %(gestionnaire), false
FROM %(schema_source).%(table_point_enq)_idmap JOIN %(schema_source).%(table_point_enq) 
	ON (%(table_point_enq)_idmap.code_poste = %(table_point_enq).code_poste AND %(table_point_enq)_idmap.num_point = %(table_point_enq).num_point); 

SELECT setval('%(schema_dest).point_enq_terrain_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_enq_terrain), false);

-- Insertion des comptages manuels
-- Véhicules de tourisme sans remorque ni caravane
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       3 as veh, 
       0 as immat,
       vl, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl is not null;
-- Véhicules de tourisme avec remorque ou caravane + camping-cars
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       4 as veh, 
       0 as immat,
       cc, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE cc is not null;
-- Véhicules de tourisme avec ou sans remorque, avec ou sans caravane + camping-cars
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       2 as veh, 
       0 as immat,
       vl_cc, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl_cc is not null;
-- Véhicules de tourisme avec ou sans remorque, avec ou sans caravane + camping-cars + VUL
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       1 as veh, 
       0 as immat,
       vl_cc_vul, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl_cc_vul is not null;
-- VUL
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       5 as veh, 
       0 as immat,
       vul, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vul is not null;
-- PL
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       6 as veh, 
       0 as immat,
       pl, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl is not null;
-- PL 2 à 3 essieux au sol
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       7 as veh, 
       0 as immat,
       pl_2_3_ess, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_2_3_ess is not null;
-- PL 4 essieux au sol et plus
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       8 as veh, 
       0 as immat,
       pl_4_ess_plus, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_4_ess_plus is not null;
-- PL avec plaques orange (toutes nationalités confondues)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       9 as veh, 
       0 as immat,
       pl_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_tmd is not null;
-- PL sans plaques orange (toutes nationalités confondues)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       10 as veh, 
       0 as immat,
       pl_sans_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_sans_tmd is not null;
-- Véhicules hors du champ de l'enquête
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       15 as veh, 
       0 as immat,
       veh_hors, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE veh_hors is not null;
-- Véhicules de tourisme sans remorque ni caravane, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       3 as veh, 
       1 as immat,
       vl_fr, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl_fr is not null;
-- Véhicules de tourisme sans remorque ni caravane, immatriculés à l'étranger (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       3 as veh, 
       2 as immat,
       vl_et, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl_et is not null;
-- Véhicules de tourisme avec remorque ou caravane + camping-cars, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       4 as veh, 
       1 as immat,
       cc_fr, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE cc_fr is not null;
-- Véhicules de tourisme avec remorque ou caravane + camping-cars, immatriculés à l'étranger (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       4 as veh, 
       2 as immat,
       cc_et, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE cc_et is not null;
-- 2 roues motorisés immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       11 as veh, 
       1 as immat,
       "2rm_fr", 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE "2rm_fr" is not null;
-- 2 roues motorisés immatriculés à l'étranger (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       11 as veh, 
       2 as immat,
       "2rm_et", 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE "2rm_et" is not null;
-- VUL immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       5 as veh, 
       1 as immat,
       vul_fr, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vul_fr is not null;
-- VUL immatriculés à l'étranger (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       5 as veh, 
       2 as immat,
       vul_et, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vul_et is not null;
-- VL hors du champ de l'enquête (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       12 as veh, 
       0 as immat,
       vl_hors, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE vl_hors is not null;
-- PL sans plaques orange, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       10 as veh, 
       1 as immat,
       pl_fr_sans_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_fr_sans_tmd is not null;
-- PL sans plaques orange, immatriculés à l'étranger(colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       10 as veh, 
       2 as immat,
       pl_et_sans_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_et_sans_tmd is not null;
-- PL avec plaques orange, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       9 as veh, 
       1 as immat,
       pl_fr_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_fr_tmd is not null;
-- PL avec plaques orange, immatriculés à l'étranger(colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       9 as veh, 
       2 as immat,
       pl_et_tmd, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_et_tmd is not null;
-- PL à 2 ou 3 essieux au sol, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       7 as veh, 
       1 as immat,
       pl_fr_2_3_ess, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_fr_2_3_ess is not null;
-- PL à 2 ou 3 essieux au sol, immatriculés à l'étranger(colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       7 as veh, 
       2 as immat,
       pl_et_2_3_ess, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_et_2_3_ess is not null;
-- PL à 4 essieux au sol ou plus, immatriculés en France (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       8 as veh, 
       1 as immat,
       pl_fr_4_ess_plus, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_fr_4_ess_plus is not null;
-- PL à 4 essieux au sol ou plus, immatriculés à l'étranger(colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       8 as veh, 
       2 as immat,
       pl_et_4_ess_plus, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE pl_et_4_ess_plus is not null;
-- Bus et cars (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       13 as veh, 
       0 as immat,
       bus_cars, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE bus_cars is not null;
-- Tracteurs (colonne standard)
INSERT INTO %(schema_dest).cpt_manuel (id_point_enq_terrain, per, duree, veh, immat, trafic, gestionnaire)
SELECT %(table_point_enq)_idmap.id, 
       per_enq::smallint, 
       %(per_ag_cpt_man), 
       14 as veh, 
       0 as immat,
       tracteurs, 
       %(gestionnaire)
FROM %(schema_source).%(table_cpt_manuel) JOIN %(schema_source).%(table_point_enq)_idmap 
ON (%(table_cpt_manuel).code_poste = %(table_point_enq)_idmap.code_poste AND %(table_cpt_manuel).num_point = %(table_point_enq)_idmap.num_point)
WHERE tracteurs is not null;

-- Insertion des questions non déclarées dans la base
SELECT setval('%(schema_dest).question_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).question), false);

WITH q AS (
	SELECT DISTINCT champ FROM %(schema_source).%(table_questionnaire)
	EXCEPT
	SELECT nom FROM %(schema_dest).question
	EXCEPT
	SELECT unnest(ARRAY[%(questions_to_delete)])
)
INSERT INTO %(schema_dest).question(nom, libelle, standard, type_rep)
SELECT quest.champ, min(quest.libelle) as libelle, False, min(case when lower(codif.type) = 'codif' then 1
																   when lower(codif.type) = 'zonage' then 3
																   when lower(codif.type) = 'num' then 2
                                                                   when lower(codif.type) = 'char' then 4
                                                              end) as type_rep
FROM %(schema_source).%(table_questionnaire) quest JOIN q ON (quest.champ = q.champ)
                                                   JOIN %(schema_source).%(table_codif) codif ON (codif.champ = quest.champ)
GROUP BY quest.champ; 

-- Insertion des codifications non déclarées dans la base
WITH nouvelles_codif AS 
(
    SELECT DISTINCT ON (n_codif_import.champ, n_codif_import.modalite, n_codif_import.libelle) n_codif_import.champ, n_codif_import.modalite, n_codif_import.libelle
	FROM %(schema_source).%(table_codif) JOIN %(schema_source).%(table_questionnaire) ON (btrim(%(table_codif).champ, ' ') = btrim(%(table_questionnaire).champ, ' '))
    WHERE lower(%(table_codif).controle) = 'codif' 
), nouvelles_codif_agreg AS 
(
	SELECT champ, array_agg(array_to_string(array[modalite, libelle], '-;-') order by modalite) as codif 
    FROM nouvelles_codif
    GROUP BY champ 
), codif_existantes AS 
(
    SELECT question.nom, array_agg(array_to_string(array[codif.code_modalite, codif.libelle], '-;-') order by codif.code_modalite) as codif
    FROM %(schema_dest).question JOIN %(schema_dest).codif ON (codif.id_question = question.id)
    GROUP BY question.nom
), codif AS
(
	SELECT nouvelles_codif_agreg.champ, nouvelles_codif_agreg.codif
	FROM nouvelles_codif_agreg LEFT JOIN codif_existantes ON (nouvelles_codif_agreg.champ = codif_existantes.nom)
	WHERE codif_existantes.nom IS NULL
), dev_codif AS
(
    SELECT champ, (string_to_array(a, '-;-'))[1] as code_modalite, (string_to_array(a, '-;-'))[2] as libelle 
    FROM codif, unnest(codif.codif) a 
)
INSERT INTO %(schema_dest).codif(id_question, code_modalite, libelle)
SELECT question.id, dev_codif.code_modalite, dev_codif.libelle
FROM dev_codif, %(schema_dest).question
WHERE (question.nom = dev_codif.champ) ;  


-- Insertion des questionnaires
INSERT INTO %(schema_dest).questionnaire(id_point_enq_terrain, ordre, id_question, quest_vl, quest_pl)
SELECT idmap.id, 
       %(table_questionnaire).ordre, 
       question.id, 
       case when quest_vl::char = '1' then true when quest_vl::char = '2' then false end, 
       case when quest_pl::char = '1' then true when quest_pl::char = '2' then false end
FROM %(schema_source).%(table_point_enq)_idmap idmap JOIN %(schema_source).%(table_questionnaire) ON (idmap.code_poste = %(table_questionnaire).code_poste and idmap.num_point::char = %(table_questionnaire).num_point::char) 
                                              JOIN %(schema_dest).question ON (question.nom = %(table_questionnaire).champ)
EXCEPT
SELECT idmap.id, 
       %(table_questionnaire).ordre, 
       question.id, 
       case when quest_vl::char = '1' then true when quest_vl::char = '2' then false end, 
       case when quest_pl::char = '1' then true when quest_pl::char = '2' then false end
FROM %(schema_source).%(table_point_enq)_idmap idmap JOIN %(schema_source).%(table_questionnaire) ON (idmap.code_poste = %(table_questionnaire).code_poste and idmap.num_point::char = %(table_questionnaire).num_point::char) 
                                              JOIN %(schema_dest).question ON (question.nom = %(table_questionnaire).champ)
WHERE question.nom = ANY(ARRAY%(questions_to_delete));  

-- Insertion des interviews
DROP TABLE IF EXISTS %(schema_source).%(table_interview)_idmap;
CREATE TABLE %(schema_source).%(table_interview)_idmap 
(
	id serial, 
    code_poste character varying,
    num_point integer,
    id_itw_ini integer
);
SELECT setval('%(schema_source).%(table_interview)_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).interview), false);

INSERT INTO %(schema_source).%(table_interview)_idmap(code_poste, num_point, id_itw_ini)
SELECT code_poste, num_point, id_itw
FROM %(schema_source).%(table_interview)
WHERE red_valide; 

INSERT INTO %(schema_dest).interview(id, id_point_enq_terrain, id_itw_ini, per_hor, vl_pl, lieu_orig, lieu_dest, type_lieu_orig, type_lieu_dest, coef_pe, coef_joe, gestionnaire, prive)
    SELECT %(table_interview)_idmap.id, 
           %(table_point_enq)_idmap.id, 
           %(table_interview).id_itw, 
           (per_enq::integer/4)*4+1, 
           CASE WHEN type_veh::integer<=14 THEN 1 WHEN %(table_interview).type_veh::integer>=15 THEN 2 END, 
           lieu_orig, 
           lieu_dest, 
		   type_lieu_orig, 
		   type_lieu_dest, 
           coef_pe, 
           coef_joe, 
           %(gestionnaire), 
           False
    FROM %(schema_source).%(table_interview) JOIN %(schema_source).%(table_point_enq)_idmap ON (%(table_point_enq)_idmap.code_poste = %(table_interview).code_poste and %(table_point_enq)_idmap.num_point::char = %(table_interview).num_point::char)
                                             JOIN %(schema_source).%(table_interview)_idmap ON (%(table_interview)_idmap.code_poste = %(table_interview).code_poste AND %(table_interview)_idmap.num_point::char = %(table_interview).num_point::char AND %(table_interview)_idmap.id_itw_ini = %(table_interview).id_itw);
