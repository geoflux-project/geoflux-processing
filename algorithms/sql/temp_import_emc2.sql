CREATE FUNCTION IF NOT EXISTS %(schema).creer_tables_depuis_larg_fixes()
RETURNS void
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
	s2, s2, s3, s4, s5, s6 character varying;
	r1, r2 record;
BEGIN
	-- Balayage de la table pour extraire tous les noms de tables à créer
	s1=$$SELECT DISTINCT fichier FROM %(schema).%(table_dessin_fichier)$$;
	FOR r1 IN EXECUTE(s1) LOOP
		s2 = $$SELECT variable, type, position, taille, libelle FROM %(schema).%(table_dessin_fichier) WHERE fichier = '$$ || r1.fichier ||$$' ORDER BY position, taille$$; 
		s3 = $$$$;
		s4 = $$$$;
		s5 = $$$$;
		FOR r2 IN EXECUTE(s2) LOOP
			IF r2.libelle IS NULL THEN r2.libelle=''; END IF; 
			s6=$$(CASE WHEN btrim(substring(data from $$ || r2.position || $$ for $$ || r2.taille || $$), ' ')='' THEN NULL ELSE btrim(substring(data from $$ || r.position || $$ for $$ || r.taille || $$), ' ') END)$$;
			IF (r2.type='date') THEN s6 = $$to_date($$ || s6 || $$)$$ ;
			ELSIF (r2.type='reel') THEN s4= $$replace($$ || s4 || $$, ',', '.')$$;
			END IF; 
			s3=s3 || r2.variable || $$ $$ || r2.type || $$,$$;
			s4=s4 || $$ COMMENT ON COLUMN %(schema).$$ || r1.fichier || $$.$$ || r2.variable || $$ IS '$$ || replace(r2.libelle, $$'$$, $$''$$) || $$';$$;		
			s5=s5 || s6 || $$::$$ || r2.type || $$,$$;
		END LOOP; 
	
		s3=substring(s3 from 1 for length(s3)-1);
		s4=$$CREATE TABLE %(schema).$$ || r1.fichier || $$ ($$ || s4 || $$);$$; 
		s5=$$INSERT INTO %(schema).$$ || r1.fichier || $$( SELECT $$ || substring(s5 from 1 for length(s5)-1) || $$ FROM %(schema).$$ || r1.fichier || $$_import)$$;
		
		RAISE NOTICE '%', s3;
		RAISE NOTICE '%', s4;
		RAISE NOTICE '%', s5;
		
		EXECUTE(s3);
		EXECUTE(s4);
		EXECUTE(s5);
		
		-- Ajout du dictionnaire des variables dans les commentaires de champs
		s2=$$SELECT variable, libelle, string_agg(modalite || $$' : '$$ || libelle, chr(10)) as comment FROM %(schema).%(table_dico_var) GROUP BY variable, libelle$$;
		FOR r2 IN EXECUTE(s2) LOOP
			IF r2.comment IS NULL-- OR char_length(r.comment) > 30000
			THEN r2.comment = $$$$;
			END IF;
			IF r2.libelle IS NULL 
			THEN r2.libelle = $$$$;
			END IF;
			s3 = $$ COMMENT ON COLUMN %(schema).$$ || r1.fichier || $$.$$ || r2.variable || ' IS ' || quote_literal(r2.libelle || chr(10) || r2.comment) ;
			IF s3 IS NOT NULL THEN EXECUTE(s3);
			END IF;
		END LOOP;
		
	END LOOP;
	
	RETURN; 
END
$BODY$;

DROP TABLE IF EXISTS %(schema).%(table_dessin_fichier);
CREATE TABLE %(schema).%(table_dessin_fichier)
(
	fichier character varying,
	variable character varying, 
	type character varying,
	position integer, 
	taille integer, 
	libelle character varying
);

DROP TABLE IF EXISTS %(schema).%(table_dico_var);
CREATE TABLE %(schema).%(table_dico_var)
(
	fichier character varying,
	variable character varying, 
	modalite character varying,
	libelle character varying
);

DROP TABLE IF EXISTS %(schema).%(table_menage);
CREATE TABLE %(schema).%(table_menage)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_personne);
CREATE TABLE %(schema).%(table_personne)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_deplacement);
CREATE TABLE %(schema).%(table_deplacement)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_trajet);
CREATE TABLE %(schema).%(table_trajet)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_boucle);
CREATE TABLE %(schema).%(table_boucle)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_opinion);
CREATE TABLE %(schema).%(table_opinion)
(
	champ character varying PRIMARY KEY
);

DROP TABLE IF EXISTS %(schema).%(table_lib_zone);
CREATE TABLE %(schema).%(table_lib_zone)
(
	type character varying, 
	code character varying, 
	libelle character varying
); 

