chcp 65001 > null
%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
	SELECT zone_o.code_zone as code_zone_o, zone_d.code_zone as code_zone_d, compo_matrice.echant, compo_matrice.valeur ^
	FROM %(matrix_schema).compo_matrice JOIN %(matrix_schema).zone zone_o ON (compo_matrice.id_zone_orig = zone_o.id) ^
										JOIN %(matrix_schema).zone zone_d ON (compo_matrice.id_zone_dest = zone_d.id) ^
	WHERE compo_matrice.id_matrice = %(id_matrix) ^
	ORDER BY 1, 2 ^
) TO ^'%(dir_path)/matrix_%(id_matrix).csv^' CSV HEADER DELIMITER ';' ENCODING 'UTF-8'; ^" %(user) 

