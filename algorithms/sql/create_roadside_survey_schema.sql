CREATE SCHEMA %(survey_schema);
COMMENT ON SCHEMA %(survey_schema) IS 'Données d''enquêtes OD routières en production. Schéma relationnel compatible avec l''outil Géoflux.';
GRANT USAGE ON SCHEMA %(survey_schema) TO PUBLIC;

CREATE TABLE %(survey_schema).gestionnaire_donnee
(
    id serial NOT NULL,
    nom character varying,
    users_group character varying,
    geom geometry(MultiPolygon,2154),
    CONSTRAINT gestionnaire_pkey PRIMARY KEY (id)
);

GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).gestionnaire_donnee TO PUBLIC;
COMMENT ON TABLE %(survey_schema).gestionnaire_donnee IS 'Gestionnaires de la donnée métier';

CREATE TABLE %(survey_schema).entreprise_etablissement
(
    siren_siret bigint NOT NULL,
    nom_court character varying,
    nom_long character varying,
    commanditaire boolean,
    prestataire boolean,
    gestionnaire_route boolean,
    gestionnaire_tc boolean,
    CONSTRAINT entreprise_etablissement_pk PRIMARY KEY (siren_siret)
);

GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).entreprise_etablissement TO PUBLIC;
COMMENT ON TABLE %(survey_schema).entreprise_etablissement
    IS 'Entreprises ou établissements au sens de la base SIRENE de l''INSEE utilisées dans les applications métier. ';
COMMENT ON COLUMN %(survey_schema).entreprise_etablissement.siren_siret
    IS 'Code SIREN (entreprise) ou SIRET (établissement) - Identifiant';
COMMENT ON COLUMN %(survey_schema).entreprise_etablissement.commanditaire
    IS 'Vrai si l''entreprise ou l''établissement peut être commanditaire d''une opération de collecte de données (structures publiques ou assimilées)';
COMMENT ON COLUMN %(survey_schema).entreprise_etablissement.prestataire
    IS 'Vrai si l''entreprise ou l''établissement peut être prestataire (réalisation ou AMO) d''une opération de collecte de données';
COMMENT ON COLUMN %(survey_schema).entreprise_etablissement.gestionnaire_route
    IS 'Vrai si l''entreprise ou l''établissement peut être gestionnaire d''un réseau routier';
COMMENT ON COLUMN %(survey_schema).entreprise_etablissement.gestionnaire_tc
    IS 'Vrai si l''entreprise ou l''établissement peut être gestionnaire d''un réseau TC';

-- Table "campagne_enq"
CREATE TABLE %(survey_schema).campagne_enq
(
    id serial PRIMARY KEY,
    nom character varying NOT NULL,
    comment character varying,
    gestionnaire integer NOT NULL REFERENCES %(survey_schema).gestionnaire_donnee (id) ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    pj character varying, 
    UNIQUE(nom)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).campagne_enq TO PUBLIC;

ALTER TABLE %(survey_schema).campagne_enq ENABLE ROW LEVEL SECURITY;
CREATE POLICY campagne_enq_select ON %(survey_schema).campagne_enq FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY campagne_enq_update ON %(survey_schema).campagne_enq FOR UPDATE USING 
(
    gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY campagne_enq_delete ON %(survey_schema).campagne_enq FOR DELETE USING 
(
    gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);
				
CREATE POLICY campagne_enq_insert ON %(survey_schema).campagne_enq FOR INSERT WITH CHECK (True);                
COMMENT ON TABLE %(survey_schema).campagne_enq IS 'Campagne d''enquêtes = ensemble de points d''enquête réalisés lors d''un même marché ou d''une même série d''interventions terrain. ';

GRANT ALL ON SEQUENCE %(survey_schema).campagne_enq_id_seq TO PUBLIC;
COMMENT ON COLUMN %(survey_schema).campagne_enq.pj IS 'Chemin d''accès au dossier Box contenant les pièces jointes associées à la campagne d''enquête';
COMMENT ON COLUMN %(survey_schema).campagne_enq.prive IS 'Vrai si l''enregistrement ne peut être lu que par son gestionnaire';
COMMENT ON COLUMN %(survey_schema).campagne_enq.gestionnaire IS 'Identifiant du gestionnaire de l''enregistrement';
COMMENT ON COLUMN %(survey_schema).campagne_enq.nom IS 'Nom de la campagne d''enquêtes, doit être unique pour l''ensemble des campagnes';
COMMENT ON COLUMN %(survey_schema).campagne_enq.comment IS 'Informations complémentaires sur la campagne d''enquêtes : commanditaire, contexte, etc.';

-- Table "etude"
CREATE TABLE %(survey_schema).etude
(
    id serial PRIMARY KEY,
    nom character varying NOT NULL,
    comment character varying,
    gestionnaire integer NOT NULL REFERENCES %(survey_schema).gestionnaire_donnee (id) ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    pj character varying, 
    UNIQUE(nom)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).etude TO PUBLIC;

ALTER TABLE %(survey_schema).etude ENABLE ROW LEVEL SECURITY;
CREATE POLICY etude_select ON %(survey_schema).etude FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY etude_update ON %(survey_schema).etude FOR UPDATE USING 
(
    gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY etude_delete ON %(survey_schema).etude FOR DELETE USING 
(
    gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);
				
CREATE POLICY etude_insert ON %(survey_schema).etude FOR INSERT WITH CHECK (True);                
COMMENT ON TABLE %(survey_schema).etude IS 'Etude = contexte de création des points d''enquête fictifs.';

GRANT ALL ON SEQUENCE %(survey_schema).etude_id_seq TO PUBLIC;
COMMENT ON COLUMN %(survey_schema).etude.pj IS 'Chemin d''accès au dossier Box contenant les pièces jointes associées à la campagne d''enquête';
COMMENT ON COLUMN %(survey_schema).etude.prive IS 'Vrai si l''enregistrement ne peut être lu que par son gestionnaire';
COMMENT ON COLUMN %(survey_schema).etude.gestionnaire IS 'Identifiant du gestionnaire de l''enregistrement';
COMMENT ON COLUMN %(survey_schema).etude.nom IS 'Nom de la campagne d''enquêtes, doit être unique pour l''ensemble des campagnes';
COMMENT ON COLUMN %(survey_schema).etude.comment IS 'Informations complémentaires sur la campagne d''enquêtes : commanditaire, contexte, etc.';

-- Table "point_cpt"
CREATE TABLE %(survey_schema).point_cpt
(
    id serial PRIMARY KEY,
    id_gest bigint REFERENCES %(survey_schema).entreprise_etablissement (siren_siret) ON UPDATE CASCADE ON DELETE NO ACTION,
    angle real,
    lib_sens character varying NOT NULL,
    sens_confondus boolean, 
    route character varying,
    pr integer,
    abs integer,
    prec_pos boolean,
    prec_loc character varying,
    materiel smallint,
    def_pl character varying,
    comment text,
    pj character varying,
    geom geometry(Point,2154),
    gestionnaire integer NOT NULL REFERENCES %(survey_schema).gestionnaire_donnee (id)  ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT point_cpt_route_check CHECK (route IS NULL OR ("substring"(route::text, 1, 1) = ANY (ARRAY['N'::text, 'D'::text, 'A'::text, 'E'::text]))),
    CONSTRAINT point_cpt_angle_check CHECK (angle >= 0 and angle <= 360),
    CONSTRAINT point_cpt_materiel_check CHECK ((materiel = ANY(ARRAY[1, 2, 3, 4, 5, 6, 7])) OR materiel IS NULL)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).point_cpt TO PUBLIC;

ALTER TABLE %(survey_schema).point_cpt ENABLE ROW LEVEL SECURITY;

CREATE POLICY point_cpt_select ON %(survey_schema).point_cpt FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_cpt_update ON %(survey_schema).point_cpt FOR UPDATE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_cpt_delete ON %(survey_schema).point_cpt FOR DELETE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )                
); 

CREATE POLICY point_cpt_insert ON %(survey_schema).point_cpt WITH CHECK (True); 

COMMENT ON TABLE %(survey_schema).point_cpt IS 'Point de comptage automatique réalisé sur le terrain, rattaché ou non directement à un point d''enquête. On a généralement un point par sens, parfois plus dans le cas de certaines intersections complexes.';
COMMENT ON COLUMN %(survey_schema).point_cpt.angle IS 'Angle par rapport à l''horizontale (pour définir graphiquement le sens)';
COMMENT ON COLUMN %(survey_schema).point_cpt.lib_sens IS 'Libellé du sens ("Vers...")';
COMMENT ON COLUMN %(survey_schema).point_cpt.sens_confondus IS 'Vrai si les deux sens sont comptés indistinctement';
COMMENT ON COLUMN %(survey_schema).point_cpt.route IS 'Nom de la route';
COMMENT ON COLUMN %(survey_schema).point_cpt.prec_pos IS 'Positionnement géographique précis : vrai ou faux, vrai seulement si la position et l''angle ont été vérifiés depuis la mise en place du nouveau système de capitalisation des enquêtes, faux si les informations étaient insuffisantes pour évaluer la précision de ces deux informations, null si la position n''a pas été vérifiée. ';
COMMENT ON COLUMN %(survey_schema).point_cpt.prec_loc IS 'Précisions texte sur la localisation du point d''enquête';
COMMENT ON COLUMN %(survey_schema).point_cpt.materiel
    IS 'Type de matériel de comptage utilisé : 
    1 = Boucle électromagnétique fixe
    2 = Tubes pneumatiques
    3 = Plaques
    4 = Radars
    5 = Caméras classiques
    6 = Caméras LAPI
    7 = Caméras sur drônes';
COMMENT ON COLUMN %(survey_schema).point_cpt.def_pl IS 'Comment un PL est-il défini ? Par ex. plus de 6 mètres, au moins 3 essieux, etc.';
COMMENT ON COLUMN %(survey_schema).point_cpt.comment IS 'Commentaires sur les conditions de réalisation du point de comptage';
COMMENT ON COLUMN %(survey_schema).point_cpt.pj IS 'Chemin d''accès au dossier Box contenant les pièces jointes associées au point de comptage';

GRANT ALL ON SEQUENCE %(survey_schema).point_cpt_id_seq TO PUBLIC; 

-- Table "point_enq_terrain"
CREATE TABLE %(survey_schema).point_enq_terrain
(
    id serial PRIMARY KEY,
    code_poste_ini character varying,
    num_point_ini integer NOT NULL,
    id_campagne_enq integer REFERENCES %(survey_schema).campagne_enq (id) ON UPDATE CASCADE ON DELETE NO ACTION,
    angle real,
    lib_sens character varying NOT NULL,
    date_enq date,
    route character varying,
    id_gest bigint REFERENCES %(survey_schema).entreprise_etablissement (siren_siret) ON UPDATE CASCADE ON DELETE NO ACTION,
    pr integer,
    abs integer,
    prec_pos boolean,
    prec_loc character varying,
    protocole smallint,
    support smallint,
    enq_vl boolean,
    enq_pl boolean,
    id_command bigint REFERENCES %(survey_schema).entreprise_etablissement (siren_siret) ON UPDATE CASCADE ON DELETE NO ACTION,
    id_amo bigint REFERENCES %(survey_schema).entreprise_etablissement (siren_siret) ON UPDATE CASCADE ON DELETE NO ACTION,
    id_prest bigint REFERENCES %(survey_schema).entreprise_etablissement (siren_siret) ON UPDATE CASCADE ON DELETE NO ACTION,
    comment text,
    nb_enq smallint,
    nb_pers smallint,
    fiable boolean,
    id_point_cpt bigint REFERENCES %(survey_schema).point_cpt(id) ON UPDATE CASCADE ON DELETE NO ACTION,
    pj character varying,
    millesime integer,
    geom geometry(Point, 2154),
    gestionnaire integer NOT NULL REFERENCES %(survey_schema).gestionnaire_donnee (id)  ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT point_enq_terrain_route_check CHECK (route IS NULL OR ("substring"(route::text, 1, 1) = ANY (ARRAY['N'::text, 'D'::text, 'A'::text, 'E'::text]))),
    CONSTRAINT point_enq_terrain_sens_check CHECK (num_point_ini > 0),
    CONSTRAINT point_enq_terrain_protocole_check CHECK ((protocole = ANY(ARRAY[1, 2, 3, 4, 5, 6, 7, 8])) OR protocole IS NULL),
    CONSTRAINT point_enq_terrain_support_check CHECK ((support = ANY (ARRAY[1, 2])) OR support IS NULL), 
    UNIQUE (code_poste_ini, num_point_ini, id_campagne_enq)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).point_enq_terrain TO PUBLIC;

ALTER TABLE %(survey_schema).point_enq_terrain ENABLE ROW LEVEL SECURITY;

CREATE POLICY point_enq_terrain_select ON %(survey_schema).point_enq_terrain FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_enq_terrain_update ON %(survey_schema).point_enq_terrain FOR UPDATE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_enq_terrain_delete ON %(survey_schema).point_enq_terrain FOR DELETE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);  

CREATE POLICY point_enq_terrain_insert ON %(survey_schema).point_enq_terrain WITH CHECK (True); 

COMMENT ON TABLE %(survey_schema).point_enq_terrain IS 'Point d''enquête réalisé sur le terrain, rattaché à un poste. On a généralement un point par sens, parfois plus dans le cas de certaines intersections complexes.';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.code_poste_ini IS 'Nom du poste dans le référentiel original de la campagne d''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.num_point_ini IS 'Numéro du point dans le référentiel original de l''enquête (par exemple, sens n°1 et sens n°2, mais le poste peut être composé de plus de deux points dans certains cas complexes)';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.lib_sens IS 'Libellé du sens ("Vers...")';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.route IS 'Nom de la route, doit commencer par ''N'', ''D'', ''A'', ''E'' ou ''C'', ne pas mettre ''RN'', ''RD'', etc.';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.prec_pos IS 'Positionnement géographique précis : vrai ou faux, vrai seulement si la position et l''angle ont été vérifiés depuis la mise en place du nouveau système de capitalisation des enquêtes, faux si les informations étaient insuffisantes pour évaluer la précision de ces deux informations, null si la position n''a pas été vérifiée. ';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.prec_loc IS 'Précisions texte sur la localisation du point d''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.protocole
    IS 'Type de protocole d''enquête : 
    1 = Distribution enveloppes T
    2 = Enquête par Internet des usagers passés au télépéage
    3 = Feux temporaires
    4 = Feux permanents 
    5 = Arrêt sur le côté
    6 = Arrêt pleine voie
    7 = Enquête embarquée (bateau, train)
    8 = Déviation du flux par une aire de repos';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.support
    IS 'Supports de collecte
    1 = papier, 
    2 = électronique';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.id_command IS 'Identifiant du commanditaire de l''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.id_gest IS 'Identifiant du gestionnaire de la route';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.id_amo IS 'Identifiant du prestataire ayant réalisé l''AMO';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.id_prest IS 'Identifiant du prestataire ayant réalisé le terrain';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.id_campagne_enq IS 'Identifiant de la campagne d''enquêtes à laquelle est rattaché le point';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.comment IS 'Commentaires sur les conditions de réalisation du point d''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.nb_enq IS 'Nombre d''enquêteurs mobilisés simultanément';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.nb_pers IS 'Nombre total de personnes mobilisées simultanément pour l''enquête (manipulateurs de feux, chefs de postes, recenseurs, enquêteurs...)';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.fiable IS 'Vrai si aucun élément n''est mentionné dans les commentaires pour mettre en doute les résultats, faux sinon. ';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.pj IS 'Chemin d''accès au dossier Box contenant les pièces jointes associées au point d''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.angle IS 'Angle par rapport à l''horizontale (pour définir graphiquement le sens)';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.gestionnaire IS 'Gestionnaire de la donnée dans la base';
COMMENT ON COLUMN %(survey_schema).point_enq_terrain.millesime IS 'Millésime du référentiel géographique utilisé pour codifier les zonages / adresses / ports / points frontières';
CREATE INDEX fki_campagne_enq_point_enq_terrain_fk ON %(survey_schema).point_enq_terrain USING btree(id_campagne_enq);

GRANT ALL ON SEQUENCE %(survey_schema).point_enq_terrain_id_seq TO PUBLIC; 

-- Table "point_enq_fictif"
CREATE TABLE %(survey_schema).point_enq_fictif
(
    id serial PRIMARY KEY,
    code_poste_ini character varying,
    num_point_ini integer NOT NULL,
    id_etude integer REFERENCES %(survey_schema).etude (id) ON UPDATE CASCADE ON DELETE SET NULL, 
	angle real,
    lib_sens character varying NOT NULL,
    route character varying,
    prec_loc character varying,
    id_command integer REFERENCES %(survey_schema).entreprise_etablissement(siren_siret),
    id_point_cpt integer REFERENCES %(survey_schema).point_cpt(id),
    comment text,
    annee integer, 
    enq_vl boolean, 
    enq_pl boolean, 
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    geom geometry(Point,2154) NOT NULL,
    CONSTRAINT point_enq_fictif_num_point_ini_check CHECK (num_point_ini > 0),
    CONSTRAINT point_enq_fictif_route_check CHECK (route IS NULL OR ("substring"(route::text, 1, 1) = ANY (ARRAY['N'::text, 'E'::text, 'D'::text, 'B'::text, 'C'::text, 'A'::text]))),
    UNIQUE (code_poste_ini, num_point_ini, id_etude)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).point_enq_fictif TO PUBLIC;

ALTER TABLE %(survey_schema).point_enq_fictif ENABLE ROW LEVEL SECURITY;

CREATE POLICY point_enq_fictif_select ON %(survey_schema).point_enq_fictif FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_enq_fictif_update ON %(survey_schema).point_enq_fictif FOR UPDATE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY point_enq_fictif_delete ON %(survey_schema).point_enq_fictif FOR DELETE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
); 

CREATE POLICY point_enq_fictif_insert ON %(survey_schema).point_enq_fictif FOR INSERT WITH CHECK (True); 

COMMENT ON TABLE %(survey_schema).point_enq_fictif IS 'Point d''enquête fictif, créé par agrégation des données de point d''enquête terrain.';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.code_poste_ini IS 'Nom du poste dans le référentiel original de la campagne d''enquêtes';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.num_point_ini IS 'Numéro du point dans le référentiel original de l''enquête (par exemple, sens n°1 et sens n°2, mais le poste peut être composé de plus de deux points dans certains cas complexes)';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.id_etude IS 'Référence à l''étude pour laquelle le point fictif a été créé. Le triplet (id_etude, code_poste_ini, num_point_ini) est un identifiant unique. ';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.lib_sens IS 'Libellé du sens ("Vers...")';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.angle IS 'Angle par rapport à l''horizontale (pour définir graphiquement le sens)';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.route IS 'Nom de la route';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.prec_loc IS 'Précisions texte sur la localisation du point d''enquête';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.comment IS 'Commentaires : méthode de construction du point';
COMMENT ON COLUMN %(survey_schema).point_enq_fictif.annee IS 'Année de référence utilisée pour reconstituer le point d''enquête';

CREATE INDEX fki_point_enq_fictif_gestionnaire_fkey ON %(survey_schema).point_enq_fictif USING btree(gestionnaire);

CREATE INDEX fki_point_enq_fictif_id_command_fkey ON %(survey_schema).point_enq_fictif USING btree(id_command);
    
CREATE INDEX fki_point_enq_fictif_id_point_cpt_fkey ON %(survey_schema).point_enq_fictif USING btree(id_point_cpt);
    
GRANT ALL ON SEQUENCE %(survey_schema).point_enq_fictif_id_seq TO PUBLIC; 

-- Table "question"
CREATE TABLE %(survey_schema).question
(
    id serial PRIMARY KEY,
    nom character varying NOT NULL,
    libelle character varying,
    standard boolean NOT NULL,
    type_rep smallint NOT NULL, 
    CONSTRAINT question_nom_key UNIQUE (nom),     
    CONSTRAINT question_type_rep_check CHECK (type_rep = ANY (ARRAY[1, 2, 3, 4]))
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).question TO PUBLIC;
COMMENT ON TABLE %(survey_schema).question IS 'Question de l''enquête';
COMMENT ON COLUMN %(survey_schema).question.nom IS 'Nom court, sans caractères spéciaux ni espaces, doit être unique dans la base';
COMMENT ON COLUMN %(survey_schema).question.type_rep IS 'Type de réponse : 1 = champ texte codifié (hors zonage), 2 = champ numérique, 3 = champ de zonage codifié, 4 = texte libre';

GRANT ALL ON SEQUENCE %(survey_schema).question_id_seq TO PUBLIC; 

-- Table "codif"
CREATE TABLE %(survey_schema).codif
(
    id serial PRIMARY KEY,
    id_question integer NOT NULL REFERENCES %(survey_schema).question (id) ON UPDATE CASCADE ON DELETE CASCADE,
    code_modalite character varying,
    libelle character varying NOT NULL,
    CONSTRAINT codif_unique UNIQUE(id_question, code_modalite)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).codif TO PUBLIC;
COMMENT ON TABLE %(survey_schema).codif IS 'Codifications sous forme de listes énumérées';

GRANT ALL ON SEQUENCE %(survey_schema).codif_id_seq TO PUBLIC; 

-- Table "questionnaire"
CREATE TABLE %(survey_schema).questionnaire
(
    id_point_enq_terrain integer REFERENCES %(survey_schema).point_enq_terrain (id) ON UPDATE CASCADE ON DELETE CASCADE,
    ordre smallint,
    id_question integer REFERENCES %(survey_schema).question (id) ON UPDATE CASCADE ON DELETE CASCADE, 
    quest_vl boolean,
    quest_pl boolean,
    CONSTRAINT questionnaire_pkey PRIMARY KEY (id_point_enq_terrain, id_question)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).questionnaire TO PUBLIC;
COMMENT ON COLUMN %(survey_schema).questionnaire.quest_vl IS 'Vrai si la question est présente dans le questionnaire VL';
COMMENT ON COLUMN %(survey_schema).questionnaire.quest_pl IS 'Vrai si la question est présente dans le questionnaire PL';

CREATE INDEX fki_questionnaire_id_question_fkey ON %(survey_schema).questionnaire USING btree(id_question);

-- Table "interview"
DROP TABLE IF EXISTS %(survey_schema).interview;
CREATE TABLE %(survey_schema).interview
(
    id serial NOT NULL PRIMARY KEY,
    id_point_enq_terrain integer NOT NULL REFERENCES %(survey_schema).point_enq_terrain (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    id_itw_ini integer,
	per_hor smallint,
    vl_pl smallint,
	lieu_orig character varying(20),
	lieu_dest character varying(20),
	type_lieu_orig smallint,
	type_lieu_dest smallint,
    coef_pe real,
	coef_joe real,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT interview_id_point_enq_terrain_id_itw_ini_key UNIQUE (id_point_enq_terrain, id_itw_ini),
    CONSTRAINT interview_per_hor_check CHECK (per_hor = ANY (ARRAY[1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93])),
    CONSTRAINT interview_vl_pl_check CHECK (vl_pl = ANY (ARRAY[1, 2])), 
	CONSTRAINT type_lieu_orig_check CHECK (type_lieu_orig = ANY (ARRAY[10,21,22,23,30,40,50,60])), 
	CONSTRAINT type_lieu_dest_check CHECK (type_lieu_dest = ANY (ARRAY[10,21,22,23,30,40,50,60]))
);
ALTER TABLE %(survey_schema).interview ENABLE ROW LEVEL SECURITY;
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).interview TO PUBLIC;

COMMENT ON TABLE %(survey_schema).interview
    IS 'Une interview = une personne interrogée, contient les champs correspondant aux questions obligatoires pour qu''une enquête soit exploitable. Les questions facultatives sont stockées dans les tables "reponse_texte" et "reponse_num" ';
COMMENT ON COLUMN %(survey_schema).interview.id_point_enq_terrain
    IS 'Identifiant du point d''enquête terrain associé à l''enquête';
COMMENT ON COLUMN %(survey_schema).interview.id_itw_ini
    IS 'Identifiant de l''interview dans le référentiel d''origine';
COMMENT ON COLUMN %(survey_schema).interview.per_hor
    IS 'Période horaire de réalisation de l''interview';
COMMENT ON COLUMN %(survey_schema).interview.vl_pl
    IS 'Type de véhicule enquêté';
COMMENT ON COLUMN %(survey_schema).interview.lieu_orig
    IS 'Code standardisé de l''origine dans le millésime du référentiel utilisé pour l''enquête (voir table des points d''enquête terrain)';
COMMENT ON COLUMN %(survey_schema).interview.lieu_dest
    IS 'Code standardisé de la destination dans le millésime du référentiel utilisé pour l''enquête (voir table des points d''enquête terrain)';
COMMENT ON COLUMN %(survey_schema).interview.coef_pe
    IS 'Coefficient de redressement sur la période de l''enquête';
COMMENT ON COLUMN %(survey_schema).interview.coef_joe
    IS 'Coefficient de redressement sur un jour moyen ouvré de la période enquêtée';

CREATE POLICY interview_delete ON %(survey_schema).interview FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY interview_insert ON %(survey_schema).interview FOR INSERT WITH CHECK (true);

CREATE POLICY interview_select ON %(survey_schema).interview FOR SELECT USING 
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY interview_update ON %(survey_schema).interview FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);
GRANT ALL ON SEQUENCE %(survey_schema).interview_id_seq TO PUBLIC; 

-- Table "reponse_texte"
CREATE TABLE %(survey_schema).reponse_texte
(
    id_question integer NOT NULL REFERENCES %(survey_schema).question (id),
    id_interview integer NOT NULL REFERENCES %(survey_schema).interview (id),
    valeur character varying NOT NULL,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT reponse_texte_pk PRIMARY KEY (id_question, id_interview), 
    CONSTRAINT valeur_check CHECK (valeur != 'N' and valeur != 'I')
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).reponse_texte TO PUBLIC;
ALTER TABLE %(survey_schema).reponse_texte ENABLE ROW LEVEL SECURITY;

CREATE POLICY reponse_texte_select ON %(survey_schema).reponse_texte FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY reponse_texte_update ON %(survey_schema).reponse_texte FOR UPDATE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY reponse_texte_delete ON %(survey_schema).reponse_texte FOR DELETE USING 
(
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY reponse_texte_insert ON %(survey_schema).reponse_texte FOR INSERT WITH CHECK (True);   
                               
COMMENT ON TABLE %(survey_schema).reponse_texte IS 'Réponse à une question texte de l''enquête';

-- Table "reponse_texte"
CREATE TABLE %(survey_schema).reponse_num
(
    id_question integer NOT NULL REFERENCES %(survey_schema).question (id),
    id_interview integer NOT NULL REFERENCES %(survey_schema).interview (id),
    valeur character varying NOT NULL,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT reponse_num_pk PRIMARY KEY (id_question, id_interview), 
    CONSTRAINT valeur_check CHECK (regexp_split_to_array(valeur, '') <@ ARRAY['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'X', '.'])
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).reponse_num TO PUBLIC; 
ALTER TABLE %(survey_schema).reponse_num ENABLE ROW LEVEL SECURITY;

CREATE POLICY reponse_num_select ON %(survey_schema).reponse_num FOR SELECT USING
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY reponse_num_update ON %(survey_schema).reponse_num FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY reponse_num_delete ON %(survey_schema).reponse_num FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);
				
CREATE POLICY reponse_num_insert ON %(survey_schema).reponse_num FOR INSERT WITH CHECK (True); 

COMMENT ON TABLE %(survey_schema).reponse_num IS 'Réponse à une question numérique de l''enquête';

DROP TABLE IF EXISTS %(survey_schema).interview_fictive;
CREATE TABLE %(survey_schema).interview_fictive
(
    id serial NOT NULL PRIMARY KEY,
    id_point_enq_fictif integer NOT NULL REFERENCES %(survey_schema).point_enq_fictif (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
    id_itw integer,
	per_hor smallint,
    vl_pl smallint,
	lieu_orig character varying(20),
	lieu_dest character varying(20),
	type_lieu_orig smallint,
	type_lieu_dest smallint,
    coef_pe real,
	coef_joe real,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT interview_fictive_per_hor_check CHECK (per_hor = ANY (ARRAY[1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93])),
    CONSTRAINT interview_fictive_vl_pl_check CHECK (vl_pl = ANY (ARRAY[1, 2])), 
	CONSTRAINT interview_fictive_type_lieu_orig_check CHECK (type_lieu_orig = ANY (ARRAY[10,21,22,23,30,40,50,60])), 
	CONSTRAINT interview_type_lieu_dest_check CHECK (type_lieu_dest = ANY (ARRAY[10,21,22,23,30,40,50,60]))
);
ALTER TABLE %(survey_schema).interview ENABLE ROW LEVEL SECURITY;
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).interview TO PUBLIC;

COMMENT ON TABLE %(survey_schema).interview_fictive
    IS 'Une interview fictive = reprise d''une interview réelle, éventuellement avec un coefficient de redressement différent et une inversion de l''origine et de la destination. ';
COMMENT ON COLUMN %(survey_schema).interview_fictive.id_point_enq_fictif
    IS 'Identifiant du point d''enquête fictif associé à l''interview';
COMMENT ON COLUMN %(survey_schema).interview_fictive.id_itw
    IS 'Identifiant de l''interview dans le point d''enquête terrain d''origine';
COMMENT ON COLUMN %(survey_schema).interview_fictive.per_hor
    IS 'Période horaire de réalisation de l''interview';
COMMENT ON COLUMN %(survey_schema).interview_fictive.vl_pl
    IS 'Type de véhicule enquêté';
COMMENT ON COLUMN %(survey_schema).interview_fictive.lieu_orig
    IS 'Code standardisé de l''origine dans le millésime du référentiel utilisé pour l''enquête (voir table des points d''enquête terrain)';
COMMENT ON COLUMN %(survey_schema).interview_fictive.lieu_dest
    IS 'Code standardisé de la destination dans le millésime du référentiel utilisé pour l''enquête (voir table des points d''enquête terrain)';
COMMENT ON COLUMN %(survey_schema).interview_fictive.coef_pe
    IS 'Coefficient de redressement sur la période de l''enquête';
COMMENT ON COLUMN %(survey_schema).interview_fictive.coef_joe
    IS 'Coefficient de redressement sur un jour moyen ouvré de la période enquêtée';

CREATE POLICY interview_fictive_delete ON %(survey_schema).interview_fictive FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY interview_fictive_insert ON %(survey_schema).interview_fictive FOR INSERT WITH CHECK (true);

CREATE POLICY interview_fictive_select ON %(survey_schema).interview_fictive FOR SELECT USING 
(
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY interview_update ON %(survey_schema).interview_fictive FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);
GRANT ALL ON SEQUENCE %(survey_schema).interview_fictive_id_seq TO PUBLIC; 

-- Table "cpt_manuel"
DROP TABLE IF EXISTS %(survey_schema).cpt_manuel;
CREATE TABLE %(survey_schema).cpt_manuel
(
    id_point_enq_terrain integer NOT NULL REFERENCES %(survey_schema).point_enq_terrain (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION,
    per smallint, 
    duree smallint, 
    veh smallint, 
    immat smallint, 
    trafic integer, 
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL, 
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT cpt_manuel_pkey PRIMARY KEY (id_point_enq_terrain, per, duree, veh, immat),
    CONSTRAINT cpt_manuel_per_check CHECK (per >= 1 AND per <= 96),
    CONSTRAINT cpt_manuel_duree_check CHECK (duree = 15 or duree = 30 or duree = 60),
    CONSTRAINT cpt_manuel_veh_check CHECK (veh>=1 and veh<=15),
    CONSTRAINT cpt_manuel_immat_check CHECK (immat>=0 and immat<=2)  
);
ALTER TABLE %(survey_schema).cpt_manuel ENABLE ROW LEVEL SECURITY;

GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).cpt_manuel TO PUBLIC;

COMMENT ON TABLE %(survey_schema).cpt_manuel IS 'Comptages manuels associés à chaque point d''enquête, couvrant la période enquêtée uniquement.';
COMMENT ON COLUMN %(survey_schema).cpt_manuel.id_point_enq_terrain IS 'Identifiant du point d''enquête terrain associé au comptage manuel';
COMMENT ON COLUMN %(survey_schema).cpt_manuel.per IS 'Période de comptage, au quart d''heure dans le protocole standard, potentiellement à la demi-heure ou à l''heure';
COMMENT ON COLUMN %(survey_schema).cpt_manuel.duree IS 'Période d''agrégation des comptages, en minutes : 15 si au quart d''heure, 30 si à la demi-heure, 60 si à l''heure';
COMMENT ON COLUMN %(survey_schema).cpt_manuel.veh IS 'Type de véhicule compté (agrégation de la codification des types de véhicules dans les enquêtes)
1 = tous véhicules de tourisme + VUL (3+4+5), 
2 = tous véhicules de tourisme (3+4),
3 = véhicules de tourisme sans remorque ni caravane,
4 = véhicules de tourisme avec caravane ou remoque, camping-cars,
5 = VUL, 
6 = tous PL, hors VUL (7+8 ou 9+10),
7 = PL de 2-3 essieux au sol,
8 = PL de plus de 3 essieux au sol,
9 = PL avec plaques orange, 
10 = PL sans plaques orange,
11 = 2RM, 
12 = VL hors du champ de l''enquête (véhicules d''urgence, auto-écoles), 
13 = PL hors du champ de l''enquête (car, bus), 
14 = Tracteurs,
15 = Tous véhicules hors du champ de l''enquête (12+13+14)';

COMMENT ON COLUMN %(survey_schema).cpt_manuel.immat IS '0 = toutes immatriculations, 1 = véhicules immatriculés en France, 2 = véhicules immatriculés à l''étranger';
COMMENT ON COLUMN %(survey_schema).cpt_manuel.trafic IS 'Volume de véhicules comptés';

CREATE POLICY cpt_manuel_delete ON %(survey_schema).cpt_manuel FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_manuel_update ON %(survey_schema).cpt_manuel FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_manuel_insert ON %(survey_schema).cpt_manuel FOR INSERT WITH CHECK (true);

CREATE POLICY cpt_manuel_select ON %(survey_schema).cpt_manuel FOR SELECT USING (
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

-- Table "cpt_auto_par_jour"
DROP TABLE IF EXISTS %(survey_schema).cpt_auto_par_jour;
CREATE TABLE %(survey_schema).cpt_auto_par_jour
(
    id_point_cpt integer NOT NULL REFERENCES %(survey_schema).point_cpt (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION,
    jour date,
    per smallint,
    duree smallint,
    vl smallint, 
	pl smallint,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT cpt_auto_pkey PRIMARY KEY (id_point_cpt, jour, per),
    CONSTRAINT cpt_auto_per_check CHECK (per >= 1 AND per <= 96), 
    CONSTRAINT duree_check CHECK (duree = 1 or duree = 6 or duree = 15 or duree = 30 or duree = 60 or duree = 1440) 
);
ALTER TABLE %(survey_schema).cpt_auto_par_jour ENABLE ROW LEVEL SECURITY; 

GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).cpt_auto_par_jour TO PUBLIC;

COMMENT ON TABLE %(survey_schema).cpt_auto_par_jour IS 'Comptages automatiques jour par jour.';
COMMENT ON COLUMN %(survey_schema).cpt_auto_par_jour.id_point_cpt IS 'Identifiant du point d''enquête terrain associé au comptage manuel';
COMMENT ON COLUMN %(survey_schema).cpt_auto_par_jour.per IS 'Période de comptage, à l''heure dans le protocole standard, potentiellement à la demi-heure ou au quart d''heure';
COMMENT ON COLUMN %(survey_schema).cpt_auto_par_jour.duree IS 'Durée de la période en minutes, 15 pour un comptage au quart d''heure, 30 pour un comptage à la demi-heure, 60 pour un comptage à l''heure. ';
COMMENT ON COLUMN %(survey_schema).cpt_auto_par_jour.vl IS 'Volume de VL comptés';
COMMENT ON COLUMN %(survey_schema).cpt_auto_par_jour.pl IS 'Volume de PL comptés';

CREATE POLICY cpt_auto_par_jour_update ON %(survey_schema).cpt_auto_par_jour FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_auto_par_jour_delete ON %(survey_schema).cpt_auto_par_jour FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_auto_par_jour_insert ON %(survey_schema).cpt_auto_par_jour FOR INSERT WITH CHECK (true);

CREATE POLICY cpt_auto_par_jour_select ON %(survey_schema).cpt_auto_par_jour FOR SELECT USING (
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

-- Table "cpt_auto_moy"
DROP TABLE IF EXISTS %(survey_schema).cpt_auto_moy;
CREATE TABLE %(survey_schema).cpt_auto_moy
(
    id_point_cpt integer NOT NULL REFERENCES %(survey_schema).point_cpt (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION,
    type_jour smallint,
    vacances_sco boolean, 
    per smallint,
    duree smallint,
    date_debut date, 
    date_fin date,
    vl smallint, 
	pl smallint,
    gestionnaire integer REFERENCES %(survey_schema).gestionnaire_donnee (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL,
    prive boolean NOT NULL DEFAULT false,
    CONSTRAINT cpt_auto_moy_pkey PRIMARY KEY (id_point_cpt, type_jour, vacances_sco, per),
    CONSTRAINT cpt_auto_per_check CHECK (per >= 1 AND per <= 96)
); 
ALTER TABLE %(survey_schema).cpt_auto_moy ENABLE ROW LEVEL SECURITY; 
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(survey_schema).cpt_auto_moy TO PUBLIC;

COMMENT ON TABLE %(survey_schema).cpt_auto_moy IS 'Comptages manuels associés à chaque point d''enquête, couvrant la période enquêtée uniquement.';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.id_point_cpt IS 'Identifiant du point d''enquête terrain associé au comptage manuel.';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.per IS 'Période de comptage, à l''heure dans le protocole standard, potentiellement au quart d''heure ou à la demi-heure.';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.duree IS 'Durée de la période en minutes, 15 pour un comptage au quart d''heure, 30 pour un comptage à la demi-heure, 60 pour un comptage à l''heure. ';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.type_jour IS '1 = tous types de jours, 2 = jours ouvrés (lundi au vendredi), 3 = jours ouvrables (lundi au samedi).';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.vacances_sco IS 'Vrai si jours de vacances scolaires de la zone du comptage, faux si hors vacances scolaires de la zone du comptage, NULL si indistincts.';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.date_debut IS 'Premier jour de comptage';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.date_fin IS 'Dernier jour de comptage';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.vl IS 'Volume de VL compté';
COMMENT ON COLUMN %(survey_schema).cpt_auto_moy.pl IS 'Volume de PL compté';

CREATE POLICY cpt_auto_moy_update ON %(survey_schema).cpt_auto_moy FOR UPDATE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_auto_moy_delete ON %(survey_schema).cpt_auto_moy FOR DELETE USING (
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE POLICY cpt_auto_moy_insert ON %(survey_schema).cpt_auto_moy FOR INSERT WITH CHECK (true);

CREATE POLICY cpt_auto_moy_select ON %(survey_schema).cpt_auto_moy FOR SELECT USING (
	prive=FALSE 
	OR 
	gestionnaire in
        (
            SELECT DISTINCT gestionnaire_donnee.id
            FROM pg_group JOIN pg_user ON pg_user.usesysid = ANY (pg_group.grolist)
                          JOIN %(survey_schema).gestionnaire_donnee ON (groname = gestionnaire_donnee.users_group)
            WHERE pg_user.usename = current_user
        )
);

CREATE OR REPLACE FUNCTION %(survey_schema).extract_interviews(
                                                                    id_points_enq_terrain integer[],
                                                                    schemaname character varying,
                                                                    tablename character varying
                                                              ) 
RETURNS void 
LANGUAGE 'plpgsql' AS 
$BODY$

DECLARE
    champs character varying;
	champs_types character varying;
	s text;
    s2 text;
    r record;
    
BEGIN
    FOR r IN SELECT unnest($1) as id 
             EXCEPT
             SELECT id
             FROM %(survey_schema).point_enq_terrain
             WHERE id = ANY($1) 
	LOOP
		RAISE exception 'Le point d''enquête % n''existe pas', r.id::text;
	END LOOP;
    
    SELECT INTO champs_types string_agg(question.nom || ' character varying', ', ' ORDER BY question.nom) 
	FROM %(survey_schema).questionnaire JOIN %(survey_schema).question ON (questionnaire.id_question = question.id)
	WHERE questionnaire.id_point_enq_terrain=ANY($1);
	
    SELECT INTO champs string_agg(question.nom,', ' ORDER BY questionnaire.ordre) 
	FROM %(survey_schema).questionnaire JOIN %(survey_schema).question ON (questionnaire.id_question = question.id)
	WHERE questionnaire.id_point_enq_terrain=ANY($1);
    
	s = $$ DROP TABLE IF EXISTS $$ || schemaname || $$.$$ || tablename || $$;
           CREATE TABLE $$ || schemaname || $$.$$ || tablename || $$ AS 
	       (
		        WITH rep AS (
					select * from crosstab('SELECT id_interview, nom, valeur 
                                            FROM (
                                                SELECT reponse_num.id_interview, question.nom, reponse_num.valeur::character varying, questionnaire.ordre
                                                FROM %(survey_schema).questionnaire JOIN %(survey_schema).question ON (questionnaire.id_question = question.id)
                                                                                    JOIN %(survey_schema).reponse_num ON (reponse_num.id_question = question.id)
                                                WHERE questionnaire.id_point_enq_terrain=ANY(''$$ || $1::character varying || $$'') AND question.type_rep = 2
                                                UNION
                                                SELECT reponse_texte.id_interview, question.nom, reponse_texte.valeur, questionnaire.ordre
                                                FROM %(survey_schema).questionnaire JOIN %(survey_schema).question ON (questionnaire.id_question = question.id)
                                                                                    JOIN %(survey_schema).reponse_texte ON (reponse_texte.id_question = question.id)
                                                WHERE questionnaire.id_point_enq_terrain=ANY(''$$ || $1::character varying || $$'') AND question.type_rep != 2
                                            ) q
											ORDER BY id_interview, ordre', 
										   'SELECT DISTINCT question.nom 
                                            FROM %(survey_schema).questionnaire JOIN %(survey_schema).question ON (questionnaire.id_question = question.id)
											WHERE questionnaire.id_point_enq_terrain=ANY(''$$ || $1::character varying || $$'')
                                            ORDER BY 1') 
					as ct (id_interview integer, $$ || champs_types || $$ ) 
				)
                SELECT campagne_enq.nom as campagne,
                       point_enq_terrain.code_poste_ini as code_poste, 
                       point_enq_terrain.num_point_ini as num_point, 
                       id_itw_ini as id_itw, 
                       per_hor, 
                       vl_pl, 
                       lieu_orig, 
                       lieu_dest, 
					   type_lieu_orig, 
					   type_lieu_dest, 
                       $$ || champs || $$, 
                       coef_pe, 
                       coef_joe
                    FROM %(survey_schema).interview JOIN %(survey_schema).point_enq_terrain ON (interview.id_point_enq_terrain = point_enq_terrain.id)
                                                    JOIN %(survey_schema).campagne_enq ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)
                                                    JOIN rep ON (rep.id_interview = interview.id)
                    WHERE point_enq_terrain.id = ANY('$$ || $1::character varying || $$')
			   );
               ALTER TABLE $$ || schemaname || $$.$$ || tablename || $$ ADD CONSTRAINT $$ || tablename || $$_pkey PRIMARY KEY (code_poste, num_point, campagne, id_itw); $$;
        raise notice '%', s;
		EXECUTE(s);
        
        -- Mise à jour des champs en non-réponse pour les VL
        s=$$
        SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini, point_enq_terrain.num_point_ini, question.nom
        FROM %(survey_schema).point_enq_terrain JOIN %(survey_schema).questionnaire ON (questionnaire.id_point_enq_terrain = point_enq_terrain.id)
                                                JOIN %(survey_schema).question ON (question.id = questionnaire.id_question)
                                                JOIN %(survey_schema).campagne_enq ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)
        WHERE questionnaire.quest_vl AND point_enq_terrain.id = ANY('$$ || $1::character varying || $$')
        $$;
        FOR r IN EXECUTE(s) LOOP
            s2 = $$UPDATE $$ || schemaname || $$.$$ || tablename || $$ 
                   SET $$ || r.nom || $$ = 'X' 
                   WHERE $$ || r.nom || $$ is null AND vl_pl = 1 AND campagne = '$$ || r.campagne || $$' AND code_poste = '$$ || r.code_poste_ini || $$' AND num_point = $$ || r.num_point_ini::character varying;
            EXECUTE(s2); 
        END LOOP;
    
        -- Mise à jour des champs en non-réponse pour les PL
        s=$$
        SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini, point_enq_terrain.num_point_ini, question.nom
        FROM %(survey_schema).point_enq_terrain JOIN %(survey_schema).questionnaire ON (questionnaire.id_point_enq_terrain = point_enq_terrain.id)
                                                JOIN %(survey_schema).question ON (question.id = questionnaire.id_question)
                                                JOIN %(survey_schema).campagne_enq ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)
        WHERE questionnaire.quest_pl AND point_enq_terrain.id = ANY('$$ || $1::character varying || $$')
        $$;
        FOR r IN EXECUTE(s) LOOP
            s2 = $$UPDATE $$ || schemaname || $$.$$ || tablename || $$ 
                   SET $$ || r.nom || $$ = 'X' 
                   WHERE $$ || r.nom || $$ is null AND vl_pl = 2 AND campagne = '$$ || r.campagne || $$' AND code_poste = '$$ || r.code_poste_ini || $$' AND num_point = $$ || r.num_point_ini::character varying;
            EXECUTE(s2);
        END LOOP;
        
        -- Mise à jour des champs non pertinents pour les VL et les PL
        s=$$
        SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini, point_enq_terrain.num_point_ini, question.nom
        FROM %(survey_schema).point_enq_terrain JOIN %(survey_schema).questionnaire ON (questionnaire.id_point_enq_terrain = point_enq_terrain.id)
                                                JOIN %(survey_schema).question ON (question.id = questionnaire.id_question)
                                                JOIN %(survey_schema).campagne_enq ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)
        WHERE point_enq_terrain.id = ANY('$$ || $1::character varying || $$')
        $$;
        FOR r IN EXECUTE(s) LOOP
            s2 = $$UPDATE $$ || schemaname || $$.$$ || tablename || $$ 
                   SET $$ || r.nom || $$ = 'N' 
                   WHERE $$ || r.nom || $$ is null AND campagne = '$$ || r.campagne || $$' AND code_poste = '$$ || r.code_poste_ini || $$' AND num_point = $$ || r.num_point_ini::character varying;
            EXECUTE(s2);           
        END LOOP;
        
    RETURN;
END

$BODY$;



CREATE OR REPLACE FUNCTION %(survey_schema).extract_cpt_manuels(
                                                                    id_points_enq_terrain integer[],
                                                                    schemaname character varying,
                                                                    tablename character varying
                                                               ) 
RETURNS void 
LANGUAGE 'plpgsql' AS 
$BODY$

DECLARE
    champs character varying[];
    liste_champs character varying;
	champs_types character varying;
    intitules_champs character varying;
	s1 text;
    s2 text;
    s3 text;
    r1 record;
    r2 record;
    
BEGIN
    FOR r1 IN SELECT unnest($1) as id 
              EXCEPT
              SELECT id
              FROM %(survey_schema).point_enq_terrain
              WHERE id = ANY($1) 
	LOOP
		RAISE exception 'Le point d''enquête % n''existe pas', r.id::text;
	END LOOP;
    
    WITH q AS (SELECT DISTINCT CASE WHEN ARRAY[veh, immat] = ARRAY[3, 0]::smallint[] THEN 'vl'
                                    WHEN ARRAY[veh, immat] = ARRAY[3, 1]::smallint[] THEN 'vl_fr'
                                    WHEN ARRAY[veh, immat] = ARRAY[3, 2]::smallint[] THEN 'vl_et'
                                    WHEN ARRAY[veh, immat] = ARRAY[4, 0]::smallint[] THEN 'cc'
                                    WHEN ARRAY[veh, immat] = ARRAY[4, 1]::smallint[] THEN 'cc_fr'
                                    WHEN ARRAY[veh, immat] = ARRAY[4, 2]::smallint[] THEN 'cc_et'
                                    WHEN ARRAY[veh, immat] = ARRAY[2, 0]::smallint[] THEN 'vl_cc'
                                    WHEN ARRAY[veh, immat] = ARRAY[1, 0]::smallint[] THEN 'vl_cc_vul'
                                    WHEN ARRAY[veh, immat] = ARRAY[5, 0]::smallint[] THEN 'vul'
                                    WHEN ARRAY[veh, immat] = ARRAY[5, 1]::smallint[] THEN 'vul_fr'
                                    WHEN ARRAY[veh, immat] = ARRAY[5, 2]::smallint[] THEN 'vul_et'
                                    WHEN ARRAY[veh, immat] = ARRAY[11, 0]::smallint[] THEN '2rm'
                                    WHEN ARRAY[veh, immat] = ARRAY[11, 1]::smallint[] THEN '2rm_fr'
                                    WHEN ARRAY[veh, immat] = ARRAY[11, 2]::smallint[] THEN '2rm_et'
                                    WHEN ARRAY[veh, immat] = ARRAY[12, 0]::smallint[] THEN 'vl_hors'
                                    WHEN ARRAY[veh, immat] = ARRAY[6, 0]::smallint[] THEN 'pl'
                                    WHEN ARRAY[veh, immat] = ARRAY[10, 0]::smallint[] THEN 'pl_sans_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[10, 1]::smallint[] THEN 'pl_fr_sans_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[10, 2]::smallint[] THEN 'pl_et_sans_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[9, 0]::smallint[] THEN 'pl_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[9, 1]::smallint[] THEN 'pl_fr_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[9, 2]::smallint[] THEN 'pl_et_tmd'
                                    WHEN ARRAY[veh, immat] = ARRAY[7, 0]::smallint[] THEN 'pl_2_3_ess'
                                    WHEN ARRAY[veh, immat] = ARRAY[7, 1]::smallint[] THEN 'pl_fr_2_3_ess'
                                    WHEN ARRAY[veh, immat] = ARRAY[7, 2]::smallint[] THEN 'pl_et_2_3_ess'
                                    WHEN ARRAY[veh, immat] = ARRAY[8, 0]::smallint[] THEN 'pl_4_ess_plus'
                                    WHEN ARRAY[veh, immat] = ARRAY[8, 1]::smallint[] THEN 'pl_fr_4_ess_plus'
                                    WHEN ARRAY[veh, immat] = ARRAY[8, 2]::smallint[] THEN 'pl_et_4_ess_plus'
                                    WHEN ARRAY[veh, immat] = ARRAY[13, 0]::smallint[] THEN 'bus_cars'
                                    WHEN ARRAY[veh, immat] = ARRAY[14, 0]::smallint[] THEN 'tracteurs'
                                    WHEN ARRAY[veh, immat] = ARRAY[15, 0]::smallint[] THEN 'veh_hors'
                               END AS type_name
               FROM %(survey_schema).cpt_manuel 
               WHERE id_point_enq_terrain = ANY($1) 
               ORDER BY 1)
	SELECT INTO champs array_agg(q.type_name order by q.type_name)
	FROM q; 
    liste_champs = '"' || array_to_string(champs, '", "') || '"';
	champs_types = '"' || array_to_string(champs, '" smallint, "') || '" smallint';
    
    s1 = $$ DROP TABLE IF EXISTS $$ || schemaname || $$.$$ || tablename || $$;
            CREATE TABLE $$ || schemaname || $$.$$ || tablename || $$ AS 
	        (
		        WITH rep AS (
                    select * from crosstab(' SELECT ARRAY[id_point_enq_terrain, per]::integer[] as row_id, 
                                                    CASE WHEN ARRAY[veh, immat] = ARRAY[3, 0]::smallint[] THEN ''vl''
                                                         WHEN ARRAY[veh, immat] = ARRAY[3, 1]::smallint[] THEN ''vl_fr''
                                                         WHEN ARRAY[veh, immat] = ARRAY[3, 2]::smallint[] THEN ''vl_et''
                                                         WHEN ARRAY[veh, immat] = ARRAY[4, 0]::smallint[] THEN ''cc''
                                                         WHEN ARRAY[veh, immat] = ARRAY[4, 1]::smallint[] THEN ''cc_fr''
                                                         WHEN ARRAY[veh, immat] = ARRAY[4, 2]::smallint[] THEN ''cc_et''
                                                         WHEN ARRAY[veh, immat] = ARRAY[2, 0]::smallint[] THEN ''vl_cc''
                                                         WHEN ARRAY[veh, immat] = ARRAY[1, 0]::smallint[] THEN ''vl_cc_vul''
                                                         WHEN ARRAY[veh, immat] = ARRAY[5, 0]::smallint[] THEN ''vul''
                                                         WHEN ARRAY[veh, immat] = ARRAY[5, 1]::smallint[] THEN ''vul_fr''
                                                         WHEN ARRAY[veh, immat] = ARRAY[5, 2]::smallint[] THEN ''vul_et''
                                                         WHEN ARRAY[veh, immat] = ARRAY[11, 0]::smallint[] THEN ''2rm''
                                                         WHEN ARRAY[veh, immat] = ARRAY[11, 1]::smallint[] THEN ''2rm_fr''
                                                         WHEN ARRAY[veh, immat] = ARRAY[11, 2]::smallint[] THEN ''2rm_et''
                                                         WHEN ARRAY[veh, immat] = ARRAY[12, 0]::smallint[] THEN ''vl_hors''
                                                         WHEN ARRAY[veh, immat] = ARRAY[6, 0]::smallint[] THEN ''pl''
                                                         WHEN ARRAY[veh, immat] = ARRAY[10, 0]::smallint[] THEN ''pl_sans_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[10, 1]::smallint[] THEN ''pl_fr_sans_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[10, 2]::smallint[] THEN ''pl_et_sans_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[9, 0]::smallint[] THEN ''pl_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[9, 1]::smallint[] THEN ''pl_fr_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[9, 2]::smallint[] THEN ''pl_et_tmd''
                                                         WHEN ARRAY[veh, immat] = ARRAY[7, 0]::smallint[] THEN ''pl_2_3_ess''
                                                         WHEN ARRAY[veh, immat] = ARRAY[7, 1]::smallint[] THEN ''pl_fr_2_3_ess''
                                                         WHEN ARRAY[veh, immat] = ARRAY[7, 2]::smallint[] THEN ''pl_et_2_3_ess''
                                                         WHEN ARRAY[veh, immat] = ARRAY[8, 0]::smallint[] THEN ''pl_4_ess_plus''
                                                         WHEN ARRAY[veh, immat] = ARRAY[8, 1]::smallint[] THEN ''pl_fr_4_ess_plus''
                                                         WHEN ARRAY[veh, immat] = ARRAY[8, 2]::smallint[] THEN ''pl_et_4_ess_plus''
                                                         WHEN ARRAY[veh, immat] = ARRAY[13, 0]::smallint[] THEN ''bus_cars''
                                                         WHEN ARRAY[veh, immat] = ARRAY[14, 0]::smallint[] THEN ''tracteurs''
                                                         WHEN ARRAY[veh, immat] = ARRAY[15, 0]::smallint[] THEN ''veh_hors''
                                                    END AS type_name, 
                                                    trafic
                                            FROM %(survey_schema).cpt_manuel 
                                            WHERE id_point_enq_terrain = ANY(''$$ || $1::character varying || $$'') 
                                            ORDER BY per, veh, immat', 
                                            'SELECT DISTINCT CASE WHEN ARRAY[veh, immat] = ARRAY[3, 0]::smallint[] THEN ''vl''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[3, 1]::smallint[] THEN ''vl_fr''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[3, 2]::smallint[] THEN ''vl_et''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[4, 0]::smallint[] THEN ''cc''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[4, 1]::smallint[] THEN ''cc_fr''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[4, 2]::smallint[] THEN ''cc_et''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[2, 0]::smallint[] THEN ''vl_cc''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[1, 0]::smallint[] THEN ''vl_cc_vul''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[5, 0]::smallint[] THEN ''vul''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[5, 1]::smallint[] THEN ''vul_fr''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[5, 2]::smallint[] THEN ''vul_et''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[11, 0]::smallint[] THEN ''2rm''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[11, 1]::smallint[] THEN ''2rm_fr''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[11, 2]::smallint[] THEN ''2rm_et''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[12, 0]::smallint[] THEN ''vl_hors''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[6, 0]::smallint[] THEN ''pl''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[10, 0]::smallint[] THEN ''pl_sans_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[10, 1]::smallint[] THEN ''pl_fr_sans_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[10, 2]::smallint[] THEN ''pl_et_sans_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[9, 0]::smallint[] THEN ''pl_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[9, 1]::smallint[] THEN ''pl_fr_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[9, 2]::smallint[] THEN ''pl_et_tmd''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[7, 0]::smallint[] THEN ''pl_2_3_ess''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[7, 1]::smallint[] THEN ''pl_fr_2_3_ess''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[7, 2]::smallint[] THEN ''pl_et_2_3_ess''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[8, 0]::smallint[] THEN ''pl_4_ess_plus''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[8, 1]::smallint[] THEN ''pl_fr_4_ess_plus''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[8, 2]::smallint[] THEN ''pl_et_4_ess_plus''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[13, 0]::smallint[] THEN ''bus_cars''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[14, 0]::smallint[] THEN ''tracteurs''
                                                                  WHEN ARRAY[veh, immat] = ARRAY[15, 0]::smallint[] THEN ''veh_hors''
                                                             END AS type_name
                                             FROM %(survey_schema).cpt_manuel 
                                             WHERE id_point_enq_terrain = ANY(''$$ || $1::character varying || $$'') 
                                             ORDER BY 1') AS ct (row_id integer[], $$ || champs_types || $$ )
                )
                SELECT row_id[1] as id_point_enq_terrain, row_id[2]::smallint as per_enq, $$ || liste_champs || $$
                FROM rep
                ORDER BY 1, 2
            )
        $$;
    raise notice '%', s1;
    EXECUTE(s1);

    RETURN;
END

$BODY$;




