
DO
$$
BEGIN
	IF (%(format)=1) THEN -- Comptages jour par jour
        DROP TABLE IF EXISTS %(schema).%(table_cpt_auto);
        CREATE TABLE %(schema).%(table_cpt_auto)
        (
            id_point smallint not null,
            jour date not null,
            per character(2) not null,
            vl smallint not null,
            pl smallint not null,
            PRIMARY KEY(id_point,jour,per)
        );

        COMMENT ON COLUMN %(schema).%(table_cpt_auto).id_point
            IS 'Identifiant unique du point de comptage (référence au fichier des points de comptage)';

        COMMENT ON COLUMN %(schema).%(table_cpt_auto).jour
            IS 'Jour de réalisation du comptage';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).per
            IS 'Code de la période de réalisation du comptage';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).vl
            IS 'Nombre de VL comptés pendant la période';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).pl
            IS 'Nombre de PL comptés pendant la période';
    
    ELSIF (%(format)=2) THEN -- Comptages moyennés sur plusieurs jours
        DROP TABLE IF EXISTS %(schema).%(table_cpt_auto);
        CREATE TABLE %(schema).%(table_cpt_auto)
        (
            id_point integer NOT NULL,
            type_jour smallint NOT NULL,
            vacances_sco integer NOT NULL,
            per smallint NOT NULL,
            date_debut date,
            date_fin date,
            vl smallint,
            pl smallint,
            PRIMARY KEY(id_point,type_jour, vacances_sco, per, duree, date_debut, date_fin)
        );
        
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).id_point
            IS 'Identifiant unique du point de comptage (référence au fichier des points de comptage)';

        COMMENT ON COLUMN %(schema).%(table_cpt_auto).type_jour
            IS '1 = tous types de jours, 2 = jours ouvrés (lundi au vendredi), 3 = jours ouvrables (lundi au samedi).';
         
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).vacances_sco
            IS '1 = oui, 2 = non, 0 = jours indistincts';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).per
            IS 'Code de la période de réalisation du comptage';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).date_debut
            IS 'Premier jour sur lequel le comptage a été réalisé';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).date_fin
            IS 'Dernier jour sur lequel le comptage a été réalisé';
        
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).vl
            IS 'Nombre de VL comptés pendant la période';
            
        COMMENT ON COLUMN %(schema).%(table_cpt_auto).pl
            IS 'Nombre de PL comptés pendant la période';
    END IF;
END
$$;



