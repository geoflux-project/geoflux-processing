CREATE TABLE %(export_schema).corresp_zones_%(id_zonage)_%(id_macrozonage)_export AS
(
	SELECT zone.code_zone, macrozone.code_zone as code_macrozone, corresp_zones.part_zone 
	FROM %(matrix_schema).corresp_zones JOIN %(matrix_schema).zone ON (corresp_zones.id_zone = zone.id) 
										JOIN %(matrix_schema).zone macrozone ON (corresp_zones.id_macrozone = macrozone.id) 
	WHERE zone.id_zonage = %(id_zonage) AND macrozone.id_zonage = %(id_macrozonage) 
	ORDER BY 1, 2
);

ALTER TABLE %(export_schema).corresp_zones_%(id_zonage)_%(id_macrozonage)_export
ADD CONSTRAINT corresp_zones_%(id_zonage)_%(id_macrozonage)_export_pkey PRIMARY KEY (code_zone, code_macrozone); 
