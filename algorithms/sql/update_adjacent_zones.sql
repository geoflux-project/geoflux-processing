
DO
$$
BEGIN

IF (%(geom_only) = False) THEN
	WITH q AS (
		SELECT zone.id, array_agg(zone_adj.id order by zone_adj.id), st_multi(st_collect(zone_adj.geom)) as geom
		FROM %(matrix_schema).zone join %(matrix_schema).zone zone_adj on (st_touches(zone.geom, zone_adj.geom))
		WHERE zone.id_zonage = %(id_zonage) and zone_adj.id_zonage = %(id_zonage) and zone.id != zone_adj.id
		GROUP BY zone.id
	)
	UPDATE %(matrix_schema).zone
	SET id_zones_adj = q.array_agg, geom_adj= q.geom
	FROM q
	WHERE q.id = zone.id;
	
ELSE
	WITH q AS (
		SELECT zone.id, st_multi(st_collect(zone_adj.geom)) as geom
		FROM %(matrix_schema).zone join %(matrix_schema).zone zone_adj on (zone_adj.id = ANY(zone.id_zones_adj))
		WHERE zone.id_zonage = %(id_zonage) and zone.id != zone_adj.id
		GROUP BY zone.id
	)
	UPDATE %(matrix_schema).zone
	SET geom_adj= q.geom
	FROM q
	WHERE q.id = zone.id;
END IF;

REFRESH MATERIALIZED VIEW %(matrix_schema).adjacent_zones;

END$$;



