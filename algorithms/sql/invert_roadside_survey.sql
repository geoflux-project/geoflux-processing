-- Insertion de l'étude si elle n'existe pas déjà
SELECT setval('%(survey_schema).etude_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(survey_schema).etude), false);
INSERT INTO %(survey_schema).etude(nom, gestionnaire, prive)
SELECT '%(nom_etude)', %(id_gestionnaire), false
EXCEPT 
SELECT nom, %(id_gestionnaire), false
FROM %(survey_schema).etude
WHERE nom = '%(nom_etude)';


WITH terrain AS (
	SELECT code_poste_ini, num_point_ini, angle, lib_sens, route, date_enq, enq_vl, enq_pl, geom
	FROM %(survey_schema).point_enq_terrain
	WHERE id = %(id_point_enq_terrain)
), fictif AS (
	SELECT COALESCE(point_enq_fictif.num_point_ini, 0) as num_point_ini
	FROM terrain LEFT JOIN %(survey_schema).point_enq_fictif ON (terrain.code_poste_ini = point_enq_fictif.code_poste_ini)
	WHERE id_etude = (SELECT id FROM o_geoflux_survey.etude WHERE nom = '%(nom_etude)')
)
INSERT INTO %(survey_schema).point_enq_fictif(id, code_poste_ini, num_point_ini, id_etude, angle, lib_sens, route, comment, annee, enq_vl, enq_pl, gestionnaire, geom)
SELECT (SELECT coalesce(max(id)+1, 1) FROM %(survey_schema).point_enq_fictif) as id, 
	   terrain.code_poste_ini, 
	   greatest((SELECT max(terrain.num_point_ini) FROM terrain), (SELECT max(fictif.num_point_ini) FROM fictif)) as num_point_ini, 
	   (SELECT id FROM o_geoflux_survey.etude WHERE nom = '%(nom_etude)') as id_etude, 
	   (terrain.angle + 180)::integer % 360 as angle, 
	   'Inverse de '|| terrain.lib_sens as lib_sens, 
	   terrain.route, 
	   'Inversion automatique du point d''enquête terrain %(id_point_enq_terrain). ' || CASE WHEN '%(interurban)' = 'True' THEN 'Méthode pour l''interurbain. ' ELSE 'Méthode pour l''urbain. ' END || 
	   CASE WHEN '%(id_point_cpt)' != '' THEN 'Redressement sur des comptages dans le sens opposé.' ELSE 'Pas de modification des coefficients de redressement.' END as comment, 
	   extract('year' from terrain.date_enq) as annee,
	   terrain.enq_vl, 
	   terrain.enq_pl, 
	   %(id_gestionnaire) as gestionnaire, 
	   terrain.geom
FROM terrain; 

SELECT setval('%(survey_schema).point_enq_fictif_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(survey_schema).point_enq_fictif), false);
SELECT setval('%(survey_schema).interview_fictive_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(survey_schema).interview_fictive), false);

DO
$$
BEGIN
	-- Actualisation des coefficients de redressement sur le comptage automatique, si celui-ci est fourni
	IF '%(id_point_cpt)' != '' THEN
		IF '%(interurban)' = 'True' THEN -- Méthode d'inversion en interurbain : on inverse toutes les OD, on redresse sur le trafic double sens et on divise par deux à la fin
			INSERT INTO %(survey_schema).interview_fictive(id_point_enq_fictif, id_itw, per_hor, vl_pl, lieu_orig, lieu_dest, type_lieu_orig, type_lieu_dest, coef_pe, coef_joe, gestionnaire)
				SELECT (SELECT max(id) FROM %(survey_schema).point_enq_fictif), interview.id, per_hor, vl_pl, lieu_dest, lieu_orig, type_lieu_dest, type_lieu_orig, coef_pe, coef_joe, %(id_gestionnaire)
				FROM %(survey_schema).interview
				WHERE interview.id_point_enq_terrain = %(id_point_enq_terrain);			
			
			CREATE TEMPORARY TABLE coef_joe_temp AS (
				WITH cpt AS ( -- Comptages dans les deux sens
					SELECT (per/4)*4+1 as per_hor, sum(vl) as vl, sum(pl) as pl
					FROM %(survey_schema).cpt_auto_par_jour
					WHERE id_point_cpt = %(id_point_cpt) OR id_point_cpt = (SELECT id_point_cpt FROM %(survey_schema).point_enq_terrain WHERE id = %(id_point_enq_terrain))
					GROUP BY (per/4)*4+1
				), enq AS ( -- Enquêtes dans les deux sens
					SELECT per_hor, coalesce(sum(coef_pe) filter(where vl_pl = 1), 0.1) as vl, coalesce(sum(coef_pe) filter(where vl_pl = 2), 0.1) as pl
					FROM (
							SELECT per_hor, vl_pl, coef_pe
							FROM %(survey_schema).interview_fictive
							WHERE id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif)
							UNION
							SELECT per_hor, vl_pl, coef_pe
							FROM %(survey_schema).interview
							WHERE id_point_enq_terrain = %(id_point_enq_terrain)
						  ) q
					GROUP BY per_hor
				)
				-- Calcul de coefficients de redressement par sens, pour chaque période et distinguant VL / PL
				SELECT coalesce(enq.per_hor, cpt.per_hor) as per_hor, 
					   (cpt.vl::double precision / enq.vl)/2 as coef_vl, 
					   (cpt.pl::double precision / enq.pl)/2 as coef_pl
				FROM cpt JOIN enq ON (cpt.per_hor = enq.per_hor)
			);
			
			-- Application des coefficients de redressement VL
			UPDATE %(survey_schema).interview_fictive
			SET coef_joe = coef_joe_temp.coef_vl
			FROM coef_joe_temp
			WHERE interview_fictive.id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif) 
			  AND interview_fictive.vl_pl = 1 
			  AND coef_joe_temp.per_hor = interview_fictive.per_hor; 
			
			-- Application des coefficients de redressement PL
			UPDATE %(survey_schema).interview_fictive
			SET coef_joe = coef_joe_temp.coef_pl
			FROM coef_joe_temp
			WHERE interview_fictive.id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif) 
			  AND interview_fictive.vl_pl = 2 
			  AND coef_joe_temp.per_hor = interview_fictive.per_hor;			
			
		ELSE -- Méthode d'inversion en urbain : on inverse les origines et les destinations et les périodes de pointe du matin et du soir
			INSERT INTO %(survey_schema).interview_fictive(id_point_enq_fictif, id_itw, per_hor, vl_pl, lieu_orig, lieu_dest, type_lieu_orig, type_lieu_dest, coef_pe, coef_joe, gestionnaire)
			(-- Reconstitution de la PPS fictive
				SELECT (SELECT max(id) FROM %(survey_schema).point_enq_fictif), interview.id, %(pps_debut) + interview.per_hor - %(ppm_debut), interview.vl_pl, interview.lieu_dest, interview.lieu_orig, 
					   interview.type_lieu_dest, interview.type_lieu_orig, coef_pe, coef_joe, %(id_gestionnaire)
				FROM %(survey_schema).interview
				WHERE interview.id_point_enq_terrain = %(id_point_enq_terrain) AND interview.per_hor <= %(ppm_fin) AND interview.per_hor >= %(ppm_debut)
			)
			UNION
			(-- Reconstitution de la PPM fictive
				SELECT (SELECT max(id) FROM %(survey_schema).point_enq_fictif), interview.id, %(ppm_debut) + interview.per_hor - %(pps_debut), interview.vl_pl, interview.lieu_dest, interview.lieu_orig, 
					   interview.type_lieu_dest, interview.type_lieu_orig, coef_pe, coef_joe, %(id_gestionnaire)
				FROM %(survey_schema).interview
				WHERE interview.id_point_enq_terrain = %(id_point_enq_terrain) AND interview.per_hor <= %(pps_fin) AND interview.per_hor >= %(pps_debut)
			);
			
			CREATE TEMPORARY TABLE coef_joe_temp AS (
				WITH cpt AS (
					SELECT (per/4)*4+1 as per_hor, sum(vl) as vl, sum(pl) as pl
					FROM %(survey_schema).cpt_auto_par_jour
					WHERE id_point_cpt = %(id_point_cpt)
					GROUP BY (per/4)*4+1
				), enq AS (
					SELECT interview_fictive.per_hor, coalesce(sum(interview_fictive.coef_pe) filter(where vl_pl = 1), 0.1) as vl, coalesce(sum(interview_fictive.coef_pe) filter(where vl_pl = 2), 0.1) as pl
					FROM %(survey_schema).interview_fictive
					WHERE id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif)
					GROUP BY per_hor
				)
			    -- Calcul de coefficients de redressement par période et distinguant VL / PL
				SELECT coalesce(enq.per_hor, cpt.per_hor) as per_hor, 
					   cpt.vl::double precision / enq.vl as coef_vl, 
					   cpt.pl::double precision / enq.pl as coef_pl
				FROM cpt JOIN enq ON (cpt.per_hor = enq.per_hor)
			);
			
			-- Application des coefficients de redressement VL
			UPDATE %(survey_schema).interview_fictive
			SET coef_joe = coef_joe_temp.coef_vl
			FROM coef_joe_temp
			WHERE interview_fictive.id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif) 
			  AND interview_fictive.vl_pl = 1 
			  AND coef_joe_temp.per_hor = interview_fictive.per_hor; 
			
			-- Application des coefficients de redressement PL
			UPDATE %(survey_schema).interview_fictive
			SET coef_joe = coef_joe_temp.coef_pl
			FROM coef_joe_temp
			WHERE interview_fictive.id_point_enq_fictif = (SELECT max(id) FROM %(survey_schema).point_enq_fictif) 
			  AND interview_fictive.vl_pl = 2 
			  AND coef_joe_temp.per_hor = interview_fictive.per_hor;
		END;
	END IF;
END
$$;

