SELECT setval('%(matrix_schema).zonage_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(matrix_schema).zonage), False);

INSERT INTO %(matrix_schema).zonage(nom)
VALUES('%(nom_zonage)');

INSERT INTO %(matrix_schema).zone(id_zonage, code_zone, nom, prec_geo, geom)
SELECT (SELECT id FROM %(matrix_schema).zonage WHERE nom = '%(nom_zonage)'), %(col_code_zone), %(col_lib_zone), %(col_prec_geo), st_transform(st_force2d(geom), %(srid))
FROM %(temp_schema).zonage_import;

DROP TABLE IF EXISTS %(temp_schema).zonage_import; 
