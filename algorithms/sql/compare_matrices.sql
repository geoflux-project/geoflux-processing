
-- Construction des indicateurs par OD sur les zones
DROP TABLE IF EXISTS %(schema_dest).geoflux_compare_matrices_od;
CREATE TABLE %(schema_dest).geoflux_compare_matrices_od AS (
	SELECT (%(schema_matrices).prepare_matrices_compar(ARRAY[%(liste_matrices_1)], ARRAY[%(liste_matrices_2)], %(id_zonage_cible))).*
);

UPDATE %(schema_dest).geoflux_compare_matrices_od
SET valeur1 = %(facteur1) * valeur1;

UPDATE %(schema_dest).geoflux_compare_matrices_od
SET valeur2 = %(facteur2) * valeur2;

ALTER TABLE %(schema_dest).geoflux_compare_matrices_od
ADD COLUMN id_macrozone_orig integer,
ADD COLUMN id_macrozone_dest integer,
ADD COLUMN distance integer, 
ADD COLUMN classe_dist smallint, 
ADD COLUMN geom Geometry(Linestring, 2154), 
ADD COLUMN adj_win_cor real, 
ADD COLUMN adj_win_ssim real;

DO
$$
IF '%(id_macrozonage)' != '' THEN
	WITH q AS (
				SELECT matrice.id_zone_orig, matrice.id_zone_dest, macrozone_o.id as id_macrozone_orig, macrozone_d.id as id_macrozone_dest
				FROM %(schema_dest).geoflux_compare_matrices_od matrice JOIN %(schema_matrices).corresp_zones corresp_zones_o ON (corresp_zones_o.id_zone = matrice.id_zone_orig)
																		JOIN %(schema_matrices).corresp_zones corresp_zones_d ON (corresp_zones_d.id_zone = matrice.id_zone_dest)
																		JOIN %(schema_matrices).zone macrozone_o ON (macrozone_o.id = corresp_zones_o.id_macrozone)
																		JOIN %(schema_matrices).zone macrozone_d ON (macrozone_d.id = corresp_zones_d.id_macrozone)
				WHERE macrozone_o.id_zonage = %(id_macrozonage) AND macrozone_d.id_zonage = %(id_macrozonage)
	)
	UPDATE %(schema_dest).geoflux_compare_matrices_od
	SET id_macrozone_orig = q.id_macrozone_orig, id_macrozone_dest = q.id_macrozone_dest
	FROM q
	WHERE q.id_zone_orig = geoflux_compare_matrices_od.id_zone_orig AND q.id_zone_dest = geoflux_compare_matrices_od.id_zone_dest;
END IF;
$$;

UPDATE %(schema_dest).geoflux_compare_matrices_od
SET distance = distance.dist_vol_oiseau
FROM %(schema_matrices).distance
WHERE geoflux_compare_matrices_od.id_zone_orig = distance.id_zone_orig AND geoflux_compare_matrices_od.id_zone_dest = geoflux_compare_matrices_od.id_zone_dest;

WITH distance_classes AS (
    SELECT row_number() over() -1 as dist_class_id, lag(class_max) over() as class_min, class_max 
    FROM (SELECT unnest(ARRAY[%(classes_dist)]) as class_max ORDER BY 1) q
    OFFSET 1
), max_class AS (
    SELECT max(dist_class_id) + 1, max(class_max), (SELECT max(distance) FROM %(schema_dest).geoflux_compare_matrices_od)
    FROM distance_classes
), all_distance_classes AS (
    SELECT * FROM distance_classes
    UNION
    SELECT * FROM max_class
)
UPDATE %(schema_dest).geoflux_compare_matrices_od
SET classe_dist = all_distance_classes.dist_class_id
FROM all_distance_classes 
WHERE (all_distance_classes.class_min <= geoflux_compare_matrices_od.distance) AND (geoflux_compare_matrices_od.distance <= all_distance_classes.class_max);

UPDATE %(schema_dest).geoflux_compare_matrices_od
SET geom = st_makeline(st_pointonsurface(zone_orig.geom), st_pointonsurface(zone_dest.geom))
FROM %(schema_matrices).zone zone_orig, %(schema_matrices).zone zone_dest
WHERE zone_orig.id = geoflux_compare_matrices_od.id_zone_orig AND zone_dest.id = geoflux_compare_matrices_od.id_zone_dest;
                                                                                             
UPDATE %(schema_dest).geoflux_compare_matrices_od
SET adj_win_cor = q.correlation, 
    adj_win_ssim = q.ssim
FROM (
		SELECT geoflux_compare_matrices_od.id_zone_orig, 
	           geoflux_compare_matrices_od.id_zone_dest, 
	           round(((covar_pop(valeurs_win.valeur1, valeurs_win.valeur2) + power(10, -2)/2) / 
					  (stddev_pop(valeurs_win.valeur1) * stddev_pop(valeurs_win.valeur2)+ power(10, -2)/2)
					 )::numeric, 2) as correlation, 
		       round(( ((2*avg(valeurs_win.valeur1)*avg(valeurs_win.valeur2) + power(10, -10)) * 
						(2*covar_pop(valeurs_win.valeur1, valeurs_win.valeur2) + power(10, -2)))
					  /(((avg(valeurs_win.valeur1))^2 + (avg(valeurs_win.valeur2))^2 + power(10, -10)) * 
						((stddev_pop(valeurs_win.valeur1))^2 + (stddev_pop(valeurs_win.valeur2))^2 + power(10, -2))
					   ))::numeric, 2) as ssim
	    FROM %(schema_dest).geoflux_compare_matrices_od JOIN %(schema_matrices).adjacent_zones as win_orig ON (win_orig.id_zone = geoflux_compare_matrices_od.id_zone_orig)
					  JOIN %(schema_matrices).adjacent_zones as win_dest ON (win_dest.id_zone = geoflux_compare_matrices_od.id_zone_dest)
					  JOIN %(schema_dest).geoflux_compare_matrices_od as valeurs_win ON (valeurs_win.id_zone_orig = win_orig.id_zone_adj AND valeurs_win.id_zone_dest = win_dest.id_zone_adj)
        GROUP BY geoflux_compare_matrices_od.id_zone_orig, geoflux_compare_matrices_od.id_zone_dest
) q
WHERE q.id_zone_orig = geoflux_compare_matrices_od.id_zone_orig AND q.id_zone_dest = geoflux_compare_matrices_od.id_zone_dest;

-- Construction des indicateurs de comparaison par zone
DROP TABLE IF EXISTS %(schema_dest).geoflux_compare_matrices_zone;
CREATE TABLE %(schema_dest).geoflux_compare_matrices_zone AS 
WITH agreg_od_emissions AS
(
	SELECT id_zone_orig as id_zone, sum(valeur1) as valeur1, sum(valeur2) as valeur2, avg(adj_win_cor) as o_adj_win_cor, avg(adj_win_ssim) as o_adj_win_mssim
	FROM %(schema_dest).geoflux_compare_matrices_od
	GROUP BY id_zone_orig
	ORDER BY 1, 2
), agreg_od_attractions AS
(
	SELECT id_zone_dest as id_zone, sum(valeur1) as valeur1, sum(valeur2) as valeur2, avg(adj_win_cor) as d_adj_win_cor, avg(adj_win_ssim) as d_adj_win_mssim
	FROM %(schema_dest).geoflux_compare_matrices_od
	GROUP BY id_zone_dest
	ORDER BY 1, 2
)
	SELECT agreg_od_emissions.id_zone, 
		   agreg_od_emissions.valeur1 as emissions1, 
		   agreg_od_emissions.valeur2 as emissions2, 
		   agreg_od_attractions.valeur1 as attractions1, 
		   agreg_od_attractions.valeur2 as attractions2, 
       	   agreg_od_emissions.o_adj_win_cor, 
	       agreg_od_attractions.d_adj_win_cor, 
	       agreg_od_emissions.o_adj_win_mssim, 
	       agreg_od_attractions.d_adj_win_mssim	
	FROM agreg_od_emissions JOIN agreg_od_attractions ON (agreg_od_emissions.id_zone = agreg_od_attractions.id_zone); 

ALTER TABLE %(schema_dest).geoflux_compare_matrices_zone
ADD COLUMN o_nlod real, 
ADD COLUMN d_nlod real, 
ADD COLUMN o_struct_nlod real, 
ADD COLUMN d_struct_nlod real, 
ADD COLUMN geom Geometry(MultiPolygon, 2154), 
ADD COLUMN reagreg boolean; 

WITH emissions AS (
	SELECT id_zone_orig, 
		   array_agg(valeur1 ORDER BY valeur1 DESC)::numeric[] as valeurs1, 
		   array_agg(id_zone_dest ORDER BY valeur1 DESC) as zones1, 
		   array_agg(valeur2 ORDER BY valeur2 DESC)::numeric[] as valeurs2, 
		   array_agg(id_zone_dest ORDER BY valeur2 DESC) as zones2
	FROM %(schema_dest).geoflux_compare_matrices_od
	GROUP BY id_zone_orig
), attractions AS (
	SELECT id_zone_dest, 
		   array_agg(valeur1 ORDER BY valeur1 DESC)::numeric[] as valeurs1, 
		   array_agg(id_zone_orig ORDER BY valeur1 DESC) as zones1, 
		   array_agg(valeur2 ORDER BY valeur2 DESC)::numeric[] as valeurs2, 
		   array_agg(id_zone_orig ORDER BY valeur2 DESC) as zones2
	FROM %(schema_dest).geoflux_compare_matrices_od
	GROUP BY id_zone_dest
), levenstein AS (
SELECT emissions.id_zone_orig as id_zone, 
	   round(%(schema_matrices).normalized_levenstein_distance(emissions.valeurs1, emissions.zones1, emissions.valeurs2, emissions.zones2),2) as emissions_nlod, 
	   round(%(schema_matrices).normalized_levenstein_distance(attractions.valeurs1, attractions.zones1, attractions.valeurs2, attractions.zones2),2) as attractions_nlod,
	   round(%(schema_matrices).normalized_structure_levenstein_distance(emissions.valeurs1, emissions.zones1, emissions.valeurs2, emissions.zones2),2) as emissions_struct_nlod, 
	   round(%(schema_matrices).normalized_structure_levenstein_distance(attractions.valeurs1, attractions.zones1, attractions.valeurs2, attractions.zones2),2) as attractions_struct_nlod						
FROM emissions JOIN attractions ON (emissions.id_zone_orig = attractions.id_zone_dest)
)
UPDATE %(schema_dest).geoflux_compare_matrices_zone
SET o_nlod = levenstein.emissions_nlod, 
    d_nlod = levenstein.attractions_nlod, 
	o_struct_nlod = levenstein.emissions_struct_nlod, 
	d_struct_nlod = levenstein.attractions_struct_nlod
FROM levenstein
WHERE levenstein.id_zone=geoflux_compare_matrices_zone.id_zone; 

UPDATE %(schema_dest).geoflux_compare_matrices_zone
SET geom = zone.geom
FROM %(schema_matrices).zone 
WHERE (zone.id = geoflux_compare_matrices_zone.id_zone); 

UPDATE %(schema_dest).geoflux_compare_matrices_zone
SET reagreg = geoflux_compare_matrices_od.reagreg_orig
FROM %(schema_dest).geoflux_compare_matrices_od 
WHERE (geoflux_compare_matrices_od.id_zone_orig = geoflux_compare_matrices_zone.id_zone);

-- Construction des indicateurs de comparaison par OD sur les macrozones
DO
$$
BEGIN
	IF '%(id_macrozonage)' != '' THEN
		DROP TABLE IF EXISTS %(schema_dest).geoflux_compare_matrices_od_macro;
		CREATE TABLE %(schema_dest).geoflux_compare_matrices_od_macro AS (
		SELECT id_macrozone_orig, 
			   id_macrozone_dest, 
			sum(matrices.valeur1) as valeur1, 
			sum(matriceS.valeur2) as valeur2, 
			   round(((covar_pop(matrices.valeur1, matrices.valeur2) + power(10, -2)/2) / (stddev_pop(matrices.valeur1) * stddev_pop(matrices.valeur2) + power(10, -2)/2))::numeric, 2) as macro_win_cor,                                
			   round(( ((2*avg(matrices.valeur1)*avg(matrices.valeur2) + power(10, -10)) * (2*covar_pop(matrices.valeur1, matrices.valeur2) + power(10, -2)))
					  /(((avg(matrices.valeur1))^2 + (avg(matrices.valeur2))^2 + power(10, -10)) * ((stddev_pop(matrices.valeur1))^2 + (stddev_pop(matrices.valeur2))^2 + power(10, -2)))
			   )::numeric, 2) as macro_win_ssim
		FROM %(schema_dest).geoflux_compare_matrices_od matrices
		GROUP BY id_macrozone_orig, id_macrozone_dest
		ORDER BY id_macrozone_orig, id_macrozone_dest);

		ALTER TABLE %(schema_dest).geoflux_compare_matrices_od_macro
		ADD COLUMN geom Geometry(Linestring, 2154);

		with bound as (
			SELECT st_makepolygon(st_exteriorring(st_union(zone.geom))) as geom
			FROM %(schema_matrices).zone join %(schema_dest).geoflux_compare_matrices_zone t on (zone.id = t.id_zone)
		)
		UPDATE %(schema_dest).geoflux_compare_matrices_od_macro
		SET geom = st_makeline(st_pointonsurface(st_intersection(bound.geom, zone_orig.geom)), st_pointonsurface(st_intersection(bound.geom,zone_dest.geom)))
		FROM %(schema_matrices).zone zone_orig, %(schema_matrices).zone zone_dest, bound
		WHERE zone_orig.id = geoflux_compare_matrices_od_macro.id_macrozone_orig AND zone_dest.id = geoflux_compare_matrices_od_macro.id_macrozone_dest;

		-- Construction des indicateurs de comparaison par macrozone
		DROP TABLE IF EXISTS %(schema_dest).geoflux_compare_matrices_macrozone;
		CREATE TABLE %(schema_dest).geoflux_compare_matrices_macrozone AS 
		WITH emissions AS
		(
			SELECT id_macrozone_orig as id_macrozone, sum(valeur1) as valeur1, sum(valeur2) as valeur2, avg(macro_win_cor) as o_macro_mcor, avg(macro_win_ssim) as o_macro_mssim
			FROM %(schema_dest).geoflux_compare_matrices_od_macro
			GROUP BY id_macrozone_orig
		), attractions AS
		(
			SELECT id_macrozone_dest as id_macrozone, sum(valeur1) as valeur1, sum(valeur2) as valeur2, avg(macro_win_cor) as d_macro_mcor, avg(macro_win_ssim) as d_macro_mssim
			FROM %(schema_dest).geoflux_compare_matrices_od_macro
			GROUP BY id_macrozone_dest
		)
		SELECT emissions.id_macrozone, 
			   emissions.valeur1 as emissions1, 
			   emissions.valeur2 as emissions2, 
			   attractions.valeur1 as attractions1, 
			   attractions.valeur2 as attractions2, 
			   emissions.o_macro_mcor, 
			   attractions.d_macro_mcor, 
			   emissions.o_macro_mssim, 
			   attractions.d_macro_mssim	
		FROM emissions JOIN attractions ON (emissions.id_macrozone = attractions.id_macrozone); 

		ALTER TABLE %(schema_dest).geoflux_compare_matrices_macrozone
		ADD COLUMN geom Geometry(MultiPolygon, 2154);

		WITH bound AS (
			SELECT st_makepolygon(st_exteriorring(st_union(zone.geom))) as geom
			FROM %(schema_matrices).zone join %(schema_dest).geoflux_compare_matrices_zone t on (zone.id = t.id_zone)
		)
		UPDATE %(schema_dest).geoflux_compare_matrices_macrozone
		SET geom = st_multi(st_collectionextract(st_intersection(zone.geom, bound.geom), 3))
		FROM %(schema_matrices).zone, bound
		WHERE zone.id = geoflux_compare_matrices_macrozone.id_macrozone;
	END IF;
END
$$;

-- Construction des indicateurs de comparaison à l'échelle de la matrice entière
DROP TABLE IF EXISTS %(schema_dest).geoflux_compare_matrices_global;
CREATE TABLE %(schema_dest).geoflux_compare_matrices_global AS 
SELECT round(avg(valeur1)::numeric, 2) as avg1, 
	   round(avg(valeur2)::numeric, 2) as avg2, 
	   round((avg(valeur1)/avg(valeur2))::numeric, 2) as ratio, 	   
       round(((covar_pop(valeur1, valeur2) + power(10,-2)/2) 
			  / (stddev_pop(valeur1) * stddev_pop(valeur2) + power(10,-2)/2))::numeric, 2) as corr_pearson, 
	   round(((covar_pop(norm_valeur1, norm_valeur2) + power(10,-2)/2) 
			  / (stddev_pop(norm_valeur1) * stddev_pop(norm_valeur2) + power(10,-2)/2))::numeric, 2) as norm_corr_pearson, 
       round((((covar_pop(valeur1, valeur2) + power(10,-2)/2) 
			   / (stddev_pop(valeur1) * stddev_pop(valeur2) + power(10,-2)/2))^2)::numeric, 2) as determ, 
       round((((covar_pop(norm_valeur1, norm_valeur2) + power(10,-2)/2) 
			   / (stddev_pop(norm_valeur1) * stddev_pop(norm_valeur2) + power(10,-2)/2))^2)::numeric, 2) as norm_determ, 
       sqrt(sum((valeur1-valeur2)^2) / (
		   (SELECT count(*) FROM %(schema_dest).geoflux_compare_matrices_zone WHERE emissions2>0)
	   	  *(SELECT count(*) FROM %(schema_dest).geoflux_compare_matrices_zone WHERE attractions2>0)
	   )) / (
	   sum(valeur2)/ (
		   (SELECT count(*) FROM %(schema_dest).geoflux_compare_matrices_zone WHERE emissions2>0)
	   	  *(SELECT count(*) FROM %(schema_dest).geoflux_compare_matrices_zone WHERE attractions2>0)
	   )) as srmse,
       round(((sum(log(2)*(norm_valeur1 + norm_valeur2)
		+ CASE WHEN valeur1 = 0 THEN 0 ELSE norm_valeur1*log(norm_valeur1) END
		+ CASE WHEN valeur2 = 0 THEN 0 ELSE norm_valeur2*log(norm_valeur2) END
		- CASE WHEN valeur1 = 0 AND valeur2 = 0 THEN 0 ELSE (norm_valeur1+norm_valeur2)*log(norm_valeur1 + norm_valeur2) END
		))/(log(2)*(SELECT count(*) FROM %(schema_dest).geoflux_compare_matrices_od)))::numeric, 5) as psy_kullback,
		round((( 2 * avg(valeur1) * avg(valeur2) + power(10,-10)) * (2*covar_pop(valeur1, valeur2) + power(10,-2) ) / 
		(((avg(valeur1))^2 + (avg(valeur2))^2 + power(10,-10) ) * ( (stddev_pop(valeur1))^2 + (stddev_pop(valeur2))^2 + power(10,-2) )))::numeric, 2) as ssim, 
	    avg(adj_win_cor) as adj_win_cor,
        avg(adj_win_ssim) as adj_win_mssim
FROM %(schema_dest).geoflux_compare_matrices_od; 

DO
$$
BEGIN
	IF '%(id_macrozonage)' != '' THEN
		ALTER TABLE %(schema_dest).geoflux_compare_matrices_global
		ADD COLUMN chi2 real, 
		ADD COLUMN norm_chi2 real, 
		ADD COLUMN macro_win_cor real, 
		ADD COLUMN macro_win_mssim real, 
		ADD COLUMN o_nlod real, 
		ADD COLUMN d_nlod real, 
		ADD COLUMN o_struct_nlod real, 
		ADD COLUMN d_struct_nlod real;

		UPDATE %(schema_dest).geoflux_compare_matrices_global
		SET macro_win_cor = (SELECT avg(macro_win_cor) FROM %(schema_dest).geoflux_compare_matrices_od_macro), 
			macro_win_mssim = (SELECT avg(macro_win_ssim) FROM %(schema_dest).geoflux_compare_matrices_od_macro), 
			o_nlod = (SELECT avg(o_nlod) FROM %(schema_dest).geoflux_compare_matrices_zone), 
			d_nlod = (SELECT avg(d_nlod) FROM %(schema_dest).geoflux_compare_matrices_zone), 
			o_struct_nlod = (SELECT avg(o_struct_nlod) FROM %(schema_dest).geoflux_compare_matrices_zone), 
			d_struct_nlod = (SELECT avg(d_struct_nlod) FROM %(schema_dest).geoflux_compare_matrices_zone); 


		WITH dist_groups AS (
		SELECT classe_dist, sum(valeur1) as s1, sum(valeur2) as s2, sum(norm_valeur1) as norm_s1, sum(norm_valeur2) as norm_s2
		FROM %(schema_dest).geoflux_compare_matrices_od
		GROUP BY classe_dist
		), indic AS (
			SELECT sum((s1 - s2)^2 / s2) as chi2, sum((norm_s1 - norm_s2)^2 / norm_s2) as norm_chi2
			FROM dist_groups
			WHERE s2>0 AND norm_s2 >0
		)
		UPDATE %(schema_dest).geoflux_compare_matrices_global
		SET chi2 = indic.chi2,
			norm_chi2 = indic.norm_chi2
		FROM indic;
	END IF;
END
$$;
