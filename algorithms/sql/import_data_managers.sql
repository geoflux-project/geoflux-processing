
-- Insertion des gestionnaires de données
INSERT INTO %(survey_schema).gestionnaire_donnee(id, nom, users_group, geom)
SELECT id, nom, users_group, geom
FROM %(ref_schema).n_gestionnaire_donnee
ORDER BY 1; 

SELECT setval('%(survey_schema).gestionnaire_donnee_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(survey_schema).gestionnaire_donnee), false);


