ALTER TABLE %(schema_source).%(table_interview)
ADD COLUMN IF NOT EXISTS adresse_orig character(20) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS adresse_dest character(20) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS voie_orig character(15) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS voie_dest character(15) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS pole_orig character(16) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS pole_dest character(16) DEFAULT 'N',
ADD COLUMN IF NOT EXISTS pays_trav character(2) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS zone_trav character(5) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS commune_trav character(10) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS pays_dom character(2) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS zone_dom character(5) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS commune_dom character(10) DEFAULT 'N',
ADD COLUMN IF NOT EXISTS lieu_orig character varying(20),
ADD COLUMN IF NOT EXISTS type_lieu_orig smallint,
ADD COLUMN IF NOT EXISTS lieu_dest character varying(20),
ADD COLUMN IF NOT EXISTS type_lieu_dest smallint,
ADD COLUMN IF NOT EXISTS lieu_trav character varying(10) DEFAULT 'N', 
ADD COLUMN IF NOT EXISTS type_lieu_trav smallint, 
ADD COLUMN IF NOT EXISTS lieu_dom character varying(10) DEFAULT 'N',
ADD COLUMN IF NOT EXISTS type_lieu_dom smallint,
ADD COLUMN IF NOT EXISTS port_debarq_orig character varying(5),
ADD COLUMN IF NOT EXISTS port_embarq_dest character varying(5), 
ADD COLUMN IF NOT EXISTS frontiere_orig character varying(5), 
ADD COLUMN IF NOT EXISTS frontiere_dest character varying(5), 
ADD COLUMN IF NOT EXISTS red_valide boolean;

UPDATE %(schema_source).%(table_interview)
SET red_valide = CASE WHEN code_controle_od::character varying = '1' AND per_enq IS NOT NULL AND per_enq::integer >= 1 AND per_enq::integer <= 96 AND type_veh IS NOT NULL AND type_veh = ANY(ARRAY['11', '12', '13','14','15','16','17','18','19']) THEN True
				 ELSE False
				 END,
    lieu_orig = CASE WHEN adresse_orig != 'N' and adresse_orig != 'X' and adresse_orig is not null THEN adresse_orig
                ELSE CASE WHEN voie_orig != 'N' and voie_orig != 'X' and voie_orig is not null THEN voie_orig
                          ELSE CASE WHEN pole_orig !='N' and pole_orig != 'X' and pole_orig is not null THEN pole_orig
                                    ELSE CASE WHEN commune_orig !='N' and commune_orig !='X' and commune_orig is not null THEN commune_orig
                                              ELSE CASE WHEN zone_orig != 'N' and zone_orig != 'X' and zone_orig is not null THEN zone_orig
                                                        ELSE CASE WHEN pays_orig != 'N' and pays_orig != 'X' and pays_orig is not null THEN pays_orig
                                                                  ELSE NULL
                                                             END
                                                   END
                                         END
                               END
                     END
                END, 
    type_lieu_orig = CASE WHEN adresse_orig != 'N' and adresse_orig != 'X' and adresse_orig is not null THEN 60
                            ELSE CASE WHEN voie_orig != 'N' and voie_orig != 'X' and voie_orig is not null THEN 50
                                      ELSE CASE WHEN pole_orig !='N' and pole_orig != 'X' and pole_orig is not null THEN 40
                                                ELSE CASE WHEN commune_orig !='N' and commune_orig !='X' and commune_orig is not null THEN 30
                                                          ELSE CASE WHEN zone_orig != 'N' and zone_orig != 'X' and zone_orig is not null THEN 
																		 CASE WHEN zone_orig not like '%0' THEN 23
																		      WHEN zone_orig like '%0' and zone_orig not like '%00' THEN 22
																		      WHEN zone_orig like '%00' THEN 21
																		 END	  
                                                                    ELSE CASE WHEN pays_orig != 'N' and pays_orig != 'X' and pays_orig is not null THEN 10
                                                                              ELSE NULL
                                                                         END
                                                               END
                                                     END
                                           END
                                 END
                     END, 
    lieu_dest = CASE WHEN adresse_dest !='N' and adresse_dest!='X' and adresse_dest is not null THEN adresse_dest
                ELSE CASE WHEN voie_dest !='N' and voie_dest!='X' and voie_dest is not null THEN voie_dest
                          ELSE CASE WHEN pole_dest !='N' and pole_dest != 'X' and pole_dest is not null THEN pole_dest
                                    ELSE CASE WHEN commune_dest !='N' and commune_dest !='X' and commune_dest is not null THEN commune_dest
                                              ELSE CASE WHEN zone_dest != 'N' and zone_dest != 'X' and zone_dest is not null THEN zone_dest
                                                        ELSE CASE WHEN pays_dest != 'N' and pays_dest != 'X' and pays_dest is not null THEN pays_dest
                                                                  ELSE NULL
                                                             END
                                                   END
                                         END
                               END
                     END
                END,      
    type_lieu_dest = CASE WHEN adresse_dest !='N' and adresse_dest!='X' and adresse_dest is not null THEN 60
                     ELSE CASE WHEN voie_dest !='N' and voie_dest!='X' and voie_dest is not null THEN 50
                          ELSE CASE WHEN pole_dest !='N' and pole_dest != 'X' and pole_dest is not null THEN 40
                                    ELSE CASE WHEN commune_dest !='N' and commune_dest !='X' and commune_dest is not null THEN 30
                                              ELSE CASE WHEN zone_dest != 'N' and zone_dest != 'X' and zone_dest is not null THEN 
															 CASE WHEN zone_dest not like '%0' THEN 23
																  WHEN zone_dest like '%0' and zone_dest not like '%00' THEN 22
																  WHEN zone_dest like '%00' THEN 21
															 END	
                                                        ELSE CASE WHEN pays_dest != 'N' and pays_dest != 'X' and pays_dest is not null THEN 10
                                                                  ELSE NULL
                                                             END
                                                   END
                                         END
                               END
                          END
                     END,                 
    lieu_trav = CASE WHEN commune_trav !='N' and commune_trav !='X' and commune_trav is not null THEN commune_trav
                     ELSE CASE WHEN zone_trav != 'N' and zone_trav != 'X' and zone_trav is not null THEN zone_trav
                               ELSE CASE WHEN pays_trav != 'N' and pays_trav != 'X' and pays_trav is not null THEN pays_trav
                                         ELSE NULL
                                    END
                          END
                END,           
    type_lieu_trav = CASE WHEN commune_trav !='N' and commune_trav !='X' and commune_trav is not null THEN 30
                     ELSE CASE WHEN zone_trav != 'N' and zone_trav != 'X' and zone_trav is not null THEN 
										CASE WHEN zone_trav like '%0' THEN 23
											 WHEN zone_trav like '%0' and zone_trav not like '%00' THEN 22
											 WHEN zone_trav like '%00' THEN 21
										END	
                               ELSE CASE WHEN pays_trav != 'N' and pays_trav != 'X' and pays_trav is not null THEN 10
                                         ELSE NULL
                                    END
                          END
                END, 
    lieu_dom = CASE WHEN commune_dom !='N' and commune_dom !='X' and commune_dom is not null THEN commune_dom
                    ELSE CASE WHEN zone_dom != 'N' and zone_dom != 'X' and zone_dom is not null THEN zone_dom
                              ELSE CASE WHEN pays_dom != 'N' and pays_dom != 'X' and pays_dom is not null THEN pays_dom
                                        ELSE NULL
                                   END
                         END
               END, 
    type_lieu_dom = CASE WHEN commune_dom !='N' and commune_dom !='X' and commune_dom is not null THEN 30
                    ELSE CASE WHEN zone_dom != 'N' and zone_dom != 'X' and zone_dom is not null THEN
										CASE WHEN zone_dom like '%0' THEN 23
											 WHEN zone_dom like '%0' and zone_dom not like '%00' THEN 22
											 WHEN zone_dom like '%00' THEN 21
										END	
                              ELSE CASE WHEN pays_dom != 'N' and pays_dom != 'X' and pays_dom is not null THEN 10
                                        ELSE NULL
                                   END
                         END
                    END; 

