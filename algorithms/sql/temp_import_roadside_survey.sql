DROP TABLE IF EXISTS %(schema).%(table_questionnaire);
CREATE TABLE %(schema).%(table_questionnaire)
(
	code_poste character varying not null,
	num_point smallint not null,
	ordre smallint not null, 
    champ character varying not null,
    libelle character varying,
	quest_vl character(1) not null, 
	quest_pl character(1) not null,
	PRIMARY KEY (champ, code_poste, num_point)
);
	
COMMENT ON COLUMN %(schema).%(table_questionnaire).code_poste
    IS 'Code du poste d''enquête';
	
COMMENT ON COLUMN %(schema).%(table_questionnaire).num_point
    IS 'Numéro du point d''enquête';
	
COMMENT ON COLUMN %(schema).%(table_questionnaire).champ
    IS 'Nom du champ';
	
COMMENT ON COLUMN %(schema).%(table_questionnaire).quest_vl
    IS 'Oui (code 1) si la question est posée dans le questionnaire VL, non (code 2) sinon';
	
COMMENT ON COLUMN %(schema).%(table_questionnaire).quest_pl
    IS 'Oui (code 1) si la question est posée dans le questionnaire PL, non (code 2) sinon';

DROP TABLE IF EXISTS %(schema).%(table_codif);
CREATE TABLE %(schema).%(table_codif)
( 
	champ character varying not null, 
    type character varying not null, 
	controle character varying not null,
	modalite character varying not null,
	libelle character varying not null, 
	PRIMARY KEY(champ, modalite), 
    CONSTRAINT type_check CHECK (lower(type) = ANY (ARRAY['char', 'num', 'codif', 'zonage']))
);

DROP TABLE IF EXISTS %(schema).%(table_cpt_manuel);
CREATE TABLE %(schema).%(table_cpt_manuel)
(
	code_poste character varying not null,
	num_point smallint not null,
	per_enq character varying(2) not null,
	vl_fr smallint,
	vl_et smallint,
	cc_fr smallint,
	cc_et smallint,
	"2rm_fr" smallint,
	"2rm_et" smallint,
	vul_fr smallint,
	vul_et smallint,
	vl_hors smallint,
	pl_fr_sans_tmd smallint,
	pl_et_sans_tmd smallint,
	pl_fr_tmd smallint,
	pl_et_tmd smallint,
    pl_fr_2_3_ess smallint,
    pl_et_2_3_ess smallint, 
    pl_fr_4_ess_plus smallint,
    pl_et_4_ess_plus smallint,
	bus_cars smallint not null,
	tracteurs smallint not null,
    vl smallint, 
    cc smallint, 
    vul smallint, 
    vl_cc smallint,
    vl_cc_vul smallint,
    "2rm" smallint,
    pl smallint, 
    pl_2_3_ess smallint, 
    pl_4_ess_plus smallint, 
    pl_tmd smallint, 
    pl_sans_tmd smallint, 
    veh_hors smallint,
	PRIMARY KEY(code_poste, num_point, per_enq)
);

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).code_poste
    IS 'Identifiant unique du poste d''enquête';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).num_point
    IS 'Numéro du point d''enquête';

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).per_enq
    IS 'Péride de réalisation du comptage';

	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).vl_fr
    IS 'Nombre de VL français comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).vl_et
    IS 'Nombre de VL étrangers comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).cc_fr
    IS 'Nombre de camping-cars et caravanes français comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).cc_et
    IS 'Nombre de camping-cars et caravanes étrangers comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel)."2rm_fr"
    IS 'Nombre de deux-roues motorisés français comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel)."2rm_et"
    IS 'Nombre de deux-roues motorisés étrangers comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).vul_fr
    IS 'Nombre de Véhicules Utilitaires Légers français comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).vul_et
    IS 'Nombre de Véhicules Utilitaires Légers étrangers comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).vl_hors
    IS 'Nombre de Véhicules Légers hors du champ de l''enquête : auto-école, véhicules d''urgence comptés pendant la période';

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).pl_fr_sans_tmd
    IS 'Nombre de Poids-Lourds français sans plaque orange comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).pl_et_sans_tmd
    IS 'Nombre de Poids-Lourds étrangers sans plaque orange comptés pendant la période';

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).pl_fr_tmd
    IS 'Nombre de Poids-Lourds français avec plaque orange comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_manuel).pl_et_tmd
    IS 'Nombre de Poids-Lourds étrangers avec plaque orange comptés pendant la période';

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).bus_cars
    IS 'Nombre de bus et d''autocars comptés pendant la période';

COMMENT ON COLUMN %(schema).%(table_cpt_manuel).tracteurs
    IS 'Nombre de tracteurs comptés pendant la période';

DROP TABLE IF EXISTS %(schema).%(table_cpt_auto);
CREATE TABLE %(schema).%(table_cpt_auto)
(
	id_point smallint not null,
	jour date not null,
	per character varying(2) not null,
	vl smallint not null,
	pl smallint not null,
	PRIMARY KEY(id_point,jour,per)
);

COMMENT ON COLUMN %(schema).%(table_cpt_auto).id_point
    IS 'Identifiant unique du point de comptage (référence au fichier des points de comptage)';

COMMENT ON COLUMN %(schema).%(table_cpt_auto).jour
    IS 'Jour de réalisation du comptage';
	
COMMENT ON COLUMN %(schema).%(table_cpt_auto).per
    IS 'Période horaire de réalisation du comptage';
	
COMMENT ON COLUMN %(schema).%(table_cpt_auto).vl
    IS 'Nombre de VL comptés pendant la période';
	
COMMENT ON COLUMN %(schema).%(table_cpt_auto).pl
    IS 'Nombre de PL comptés pendant la période';

