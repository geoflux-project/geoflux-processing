INSERT INTO %(matrix_schema).corresp_zones(id_zone, id_macrozone, part_zone, geom)
SELECT zone.id, macrozone.id, corresp_zones_import.part_zone, st_multi(st_intersection(zone.geom, macrozone.geom)) 
FROM %(temp_schema).corresp_zones_import JOIN %(matrix_schema).zone ON (zone.code_zone = corresp_zones_import.code_zone)
										 JOIN %(matrix_schema).zone macrozone ON (macrozone.code_zone = corresp_zones_import.code_macrozone)
										 JOIN %(matrix_schema).zonage ON (zone.id_zonage = zonage.id)
										 JOIN %(matrix_schema).zonage macrozonage ON (macrozone.id_zonage = macrozonage.id)
WHERE zonage.id = %(id_zonage) AND macrozonage.id = %(id_macrozonage); 

DROP TABLE IF EXISTS %(temp_schema).corresp_zones_import;
