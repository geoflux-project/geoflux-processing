CREATE TABLE %(export_schema).zonage_%(id_zonage)_export AS
(
	SELECT code_zone, nom, prec_geo, geom 
	FROM %(matrix_schema).zone 
	WHERE id_zonage = %(id_zonage) 
	ORDER BY code_zone
);

ALTER TABLE %(export_schema).zonage_%(id_zonage)_export
ADD CONSTRAINT zonage_%(id_zonage)_export_pkey PRIMARY KEY (code_zone); 

CREATE INDEX zonage_%(id_zonage)_export_geom_idx
ON %(export_schema).zonage_%(id_zonage)_export USING gist(geom);
