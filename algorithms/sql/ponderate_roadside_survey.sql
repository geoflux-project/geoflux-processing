ALTER TABLE %(schema).%(survey_table)
	ADD COLUMN IF NOT EXISTS red_per_enq character varying, 
    ADD COLUMN IF NOT EXISTS red_type_veh1 character varying,
	ADD COLUMN IF NOT EXISTS red_type_veh2 character varying,
	ADD COLUMN IF NOT EXISTS red_type_veh3 character varying,
	ADD COLUMN IF NOT EXISTS red_per_hor character varying, 
	ADD COLUMN IF NOT EXISTS red_valide boolean,
	ADD COLUMN IF NOT EXISTS coef1 real, 
	ADD COLUMN IF NOT EXISTS coef2 real, 
	ADD COLUMN IF NOT EXISTS coef3 real, 
	ADD COLUMN IF NOT EXISTS coef4 real, 
	ADD COLUMN IF NOT EXISTS coef_pe real, 
    ADD COLUMN IF NOT EXISTS coef_joe real;

COMMENT ON COLUMN %(schema).%(survey_table).red_per_enq
	IS 'Pour le redressement : initialisée à la même valeur que per_enq, mais peut être modifiée manuellement pour basculer certaines enquêtes d''une période sur l''autre en vue de diminuer l''étendue des poids finaux. ';
COMMENT ON COLUMN %(schema).%(survey_table).red_type_veh1
	IS 'Pour le redressement : types de véhicules agrégé selon les principes des comptages manuels, en distinguant VL (= VL + CC + VUL) français, VL étranger, PL sans TMD français, PL sans TMD étranger, PL avec TMD. ';
COMMENT ON COLUMN %(schema).%(survey_table).red_type_veh2
	IS 'Pour le redressement : types de véhicules agrégé selon les principes des comptages manuels, en distinguant VL (= VL + CC + VUL) des PL. ';
COMMENT ON COLUMN %(schema).%(survey_table).red_type_veh3
	IS 'Pour le redressement : types de véhicules agrégé selon les principes des comptages automatiques, en distinguant VL (= VL + VUL), PL (= CC + PL). ';
COMMENT ON COLUMN %(schema).%(survey_table).red_per_hor 
	IS 'Pour le redressement : périodes d''enquêtes agrégées au niveau horaire. ';
COMMENT ON COLUMN %(schema).%(survey_table).coef1 
	IS 'Coefficient de redressement global sur la période enquêtée, à partir du comptage manuel, en distinguant VL et PL, français et étranger et éventuellement les TMD.';
COMMENT ON COLUMN %(schema).%(survey_table).coef2 
	IS 'Coefficient additionnel pour caler sur les volumes horaires du comptage manuel, en distinguant VL et PL.';
COMMENT ON COLUMN %(schema).%(survey_table).coef3 
	IS 'Coefficient additionnel pour caler sur les volumes horaires du comptage automatique (moyenne de plusieurs jours ouvrés), en distinguant VL et PL.';
COMMENT ON COLUMN %(schema).%(survey_table).coef4 
	IS 'Coefficient additionnel pour caler sur les volumes journaliers du comptage automatique du mois de l''enquête, en distinguant VL et PL.';
COMMENT ON COLUMN %(schema).%(survey_table).coef_pe
	IS 'Coefficient global sur la période d''enquête = coef1 * coef2 * coef3.';
COMMENT ON COLUMN %(schema).%(survey_table).coef_joe
	IS 'Coefficient global sur un jour ouvré moyen du mois de l''enquête = coef1 * coef2 * coef3 * coef4.';

-- Prérequis : que la colonne type_veh existe et ait une réponse, que la colonne immat_pays existe, que la colonne plaques_orange existe
-- Calcul des variables spécifiques pour le redressement
UPDATE %(schema).%(survey_table)
SET red_per_enq = CASE WHEN red_per_enq IS NULL THEN per_enq ELSE red_per_enq END;

UPDATE %(schema).%(survey_table) 
SET red_valide = CASE WHEN code_controle_od::character varying = '1'
                  AND red_per_enq IS NOT NULL AND red_per_enq = ANY(ARRAY['01', '02', '03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96']) 
				  AND type_veh IS NOT NULL AND type_veh = ANY(ARRAY['11', '12', '13','14','15','16','17','18','19'])
				 THEN True
				 ELSE False
				 END,
    red_type_veh2 = CASE WHEN type_veh = ANY(ARRAY['11', '12', '13', '14', '15']) THEN 'VL'
	                     WHEN type_veh = ANY(ARRAY['16', '17', '18']) THEN 'PL'
				    END, 	
	red_type_veh3 = CASE WHEN type_veh = ANY(ARRAY['11',  '14', '15', '19']) THEN 'VL' 
						 WHEN type_veh = ANY(ARRAY['12', '13', '16', '17', '18']) THEN 'PL' 
				    END, 
	red_per_hor = CASE WHEN (red_per_enq::integer-1)/4*4+1< 10 
	                   THEN '0' || ((red_per_enq::integer-1)/4*4+1)::character varying 
					   ELSE ((red_per_enq::integer-1)/4*4+1)::character varying 
				  END; 

-- Comparaison comptages auto, manuels et échantillons
DROP TABLE IF EXISTS %(schema).%(survey_table)_vol_hor;
CREATE TABLE %(schema).%(survey_table)_vol_hor AS
WITH auto AS (
SELECT point_enq.code_poste, point_enq.num_point, auto.per, 
	avg(auto.vl) as auto_vl, 
	avg(auto.pl) as auto_pl, 
	avg(auto.pl + auto.vl) as auto_tv
FROM %(schema).%(auto_counts_table) auto JOIN %(schema).%(auto_counts_point_table) auto_point ON (auto.id_point = auto_point.id)
                                         JOIN %(schema).%(survey_point_table) point_enq on (point_enq.point_cpt = auto_point.id)
WHERE extract('dow' from jour) = ANY(ARRAY[1, 2, 3, 4, 5])
GROUP BY point_enq.code_poste, point_enq.num_point, auto.per
), manual AS (
SELECT code_poste, num_point, CASE WHEN (per_enq::integer-1)/4*4+1< 10 
                                   THEN '0' || ((per_enq::integer-1)/4*4+1)::character varying 
                                   ELSE ((per_enq::integer-1)/4*4+1)::character varying 
                              END as per, 
       sum(coalesce(vl_et, 0)+coalesce(cc_et, 0)+coalesce(vul_et, 0)+coalesce(vl_fr, 0)+coalesce(cc_fr, 0)+coalesce(vul_fr, 0)) as manual_vl, 
       sum(coalesce(pl_et_tmd, 0)+coalesce(pl_fr_tmd, 0)+coalesce(pl_et_sans_tmd, 0)+coalesce(pl_fr_sans_tmd, 0)) as manual_pl,
       sum(coalesce(vl_et, 0)+coalesce(cc_et, 0)+coalesce(vul_et, 0)+coalesce(vl_fr, 0)+coalesce(cc_fr, 0)+coalesce(vul_fr, 0) + coalesce(pl_et_tmd, 0)+coalesce(pl_fr_tmd, 0)+coalesce(pl_et_sans_tmd, 0)+coalesce(pl_fr_sans_tmd, 0)) as manual_tv
FROM %(schema).%(manual_counts_table) manual
GROUP BY code_poste, num_point, (per_enq::integer-1)/4*4+1
), enq AS (
SELECT code_poste, 
	   num_point, 
       red_per_hor,
	   count(*) filter(where red_type_veh2 = 'VL') as ech_vl,
       count(*) filter(where red_type_veh2 = 'PL') as ech_pl
FROM %(schema).%(survey_table)
WHERE red_valide
GROUP BY code_poste, num_point, red_per_hor
)
SELECT auto.code_poste, 
       auto.num_point, 
       auto.per, 
       auto_vl::integer, 
       auto_pl::integer, 
       auto_tv::integer, 
       manual_vl::integer, 
       manual_pl::integer, 
       manual_tv::integer, 
       round((auto_vl::real / manual_vl::real)::numeric, 2) as ratio_cpt_vl, 
       round((auto_pl::real / manual_pl::real)::numeric, 2) as ratio_cpt_pl, 
       round((auto_tv::real / manual_tv::real)::numeric, 2) as ratio_cpt_tv, 
       enq.ech_vl, 
       enq.ech_pl,
       round((enq.ech_vl::real / manual_vl::real)::numeric, 3) as ts_vl,
       round((enq.ech_pl::real / manual_pl::real)::numeric, 3) as ts_pl
FROM auto JOIN manual ON (auto.code_poste = manual.code_poste AND auto.num_point = manual.num_point AND auto.per = manual.per)
          JOIN enq ON (enq.code_poste = manual.code_poste AND enq.num_point::character varying = manual.num_point::character varying AND enq.red_per_hor::character varying = manual.per::character varying)
ORDER BY auto.code_poste, auto.num_point, auto.per;

-- Liste des types de véhicules : VL, PL, VL-ET, VL-FR, PL-ET, PL-FR, PL-TMD, PT-FR-NO-TMD, PL-ET-NO-TMD

-- Calcul du coef1
DO
$$
BEGIN
    IF NOT %(ignore_red1) THEN
        UPDATE %(schema).%(survey_table)
        SET red_type_veh1 = CASE WHEN type_veh = ANY(ARRAY['11', '12', '13', '14', '15']) THEN
                                                CASE WHEN immat_pays = 'FR' THEN 'VL-FR'
                                                     WHEN immat_pays = 'X' or immat_pays = 'N' or immat_pays is null THEN 'VL'
                                                     ELSE 'VL-ET'
                                                END
                                 WHEN type_veh = ANY(ARRAY['16', '17', '18']) THEN
                                                    CASE WHEN plaques_orange = '1' THEN 
                                                                CASE WHEN immat_pays = 'FR' THEN 'PL-FR-TMD'
                                                                     WHEN immat_pays = 'X' or immat_pays = 'N' or immat_pays is null THEN 'PL-TMD'
                                                                     ELSE 'PL-ET-TMD'
                                                                END
                                                         WHEN plaques_orange = '2' THEN 
                                                                CASE WHEN immat_pays = 'FR' THEN 'PL-FR-NO-TMD'
                                                                     WHEN immat_pays = 'X' or immat_pays = 'N' or immat_pays is null THEN 'PL-NO-TMD'
                                                                     ELSE 'PL-ET-NO-TMD'
                                                                END
                                                         ELSE 
                                                                CASE WHEN immat_pays = 'FR' THEN 'PL-FR'
                                                                     WHEN immat_pays = 'X' or immat_pays = 'N' or immat_pays is null THEN 'PL'
                                                                     ELSE 'PL-ET'
                                                                END
                                                    END
                            END;
        
        DROP TABLE IF EXISTS %(schema).%(survey_table)_coef1; 
        CREATE TABLE %(schema).%(survey_table)_coef1 AS
        WITH base_enq_type_veh1_penq AS
        (
            SELECT code_poste,
                   num_point,
                   'VL' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['VL-FR', 'VL-ET', 'VL'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'PL' as type_veh, 
                   count(*) filter(where red_type_veh1 = any(array['PL-FR', 'PL-ET', 'PL-FR-NO-TMD', 'PL-FR-TMD', 'PL-ET-NO-TMD', 'PL-ET-TMD', 'PL-TMD', 'PL-NO-TMD', 'PL'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'VL-ET' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['VL-ET'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point, 
                   'VL-FR' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['VL-FR'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'PL-ET' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-ET', 'PL-ET-NO-TMD', 'PL-ET-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-FR' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-FR', 'PL-FR-NO-TMD', 'PL-FR-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-FR-NO-TMD' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-FR-NO-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-ET-NO-TMD' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-ET-NO-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-TMD' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-FR-TMD', 'PL-ET-TMD', 'PL-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-NO-TMD' as type_veh,
                   count(*) filter(where red_type_veh1 = any(array['PL-NO-TMD', 'PL-ET-NO-TMD', 'PL-FR-NO-TMD'])) as valeur
            FROM %(schema).%(survey_table)
            WHERE red_valide
            GROUP BY code_poste, num_point
        ), cpt_manuel_penq AS
        (
            SELECT code_poste,
                   num_point,
                   'VL' as type_veh,
                   sum(coalesce(vl_et, 0)+coalesce(cc_et, 0)+coalesce(vul_et, 0)+coalesce(vl_fr, 0)+coalesce(cc_fr, 0)+coalesce(vul_fr, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'PL' as type_veh, 
                   sum(coalesce(pl_et_tmd, 0)+coalesce(pl_fr_tmd, 0)+coalesce(pl_et_sans_tmd, 0)+coalesce(pl_fr_sans_tmd, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'VL-ET' as type_veh,
                   sum(coalesce(vl_et, 0)+coalesce(cc_et, 0)+coalesce(vul_et, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point, 
                   'VL-FR' as type_veh,
                   sum(coalesce(vl_fr, 0)+coalesce(cc_fr, 0)+coalesce(vul_fr, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste,
                   num_point,
                   'PL-ET' as type_veh,
                   sum(pl_et_sans_tmd + pl_et_tmd) as valeur
            FROM %(schema).%(manual_counts_table)	
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-FR' as type_veh,
                   sum(coalesce(pl_fr_sans_tmd,0) + coalesce(pl_fr_tmd,0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-FR-NO-TMD' as type_veh,
                   sum(coalesce(pl_fr_sans_tmd, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-ET-NO-TMD' as type_veh,
                   sum(coalesce(pl_et_sans_tmd,0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-TMD' as type_veh,
                   sum(coalesce(pl_et_tmd, 0)+coalesce(pl_fr_tmd, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
            UNION
            SELECT code_poste, 
                   num_point,
                   'PL-NO-TMD' as type_veh,
                   sum(coalesce(pl_et_sans_tmd, 0)+coalesce(pl_fr_sans_tmd, 0)) as valeur
            FROM %(schema).%(manual_counts_table)
            GROUP BY code_poste, num_point
        ORDER BY 1, 2, 3, 4
        )
        SELECT cpt.code_poste, cpt.num_point::character varying, cpt.type_veh, cpt.valeur as rec, obs.valeur as obs, case when obs.valeur >0 then cpt.valeur::real / obs.valeur::real else null end as coef1
        FROM cpt_manuel_penq cpt JOIN base_enq_type_veh1_penq obs
        ON (cpt.code_poste = obs.code_poste 
          AND cpt.num_point::character varying = obs.num_point::character varying 
          AND cpt.type_veh = obs.type_veh)
        ORDER BY 1, 2, 3; 
          
        UPDATE %(schema).%(survey_table) base_enq
        SET coef1 = t.coef1
        FROM %(schema).%(survey_table)_coef1 t
        WHERE base_enq.red_valide 
          AND base_enq.code_poste = t.code_poste
          AND base_enq.num_point::character varying = t.num_point::character varying
          AND base_enq.red_type_veh1 = t.type_veh; 
        
        UPDATE %(schema).%(survey_table) base_enq
        SET coef1 = t.coef1
        FROM %(schema).%(survey_table)_coef1 t
        WHERE base_enq.red_valide 
          AND base_enq.code_poste = t.code_poste
          AND base_enq.num_point::character varying = t.num_point::character varying
          AND (base_enq.red_type_veh1 = 'PL-FR-TMD' or base_enq.red_type_veh1 = 'PL-ET-TMD') AND t.type_veh = 'PL-TMD'; 
    ELSE 
        UPDATE %(schema).%(survey_table) base_enq
        SET coef1 = 1
        WHERE base_enq.red_valide; 
    END IF;
END
$$;


-- Calcul du coef2
DROP TABLE IF EXISTS %(schema).%(survey_table)_coef2; 
CREATE TABLE %(schema).%(survey_table)_coef2 AS
WITH base_enq_type_veh2_hor AS
(
	SELECT code_poste, 
	       num_point,
	       red_type_veh2, 
		   red_per_hor, 
           count(*) as eff_brut,
		   sum(coef1) as valeur
	FROM %(schema).%(survey_table)
    where red_valide
	GROUP BY code_poste, num_point, red_type_veh2, red_per_hor
), cpt_manuel_hor AS
(
	SELECT code_poste,
	       num_point,
		   CASE WHEN (per_enq::integer-1)/4*4+1< 10 
	            THEN '0' || ((per_enq::integer-1)/4*4+1)::character varying 
				ELSE ((per_enq::integer-1)/4*4+1)::character varying 
		   END as per_hor,
		   'VL' as type_veh,
	   	   sum(coalesce(vl_et, 0)+coalesce(cc_et, 0)+coalesce(vul_et, 0)+coalesce(vl_fr, 0)+coalesce(cc_fr, 0)+coalesce(vul_fr, 0)) as valeur
	FROM %(schema).%(manual_counts_table)
	GROUP BY code_poste, num_point, (per_enq::integer-1)/4*4+1
	UNION
	SELECT code_poste,
	       num_point,
		   CASE WHEN (per_enq::integer-1)/4*4+1< 10 
	            THEN '0' || ((per_enq::integer-1)/4*4+1)::character varying 
				ELSE ((per_enq::integer-1)/4*4+1)::character varying 
		   END as per_hor,
		   'PL' as type_veh,
	       sum(coalesce(pl_et_tmd, 0)+coalesce(pl_fr_tmd, 0)+coalesce(pl_et_sans_tmd, 0)+coalesce(pl_fr_sans_tmd, 0)) as pl
	FROM %(schema).%(manual_counts_table)
	GROUP BY code_poste, num_point, (per_enq::integer-1)/4*4+1
	ORDER BY code_poste, num_point, per_hor, type_veh
)
SELECT obs.code_poste, 
       obs.num_point::character varying, 
       obs.red_type_veh2, 
       obs.red_per_hor, 
       obs.eff_brut, 
       round(cpt.valeur, 3) as rec, 
       obs.valeur as obs, 
       case when obs.valeur >0 then cpt.valeur::real / obs.valeur::real else null end as coef2
FROM cpt_manuel_hor cpt JOIN base_enq_type_veh2_hor obs
ON (cpt.code_poste = obs.code_poste 
  AND cpt.num_point::character varying = obs.num_point::character varying 
  AND cpt.type_veh = obs.red_type_veh2 
  AND cpt.per_hor::character varying = obs.red_per_hor::character varying)
ORDER BY 1, 2, 3, 4; 

UPDATE %(schema).%(survey_table) base_enq
SET coef2 = t.coef2
FROM %(schema).%(survey_table)_coef2 t
WHERE base_enq.red_valide 
  AND base_enq.code_poste = t.code_poste
  AND base_enq.num_point::character varying = t.num_point::character varying
  AND base_enq.red_type_veh2 = t.red_type_veh2 
  AND base_enq.red_per_hor = t.red_per_hor; 

-- Calcul du coef3
DROP TABLE IF EXISTS %(schema).%(survey_table)_coef3; 
CREATE TABLE %(schema).%(survey_table)_coef3 AS
WITH base_enq_type_veh3_hor AS
(
	SELECT code_poste, 
	       num_point,
	       red_type_veh3, 
		   red_per_hor, 
           count(*) as eff_brut, 
		   sum(coef1*coef2) as valeur
	FROM %(schema).%(survey_table)
    WHERE red_valide
	GROUP BY code_poste, num_point, red_type_veh3, red_per_hor
), cpt_auto_hor AS
(
	SELECT point_enq.code_poste,
		   point_enq.num_point,
		   auto.per,
		   'VL' as type_veh,
	   	   avg(auto.vl) as valeur
	FROM %(schema).%(auto_counts_table) auto JOIN %(schema).%(auto_counts_point_table) auto_point ON (auto.id_point = auto_point.id) 
                                             JOIN %(schema).%(survey_point_table) point_enq on (point_enq.point_cpt = auto_point.id)
	WHERE extract('dow' from jour) = ANY(ARRAY[1, 2, 3, 4, 5])
	GROUP BY point_enq.code_poste, point_enq.num_point, auto.per
	UNION
	SELECT point_enq.code_poste,
		   point_enq.num_point,
		   auto.per,
		   'PL' as type_veh,
	   	   avg(auto.pl) as valeur
	FROM %(schema).%(auto_counts_table) auto JOIN %(schema).%(auto_counts_point_table) auto_point ON (auto.id_point = auto_point.id)  
                                             JOIN %(schema).%(survey_point_table) point_enq on (point_enq.point_cpt = auto_point.id)
	WHERE extract('dow' from jour) = ANY(ARRAY[1, 2, 3, 4, 5])
	GROUP BY point_enq.code_poste, point_enq.num_point, auto.per
)
SELECT obs.code_poste, 
       obs.num_point::character varying, 
       obs.red_type_veh3, 
       obs.red_per_hor, 
       obs.eff_brut, 
       round(cpt.valeur, 3) as rec, 
       obs.valeur as obs, 
       case when obs.valeur > 0 then cpt.valeur::real / obs.valeur::real else null end as coef3
FROM cpt_auto_hor cpt JOIN base_enq_type_veh3_hor obs
ON (cpt.code_poste = obs.code_poste 
  AND cpt.num_point::character varying = obs.num_point::character varying 
  AND cpt.type_veh = obs.red_type_veh3 
  AND cpt.per::character varying = obs.red_per_hor::character varying)
ORDER BY 1, 2, 3, 4; 

UPDATE %(schema).%(survey_table) base_enq
SET coef3 = t.coef3
FROM %(schema).%(survey_table)_coef3 t
WHERE base_enq.red_valide 
  AND base_enq.code_poste = t.code_poste
  AND base_enq.num_point::character varying = t.num_point::character varying
  AND base_enq.red_type_veh3 = t.red_type_veh3 
  AND base_enq.red_per_hor = t.red_per_hor; 

-- Calcul du coef4
DROP TABLE IF EXISTS %(schema).%(survey_table)_coef4; 
CREATE TABLE %(schema).%(survey_table)_coef4 AS
WITH base_enq_type_veh3_penq AS
(
	SELECT code_poste, 
	       num_point,
	       red_type_veh3,
           count(*) as eff_brut, 
		   sum(coef1*coef2*coef3) as valeur
	FROM %(schema).%(survey_table)
    WHERE red_valide
	GROUP BY code_poste, num_point, red_type_veh3
	ORDER BY 1, 2, 3
), cpt_auto_par_jour AS
(
	SELECT point_enq.code_poste,
		   point_enq.num_point, 
           auto.jour, 
	       'VL' as type_veh,
	   	   sum(vl) as valeur
	FROM %(schema).%(auto_counts_table) auto JOIN %(schema).%(auto_counts_point_table) auto_point ON (auto.id_point = auto_point.id)  
                                             JOIN %(schema).%(survey_point_table) point_enq on (point_enq.point_cpt = auto_point.id)
	GROUP BY point_enq.code_poste, point_enq.num_point, auto.jour
    UNION
    SELECT point_enq.code_poste,
		   point_enq.num_point, 
           auto.jour, 
	       'PL' as type_veh,
	   	   sum(pl) as valeur
	FROM %(schema).%(auto_counts_table) auto JOIN %(schema).%(auto_counts_point_table) auto_point ON (auto.id_point = auto_point.id)  
                                             JOIN %(schema).%(survey_point_table) point_enq on (point_enq.point_cpt = auto_point.id)
	GROUP BY point_enq.code_poste, point_enq.num_point, auto.jour
), cpt_auto_jmo AS
(
    SELECT code_poste,
		   num_point, 
           type_veh, 
           avg(valeur) as valeur
    FROM cpt_auto_par_jour
	WHERE extract('dow' from jour) = ANY(ARRAY[1, 2, 3, 4, 5])
	GROUP BY code_poste, num_point, type_veh
)
SELECT obs.code_poste, 
       obs.num_point::character varying, 
       obs.red_type_veh3, 
       obs.eff_brut, 
       round(cpt.valeur, 3) as rec, 
       obs.valeur as obs, 
       case when obs.valeur > 0 then cpt.valeur::real / obs.valeur::real else null end as coef4
FROM cpt_auto_jmo cpt JOIN base_enq_type_veh3_penq obs
ON (cpt.code_poste = obs.code_poste 
  AND cpt.num_point::character varying = obs.num_point::character varying 
  AND cpt.type_veh = obs.red_type_veh3)
ORDER BY 1, 2, 3, 4; 

UPDATE %(schema).%(survey_table) base_enq
SET coef4 = t.coef4
FROM %(schema).%(survey_table)_coef4 t
WHERE base_enq.red_valide 
  AND base_enq.code_poste = t.code_poste
  AND base_enq.num_point::character varying = t.num_point::character varying
  AND base_enq.red_type_veh3 = t.red_type_veh3; 

UPDATE %(schema).%(survey_table)
SET coef_joe = coef1*coef2*coef3*coef4, coef_pe = coef1*coef2*coef3; 


