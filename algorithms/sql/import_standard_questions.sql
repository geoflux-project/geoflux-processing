
-- Insertion des questions
WITH q AS (
    SELECT champ
    FROM %(ref_schema).n_codif_standard
    GROUP BY champ
    EXCEPT
    SELECT unnest(ARRAY%(questions_to_delete))
    ORDER BY champ
)
INSERT INTO %(survey_schema).question(nom, standard, type_rep)
SELECT DISTINCT ON (n_codif_standard.champ) n_codif_standard.champ, true, CASE WHEN lower(btrim(n_codif_standard.type, ' ')) = 'codif' THEN 1
                                                                               WHEN lower(btrim(n_codif_standard.type, ' ')) = 'zonage' THEN 3
                                                                               WHEN lower(btrim(n_codif_standard.type, ' ')) = 'char' THEN 4
                                                                               WHEN lower(btrim(n_codif_standard.type, ' ')) = 'num' THEN 2
                                                                          END
FROM q JOIN %(ref_schema).n_codif_standard ON (q.champ = n_codif_standard.champ); 

-- Insertion des codifications
INSERT INTO %(survey_schema).codif(id_question, code_modalite, libelle)
SELECT question.id, n_codif_standard.modalite, n_codif_standard.libelle
FROM %(ref_schema).n_codif_standard JOIN %(survey_schema).question ON (question.nom = n_codif_standard.champ)
WHERE lower(btrim(type, ' ')) = 'codif' AND n_codif_standard.modalite != 'N' AND n_codif_standard.modalite != 'I'
ORDER BY 1, 2; 


