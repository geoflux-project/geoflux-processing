-- Export des questionnaires
DROP TABLE IF EXISTS %(export_schema).%(table_questionnaire);
CREATE TABLE %(export_schema).%(table_questionnaire) AS (
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 1 as ordre, 'per_hor' as champ, 'Période horaire de l''enquête' as libelle, true as quest_vl, true as quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 2 as ordre, 'vl_pl' as champ, 'Type de véhicule enquêté (VL ou PL)' as libelle, true as quest_vl, true as quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 3 as ordre, 'lieu_orig' as champ, 'Origine du déplacement' as libelle, true as quest_vl, true as quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 4 as ordre, 'lieu_dest' as champ, 'Destination du déplacement' as libelle, true as quest_vl, true as quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 5 as ordre, 'coef_pe' as champ, 'Coefficient de redressement sur la période d''enquête' as libelle, true as quest_vl, true as quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 6 as ordre, 'coef_joe' as champ, 'Coefficient de redressement sur un jour ouvré moyen de la semaine de l''enquête' as libelle, true as quest_vl, true as quest_pl 
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)    
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain))
    UNION
    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 6+row_number() over(order by questionnaire.ordre), question.nom as champ, question.libelle, questionnaire.quest_vl, questionnaire.quest_pl
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq)
                                       JOIN %(survey_schema).questionnaire ON (point_enq_terrain.id = questionnaire.id_point_enq_terrain)
                                       JOIN %(survey_schema).question ON (question.id = questionnaire.id_question)
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    ORDER BY 1, 2, 3, 4
);                                  

-- Export des codifications
DROP TABLE IF EXISTS %(export_schema).%(table_codif);
CREATE TABLE %(export_schema).%(table_codif) AS (
    SELECT question.nom as champ, CASE WHEN question.type_rep = 1 THEN 'codif' 
                                       WHEN question.type_rep = 2 THEN 'num'
                                       WHEN question.type_rep = 3 THEN 'zonage' 
                                       WHEN question.type_rep = 4 THEN 'char' 
                                  END as type, coalesce(codif.code_modalite, 'X') as modalite, coalesce(codif.libelle, 'X') as libelle
    FROM %(survey_schema).question LEFT JOIN %(survey_schema).codif ON (codif.id_question = question.id)
                                   JOIN %(survey_schema).questionnaire ON (questionnaire.id_question = question.id)
    WHERE questionnaire.id_point_enq_terrain = ANY(ARRAY%(points_enq_terrain))
    ORDER BY 1, 3
);  

-- Export des points d'enquête terrain
DROP TABLE IF EXISTS %(export_schema).%(table_point_enq_terrain);
CREATE TABLE %(export_schema).%(table_point_enq_terrain) AS (
    SELECT campagne_enq.nom as campagne, p.code_poste_ini as code_poste, p.num_point_ini as num_point, p.route, 
           p.id_gest as gest, p.id_command as command, p.id_amo as amo, p.id_prest as prest, 
           p.pr, p.abs, p.angle, p.lib_sens, p.prec_loc, p.id_point_cpt as point_cpt, 
           p.date_enq, p.nb_enq, p.nb_pers, case when p.enq_vl then '1' when p.enq_vl = False then '2' else 'X' end as enq_vl, case when p.enq_pl then '1' when p.enq_pl = False then '2' else 'X' end as enq_pl, 
           p.support, p.protocole, p.millesime, p.comment, p.geom
    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain p ON (campagne_enq.id = p.id_campagne_enq)
    WHERE p.id = ANY(ARRAY%(points_enq_terrain))
    ORDER BY 1, 2, 3
) ;

-- Export des points d'enquête fictifs
DROP TABLE IF EXISTS %(export_schema).%(table_point_enq_fictif);
CREATE TABLE %(export_schema).%(table_point_enq_fictif) AS (
    SELECT etude.nom as etude, p.code_poste_ini as code_poste, p.num_point_ini as num_point, p.route, 
           p.id_command as command, p.angle, p.lib_sens, p.prec_loc, p.id_point_cpt as point_cpt, p.annee, 
		   case when p.enq_vl then '1' when p.enq_vl = False then '2' else 'X' end as enq_vl, case when p.enq_pl then '1' when p.enq_pl = False then '2' else 'X' end as enq_pl, p.comment, p.geom
    FROM %(survey_schema).etude JOIN %(survey_schema).point_enq_fictif p ON (etude.id = p.id_etude)
    WHERE p.id = ANY(ARRAY%(points_enq_fictifs))
    ORDER BY 1, 2, 3 ) ;

-- Export des points de comptage
DROP TABLE IF EXISTS %(export_schema).%(table_point_cpt);
CREATE TABLE %(export_schema).%(table_point_cpt) AS (
    SELECT point_cpt.id, point_cpt.angle, point_cpt.materiel, point_cpt.route, point_cpt.pr, point_cpt.abs, point_cpt.lib_sens, point_cpt.def_pl, point_cpt.comment, point_cpt.geom 
    FROM %(survey_schema).point_cpt JOIN %(survey_schema).point_enq_terrain ON (point_cpt.id = point_enq_terrain.id_point_cpt)
                                    JOIN %(survey_schema).cpt_auto_par_jour ON (cpt_auto_par_jour.id_point_cpt = point_cpt.id)
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) 
    GROUP BY point_cpt.id, point_cpt.angle, point_cpt.materiel, point_cpt.route, point_cpt.pr, point_cpt.abs, point_cpt.lib_sens, point_cpt.def_pl, point_cpt.comment
    ORDER BY 1
);                                    

-- Export des comptages automatiques
DROP TABLE IF EXISTS %(export_schema).%(table_cpt_auto);
CREATE TABLE %(export_schema).%(table_cpt_auto) AS (
    SELECT point_cpt.id as id_point, cpt_auto_par_jour.jour, cpt_auto_par_jour.per, cpt_auto_par_jour.vl, cpt_auto_par_jour.pl
    FROM %(survey_schema).point_cpt JOIN %(survey_schema).point_enq_terrain ON (point_cpt.id = point_enq_terrain.id_point_cpt)
                                    JOIN %(survey_schema).cpt_auto_par_jour ON (cpt_auto_par_jour.id_point_cpt = point_cpt.id)
    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain))
    ORDER BY 1, 2, 3
);                                    

-- Export des interviews terrain
DROP TABLE IF EXISTS %(export_schema).%(table_itw);
SELECT %(survey_schema).extract_interviews(ARRAY%(points_enq_terrain)::integer[], '%(export_schema)'::character varying, '%(table_itw)'::character varying);

CREATE INDEX %(table_itw)_type_lieu_orig_idx ON %(export_schema).%(table_itw) USING btree(type_lieu_orig);
CREATE INDEX %(table_itw)_type_lieu_dest_idx ON %(export_schema).%(table_itw) USING btree(type_lieu_dest);
CREATE INDEX %(table_itw)_lieu_orig_idx ON %(export_schema).%(table_itw) USING btree(lieu_orig); 
CREATE INDEX %(table_itw)_lieu_dest_idx ON %(export_schema).%(table_itw) USING btree(lieu_dest);

-- Affectation des interviews terrain au zonage
DO
$$
DECLARE
r record;
s character varying;
BEGIN
IF (%(zonage) = True) THEN
	ALTER TABLE %(export_schema).%(table_itw) ADD COLUMN code_zone_orig character varying, ADD COLUMN code_zone_dest character varying;
	
	FOR r IN SELECT DISTINCT campagne, code_poste, num_point, millesime FROM %(export_schema).%(table_point_enq_terrain)
	LOOP
		s = '
				WITH codes_zones_orig AS (
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_pays)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig = 10 AND geo_orig.type_lieu = 1 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_zone)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig/10 = 2 AND geo_orig.type_lieu = 2 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_com)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig = 30 AND geo_orig.type_lieu = 3 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_pole)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig = 40 AND geo_orig.type_lieu = 4 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_voie)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig = 50 AND geo_orig.type_lieu = 5 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_adr)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_orig = 60 AND geo_orig.type_lieu = 6 AND zone_orig.id_zonage = %(id_zonage)
				), codes_zones_dest AS (
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_pays)
														         JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest = 10 AND geo_dest.type_lieu = 1 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_zone)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest/10 = 2 AND geo_dest.type_lieu = 2 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_com)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest = 30 AND geo_dest.type_lieu = 3 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_pole)
														         JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest = 40 AND geo_dest.type_lieu = 4 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_voie)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest = 50 AND geo_dest.type_lieu = 5 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.campagne, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_adr)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.campagne = ''' || r.campagne || ''' AND interview.code_poste = ''' || r.code_poste || ''' AND interview.num_point = ' || r.num_point::text || ' AND interview.type_lieu_dest = 60 AND geo_dest.type_lieu = 6 AND zone_dest.id_zonage = %(id_zonage)
				), orig_dest AS (
					SELECT coalesce(o.campagne, d.campagne) as campagne, coalesce(o.code_poste, d.code_poste) as code_poste, coalesce(o.num_point, d.num_point) as num_point, coalesce(o.id_itw, d.id_itw) as id_itw, o.code_zone_orig, d.code_zone_dest 
					FROM codes_zones_orig o FULL JOIN codes_zones_dest d ON (o.campagne = d.campagne AND o.code_poste = d.code_poste AND o.num_point = d.num_point AND o.id_itw = d.id_itw)
				)
				UPDATE %(export_schema).%(table_itw) interview
				SET code_zone_orig = orig_dest.code_zone_orig, code_zone_dest = orig_dest.code_zone_dest
				FROM orig_dest
				WHERE (orig_dest.campagne = interview.campagne AND orig_dest.code_poste = interview.code_poste AND orig_dest.num_point = interview.num_point AND orig_dest.id_itw = interview.id_itw) ;  
			';
			EXECUTE(s);
	END LOOP;
END IF;
END
$$;

-- Export des interviews fictives
-- Rem : A améliorer, car pour le moment, seule la partie "base unifiée" des interviews fictives est exportée. On pourrait extraire aussi les questions supplémentaires. 
-- Pour cela, il faudrait développer une fonction du même type que extract_interviews() utilisée pour créer la table des interviews terrain. 
DROP TABLE IF EXISTS %(export_schema).%(table_itw_fictive);
CREATE TABLE %(export_schema).%(table_itw_fictive) AS (
	SELECT etude.nom as etude,
           point_enq_fictif.code_poste_ini as code_poste, 
           point_enq_fictif.num_point_ini as num_point, 
           id_itw, 
           per_hor, 
           vl_pl, 
           lieu_orig, 
           lieu_dest, 
		   type_lieu_orig, 
		   type_lieu_dest, 
           coef_pe, 
           coef_joe
	FROM %(survey_schema).interview_fictive JOIN %(survey_schema).point_enq_fictif ON (interview_fictive.id_point_enq_fictif = point_enq_fictif.id)
											JOIN %(survey_schema).etude ON (point_enq_fictif.id_etude = etude.id)
	WHERE point_enq_fictif.id = ANY(ARRAY%(points_enq_fictifs))
); 					   


CREATE INDEX %(table_itw_fictive)_type_lieu_orig_idx ON %(export_schema).%(table_itw_fictive) USING btree(type_lieu_orig);
CREATE INDEX %(table_itw_fictive)_type_lieu_dest_idx ON %(export_schema).%(table_itw_fictive) USING btree(type_lieu_dest);
CREATE INDEX %(table_itw_fictive)_lieu_orig_idx ON %(export_schema).%(table_itw_fictive) USING btree(lieu_orig); 
CREATE INDEX %(table_itw_fictive)_lieu_dest_idx ON %(export_schema).%(table_itw_fictive) USING btree(lieu_dest);

-- Affectation des interviews fictives au zonage
DO
$$
DECLARE
r record;
s character varying;
BEGIN
IF (%(zonage) = True) THEN
	ALTER TABLE %(export_schema).%(table_itw_fictive) ADD COLUMN code_zone_orig character varying, ADD COLUMN code_zone_dest character varying;
	
	FOR r IN SELECT DISTINCT interview_fictive.id_itw, point_enq_terrain.millesime 
	         FROM %(survey_schema).interview_fictive JOIN %(survey_schema).interview ON (interview_fictive.id_itw = interview.id)
			                                         JOIN %(survey_schema).point_enq_terrain ON (interview.id_point_enq_terrain = point_enq_terrain.id)
             WHERE interview_fictive.id_point_enq_fictif = ANY(ARRAY%(points_enq_fictifs))
	LOOP
		s = '
				WITH codes_zones_orig AS (
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_pays)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig = 10 AND geo_orig.type_lieu = 1 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_zone)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig/10 = 2 AND geo_orig.type_lieu = 2 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_com)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig = 30 AND geo_orig.type_lieu = 3 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_pole)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig = 40 AND geo_orig.type_lieu = 4 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_voie)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig = 50 AND geo_orig.type_lieu = 5 AND zone_orig.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_orig.code_zone as code_zone_orig
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_orig ON (interview.lieu_orig = geo_orig.code_adr)
															     JOIN %(matrix_schema).zone zone_orig ON (zone_orig.prec_geo <= interview.type_lieu_orig AND st_within(geo_orig.geom_point, zone_orig.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_orig = 60 AND geo_orig.type_lieu = 6 AND zone_orig.id_zonage = %(id_zonage)
				), codes_zones_dest AS (
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_pays)
														         JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest = 10 AND geo_dest.type_lieu = 1 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_zone)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest/10 = 2 AND geo_dest.type_lieu = 2 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_com)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest = 30 AND geo_dest.type_lieu = 3 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_pole)
														         JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest = 40 AND geo_dest.type_lieu = 4 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_voie)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest = 50 AND geo_dest.type_lieu = 5 AND zone_dest.id_zonage = %(id_zonage)
					UNION
					SELECT interview.etude, interview.code_poste, interview.num_point, interview.id_itw, zone_dest.code_zone as code_zone_dest
					FROM %(export_schema).%(table_itw_fictive) interview JOIN %(ref_schema).n_geographie_' || r.millesime::text || ' geo_dest ON (interview.lieu_dest = geo_dest.code_adr)
															     JOIN %(matrix_schema).zone zone_dest ON (zone_dest.prec_geo <= interview.type_lieu_dest AND st_within(geo_dest.geom_point, zone_dest.geom))
					WHERE interview.id_itw = ''' || r.id_itw || ''' AND interview.type_lieu_dest = 60 AND geo_dest.type_lieu = 6 AND zone_dest.id_zonage = %(id_zonage)
				), orig_dest AS (
					SELECT coalesce(o.etude, d.etude) as etude, coalesce(o.code_poste, d.code_poste) as code_poste, coalesce(o.num_point, d.num_point) as num_point, coalesce(o.id_itw, d.id_itw) as id_itw, o.code_zone_orig, d.code_zone_dest 
					FROM codes_zones_orig o FULL JOIN codes_zones_dest d ON (o.etude = d.etude AND o.code_poste = d.code_poste AND o.num_point = d.num_point AND o.id_itw = d.id_itw)
				)
				UPDATE %(export_schema).%(table_itw_fictive) interview
				SET code_zone_orig = orig_dest.code_zone_orig, code_zone_dest = orig_dest.code_zone_dest
				FROM orig_dest
				WHERE (orig_dest.etude = interview.etude AND orig_dest.code_poste = interview.code_poste AND orig_dest.num_point = interview.num_point AND orig_dest.id_itw = interview.id_itw) ;  
			';
			EXECUTE(s);
	END LOOP;
END IF;
END
$$;



-- Export des comptages manuels
SELECT %(survey_schema).extract_cpt_manuels(ARRAY%(points_enq_terrain)::integer[], '%(export_schema)'::character varying, '%(table_cpt_manuel)'::character varying);

