SELECT setval('%(matrix_schema).matrice_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(matrix_schema).matrice), False);

INSERT INTO %(matrix_schema).matrice(nom, id_zonage, scenario, periode, modes, motifs)
VALUES('%(nom_matrice)', %(id_zonage), '%(nom_scenario)', '%(periode)', ARRAY%(modes), ARRAY%(motifs));
  
DO
$$
DECLARE
ech boolean;
BEGIN
    INSERT INTO %(matrix_schema).compo_matrice(id_matrice, id_zone_orig, id_zone_dest, valeur)
        SELECT (SELECT id FROM %(matrix_schema).matrice WHERE nom = '%(nom_matrice)'), 
               zone_o.id, 
               zone_d.id, 
               matrix_import.valeur::real
        FROM %(temp_schema).matrix_import JOIN %(matrix_schema).zone zone_o ON (matrix_import.code_zone_o = zone_o.code_zone)
                                                  JOIN %(matrix_schema).zone zone_d ON (matrix_import.code_zone_d = zone_d.code_zone)
                                                  JOIN %(matrix_schema).zonage ON (zonage.id = zone_o.id_zonage AND zonage.id = zone_d.id_zonage)
        WHERE matrix_import.valeur>0 AND zonage.id = %(id_zonage);
        
    IF (%(ech) = True) THEN
        UPDATE %(matrix_schema).compo_matrice
        SET echantillon = matrix_import.ech
        FROM %(temp_schema).matrix_import, %(matrix_schema).zone zone_o, %(matrix_schema).zone zone_d
        WHERE matrix_import.code_zone_o = zone_o.code_zone AND matrix_import.code_zone_d = zone_d.code_zone AND compo_matrice.id_matrice = (SELECT id FROM %(matrix_schema).matrice WHERE nom = '%(nom_matrice)');
    END IF;    
END
$$;

DROP TABLE %(temp_schema).matrix_import;

