CREATE SCHEMA %(matrix_schema);
GRANT USAGE ON SCHEMA %(matrix_schema) TO public;

DROP AGGREGATE IF EXISTS array_cat_agg(anyarray) CASCADE;
CREATE AGGREGATE array_cat_agg(anyarray) 
(
    SFUNC=array_cat,
    STYPE=anyarray
);

-- Zonage
DROP TABLE IF EXISTS %(matrix_schema).zonage CASCADE;
CREATE TABLE IF NOT EXISTS %(matrix_schema).zonage
(
    id serial PRIMARY KEY, 
    nom character varying UNIQUE NOT NULL
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).zonage TO public;
GRANT ALL ON SEQUENCE %(matrix_schema).zonage_id_seq TO public;

-- Zone
DROP TABLE IF EXISTS %(matrix_schema).zone CASCADE;
CREATE TABLE IF NOT EXISTS %(matrix_schema).zone
(
    id serial PRIMARY KEY, 
    id_zonage integer REFERENCES %(matrix_schema).zonage(id) ON DELETE CASCADE ON UPDATE CASCADE, 
    code_zone character varying, 
    nom character varying,
    prec_geo integer, 
    id_zones_adj integer[], 
    geom Geometry(MultiPolygon, 2154), 
	geom_adj Geometry(MultiPolygon, 2154),
    CONSTRAINT zone_id_zonage_code_zone_key UNIQUE (id_zonage, code_zone)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).zone TO public;
GRANT ALL ON SEQUENCE %(matrix_schema).zone_id_seq TO public;

CREATE INDEX zone_geom_idx ON %(matrix_schema).zone USING gist (geom); 
CREATE INDEX zone_id_zonage_idx ON %(matrix_schema).zone USING btree (id_zonage); 
CREATE INDEX zone_prec_geo_idx ON %(matrix_schema).zone USING btree (prec_geo); 

CREATE MATERIALIZED VIEW %(matrix_schema).adjacent_zones AS
SELECT zone.id_zonage, zone.id as id_zone, a.id AS id_zone_adj, a.geom
FROM %(matrix_schema).zone JOIN %(matrix_schema).zone a ON (a.id = ANY (array_append(zone.id_zones_adj, zone.id)));

CREATE INDEX adjacent_zones_id_zonage_idx ON %(matrix_schema).adjacent_zones USING btree (id_zonage); 
CREATE INDEX adjacent_zones_id_zone_idx ON %(matrix_schema).adjacent_zones USING btree (id_zone); 
CREATE INDEX adjacent_zones_id_zone_adj_idx ON %(matrix_schema).adjacent_zones USING btree (id_zone_adj);

DROP TABLE IF EXISTS %(matrix_schema).corresp_zones CASCADE;
CREATE TABLE IF NOT EXISTS %(matrix_schema).corresp_zones
(
    id serial PRIMARY KEY,
    id_zone integer REFERENCES %(matrix_schema).zone ON DELETE CASCADE ON UPDATE CASCADE, 
    id_macrozone integer REFERENCES %(matrix_schema).zone ON DELETE CASCADE ON UPDATE CASCADE, 
    part_zone float, 
    geom Geometry(MultiPolygon, 2154), 
    CONSTRAINT corresp_zone_id_zone_id_macrozone_key UNIQUE (id_zone, id_macrozone)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).corresp_zones TO public;

CREATE INDEX corresp_zones_id_zonage ON %(matrix_schema).corresp_zones USING btree (id_zone);

CREATE INDEX corresp_zones_id_macrozonage ON %(matrix_schema).corresp_zones USING btree (id_macrozone);

CREATE VIEW %(matrix_schema).corresp_zones_details AS
SELECT zonage.nom as nom_zonage, zone.code_zone, zone.nom as nom_zone, macrozonage.nom as nom_macrozonage, macrozone.code_zone as code_macrozone, macrozone.nom as nom_macrozone, corresp_zones.part_zone, corresp_zones.geom
	FROM %(matrix_schema).corresp_zones JOIN %(matrix_schema).zone ON (corresp_zones.id_zone = zone.id)
	                                     JOIN %(matrix_schema).zone macrozone ON (corresp_zones.id_macrozone = macrozone.id)
										 JOIN %(matrix_schema).zonage ON (zone.id_zonage = zonage.id)
										 JOIN %(matrix_schema).zonage macrozonage ON (macrozone.id_zonage = macrozonage.id)
ORDER BY zonage.nom, macrozonage.nom, zone.code_zone, macrozone.code_zone;

-- Matrice
DROP TABLE IF EXISTS %(matrix_schema).matrice CASCADE;
CREATE TABLE IF NOT EXISTS %(matrix_schema).matrice
(
    id serial PRIMARY KEY, 
    nom character varying UNIQUE,                 
    id_zonage integer REFERENCES %(matrix_schema).zonage ON DELETE CASCADE ON UPDATE CASCADE, 
    scenario character varying,
    periode character varying CHECK ( periode = ANY( ARRAY['Jour', 'HPM', 'PPM', 'HC', 'HCJ', 'HCN', 'PC', 'PCJ', 'PCN', 'HPS', 'PPS', 'Période d''enquête']::character varying[] ) ), 
    modes character varying[] CHECK ( modes <@ ARRAY['Tous modes', 'VP', 'PL', '2R', '2RM', 'TV', 'TC', 'TCU', 'Car', 'Fer', 'TCI', 'Marche', 'Vélo', 'VP conducteur', 'VP passager', 'VUL']::character varying[] ),
    motifs character varying[] CHECK ( motifs <@ ARRAY['Tous motifs', 'Domicile', 'Travail sur lieu habituel', 'Affaires professionnelles', 'Travail', 'Accompagnement', 'Achats quotidiens', 'Achats exceptionnels', 'Achats', 'Ecole primaire', 'Ecole secondaire', 'Enseignement supérieur', 'Loisirs', 'Visites']::character varying[] ), 
    types_flux character varying[] CHECK ( types_flux <@ ARRAY['Internes', 'Echange', 'Transit']::character varying[] ), 
    comment character varying
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).matrice TO public;
GRANT ALL ON SEQUENCE %(matrix_schema).matrice_id_seq TO public;

DROP TABLE IF EXISTS %(matrix_schema).compo_matrice CASCADE;
CREATE TABLE IF NOT EXISTS %(matrix_schema).compo_matrice
(
    id_matrice integer REFERENCES %(matrix_schema).matrice(id) ON DELETE CASCADE ON UPDATE CASCADE, 
    id_zone_orig integer REFERENCES %(matrix_schema).zone(id) ON DELETE CASCADE ON UPDATE CASCADE, 
    id_zone_dest integer REFERENCES %(matrix_schema).zone(id) ON DELETE CASCADE ON UPDATE CASCADE,
    echant integer, 
    valeur real,
    CONSTRAINT compo_matrice_pkey PRIMARY KEY (id_matrice, id_zone_orig, id_zone_dest)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).compo_matrice TO public;

CREATE INDEX compo_matrice_valeur_idx ON %(matrix_schema).compo_matrice USING btree(valeur DESC NULLS LAST);

CREATE INDEX compo_matrice_echant_idx ON %(matrix_schema).compo_matrice USING btree (echant DESC NULLS LAST);
    
CREATE INDEX compo_matrice_id_matrice_idx ON %(matrix_schema).compo_matrice USING btree (id_matrice ASC NULLS LAST);
    
CREATE INDEX compo_matrice_id_zone_orig_idx ON %(matrix_schema).compo_matrice USING btree (id_zone_orig ASC NULLS LAST);

CREATE INDEX compo_matrice_id_zone_dest_idx ON %(matrix_schema).compo_matrice USING btree (id_zone_dest ASC NULLS LAST);

ALTER TABLE %(matrix_schema).compo_matrice ADD CONSTRAINT compo_matrice_echant_check CHECK (echant >= 0 or echant is null);
ALTER TABLE %(matrix_schema).compo_matrice ADD CONSTRAINT compo_matrice_valeur_check CHECK (valeur >= 0 and valeur is not null);

CREATE TABLE %(matrix_schema).distance
(
    id_zone_orig integer REFERENCES %(matrix_schema).zone ON UPDATE CASCADE ON DELETE CASCADE,
    id_zone_dest integer REFERENCES %(matrix_schema).zone ON UPDATE CASCADE ON DELETE CASCADE,
    dist_vol_oiseau numeric(8,3),
	dist numeric(8,3),
    geom Geometry(Linestring, 2154)
);
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE %(matrix_schema).distance TO public;

CREATE INDEX distance_id_zone_orig_idx ON %(matrix_schema).distance USING btree (id_zone_orig ASC NULLS LAST);

CREATE INDEX distance_id_zone_dest_idx ON %(matrix_schema).distance USING btree (id_zone_dest ASC NULLS LAST);

-- Fonction pour sommer des matrices
DROP FUNCTION IF EXISTS %(matrix_schema).somme(integer[]); 
CREATE OR REPLACE FUNCTION %(matrix_schema).somme(
	id_matrices integer[])
    RETURNS TABLE(id_zone_o integer, id_zone_d integer, val real) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    nb_zonages smallint;
BEGIN     
    SELECT INTO nb_zonages count(DISTINCT id_zonage) FROM %(matrix_schema).matrice WHERE id = ANY($1); 
    IF (nb_zonages > 1) THEN RAISE EXCEPTION 'Matrices sur des zonages différents';
    ELSE 
        RETURN QUERY    
            SELECT id_zone_orig, id_zone_dest, sum(coalesce(valeur, 0)) as valeur
            FROM %(matrix_schema).compo_matrice
            WHERE id_matrice = ANY($1)
			GROUP BY id_zone_orig, id_zone_dest; 
    END IF;
    
    RETURN;
END
$BODY$;

-- Fonction qui prépare la comparaison de deux matrices en faisant une jointure des valeurs et des valeurs normalisées (+ vérification de la cohérence des zonages)
CREATE OR REPLACE FUNCTION %(matrix_schema).prepare_matrices_compar(
	id_matrices1 integer[],
	id_matrices2 integer[],
	id_zonage_cible integer)
    RETURNS TABLE(id_zone_orig integer, id_zone_dest integer, reagreg_orig boolean, reagreg_dest boolean, valeur1 real, valeur2 real, norm_valeur1 double precision, norm_valeur2 double precision) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    id_zonage1 integer;
    id_zonage2 integer; 
	nb_zonage integer;
	m_fines integer[];
	m_agreg integer[];
    id_zonage_fin integer;
BEGIN     
    SELECT INTO nb_zonage count(DISTINCT id_zonage) FROM %(matrix_schema).matrice WHERE id = ANY($1); 
	IF nb_zonage > 1 THEN RAISE EXCEPTION 'Les matrices #1 ne sont pas toutes sur le même zonage, impossible de les sommer.'; END IF;
    SELECT INTO nb_zonage count(DISTINCT id_zonage) FROM %(matrix_schema).matrice WHERE id = ANY($2); 
	IF nb_zonage > 1 THEN RAISE EXCEPTION 'Les matrices #2 ne sont pas toutes sur le même zonage, impossible de les sommer.'; END IF;
	
	SELECT INTO id_zonage1 min(id_zonage) FROM %(matrix_schema).matrice WHERE id = ANY($1); 
    SELECT INTO id_zonage2 min(id_zonage) FROM %(matrix_schema).matrice WHERE id = ANY($2);     
    IF (id_zonage1 = $3) AND (id_zonage2 = $3) THEN -- Les matrices 1 et 2 sont sur le zonage cible, aucune des deux n'a à être réagrégée
        RETURN QUERY    
			WITH matrice_vide AS (
				SELECT zone_orig.id as id_zone_orig, zone_dest.id as id_zone_dest
				FROM %(matrix_schema).zone zone_orig CROSS JOIN %(matrix_schema).zone zone_dest
				WHERE zone_orig.id_zonage = $3 AND zone_dest.id_zonage = $3
			), matrice1 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, sum(coalesce(compo_matrice.valeur, 0)) as valeur
				FROM matrice_vide LEFT JOIN %(matrix_schema).compo_matrice ON (compo_matrice.id_zone_orig = matrice_vide.id_zone_orig AND compo_matrice.id_zone_dest = matrice_vide.id_zone_dest)
				WHERE compo_matrice.id_matrice = ANY($1)
				GROUP BY matrice_vide.id_zone_orig, matrice_vide.id_zone_dest
			), matrice2 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, sum(coalesce(compo_matrice.valeur, 0)) as valeur
				FROM matrice_vide LEFT JOIN %(matrix_schema).compo_matrice ON (compo_matrice.id_zone_orig = matrice_vide.id_zone_orig AND compo_matrice.id_zone_dest = matrice_vide.id_zone_dest)
				WHERE compo_matrice.id_matrice = ANY($2)
				GROUP BY matrice_vide.id_zone_orig, matrice_vide.id_zone_dest
			)
			SELECT matrice1.id_zone_orig, 
			       matrice1.id_zone_dest, 
				   false as reagreg_orig, 
				   false as reagreg_dest, 
				   matrice1.valeur::real as valeur1, 
				   matrice2.valeur::real as valeur2, 
				   matrice1.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice1) as norm_valeur1, 
				   matrice2.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice2) as norm_valeur2
			FROM matrice1 JOIN matrice2 ON (matrice1.id_zone_orig = matrice2.id_zone_orig AND matrice1.id_zone_dest = matrice2.id_zone_dest);
			
    ELSIF ((id_zonage1 = $3) or (id_zonage2 = $3)) THEN
        IF (id_zonage1 = $3) -- Les matrices #2 doivent être réagrégées
        THEN
            m_fines = $2;
            m_agreg = $1;
            id_zonage_fin = id_zonage2;
        ELSE -- Les matrices #1 doivent être réagrégées
            m_fines = $1;
            m_agreg = $2;
            id_zonage_fin = id_zonage1;
        END IF;
        
        RETURN QUERY    
			WITH macrozone AS (
				SELECT macrozone.id as id
				FROM %(matrix_schema).zone JOIN %(matrix_schema).corresp_zones ON (zone.id = corresp_zones.id_zone)
												  JOIN %(matrix_schema).zone macrozone ON (macrozone.id = corresp_zones.id_macrozone)
				WHERE zone.id_zonage = id_zonage_fin AND macrozone.id_zonage = $3
				GROUP BY macrozone.id
			), matrice_vide AS (
				SELECT zone_orig.id as id_zone_orig, zone_dest.id as id_zone_dest
				FROM macrozone zone_orig CROSS JOIN macrozone zone_dest
			), matrice1_reagreg AS (
				SELECT macrozone_o.id as id_zone_orig, 
				       macrozone_d.id as id_zone_dest, 
				       bool_or((corresp_zones_o.part_zone != 1)) as reagreg_orig,
				       bool_or((corresp_zones_d.part_zone != 1)) as reagreg_dest, 
				       sum(coalesce(corresp_zones_o.part_zone * corresp_zones_d.part_zone * compo_matrice.valeur, 0)) as valeur
				FROM %(matrix_schema).compo_matrice JOIN %(matrix_schema).corresp_zones corresp_zones_o ON (compo_matrice.id_zone_orig = corresp_zones_o.id_zone)
																  JOIN %(matrix_schema).corresp_zones corresp_zones_d ON (compo_matrice.id_zone_dest = corresp_zones_d.id_zone)
																  JOIN %(matrix_schema).zone macrozone_o ON (macrozone_o.id = corresp_zones_o.id_macrozone)
																  JOIN %(matrix_schema).zone macrozone_d ON (macrozone_d.id = corresp_zones_d.id_macrozone)
				WHERE macrozone_o.id_zonage = $3 AND macrozone_d.id_zonage = $3 AND compo_matrice.id_matrice = ANY(m_fines)
				GROUP BY macrozone_o.id, macrozone_d.id
			), matrice1 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, coalesce(matrice1_reagreg.valeur, 0) as valeur, matrice1_reagreg.reagreg_orig, matrice1_reagreg.reagreg_dest
				FROM matrice_vide LEFT JOIN matrice1_reagreg ON (matrice_vide.id_zone_orig = matrice1_reagreg.id_zone_orig AND matrice_vide.id_zone_dest = matrice1_reagreg.id_zone_dest)
			), matrice2 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, sum(coalesce(compo_matrice.valeur, 0)) as valeur
				FROM matrice_vide LEFT JOIN %(matrix_schema).compo_matrice ON (compo_matrice.id_zone_orig = matrice_vide.id_zone_orig AND compo_matrice.id_zone_dest = matrice_vide.id_zone_dest)
				WHERE compo_matrice.id_matrice = ANY(m_agreg)
				GROUP BY matrice_vide.id_zone_orig, matrice_vide.id_zone_dest
			)
			SELECT matrice1.id_zone_orig, 
			       matrice1.id_zone_dest, 
				   matrice1.reagreg_orig, 
				   matrice1.reagreg_dest, 
				   matrice1.valeur::real as valeur1, 
				   matrice2.valeur::real as valeur2, 
				   matrice1.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice1) as norm_valeur1, 
				   matrice2.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice2) as norm_valeur2
			FROM matrice1 JOIN matrice2 ON (matrice1.id_zone_orig = matrice2.id_zone_orig AND matrice1.id_zone_dest = matrice2.id_zone_dest);

    ELSE -- Les matrices 1 et 2 doivent être réagrégées
        RETURN QUERY
			WITH macrozone AS (
				SELECT macrozone.id as id
				FROM %(matrix_schema).zone JOIN %(matrix_schema).corresp_zones ON (zone.id = corresp_zones.id_zone)
												  JOIN %(matrix_schema).zone macrozone ON (macrozone.id = corresp_zones.id_macrozone)
				WHERE zone.id_zonage = id_zonage1 AND macrozone.id_zonage = $3
				GROUP BY macrozone.id
				UNION
				SELECT macrozone.id as id
				FROM %(matrix_schema).zone JOIN %(matrix_schema).corresp_zones ON (zone.id = corresp_zones.id_zone)
												  JOIN %(matrix_schema).zone macrozone ON (macrozone.id = corresp_zones.id_macrozone)
				WHERE zone.id_zonage = id_zonage2 AND macrozone.id_zonage = $3
				GROUP BY macrozone.id
			), matrice_vide AS (
				SELECT zone_orig.id as id_zone_orig, zone_dest.id as id_zone_dest
				FROM macrozone zone_orig CROSS JOIN macrozone zone_dest
			), matrice1_reagreg AS (
				SELECT macrozone_o.id as id_zone_orig, 
				       macrozone_d.id as id_zone_dest, 					   
				       bool_or((corresp_zones_o.part_zone != 1)) as reagreg_orig, 
				       bool_or((corresp_zones_d.part_zone != 1)) as reagreg_dest, 
					   sum(coalesce(corresp_zones_o.part_zone * corresp_zones_d.part_zone * compo_matrice.valeur, 0)) as valeur
				FROM %(matrix_schema).compo_matrice JOIN %(matrix_schema).corresp_zones corresp_zones_o ON (compo_matrice.id_zone_orig = corresp_zones_o.id_zone)
														   		  JOIN %(matrix_schema).corresp_zones corresp_zones_d ON (compo_matrice.id_zone_dest = corresp_zones_d.id_zone)
														   		  JOIN %(matrix_schema).zone macrozone_o ON (macrozone_o.id = corresp_zones_o.id_macrozone)
														   		  JOIN %(matrix_schema).zone macrozone_d ON (macrozone_d.id = corresp_zones_d.id_macrozone)
				WHERE macrozone_o.id_zonage = $3 AND macrozone_d.id_zonage = $3 AND compo_matrice.id_matrice = ANY($1)
				GROUP BY macrozone_o.id, macrozone_d.id
			), matrice1 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, matrice1_reagreg.reagreg_orig, matrice1_reagreg.reagreg_dest, coalesce(matrice1_reagreg.valeur, 0) as valeur
				FROM matrice_vide LEFT JOIN matrice1_reagreg ON (matrice_vide.id_zone_orig = matrice1_reagreg.id_zone_orig AND matrice_vide.id_zone_dest = matrice1_reagreg.id_zone_dest)
			), matrice2_reagreg AS (
				SELECT macrozone_o.id as id_zone_orig, macrozone_d.id as id_zone_dest, 
				       bool_or((corresp_zones_o.part_zone != 1)) as reagreg_orig, 
				       bool_or((corresp_zones_d.part_zone != 1)) as reagreg_dest, 
				       sum(coalesce(corresp_zones_o.part_zone * corresp_zones_d.part_zone * compo_matrice.valeur, 0)) as valeur
				FROM %(matrix_schema).compo_matrice JOIN %(matrix_schema).corresp_zones corresp_zones_o ON (compo_matrice.id_zone_orig = corresp_zones_o.id_zone)
														   		  JOIN %(matrix_schema).corresp_zones corresp_zones_d ON (compo_matrice.id_zone_dest = corresp_zones_d.id_zone)
														   		  JOIN %(matrix_schema).zone macrozone_o ON (macrozone_o.id = corresp_zones_o.id_macrozone)
														   		  JOIN %(matrix_schema).zone macrozone_d ON (macrozone_d.id = corresp_zones_d.id_macrozone)
				WHERE macrozone_o.id_zonage = $3 AND macrozone_d.id_zonage = $3 AND compo_matrice.id_matrice = ANY($2)
				GROUP BY macrozone_o.id, macrozone_d.id
			), matrice2 AS (
				SELECT matrice_vide.id_zone_orig, matrice_vide.id_zone_dest, matrice2_reagreg.reagreg_orig, matrice2_reagreg.reagreg_dest, coalesce(matrice2_reagreg.valeur, 0) as valeur
				FROM matrice_vide LEFT JOIN matrice2_reagreg ON (matrice_vide.id_zone_orig = matrice2_reagreg.id_zone_orig AND matrice_vide.id_zone_dest = matrice2_reagreg.id_zone_dest)
			)
			SELECT matrice1.id_zone_orig, 
			       matrice1.id_zone_dest, 
				   matrice1.reagreg_orig,
				   matrice2.reagreg_orig,
				   matrice1.valeur::real as valeur1, 
				   matrice2.valeur::real as valeur2, 
				   matrice1.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice1) as norm_valeur1, 
				   matrice2.valeur::double precision / (SELECT sum(valeur::double precision) as valeur FROM matrice2) as norm_valeur2
			FROM matrice1 JOIN matrice2 ON (matrice1.id_zone_orig = matrice2.id_zone_orig AND matrice1.id_zone_dest = matrice2.id_zone_dest);
    END IF;
    
    RETURN;
END
$BODY$;

-- Calcul de la distance de Levenstein normalisée entre deux matrices, sur une ligne ou une colonne donnée
CREATE OR REPLACE FUNCTION %(matrix_schema).normalized_levenstein_distance(
	valeurs1 numeric[],
	zones1 integer[],
	valeurs2 numeric[],
	zones2 integer[])
    RETURNS numeric
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    m_size integer;
    m numeric[][];
    r numeric;
BEGIN
    
    IF NOT (array_length(valeurs1, 1) = array_length(valeurs2, 1) AND array_length(valeurs2, 1) = array_length(zones1, 1) AND array_length(zones1, 1) = array_length(zones2, 1)) 
    THEN RAISE EXCEPTION 'Input arrays must have the same lengths';
    ELSE m_size = array_length(valeurs1, 1);
    END IF;
    
	m=array_fill(0.::numeric, ARRAY[m_size+1, m_size+1]);
	
    FOR j IN 2..m_size+1
    LOOP 
        m[1][j] =  m[1][j-1] + valeurs1[j-1];
    END LOOP;
	
	FOR i IN 2..m_size+1
    LOOP
        m[i][1] = m[i-1][1] + valeurs2[i-1] ;
    END LOOP;    
    IF (m[1][m_size+1]=0 OR m[m_size+1][1]=0) THEN r=1::numeric;
	ELSE 
        FOR i IN 2..m_size+1 LOOP
            FOR j IN 2..m_size+1 LOOP
                m[i][j] = least(
                                least(m[i-1][j]+valeurs2[i-1], m[i][j-1]+valeurs1[j-1])
                              , m[i-1][j-1]+CASE WHEN zones1[j-1] = zones2[i-1] THEN abs(valeurs1[j-1]-valeurs2[i-1]) ELSE abs(valeurs1[j-1]+valeurs2[i-1]) END);
            END LOOP;
        END LOOP;
        r = (m[m_size+1][m_size+1]/(m[1][m_size+1]+m[m_size+1][1]))::numeric;
    END IF;
    
    RETURN r;
END
$BODY$;

-- Calcul de la distance de Levenstein normalisée entre deux matrices normalisées, sur une ligne ou une colonne donnée
CREATE OR REPLACE FUNCTION %(matrix_schema).normalized_structure_levenstein_distance(
	valeurs1 numeric[],
	zones1 integer[],
	valeurs2 numeric[],
	zones2 integer[])
    RETURNS numeric
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    m_size integer;
    m numeric[][];
	s1 numeric;
	s2 numeric;
    r numeric;
BEGIN
    
    IF NOT (array_length(valeurs1, 1) = array_length(valeurs2, 1) AND array_length(valeurs2, 1) = array_length(zones1, 1) AND array_length(zones1, 1) = array_length(zones2, 1)) 
    THEN RAISE EXCEPTION 'Input arrays must have the same lengths';
    ELSE m_size = array_length(valeurs1, 1);
    END IF;
    
	SELECT INTO s1 sum(unnest) FROM (SELECT unnest(valeurs1)) q; 
	SELECT INTO s2 sum(unnest) FROM (SELECT unnest(valeurs2)) q; 
	
	IF (s1=0 OR s2=0) THEN r=1::numeric;
	ELSE 
        m=array_fill(0.::real, ARRAY[m_size+1, m_size+1]);
		
        FOR j IN 2..m_size+1
        LOOP 
            m[1][j] =  m[1][j-1] + CASE WHEN s1=0 THEN 0 ELSE valeurs1[j-1]/s1 END;
        END LOOP;
        
        FOR i IN 2..m_size+1
        LOOP
            m[i][1] = m[i-1][1] + CASE WHEN s2=0 THEN 0 ELSE valeurs2[i-1]/s2 END;
        END LOOP; 
        
        FOR i IN 2..m_size+1 LOOP
            FOR j IN 2..m_size+1 LOOP
                m[i][j] = least(
                                    least(m[i-1][j] + valeurs2[i-1]/s2, m[i][j-1]+valeurs1[j-1]/s1)
                                  , m[i-1][j-1] + CASE WHEN zones1[j-1] = zones2[i-1] 
                                                       THEN abs(valeurs1[j-1]/s1 - valeurs2[i-1]/s2)
                                                       ELSE abs(valeurs1[j-1]/s1 + valeurs2[i-1]/s2) 
                                                  END
                               );
            END LOOP;
        END LOOP;
        r = (m[m_size+1][m_size+1]/2)::numeric;
    END IF;
    RETURN r;
END
$BODY$;

-- Calcul de l'intervalle de confiance associé à une proportion
CREATE OR REPLACE FUNCTION %(matrix_schema).intervalle_confiance_proportion(
	sous_echantillon integer,
	echantillon integer,
	population double precision,
	quantile_loi_normale double precision DEFAULT 1.96)
    RETURNS double precision[]
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    moyenne double precision;
    ecart_type double precision;
    facteur_correctif double precision;
BEGIN
    ecart_type = sqrt(sous_echantillon::double precision/echantillon*(1-sous_echantillon::double precision/echantillon));     
    facteur_correctif = sqrt((population - echantillon)::double precision/population);
    
    return ARRAY[(sous_echantillon::double precision/echantillon - quantile_loi_normale * ecart_type/sqrt(echantillon) * facteur_correctif)*population, (sous_echantillon::double precision/echantillon + quantile_loi_normale * ecart_type/sqrt(echantillon) * facteur_correctif)*population];
END;
$BODY$;




