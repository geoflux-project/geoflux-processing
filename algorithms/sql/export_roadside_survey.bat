chcp 65001 > nul
%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 1 as ordre, ^'per_hor^' as champ, ^'Période horaire de l''enquête^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 2 as ordre, ^'vl_pl^' as champ, ^'Type de véhicule enquêté (VL ou PL)^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 3 as ordre, ^'lieu_orig^' as champ, ^'Origine du déplacement^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 4 as ordre, ^'lieu_dest^' as champ, ^'Destination du déplacement^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 5 as ordre, ^'coef_pe^' as champ, ^'Coefficient de redressement sur la période d^'^'enquête^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 6 as ordre, ^'coef_joe^' as champ, ^'Coefficient de redressement sur un jour ouvré moyen de la semaine de l^'^'enquête^' as libelle, true as quest_vl, true as quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    UNION ^
                    SELECT campagne_enq.nom as campagne, point_enq_terrain.code_poste_ini as code_poste, point_enq_terrain.num_point_ini as num_point, 6+row_number() over(order by questionnaire.ordre), question.nom as champ, question.libelle, questionnaire.quest_vl, questionnaire.quest_pl ^
                    FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain ON (campagne_enq.id = point_enq_terrain.id_campagne_enq) ^
                                                       JOIN %(survey_schema).questionnaire ON (point_enq_terrain.id = questionnaire.id_point_enq_terrain) ^
                                                       JOIN %(survey_schema).question ON (question.id = questionnaire.id_question) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    ORDER BY 1, 2, 3, 4 ^
                    ) TO %(dir_path)/%(table_questionnaire).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8'; ^" %(user) 

%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT question.nom as champ, CASE WHEN question.type_rep = 1 THEN 'codif' ^
                                                       WHEN question.type_rep = 2 THEN 'num' ^
                                                       WHEN question.type_rep = 3 THEN 'zonage' ^
                                                       WHEN question.type_rep = 4 THEN 'char' ^
                                                  END as type, coalesce(codif.code_modalite, 'X') as modalite, coalesce(codif.libelle, 'X') as libelle ^
                    FROM %(survey_schema).question LEFT JOIN %(survey_schema).codif ON (codif.id_question = question.id) ^
                                                    JOIN %(survey_schema).questionnaire ON (questionnaire.id_question = question.id) ^
                    WHERE questionnaire.id_point_enq_terrain = ANY(ARRAY%(points_enq_terrain)) ^
                    ORDER BY 1, 3 ) TO %(dir_path)/%(table_codif).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8' ^" %(user)
                    
%(pgsql2shp_exe) -f %(dir_path)/%(table_point_enq_terrain).shp -h %(host) -p %(port) -u %(user) %(db_name) ^"SELECT campagne_enq.nom as campagne, p.code_poste_ini as code_poste, p.num_point_ini as num_point, p.route, ^
                                                                                                              p.id_gest as gest, p.id_command as command, p.id_amo as amo, p.id_prest as prest, ^
                                                                                                              p.pr, p.abs, p.angle, p.lib_sens, p.prec_loc, p.id_point_cpt as point_cpt, ^
                                                                                                              p.date_enq, p.nb_enq, p.nb_pers, case when p.enq_vl then ^'1^' when p.enq_vl = False then ^'2^' else ^'X^' end as enq_vl, case when p.enq_pl then ^'1^' when p.enq_pl = False then ^'2^' else ^'X^' end as enq_pl, ^
                                                                                                              p.support, p.protocole, p.millesime, p.comment, p.geom ^
                                                                                                         FROM %(survey_schema).campagne_enq JOIN %(survey_schema).point_enq_terrain p ON (campagne_enq.id = p.id_campagne_enq) ^
                                                                                                         WHERE p.id = ANY(ARRAY%(points_enq_terrain)) ^
                                                                                                         ORDER BY 1, 2, 3 ^"

%(pgsql2shp_exe) -f %(dir_path)/%(table_point_enq_fictif).shp -h %(host) -p %(port) -u %(user) %(db_name) ^"SELECT etude.nom as etude, p.code_poste_ini as code_poste, p.num_point_ini as num_point, p.route, ^
                                                                                                              p.id_command as command, p.angle, p.lib_sens, p.prec_loc, p.id_point_cpt as point_cpt, p.annee, ^
                                                                                                              case when p.enq_vl then ^'1^' when p.enq_vl = False then ^'2^' else ^'X^' end as enq_vl, case when p.enq_pl then ^'1^' when p.enq_pl = False then ^'2^' else ^'X^' end as enq_pl, ^
                                                                                                              p.comment, p.geom ^
                                                                                                         FROM %(survey_schema).etude JOIN %(survey_schema).point_enq_fictif p ON (etude.id = p.id_etude) ^
                                                                                                         WHERE p.id = ANY(ARRAY%(points_enq_fictifs)) ^
                                                                                                         ORDER BY 1, 2, 3 ^"
																										 
%(pgsql2shp_exe) -f %(dir_path)/%(table_point_cpt).shp -h %(host) -p %(port) -u %(user) %(db_name) ^"SELECT point_cpt.id, point_cpt.angle, point_cpt.materiel, point_cpt.route, point_cpt.pr, point_cpt.abs, point_cpt.lib_sens, point_cpt.def_pl, point_cpt.comment, point_cpt.geom ^
                                                                                                    FROM %(survey_schema).point_cpt JOIN %(survey_schema).point_enq_terrain ON (point_cpt.id = point_enq_terrain.id_point_cpt) ^
                                                                                                                                    JOIN %(survey_schema).cpt_auto_par_jour ON (cpt_auto_par_jour.id_point_cpt = point_cpt.id) ^
                                                                                                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                                                                                                    GROUP BY point_cpt.id, point_cpt.angle, point_cpt.materiel, point_cpt.route, point_cpt.pr, point_cpt.abs, point_cpt.lib_sens, point_cpt.def_pl, point_cpt.comment ^
                                                                                                    ORDER BY 1 ^"

%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT point_cpt.id as id_point, cpt_auto_par_jour.jour, cpt_auto_par_jour.per, cpt_auto_par_jour.vl, cpt_auto_par_jour.pl ^
                    FROM %(survey_schema).point_cpt JOIN %(survey_schema).point_enq_terrain ON (point_cpt.id = point_enq_terrain.id_point_cpt) ^
                                                     JOIN %(survey_schema).cpt_auto_par_jour ON (cpt_auto_par_jour.id_point_cpt = point_cpt.id) ^
                    WHERE point_enq_terrain.id = ANY(ARRAY%(points_enq_terrain)) ^
                    ORDER BY 1, 2, 3 ) TO %(dir_path)/%(table_cpt_auto).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8' ^" %(user)

%(psql_exe) -h %(host) -p %(port) -d %(db_name) -c ^"\copy ( ^
                    SELECT etude.nom as etude, ^
					       point_enq_fictif.code_poste_ini as code_poste, ^
						   point_enq_fictif.num_point_ini as num_point, ^
						   id_itw, ^
						   per_hor, ^
						   vl_pl, ^
						   lieu_orig, ^
						   lieu_dest, ^
						   type_lieu_orig, ^
						   type_lieu_dest, ^
						   coef_pe, ^
						   coef_joe ^
					FROM %(survey_schema).interview_fictive JOIN %(survey_schema).point_enq_fictif ON (interview_fictive.id_point_enq_fictif = point_enq_fictif.id) ^
															JOIN %(survey_schema).etude ON (point_enq_fictif.id_etude = etude.id) ^
					WHERE point_enq_fictif.id = ANY(ARRAY%(points_enq_fictifs)) ^
                    ) TO %(dir_path)/%(table_interview_fictive).csv CSV HEADER DELIMITER ';' ENCODING 'UTF-8' ^" %(user)
