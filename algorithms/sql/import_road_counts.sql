-- Insertion des intervenants
INSERT INTO %(schema_dest).entreprise_etablissement(siren_siret)
SELECT gest::bigint FROM %(schema_source).%(table_point_cpt_auto)
UNION
SELECT prest::bigint FROM %(schema_source).%(table_point_cpt_auto)
EXCEPT
SELECT siren_siret FROM %(schema_dest).entreprise_etablissement;

UPDATE %(schema_dest).entreprise_etablissement
SET prestataire = TRUE
FROM %(schema_source).%(table_point_cpt_auto)
WHERE %(table_point_cpt_auto).prest::bigint = entreprise_etablissement.siren_siret;

UPDATE %(schema_dest).entreprise_etablissement
SET gestionnaire_route = TRUE
FROM %(schema_source).%(table_point_cpt_auto)
WHERE %(table_point_cpt_auto).gest::bigint = entreprise_etablissement.siren_siret;

-- Insertion des points de comptage automatiques
DROP TABLE IF EXISTS %(schema_source).%(table_point_cpt_auto)_idmap;
CREATE TABLE %(schema_source).%(table_point_cpt_auto)_idmap 
(
	id serial, 
	id_ini integer
);
SELECT setval('%(schema_source).%(table_point_cpt_auto)_idmap_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_cpt), false);

INSERT INTO %(schema_source).%(table_point_cpt_auto)_idmap(id_ini)
SELECT id
FROM %(schema_source).%(table_point_cpt_auto);

INSERT INTO %(schema_dest).point_cpt (id, id_gest, angle, lib_sens, sens_confondus, route, pr, abs, materiel, def_pl, comment, geom, gestionnaire, prive)
SELECT %(table_point_cpt_auto)_idmap.id, gest::bigint, angle, lib_sens, false, route, pr, abs, materiel::smallint, def_pl, comment, geom, %(gestionnaire), false
FROM %(schema_source).%(table_point_cpt_auto) JOIN %(schema_source).%(table_point_cpt_auto)_idmap ON (%(table_point_cpt_auto).id = %(table_point_cpt_auto)_idmap.id_ini);

SELECT setval('%(schema_dest).point_cpt_id_seq', (SELECT coalesce(max(id)+1, 1) FROM %(schema_dest).point_cpt), false);

-- Insertion des comptages automatiques
DO
$$
BEGIN
	IF (%(format)=1) THEN
        INSERT INTO %(schema_dest).cpt_auto_par_jour (id_point_cpt, jour, per, duree, vl, pl, gestionnaire)
        SELECT %(table_point_cpt_auto)_idmap.id, jour::date, per::smallint, %(per_ag_cpt_auto), vl, pl, %(gestionnaire)
        FROM %(schema_source).%(table_cpt_auto) JOIN %(schema_source).%(table_point_cpt_auto)_idmap ON (%(table_cpt_auto).id_point = %(table_point_cpt_auto)_idmap.id_ini);
	ELSIF (%(format)=2) THEN
        INSERT INTO %(schema_dest).cpt_auto_moy (id_point_cpt, type_jour, vacances_sco, per, duree, date_debut, date_fin, vl, pl, gestionnaire)
        SELECT %(table_point_cpt_auto)_idmap.id, type_jour, CASE WHEN vacances_sco = 1 THEN True WHEN vacances_sco = 2 THEN False WHEN vacances_sco = 0 THEN NULL END, per::smallint, %(per_ag_cpt_auto), date_debut, date_fin, vl, pl, %(gestionnaire)
        FROM %(schema_source).%(table_cpt_auto) JOIN %(schema_source).%(table_point_cpt_auto)_idmap ON (%(table_cpt_auto).id_point = %(table_point_cpt_auto)_idmap.id_ini);
	END IF;
END
$$;



