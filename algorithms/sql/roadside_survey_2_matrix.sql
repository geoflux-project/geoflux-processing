-- Création de la nouvelle matrice
INSERT INTO %(matrix_schema).matrice( nom, periode, id_zonage )
VALUES( '%(matrix_name)', '%(periode)', %(id_zonage) );

DO
$$
BEGIN

	CREATE TEMPORARY TABLE flux (
		id_zone_orig integer, 
		id_zone_dest integer, 
		coef real
	);
	
	IF '%(table_corresp)' = '' THEN
		-- Simple addition des flux présents dans les tables des interviews
		IF '%(table_itw_terrain)' != '' THEN
			INSERT INTO flux(id_zone_orig, id_zone_dest, coef)
			SELECT zone_orig.id, zone_dest.id, interview.%(coef)
			FROM %(work_schema).%(table_itw_terrain) interview JOIN %(matrix_schema).zone zone_orig ON (zone_orig.code_zone = interview.code_zone_orig)
															   JOIN %(matrix_schema).zone zone_dest ON (zone_dest.code_zone = interview.code_zone_dest)
			WHERE zone_orig.id_zonage = %(id_zonage) AND zone_dest.id_zonage = %(id_zonage) AND %(filter_itw);
		END IF;
		
		IF '%(table_itw_fictive)' != '' THEN
			INSERT INTO flux(id_zone_orig, id_zone_dest, coef)
			SELECT zone_orig.id, zone_dest.id, interview.%(coef)
			FROM %(work_schema).%(table_itw_fictive) interview JOIN %(matrix_schema).zone zone_orig ON (zone_orig.code_zone = interview.code_zone_orig)
															   JOIN %(matrix_schema).zone zone_dest ON (zone_dest.code_zone = interview.code_zone_dest)
			WHERE zone_orig.id_zonage = %(id_zonage) AND zone_dest.id_zonage = %(id_zonage) AND %(filter_itw); 
		END IF;
	ELSE
		-- Composition des flux selon la table des correspondances OD - points d'enquête 
		IF '%(table_itw_terrain)' != '' THEN
			INSERT INTO flux(id_zone_orig, id_zone_dest, coef)
			SELECT zone_orig.id, zone_dest.id, interview.%(coef)
			FROM %(work_schema).%(table_itw_terrain) interview JOIN %(matrix_schema).zone zone_orig ON (zone_orig.code_zone = interview.code_zone_orig)
															   JOIN %(matrix_schema).zone zone_dest ON (zone_dest.code_zone = interview.code_zone_dest)
															   JOIN %(survey_schema).point_enq_terrain ON (point_enq_terrain.code_poste_ini = interview.code_poste AND point_enq_terrain.num_point_ini = interview.num_point AND point_enq_terrain.id_campagne_enq = (SELECT id FROM %(survey_schema).campagne_enq WHERE nom = interview.campagne))
															   JOIN %(work_schema).%(table_corresp) corresp ON (zone_orig.code_zone = corresp.code_zone_orig AND zone_dest.code_zone = corresp.code_zone_dest AND corresp.id_point_enq_terrain = point_enq_terrain.id)
			WHERE zone_orig.id_zonage = %(id_zonage) AND zone_dest.id_zonage = %(id_zonage) AND %(filter_itw);
		END IF;
		
		IF '%(table_itw_fictive)' != '' THEN
			INSERT INTO flux(id_zone_orig, id_zone_dest, coef)
			SELECT zone_orig.id, zone_dest.id, interview.%(coef)
			FROM %(work_schema).%(table_itw_fictive) interview JOIN %(matrix_schema).zone zone_orig ON (zone_orig.code_zone = interview.code_zone_orig)
															   JOIN %(matrix_schema).zone zone_dest ON (zone_dest.code_zone = interview.code_zone_dest)
															   JOIN %(survey_schema).point_enq_fictif ON (point_enq_fictif.code_poste_ini = interview.code_poste AND point_enq_fictif.num_point_ini = interview.num_point AND point_enq_fictif.id_etude = (SELECT id FROM %(survey_schema).etude WHERE nom = interview.etude))
															   JOIN %(work_schema).%(table_corresp) corresp ON (zone_orig.code_zone = corresp.code_zone_orig AND zone_dest.code_zone = corresp.code_zone_dest AND corresp.id_point_enq_fictif = point_enq_fictif.id)
			WHERE zone_orig.id_zonage = %(id_zonage) AND zone_dest.id_zonage = %(id_zonage) AND %(filter_itw); 
		END IF;
	END IF;
	
	INSERT INTO %(matrix_schema).compo_matrice( id_matrice, id_zone_orig, id_zone_dest, echant, valeur )
	SELECT (SELECT id FROM %(matrix_schema).matrice WHERE nom = '%(matrix_name)'), 
		   id_zone_orig, 
		   id_zone_dest, 
		   count(coef), 
		   sum(coef)
	FROM flux
	GROUP BY id_zone_orig, id_zone_dest;
		
END
$$;