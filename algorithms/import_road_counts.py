# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Geoflux
                                 A QGIS plugin
 Traitement de données d'enquêtes OD
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2021-01-04
        copyright            : (C) 2021 by Aurélie Bousquet - Cerema
        email                : aurelie.bousquet@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Aurélie Bousquet - Cerema'
__date__ = '2021-01-04'
__copyright__ = '(C) 2021 by Aurélie Bousquet - Cerema'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtGui import QIcon
from PyQt5.QtCore import QCoreApplication,QVariant
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import *
from qgis.core import (QgsProcessingException, QgsProcessingParameterString, QgsProcessing, QgsProcessingAlgorithm, QgsProcessingParameterFile) 
from processing.tools import postgis
import csv
import processing

from .tools.psqlloader import *
from .tools.functions import *
from .tools.standard import *

pluginPath = os.path.dirname(os.path.dirname(__file__))

class ImportRoadCounts(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    DATABASE = 'DATABASE' 
    SCHEMA_DEST = 'SCHEMA_DEST'
    SCHEMA_SOURCE = 'SCHEMA_SOURCE'
    PER_AG_CPT_AUTO = 'PER_AG_CPT_AUTO'
    FORMAT = 'FORMAT'
    FICHIER_POINT_CPT_AUTO = 'FICHIER_POINT_CPT_AUTO' 
    FICHIER_CPT_AUTO = 'FICHIER_CPT_AUTO'
    GESTIONNAIRE = 'GESTIONNAIRE'
    TEST = 'TEST'
    LOGFILE = 'LOGFILE'
    LISTE_FORMATS = ['Par jour', 'Moyenne sur plusieurs jours']
    
    def initAlgorithm(self, config):
        db_param = QgsProcessingParameterString(
            self.DATABASE,
            self.tr('Connexion base de données PostgreSQL'))
        db_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.ConnectionWidgetWrapper'}})
        self.addParameter(db_param)
        
        
        schema_dest_param = QgsProcessingParameterString(
            self.SCHEMA_DEST,
            self.tr('Schéma relatonnel contenant les données d''enquête'), 'o_geoflux_survey', False, False)
        schema_dest_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.SchemaWidgetWrapper',
                'connection_param': self.DATABASE}})
        self.addParameter(schema_dest_param)
                
        schema_source_param = QgsProcessingParameterString(
            self.SCHEMA_SOURCE,
            self.tr('Schéma d''écriture temporaire'), '', False, False)
        schema_source_param.setMetadata({
            'widget_wrapper': {
                'class': 'processing.gui.wrappers_postgis.SchemaWidgetWrapper',
                'connection_param': self.DATABASE}})
        self.addParameter(schema_source_param)
        
        self.addParameter(
            QgsProcessingParameterEnum(
                self.PER_AG_CPT_AUTO,
                self.tr(unicode("Période d'agrégation des données de comptages automatiques (en minutes)")), 
                ['15', '30', '60'],
                allowMultiple = False, 
                defaultValue = 2,
                optional = False
            )
        )
        
        self.addParameter(
            QgsProcessingParameterEnum(
                self.FORMAT,
                self.tr(unicode("Format des données")), 
                self.LISTE_FORMATS,
                allowMultiple = False, 
                defaultValue = 0,
                optional = False
            )
        )
        
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.FICHIER_POINT_CPT_AUTO,
                self.tr("Couche des points de comptage automatique (.shp)"),
                types=[QgsProcessing.TypeVectorPoint], 
                defaultValue=None
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.FICHIER_CPT_AUTO,
                self.tr("Fichier des comptages automatiques (.csv)"),
                behavior = QgsProcessingParameterFile.File, 
                fileFilter = 'CSV files (*.csv)'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterNumber(
                self.GESTIONNAIRE,
                self.tr(unicode("Code du gestionnaire des données")), 
                QgsProcessingParameterNumber.Integer, 
                1, 
                1
            )
        )
        
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.TEST,
                self.tr(unicode("Tester l'import sans le réaliser")),
                True,
                False
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.LOGFILE,
                self.tr('Fichier de log de PSQL'),
                optional = False, 
                fileFilter = 'Log files (*.log)'
            )
        )
        
    def processAlgorithm(self, parameters, context, feedback):
        db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="temp_connection")        
        connection = self.parameterAsString(parameters, self.DATABASE, context)
        uri = postgis.uri_from_name(connection)
        db.setHostName(uri.host())
        db.setPort(int(uri.port()))
        db.setDatabaseName(uri.database())
        db.setUserName(uri.username())
        db.setPassword(uri.password())
        ok = db.open()
        test = self.parameterAsBoolean(parameters, self.TEST, context) 
        schema_dest = self.parameterAsString(parameters, self.SCHEMA_DEST, context) 
        schema_source = self.parameterAsString(parameters, self.SCHEMA_SOURCE, context)
        per_ag_cpt_auto = PER_AG[self.parameterAsInt(parameters, self.PER_AG_CPT_AUTO, context)]
        format = self.parameterAsInt(parameters, self.FORMAT, context)
        point_comptage_auto = self.parameterAsVectorLayer(parameters, self.FICHIER_POINT_CPT_AUTO, context)
        comptage_auto = self.parameterAsFile(parameters, self.FICHIER_CPT_AUTO, context)
        log_file = os.path.splitext(self.parameterAsFileOutput(parameters, self.LOGFILE, context))[0]
        gestionnaire = self.parameterAsInt(parameters, self.GESTIONNAIRE, context) 
        
        # Create temp table structure
        ploader = PsqlLoader(host = uri.host(), user = uri.username(), port = uri.port(), dbname = uri.database(), sqlfile = "", logfile=log_file)
        subs = {}
        subs['schema'] = schema_source
        subs['table_cpt_auto'] = TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT
        subs['format'] = str(format + 1)
        sqlfile = pluginPath+"/algorithms/sql/temp_import_road_counts.sql"        
        if (os.path.isfile(sqlfile)):
            ploader.set_from_template(sqlfile, subs)
            ploader.load()
        
        # Import des points de comptages dans le schéma temporaire
        f = [i.name() for i in point_comptage_auto.fields()]
        lst = [value for value in POINT_CPT_AUTO_MANDATORY_COL if (value not in f and value != "geom")]
        if (lst != []):
            raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des points de comptages automatiques, colonne(s) """+str(lst)+""" manquante(s).\n""") )
        else: 
            processing.run("qgis:importintopostgis", { 'CREATEINDEX' : True, \
                                                       'DATABASE' : connection, \
                                                       'DROP_STRING_LENGTH' : True, \
                                                       'ENCODING' : 'UTF-8', \
                                                       'FORCE_SINGLEPART' : False, \
                                                       'GEOMETRY_COLUMN' : 'geom', \
                                                       'INPUT' : point_comptage_auto, \
                                                       'LOWERCASE_NAMES' : True, \
                                                       'OVERWRITE' : True, \
                                                       'PRIMARY_KEY' : "id", \
                                                       'SCHEMA' : schema_source, \
                                                       'TABLENAME' : TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT \
                                                     } \
                          )
        
        # Import des comptages dans le schéma temporaire
        if (comptage_auto != ""): 
            with open(comptage_auto, 'r', newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ';', quotechar='"')
                if (format == 0): # Comptages jour par jour
                    lst = [value for value in CPT_AUTO_MANDATORY_COL if value not in reader.fieldnames]
                    if (lst != []):
                        raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des comptages automatiques, colonne(s) """+str(lst)+""" manquante(s).\n""") )
                    else:
                        for row in reader:
                            s="INSERT INTO "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+"( id_point, jour, per, vl, pl ) \
                               VALUES ("+str(row['id_point'])+", '"+str(row['jour'])+"'::date, '"+str(row['per'])+"', "+str(row['vl'])+", "+str(row['pl'])+")"
                            q = QtSql.QSqlQuery(db)
                            q.exec_(unicode(s))
                elif (format == 1): # Comptages moyennés sur plusieurs jours
                    lst = [value for value in CPT_AUTO_MANDATORY_COL2 if value not in reader.fieldnames]
                    if (lst != []):
                        raise QgsProcessingException( self.tr("""Erreur dans la structure du fichier des comptages automatiques, colonne(s) """+str(lst)+""" manquante(s).\n""") )
                    else:
                        for row in reader:
                            s="INSERT INTO "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+"( id_point, type_jour, vacances_sco, per, date_debut, date_fin, vl, pl ) \
                               VALUES ("+str(row['id_point'])+", "+str(row['type_jour'])+"::integer, "+str(row['vacances_sco'])+"::integer, '"+str(row['per'])+"', '"+str(row['date_debut'])+"'::date, '"+str(row['date_fin'])+"'::date, "+str(row['vl'])+", "+str(row['pl'])+")"
                            q = QtSql.QSqlQuery(db)
                            q.exec_(unicode(s))
        
        # Teste si toutes les données sont rattachées à un point de comptage déclaré et si tous les points de comptage déclarés ont des données attachées
        s = "SELECT cpt.id_point, point_cpt.id \
             FROM "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+" cpt FULL JOIN "+schema_source+"."+TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT+" point_cpt ON (point_cpt.id = cpt.id_point) \
             WHERE cpt.id_point IS NULL OR point_cpt.id IS NULL"
        q = QtSql.QSqlQuery(db)
        q.exec_(unicode(s))
        if (q.size()>0):
            while (q.next()):
                if (q.value(0) == None):
                    raise QgsProcessingException(self.tr("Le point de comptage "+str(q.value(1))+" n'a aucune donnée de comptage associée."))
                if (q.value(1) == None):
                    raise QgsProcessingException(self.tr("Des données sont associées au point de comptage "+str(q.value(0))+" mais celui-ci n'est pas déclaré."))
        else:
            feedback.pushInfo(self.tr("Les points de comptages déclarés et les données de comptage sont cohérents entre eux. \n"))
        
        # Teste si les données de comptage sont conformes à la période d'agrégation déclarée
        s = "SELECT DISTINCT per FROM "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT
        q = QtSql.QSqlQuery(db)
        q.exec_(unicode(s))
        while q.next(): 
            if (((per_ag_cpt_auto == 15) and (int(q.value(0))<=0 or int(q.value(0))>96)) 
            or ((per_ag_cpt_auto == 30) and (int(q.value(0))<=0 or int(q.value(0))>95 or (int(q.value(0))%2!=1))) 
            or ((per_ag_cpt_auto == 60) and (int(q.value(0))>93 or int(q.value(0))<=0 or (int(q.value(0))%4!=1)))):
                raise QgsProcessingException(self.tr("La période "+str(q.value(0))+" présente dans la table des comptages automatiques n'est pas compatible avec la période d'agrégation des données déclarée de "+per_ag_cpt_auto+" minutes."))
        
        # Teste si toutes les périodes sont représentées
        if (format == 0): # Comptages jour par jour
            s = "SELECT id_point, jour, count(distinct per), array_agg(per ORDER BY per) FROM "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+" GROUP BY id_point, jour"
            q = QtSql.QSqlQuery(db)
            q.exec_(unicode(s))
            if (q.size()>0):
                while q.next():
                    if (((q.value(2)<24) and (per_ag_cpt_auto == 60)) or ((q.value(2)<48) and (per_ag_cpt_auto == 30)) or ((q.value(2)<96) and (per_ag_cpt_auto == 15))):
                        raise QgsProcessingException(self.tr("Pour le point "+str(q.value(0))+", le "+(q.value(1)).toString("yyyy-MM-dd")+", des périodes sont manquantes. Seules les périodes suivantes sont représentées : "+str(q.value(3))+".")) 
                    else:
                        feedback.pushInfo(self.tr("Pour le point "+str(q.value(0))+", les comptages automatiques sont conformes à la période d'agrégation déclarée. \n"))
            else:
                feedback.pushInfo(self.tr("Les comptages automatiques ne contiennent pas de données. \n"))
            
        elif (format == 1): # Comptages moyennés sur plusieurs jours
            s = "SELECT id_point, type_jour, vacances_sco, date_debut, date_fin, count(distinct per), array_agg(per ORDER BY per) FROM "+schema_source+"."+TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT+" GROUP BY id_point, type_jour, vacances_sco, date_debut, date_fin"
            q = QtSql.QSqlQuery(db)
            q.exec_(unicode(s))
            if (q.size()>0):
                while q.next():
                    if (((q.value(5)<24) and (per_ag_cpt_auto == 60)) or ((q.value(5)<48) and (per_ag_cpt_auto == 30)) or ((q.value(5)<96) and (per_ag_cpt_auto == 15))):
                        raise QgsProcessingException(self.tr("Pour le point "+str(q.value(0))+", type_jour = "+str(q.value(1))+", vacances_sco = "+str(q.value(2))+", date_debut = "+(q.value(3)).toString("yyyy-MM-dd")+", date_fin = "+(q.value(4)).toString("yyyy-MM-dd")+", des périodes sont manquantes. Seules les périodes suivantes sont représentées : "+str(q.value(6))+"."))
                    else:
                        feedback.pushInfo(self.tr("Pour le point "+str(q.value(0))+", type_jour = "+str(q.value(1))+", vacances_sco = "+str(q.value(2))+", date_debut = "+(q.value(3)).toString("yyyy-MM-dd")+", date_fin = "+(q.value(4)).toString("yyyy-MM-dd")+", les comptages automatiques sont conformes à la période d'agrégation déclarée. \n"))
            else:
                feedback.pushInfo(self.tr("Les comptages automatiques ne contiennent pas de données. \n"))
        
        # Import des comptages en base de données relationnelle
        if (test == False):
            ploader = PsqlLoader(host = uri.host(), user = uri.username(), port = uri.port(), dbname = uri.database(), sqlfile = "", logfile=log_file)
            subs = {}
            subs['schema_dest'] = schema_dest
            subs['schema_source'] = schema_source
            subs['per_ag_cpt_auto'] = str(per_ag_cpt_auto)
            subs['table_point_cpt_auto'] = TEMP_TABLE_POINT_CPT_AUTO+SUFFIXE_IMPORT
            subs['table_cpt_auto'] = TEMP_TABLE_CPT_AUTO+SUFFIXE_IMPORT
            subs['format'] = str(format + 1)
            subs["gestionnaire"] = str(gestionnaire) 
            sqlfile = pluginPath+"/algorithms/sql/import_road_counts.sql"
            if (os.path.isfile(sqlfile)):
                ploader.set_from_template(sqlfile, subs)
                ploader.load()
        
        return {}

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "import_road_counts"
    
    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(unicode("Importer comptages routiers dans la base relationnelle"))
    
    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(unicode("Importer des données"))

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "import_data"
    
    def icon(self):
        return QIcon(os.path.join(pluginPath, "icons", "icon_import.png"))
    
    def tr(self, string):
        return QCoreApplication.translate('Processing', string)
    
    def shortHelpString(self):
        return self.tr("""Module d'import de données de comptages automatiques dans la base relationnelle de Géoflux.""")
    
    def createInstance(self):
        return ImportRoadCounts()


